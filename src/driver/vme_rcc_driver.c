// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_driver.c						*/
/*									*/
/* RCC VMEbus driver							*/
/*									*/
/* 25. Oct. 01  MAJO  created						*/
/* 01. Nov. 01  JOP  prepare for interrupts				*/
/* 26. Nov. 01  JOP  bus errors						*/
/* 15. Jan. 02  JOP  clean link & berr structures in release		*/
/* 26. Feb. 02  JOP  update to Linux 2.4				*/
/* 17. Apr. 02  JOP  RORA interrupts					*/
/* 27. May. 02  JOP  modify API for IRs					*/
/*									*/
/************ C 2001 - The software with that certain something *********/


//MJ - FIXME - We have to decide what to do with the pid in the long run


/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.4 onwards			*/
/*- This driver requires a UniverseII. Universe I is not supported.	*/
/*- The proper way of accessing the Universe would be through 		*/
/*  writel/readl functions. For a first implementation I have chosen to */
/*  use pointer based I/O as this simplifies the re-use of old code	*/ 
/************************************************************************/

// Module Versioning a la Rubini p.316

#include <linux/config.h>
#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
  #define MODVERSIONS
#endif

#if defined(MODVERSIONS)
  #include <linux/modversions.h>
#endif

#include <linux/delay.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include "vme_rcc/universe.h"
#include "vme_rcc/cct_berr.h"
#include "vme_rcc/vme_rcc_driver.h"

EXPORT_NO_SYMBOLS;

#ifdef MODULE
  MODULE_PARM (debug, "i");
  MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
  MODULE_PARM (errorlog, "i");
  MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");
  MODULE_DESCRIPTION("VMEbus Tundra Universe");
  MODULE_AUTHOR("Markus Joos & J.O.Petersen, CERN/EP");
  #ifdef MODULE_LICENSE
    MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
  #endif
#endif

/*********/
/*Globals*/
/*********/
static int berr_interlock = 0, vmetab_ok = 0, debug = 0, errorlog = 1;
static int vmercc_major = 0; // use dynamic allocation
static universe_regs_t *uni;
static all_mstslvmap_t stmap;
static struct semaphore dma_semaphore;
static struct timer_list dma_timer;
static u_int board_type, dma_to;
static VME_DMAhandle_t dma_donelist[VME_DMADONESIZE];
static master_map_t master_map_table;
static volatile VME_DMAhandle_t current_dmahandle;
static u_int vme_irq_line;
static u_int irq_level_table[7];
static u_int irq_sysfail;
static link_dsc_table_t link_table;		// link descriptors
static berr_proc_table_t berr_proc_table;	// processes with BERR handling
static sysfail_link_dsc_t sysfail_link;	        // processes with SYSFAIL handling
static berr_dsc_t berr_dsc;			// last BERR
#if defined (VMEDEBUG) || defined (INTDEBUG)
  static unsigned int *marker;
#endif
static struct proc_dir_entry *vme_rcc_file;
static struct vme_proc_data_t vme_proc_data;
static InterruptCounters_t Interrupt_counters = {0,0,0,0,0,0,0, {0,0,0,0,0,0,0}, 0};
static unsigned int berr_int_count = 0;
static int dma_sem_busy = 0, sem_positive[2] = {0,0};
static u_int txfifo_wait_1;		        // # waits on TXFIFO empty
static u_int txfifo_wait_2;
static char *proc_read_text;


/************/
/*Prototypes*/
/************/
/******************************/
/*Standard function prototypes*/
/******************************/
static int vme_rcc_open(struct inode *ino, struct file *filep);
static int vme_rcc_release(struct inode *ino, struct file *filep);
static int vme_rcc_ioctl(struct inode *inode, struct file *file, u_int cmd, u_long arg);
static int vme_rcc_mmap(struct file *file, struct vm_area_struct *vma);
static int vme_rcc_write_procmem(struct file *file, const char *buffer, u_long count, void *data);
static int vme_rcc_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data);
static void vme_rcc_vmaClose(struct vm_area_struct *vma);

/*****************************/
/*Special function prototypes*/
/*****************************/
static int berr_check(u_int *addr, u_int *multi, u_int *am);
static int cct_berrInt(void);
static void fill_mstmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap);
static void fill_slvmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap);
static void vme_dmaTimeout(u_long arg);
static void vme_intTimeout(u_long arg);
static void vme_intSysfailTimeout(u_long arg);
static void vme_dma_handler(void);
static void vme_irq_handler(int level);
static void init_cct_berr(void);
static void mask_cct_berr(void);
static void send_berr_signals(void);
static void read_berr_capture(void);
static void universe_irq_handler (int irq, void *dev_id, struct pt_regs *regs);



static struct vm_operations_struct vme_vm_ops =
{       
  close: vme_rcc_vmaClose
};

static struct file_operations fops = 
{
  ioctl:   vme_rcc_ioctl,
  open:    vme_rcc_open,    
  mmap:    vme_rcc_mmap,
  release: vme_rcc_release
};

/****************************/
/* Standard driver function */
/****************************/

/************************************************************/
static int vme_rcc_open(struct inode *ino, struct file *filep)
/************************************************************/
{
  char* buf;
  int vct, berr, loop;
  private_data_t *pdata;

  //MOD_INC_USE_COUNT;
  //kdebug(("vme_rcc(open): MOD_INC_USE_COUNT called from process %d\n", current->pid))

  buf = (char *)kmalloc(sizeof(private_data_t), GFP_KERNEL);
  if (buf == NULL)
  {
    kerror(("vme_rcc(open): error from kmalloc\n"))
    return(-VME_EFAULT);
  }

  pdata = (private_data_t *)buf;
  kdebug(("vme_rcc(open): pointer to link flags in file descriptor = %x\n", (u_int)&pdata->link_dsc_flag[0]))
  for (vct = 0; vct < VME_VCTSIZE; vct++)
    pdata->link_dsc_flag[vct] = 0;

  kdebug(("vme_rcc(open): pointer to berr flags in file descriptor = %x\n", (u_int)&pdata->berr_dsc_flag[0]))
  for (berr = 0; berr < VME_MAX_BERR_PROCS; berr++)
    pdata->berr_dsc_flag[berr] = 0;

  pdata->sysfail_dsc_flag = 0;
  
  for (loop = 0; loop < VME_DMADONESIZE; loop++)
    pdata->dma[loop] = 0;
  
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
    pdata->mastermap[loop] = 0;
  
  filep->private_data = buf;

  return(0);
}


/***************************************************************/
static int vme_rcc_release(struct inode *ino, struct file *filep)
/***************************************************************/
{
  int vct, no_freed, isave, free_flag, loop;
  VME_IntHandle_t *group_ptr;
  VME_IntHandle_t *group_ptr_save[VME_VCTSIZE];
  private_data_t *pdata;
  u_int i;

  pdata = (private_data_t *)filep->private_data;

  down(&link_table.link_dsc_sem);

  no_freed = 0;
  group_ptr_save[0] = (VME_IntHandle_t*) 0xffffffff;

  kdebug(("vme_rcc(release): pid = %d\n", current->pid))

  for (vct = 0; vct < VME_VCTSIZE; vct++)
  {
    if (pdata->link_dsc_flag[vct] == 1)
    {
      if ( link_table.link_dsc[vct].group_ptr )	// grouped vector
      {
        group_ptr = link_table.link_dsc[vct].group_ptr;
        kdebug(("vme_rcc(release): vector = %d, group_ptr = 0x%p\n", vct, group_ptr))

        free_flag = 1;

        for (isave = 0; isave <= no_freed; isave++)
        {
          if (group_ptr == group_ptr_save[isave])
          {
            free_flag = 0;	// already freed
            break;
          }
        }

        kdebug(("vme_rcc(release): free_flag = %d\n", free_flag))
        if (free_flag)
        {
          kdebug(("vme_rcc(release): group_ptr = 0x%p is now kfree'd\n", group_ptr))
          kfree(group_ptr);
          group_ptr_save[no_freed] = group_ptr;
          no_freed++;
          kdebug(("vme_rcc(release): no_freed = %d \n", no_freed))
        }
      }

      link_table.link_dsc[vct].pid = 0;
      link_table.link_dsc[vct].vct = 0;
      link_table.link_dsc[vct].lvl = 0;
      link_table.link_dsc[vct].pending = 0;
      link_table.link_dsc[vct].total_count = 0;
      link_table.link_dsc[vct].group_ptr = 0;
      sema_init(&link_table.link_dsc[vct].sem,0);
      link_table.link_dsc[vct].sig = 0;

      pdata->link_dsc_flag[vct] = 0;
    }
  }
  up(&link_table.link_dsc_sem);

  // and now the BERR links
  down(&berr_proc_table.proc_sem);
  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    if (pdata->berr_dsc_flag[i] == 1)
    {
      kdebug(("vme_rcc(release): clearing BERR entry %d \n", i))
      berr_proc_table.berr_link_dsc[i].pid = 0;
      berr_proc_table.berr_link_dsc[i].sig = 0;

      pdata->link_dsc_flag[i] = 0;
    }
  }
  up(&berr_proc_table.proc_sem);
  
  down(&sysfail_link.proc_sem);
  if (pdata->sysfail_dsc_flag)
  {
    kdebug(("vme_rcc(release): clearing SYSFAIL entry that has been left open by process %d\n", sysfail_link.pid))
    sysfail_link.pid = 0;
    sysfail_link.sig = 0;
    pdata->sysfail_dsc_flag = 0;
  }
  up(&sysfail_link.proc_sem);  

  // Release orphaned master mappings
  down(&master_map_table.sem);
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
  {
    if (pdata->mastermap[loop])
    {
      kdebug(("vme_rcc(release): Unmapping orphaned virtual address 0x%08x for process %d\n", master_map_table.map[loop].vaddr, master_map_table.map[loop].pid))
      iounmap((void *)master_map_table.map[loop].vaddr);
      master_map_table.map[loop].vaddr = 0;
      master_map_table.map[loop].pid = 0;
      pdata->mastermap[loop] = 0;
    }
  }
  up(&master_map_table.sem);

  // and finally the DMA done list
  for (loop = 0; loop < VME_DMADONESIZE; loop++)
  {
    if (pdata->dma[loop])
    {
      kdebug(("vme_rcc(release): entry %d in dma_done list released (was owned by process %d)\n", loop, dma_donelist[loop].pid))
      dma_donelist[loop].pid = 0;
      dma_donelist[loop].handle = 0xffffffff;
      pdata->dma[loop] = 0;
    }
  }

  kfree(filep->private_data);
  kdebug(("vme_rcc(release): kfreed file private data @ %x\n", (u_int)filep->private_data))
  filep->private_data = NULL;	// Hannapel?

  //MOD_DEC_USE_COUNT;
  return(0);
}


/****************************************************************************************************/
static int vme_rcc_ioctl(struct inode *inode, struct file *filep, unsigned int cmd, unsigned long arg)
/****************************************************************************************************/
{
  private_data_t *pdata;

  kdebug(("vme_rcc(ioctl): filep at 0x%08x\n", filep))
  pdata = (private_data_t *)filep->private_data;

  switch (cmd)
  {    
    case VMEMASTERMAP:
    {
      int ok2, ok = 0;
      u_int vaddr;
      int loop, loop2;
      
      if (copy_from_user(&pdata->mm, (void *)arg, sizeof(VME_MasterMapInt_t)) !=0)
      {
	kerror(("vme_rcc(ioctl,VMEMASTERMAP): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.vmebus_address   = 0x%08x\n", pdata->mm.in.vmebus_address))
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.window_size      = 0x%08x\n", pdata->mm.in.window_size))
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.address_modifier = 0x%08x\n", pdata->mm.in.address_modifier))
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.options          = 0x%08x\n", pdata->mm.in.options))

      for (loop = 0; loop < 8; loop++)
      {
	if (pdata->mm.in.vmebus_address >= stmap.master[loop].vbase && 
           (pdata->mm.in.vmebus_address + pdata->mm.in.window_size) <= stmap.master[loop].vtop && 
	   stmap.master[loop].enab == 1 && 
	   stmap.master[loop].am == pdata->mm.in.address_modifier &&
           stmap.master[loop].options == pdata->mm.in.options)
        {
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): Decoder %d matches parameters\n", loop))
          pdata->mm.pci_address = stmap.master[loop].pbase + (pdata->mm.in.vmebus_address-stmap.master[loop].vbase); 
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): PCI address = 0x%08x\n", pdata->mm.pci_address))

          //find a free slot in the master map table
          down(&master_map_table.sem);
          ok2 = 0;
          for (loop2 = 0; loop2 < VME_MAX_MASTERMAP; loop2++)
          {
            if (master_map_table.map[loop2].pid == 0)
            {
              kdebug(("vme_rcc(ioctl,VMEMASTERMAP): Will use entry %d in master_map_table\n", loop2))
              ok2 = 1;
              break;
            }
          }
          if (!ok2)
          {
            kdebug(("vme_rcc(ioctl,VMEMASTERMAP): master_map_table is full\n"))
            return(-VME_NOSTATMAP2);
          }

	  vaddr = (u_int)ioremap(pdata->mm.pci_address, pdata->mm.in.window_size);
          if (!vaddr)
          {
            kerror(("vme_rcc(ioctl,VMEMASTERMAP): error from ioremap\n"))
            up(&master_map_table.sem);
            return(-VME_EFAULT);
          }
          master_map_table.map[loop2].vaddr = vaddr;
          master_map_table.map[loop2].pid = current->pid;
          pdata->mastermap[loop2] = 1;
          up(&master_map_table.sem);

          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): kernel virtual address = 0x%08x   pid = %d\n", vaddr, current->pid))
          pdata->mm.kvirt_address = vaddr;

	  ok = 1;
	  break;
        }
        else
        {
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].vbase   = 0x%08x\n", loop, stmap.master[loop].vbase))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].vtop    = 0x%08x\n", loop, stmap.master[loop].vtop))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].enab    = %d\n", loop, stmap.master[loop].enab))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].am      = %d\n", loop, stmap.master[loop].am))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].options = 0x%08x\n", loop, stmap.master[loop].options))
        }
      }
      if (!ok)
	return(-VME_NOSTATMAP);

      if (copy_to_user((void *)arg, &pdata->mm, sizeof(VME_MasterMapInt_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEMASTERMAP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }

    case VMEMASTERUNMAP:
    {    
      u_int ok, mm, loop;

      if (copy_from_user(&mm, (void *)arg, sizeof(unsigned int)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEMASTERUNMAP): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }

      down(&master_map_table.sem);
      ok = 0;
      for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
      {
	kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): master_map_table.map[%d].vaddr = 0x%08x  <-> mm = 0x%08x\n", loop, master_map_table.map[loop].vaddr, mm));
	kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): master_map_table.map[%d].pid = %d <-> current->pid = %d\n", loop, master_map_table.map[loop].pid, current->pid));
      
//        if (master_map_table.map[loop].vaddr == mm && master_map_table.map[loop].pid == current->pid) //MJ: do we need the second condition?
        if (master_map_table.map[loop].vaddr == mm) 
        {
          kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): Entry %d in master_map_table matches\n", loop))
          ok = 1;
          iounmap((void *)mm);
          kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): unmapping address 0x%08x\n", mm))
          master_map_table.map[loop].vaddr = 0;
          master_map_table.map[loop].pid = 0;
          pdata->mastermap[loop] = 0;
          break;
        }
      }
      up(&master_map_table.sem);
  
      if (!ok)
      {
        kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): No entry in master_map_table matched for address 0x%08x\n", mm))
        return(-VME_IOUNMAP);
      }
      break;
    }

    case VMEMASTERMAPDUMP:
    {
      char *buf;
      int len, loop;

      buf = (char *)kmalloc(TEXT_SIZE1, GFP_KERNEL);
      if (buf == NULL)
      {
        kerror(("vme_rcc(ioctl,VMEMASTERMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }

      len = 0;
      len += sprintf(buf + len, "Master mapping:\n");
      len += sprintf(buf + len, "LSI  VME address range  PCI address range   EN   WP   AM\n");
      for (loop = 0; loop < 8; loop++)
	len += sprintf(buf + len, "%d    %08x-%08x  %08x-%08x    %d    %d    %d\n"
        	      ,loop
	 	      ,stmap.master[loop].vbase
		      ,stmap.master[loop].vtop
		      ,stmap.master[loop].pbase
		      ,stmap.master[loop].ptop
		      ,stmap.master[loop].enab
		      ,stmap.master[loop].wp
		      ,stmap.master[loop].am);
      if (copy_to_user((void *)arg, buf, TEXT_SIZE1 * sizeof(char)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEMASTERMAPDUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
            
      kfree(buf);
      break;
    }
     
    case VMESYSFAILLINK:
    {
      down(&sysfail_link.proc_sem);

      if (sysfail_link.pid != 0)  // Already linked by any other process?
      {
         up(&sysfail_link.proc_sem);
         return(-VME_SYSFAILTBLFULL);
      }

      sysfail_link.pid = current->pid;
      pdata->sysfail_dsc_flag = 1;
      sema_init(&sysfail_link.sem, 0);     // initialise semaphore count
      up(&sysfail_link.proc_sem);
      break;
    }

    case VMESYSFAILUNLINK:
    {
      down(&sysfail_link.proc_sem);

      if (pdata->sysfail_dsc_flag == 0)          //Check if the current process (group) has linked it
      {
        up(&sysfail_link.proc_sem);
        return(-VME_SYSFAILTBLNOTLINKED);
      }

      sysfail_link.pid = 0;
      sysfail_link.sig = 0;
      pdata->sysfail_dsc_flag = 0;
      up(&sysfail_link.proc_sem);

      break;
    }

    case VMESYSFAILREGISTERSIGNAL:
    {
      if (copy_from_user(&pdata->VME_SysfailRegSig, (void *)arg, sizeof(VME_RegSig_t)) != 0)
      {
        kerror(("vme_rcc(ioctl,VMESYSFAILREGISTERSIGNAL): error from copy_from_user\n"))
        return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMESYSFAILREGISTERSIGNAL): signal # = %d\n", pdata->VME_SysfailRegSig.signum))
      down(&sysfail_link.proc_sem);

      if (sysfail_link.pid != current->pid)
      {
        up(&sysfail_link.proc_sem);
        return(-VME_SYSFAILNOTLINKED);
      }
      if (pdata->VME_SysfailRegSig.signum)  // register
        sysfail_link.sig = pdata->VME_SysfailRegSig.signum;
      else                           // unregister
        sysfail_link.sig = 0;
      up(&sysfail_link.proc_sem);
      break;
    }
    
    case VMESYSFAILWAIT:
    {
      struct timer_list int_timer;
      int use_timer = 0;
      int result = 0;

      if (copy_from_user(&pdata->VME_WaitInt, (void *)arg, sizeof(VME_WaitInt_t)) != 0)
      {
 	kerror(("vme_rcc(ioctl,VMESYSFAILWAIT): error from copy_from_user\n"))
 	 return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMESYSFAILWAIT): timeout = %d\n", pdata->VME_WaitInt.timeout))

      if (pdata->VME_WaitInt.timeout > 0) 
      {
        init_timer(&int_timer);
        int_timer.function = vme_intSysfailTimeout;
        int_timer.data = (unsigned long)&pdata->VME_WaitInt;
        int_timer.expires = jiffies + pdata->VME_WaitInt.timeout;
        add_timer(&int_timer);
        use_timer = 1;
      }

      if (pdata->VME_WaitInt.timeout != 0) 		// Wait : with timeout or not
      {
        kdebug(("vme_rcc(ioctl,VMESYSFAILWAIT): Before Down, semaphore = %d\n", sysfail_link.sem.count.counter))
        down_interruptible(&sysfail_link.sem);	// wait ..
        if (signal_pending(current))
        {					        // interrupted system call
          kdebug(("vme_rcc(ioctl,VMESYSFAILWAIT): Interrupted by signal\n"))
          result = -VME_INTBYSIGNAL;		// or ERESTARTSYS ?
        }
        else if ((use_timer == 1) && (pdata->VME_WaitInt.timeout == 0))
          result = -VME_TIMEOUT;
      }
      
      if (use_timer == 1)
        del_timer(&int_timer);	// OK also if timer ran out

      if (copy_to_user((void *)arg, &pdata->VME_WaitInt, sizeof(VME_WaitInt_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMESYSFAILWAIT): error from copy_to_user\n"))
        return(-VME_EFAULT);
      }

      return (result);
      break;	// dummy ..
    }

    // SYSFAIL is latched in lint_stat (I believe). Therefore, even if SYSFAIL is removed from VMEbus
    // the interrupt status bit has to be cleared in LINT_STAT
    // this will only work if SYSFAIL is not on the bus

    case VMESYSFAILREENABLE:
    {
      unsigned int lint_stat, lint_en;

      lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
      lint_stat |= 0x4000;
      kdebug(("vme_rcc(ioctl,VMESYSFAILREENABLE): lint_stat = %x\n", lint_stat))
      writel(lint_stat, (char*)uni + LINT_STAT);	// only success if SYSFAIL is not asserted on VMEbus

      lint_en = readl((char*)uni + LINT_EN);	// read enabled irq bits
      kdebug(("vme_rcc(ioctl,VMESYSFAILREENABLE): lint_en = 0x%x\n", lint_en))
      lint_en |= 0x4000;				
      kdebug(("vme_rcc(ioctl,VMESYSFAILREENABLE): lint_en after masking = 0x%x\n", lint_en))
      writel(lint_en, (char*)uni + LINT_EN);	//reenable SYSFAIL
      break;
    }

    // reset the SYSFAIL interrupt bit
    // if it comes back on, the SYSFAIL is on VMEbus

    case VMESYSFAILPOLL:
    {
      int flag;
      unsigned int lint_stat;

      lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
      lint_stat |= 0x4000;
      kdebug(("vme_rcc(ioctl,VMESYSFAILPOLL): lint_stat = %x\n", lint_stat))
      writel(lint_stat, (char*)uni + LINT_STAT);	// only success if SYSFAIL is not asserted on VMEbus

      lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
      kdebug(("vme_rcc(ioctl,VMESYSFAILPOLL): lint_stat = %x\n", lint_stat))
      if (lint_stat & 0x4000)	// the SYSFAIL bit
        flag = 1;
      else
        flag = 0;

      if (copy_to_user((void *)arg, &flag, sizeof(int)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMESYSFAILPOLL): error from copy_to_user\n"))
        return(-VME_EFAULT);
      }
      break;
    }
    
    case VMESYSFAILSET:
    {   
      unsigned int vcsr_set;

      vcsr_set = readl((char*)uni + VCSR_SET);
      vcsr_set |= 0x40000000;				// bit SYSFAIL
      kdebug(("vme_rcc(ioctl,VMESYSFAILSET): vcsr_set = %x\n", vcsr_set))
      writel(vcsr_set, (char*)uni + VCSR_SET);
      break;
    }
    
  
    case VMESYSFAILRESET:
    {   
      unsigned int vcsr_clr;

      vcsr_clr = readl((char*)uni + VCSR_CLR);
      vcsr_clr |= 0x40000000;				// bit SYSFAIL
      kdebug(("vme_rcc(ioctl,VMESYSFAILRESET): vcsr_clr = %x\n", vcsr_clr))
      writel(vcsr_clr, (char*)uni + VCSR_CLR);
      break;
    }

    case VMEBERRREGISTERSIGNAL:
    {
      u_int i;
      int index;

      if (copy_from_user(&pdata->VME_BerrRegSig, (void *)arg, sizeof(VME_RegSig_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEBERRREGISTERSIGNAL): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc(ioctl,VMEBERRREGISTERSIGNAL): signal # = %d\n", pdata->VME_BerrRegSig.signum))

      down(&berr_proc_table.proc_sem);

      if (pdata->VME_BerrRegSig.signum)
      {

        // find a free slot
        index = -1;
        for (i = 0; i < VME_MAX_BERR_PROCS; i++)
        {
          if (berr_proc_table.berr_link_dsc[i].pid == 0)
          {
            index = i;
            break;
          }
        }
        if (index == -1)
        {
          up(&berr_proc_table.proc_sem);
          return(-VME_BERRTBLFULL);
        }

        berr_proc_table.berr_link_dsc[index].pid = current->pid;
        berr_proc_table.berr_link_dsc[index].sig = pdata->VME_BerrRegSig.signum;

        berr_dsc.flag = 0;	// enable BERR latching via signal
				// race: could occur while a(nother) bus error is latched and not yet serviced ..

        pdata->berr_dsc_flag[index] = 1;	// remember which file berr belongs to
        
      }
      else		// unregister
      {
        index = -1;
        for (i = 0; i < VME_MAX_BERR_PROCS; i++)
        {
          if (pdata->berr_dsc_flag[i] == 1)
          {
            index = i;
            break;
          }
        }
        if (index == -1)
        {
          up(&berr_proc_table.proc_sem);
          return(-VME_BERRNOTFOUND);
        }

        berr_proc_table.berr_link_dsc[index].pid = 0;
        berr_proc_table.berr_link_dsc[index].sig = 0;

        pdata->berr_dsc_flag[index] = 0;
      }

      up(&berr_proc_table.proc_sem);

      break;
    }

    case VMEBERRINFO:
    {
      unsigned long flags;

      if (berr_dsc.flag == 0)		// slight risk of race with ISR ..
      {
        return(-VME_NOBUSERROR);
      }

      save_flags(flags);
      cli();

      pdata->VME_BerrInfo.vmebus_address = berr_dsc.vmeadd;
      pdata->VME_BerrInfo.address_modifier = berr_dsc.am;
      pdata->VME_BerrInfo.multiple = berr_dsc.flag;
      pdata->VME_BerrInfo.lword = berr_dsc.lword;
      pdata->VME_BerrInfo.iack = berr_dsc.iack;
      pdata->VME_BerrInfo.ds0 = berr_dsc.ds0;
      pdata->VME_BerrInfo.ds1 = berr_dsc.ds1;
      pdata->VME_BerrInfo.wr = berr_dsc.wr;

      berr_dsc.vmeadd = 0;
      berr_dsc.am = 0;
      berr_dsc.multiple = 0;
      berr_dsc.iack = 0;
      berr_dsc.flag = 0;
      berr_dsc.lword = 0;
      berr_dsc.ds0 = 0;
      berr_dsc.ds1 = 0;
      berr_dsc.wr = 0;

      restore_flags( flags );

      if (copy_to_user((void *)arg, &pdata->VME_BerrInfo, sizeof(VME_BusErrorInfo_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEBERRINFO): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }
		
    case VMESLAVEMAP:
    {
      int loop, ok = 0;

      if (copy_from_user(&pdata->sm, (void *)arg, sizeof(VME_SlaveMapInt_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAP): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.system_iobus_address = 0x%08x\n", pdata->sm.in.system_iobus_address))
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.window_size          = 0x%08x\n", pdata->sm.in.window_size))
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.address_width        = 0x%08x\n", pdata->sm.in.address_width))
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.options              = 0x%08x\n", pdata->sm.in.options))

      for(loop = 0; loop < 8; loop++)
      {
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].pbase = 0x%08x\n", loop, stmap.slave[loop].pbase))
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].ptop  = 0x%08x\n", loop, stmap.slave[loop].ptop))
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].enab  = 0x%08x\n", loop, stmap.slave[loop].enab))
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].space = 0x%08x\n", loop, stmap.slave[loop].space))
	if (pdata->sm.in.system_iobus_address >= stmap.slave[loop].pbase && 
           (pdata->sm.in.system_iobus_address + pdata->sm.in.window_size) <= stmap.slave[loop].ptop && 
	   stmap.slave[loop].enab == 1 && 
	   stmap.slave[loop].am == pdata->sm.in.address_width)
          {
	  kdebug(("vme_rcc(ioctl,VMESLAVEMAP): Decoder %d matches parameters\n", loop))
	  pdata->sm.vme_address = stmap.slave[loop].vbase + (pdata->sm.in.system_iobus_address - stmap.slave[loop].pbase);
          kdebug(("vme_rcc(ioctl,VMESLAVEMAP): VME address = 0x%08x\n", pdata->sm.vme_address))
	  ok = 1;
	  break;
          }
	}
      if (!ok)
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAP): No matching mapping found\n"))
	return(-VME_NOSTATMAP);   
      }
      if (copy_to_user((void *)arg, &pdata->sm, sizeof(VME_SlaveMapInt_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    } 

    case VMESLAVEMAPDUMP: 
    {  
      char *buf;
      int len, loop;

      buf = (char *)kmalloc(TEXT_SIZE1, GFP_KERNEL);
      if (buf == NULL)
      {
        kerror(("vme_rcc(ioctl,VMESLAVEMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }
      len = 0;
      len += sprintf(buf + len, "Slave mapping:\n");
      len += sprintf(buf + len, "VSI  VME address range  PCI address range   EN   WP   RP   AM PCI Space\n");
      for (loop = 0 ;loop < 8; loop++)
	len += sprintf(buf + len, "%d    %08x-%08x  %08x-%08x    %d    %d    %d    %d         %d\n"
          	      ,loop
		      ,stmap.slave[loop].vbase
	  	      ,stmap.slave[loop].vtop
		      ,stmap.slave[loop].pbase
		      ,stmap.slave[loop].ptop
		      ,stmap.slave[loop].enab
		      ,stmap.slave[loop].wp
		      ,stmap.slave[loop].rp
		      ,stmap.slave[loop].am
		      ,stmap.slave[loop].space);
      if (copy_to_user((void *)arg, buf, TEXT_SIZE1*sizeof(char)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAPDUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      kfree(buf);
      break;
    }

    case VMESCSAFE:
    {    
      u_int addr, multi, am, *lptr, ldata;
      u_short *sptr, sdata;
      u_char *bptr, bdata;
      int ret;

      if (copy_from_user(&pdata->sc, (void *)arg, sizeof(VME_SingleCycle_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMESCSAFE): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.kvirt_address = 0x%08x\n", pdata->sc.kvirt_address))
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.offset        = 0x%08x\n", pdata->sc.offset))
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.nbytes        = %d\n", pdata->sc.nbytes))
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.rw            = %d\n", pdata->sc.rw))

      //Wait until the Universe has emptied the write posting buffer
      while (!(uni->misc_stat & 0x40000))
      {
        txfifo_wait_1++;
        kdebug(("vme_rcc(ioctl,VMESCSAFE): TXFIFO still not empty\n"))
      }
        
      //Disable the CCT BERR logic for the duration of the safe single cycle
      mask_cct_berr();
        
      if (pdata->sc.rw == 1)             //read
      {
	if (pdata->sc.nbytes == 4)       //D32
	{
	  lptr = (u_int *)(pdata->sc.kvirt_address + pdata->sc.offset);
	  ldata = *lptr;
	  pdata->sc.data = (u_int)ldata;
	}
	else if (pdata->sc.nbytes == 2)  //D16
        {
	  sptr = (u_short *)(pdata->sc.kvirt_address + pdata->sc.offset);
	  sdata = *sptr;
	  pdata->sc.data = (u_int)sdata;
	}
	else                      //D8
	{
	  bptr = (u_char *)(pdata->sc.kvirt_address + pdata->sc.offset);
	  bdata = *bptr;
	  pdata->sc.data = (u_int)bdata;
	}
      }
      else                        //write
      {
	if (pdata->sc.nbytes == 4)       //D32
	{
	  lptr = (u_int *)(pdata->sc.kvirt_address + pdata->sc.offset);
	  ldata = pdata->sc.data;
	  *lptr = ldata;
	}
	else if (pdata->sc.nbytes == 2)  //D16
	{
	  sptr = (u_short *)(pdata->sc.kvirt_address + pdata->sc.offset);
	  sdata = (u_short)pdata->sc.data;
	  *sptr = sdata;
	}
	else                      //D8
	{
	  bptr = (u_char *)(pdata->sc.kvirt_address + pdata->sc.offset);
	  bdata = (u_char)pdata->sc.data;
	  *bptr = bdata;
	}
        
        //Wait again until the Universe has emptied the write posting buffer
        while (!(uni->misc_stat & 0x40000))
        {
          txfifo_wait_2++;
          kdebug(("vme_rcc(ioctl,VMESCSAFE): TXFIFO still not empty (2)\n"))
        }
      }

      //MJ: Do we have to wait a bit for bus errors to arrive?
      //MJ: What to do with addr, multi and am in case of BERR?
      ret = berr_check(&addr, &multi, &am);
      if (ret)
      {
	kerror(("vme_rcc(ioctl,VMESCSAFE): Bus error received\n"))
	init_cct_berr();
	return(-VME_BUSERROR);
      }
      
      //Reenable the CCT BERR logic and clear the BERR flag in the CCT logic
      init_cct_berr();

      if (copy_to_user((void *)arg, &pdata->sc, sizeof(VME_SingleCycle_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMESCSAFE): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    } 

    case VMEDMASTART:
    {
      int ret, loop, ok;
      unsigned long flags;
     
      //Check if there is space in the done list
      ok = 0;
      for (loop = 0; loop < VME_DMADONESIZE; loop++)
        if (dma_donelist[loop].handle == 0xffffffff && dma_donelist[loop].pid == 0)
        {
          ok = 1;
          break;
        }
      if (!ok)
      {
        kerror(("vme_rcc(ioctl,VMEDMASTART): no memory left in done list\n"))
        return(-VME_NODOMEMEM);
      }
      else
        kdebug(("vme_rcc(ioctl,VMEDMASTART): using entry %d in done list\n", loop))

      current_dmahandle.index = loop;
      pdata->dma[loop] = 1;
      
      //Get the DMA semaphore
      //MJ: The API document says that the function should return VME_BUSY if the DMA controller
      //    is in use (semaphore=0). There seems to be a function (down_trylock) but it is so badly
      //    documented (i.e. not at all) that I don't dare to use it. Therefore I do it the forbidden way.
      
      #ifdef VMEDEBUG 
        *marker=((current->pid) << 8) | 2; 
      #endif

      save_flags(flags);
      cli();                                     //disable interrupts to have a save look at the semaphore
      if (dma_semaphore.count.counter < 1)
      {
        restore_flags(flags);
        dma_sem_busy++;
        kerror(("vme_rcc(ioctl,VMEDMASTART): failed to get semaphore\n"))
        return(-VME_DMABUSY);
      }
      
      ret = down_interruptible(&dma_semaphore);        //we should now have the garantee to get the semaphore
      restore_flags(flags);                      //re-enable interrupts
      if (ret)
	return(-VME_ERESTARTSYS);

      #ifdef VMEDEBUG 
        *marker=((current->pid) << 8) | 3; 
      #endif

      if (copy_from_user(&pdata->dma_params, (void *)arg, sizeof(VME_DMAstart_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEDMASTART): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc(ioctl,VMEDMASTART): PCI address of chain = 0x%08x\n", pdata->dma_params.paddr))
      kdebug(("vme_rcc(ioctl,VMEDMASTART): Handle chain         = 0x%08x\n", pdata->dma_params.handle))

      current_dmahandle.handle = pdata->dma_params.handle;
      current_dmahandle.pid    = current->pid;
      kdebug(("vme_rcc(ioctl,VMEDMASTART): current_dmahandle.handle = %d\n", current_dmahandle.handle))
      kdebug(("vme_rcc(ioctl,VMEDMASTART): current_dmahandle.pid    = %d\n", current_dmahandle.pid)) 

      //start the chained DMA
      uni->dgcs |= 0x00006f00;  //clear all status and error flags
      uni->dcpp = pdata->dma_params.paddr; 
      uni->dgcs = 0x88000008;   //start chaind DMA, enable EOT interrupt
      kdebug(("vme_rcc(ioctl,VMEDMASTART): Transfer started\n"))
      
      #ifdef VMEDEBUG 
        *marker=((current->pid) << 8) | 4; 
      #endif

      break;        
    }

    case VMEDMAPOLL:
    {
      u_int ok, loop = 0;
      
      if (copy_from_user(&pdata->dma_poll_params, (void *)arg, sizeof(VME_DMAhandle_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEDMAPOLL): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
        
      #ifdef VMEDEBUG 
        *marker=((current->pid) << 8) | 5; 
      #endif

      current_dmahandle.timeout = 1;
      
      if (pdata->dma_poll_params.timeoutval > 0)
      {
        dma_timer.function = vme_dmaTimeout;
        dma_timer.data = (unsigned long)&dma_to;
        dma_timer.expires = jiffies + pdata->dma_poll_params.timeoutval;
        add_timer(&dma_timer);
      }

      ok = 0;

      #ifdef VMEDEBUG 
        *marker=((current->pid) << 8) | 7; 
      #endif

      while (!ok && current_dmahandle.timeout == 1)
      {
        for (loop = 0; loop < VME_DMADONESIZE; loop++)
        {
          kdebug(("vme_rcc(ioctl,VMEDMAPOLL): dma_donelist[%d].handle = %d  params.handle = %d\n", loop, dma_donelist[loop].handle, pdata->dma_poll_params.handle))
          kdebug(("vme_rcc(ioctl,VMEDMAPOLL): dma_donelist[%d].pid    = %d  pid           = %d\n", loop, dma_donelist[loop].pid,  current->pid))

          if (dma_donelist[loop].handle == pdata->dma_poll_params.handle && dma_donelist[loop].pid == (u_int) current->pid)
          {
            ok = 1;
            kdebug(("vme_rcc(ioctl,VMEDMAPOLL): DMA confirmation found under index %d\n", loop))
            #ifdef VMEDEBUG 
              *marker=((current->pid) << 8) | 0xa; 
              *marker=pdata->dma_poll_params.handle;
              *marker=loop;
            #endif
            break;
          }
        }
        if (pdata->dma_poll_params.timeoutval == 0)
          break;  //Don't poll, just probe
      }
      
      #ifdef VMEDEBUG 
        *marker=((current->pid) << 8) | 0x60 | current_dmahandle.timeout; 
      #endif
      
      if (!ok)  //This DMA has not yet completed
      {
        #ifdef VMEDEBUG 
          *marker=0x4444; 
        #endif
      
        kdebug(("vme_rcc(ioctl,VMEDMAPOLL): Still busy\n"))
        pdata->dma_poll_params.ctrl    = 0;        
        pdata->dma_poll_params.counter = 0x80000000;  // Just to avoid 0
        pdata->dma_poll_params.timeout = current_dmahandle.timeout;
      }
      else
      {
        #ifdef VMEDEBUG 
          *marker=((current->pid) << 8) | 9; 
          *marker=loop;
        #endif

        kdebug(("vme_rcc(ioctl,VMEDMAPOLL): loop = %d\n", loop))
        pdata->dma_poll_params.ctrl    = dma_donelist[loop].ctrl;
        pdata->dma_poll_params.counter = dma_donelist[loop].counter;
        pdata->dma_poll_params.timeout = current_dmahandle.timeout;
        dma_donelist[loop].handle = 0xffffffff;
        dma_donelist[loop].pid = 0;
        pdata->dma[loop] = 0;
        kdebug(("vme_rcc(ioctl,VMEDMAPOLL): params.ctrl    = 0x%08x\n", pdata->dma_poll_params.ctrl))
        kdebug(("vme_rcc(ioctl,VMEDMAPOLL): params.counter = 0x%08x\n", pdata->dma_poll_params.counter))
        kdebug(("vme_rcc(ioctl,VMEDMAPOLL): params.timeout = %d\n", pdata->dma_poll_params.timeout))
      }
      
      //Stop the timer
      del_timer(&dma_timer);	// OK also if timer ran out
      #ifdef VMEDEBUG 
        *marker=((current->pid) << 8) | 8; 
      #endif
      

      if (copy_to_user((void *)arg, &pdata->dma_poll_params, sizeof(VME_DMAhandle_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEDMAPOLL): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }

    case VMEDMADUMP: 
    {  
      char *buf;
      int loop,len;
      u_int data, value;
      
      buf = (char *)kmalloc(TEXT_SIZE2, GFP_KERNEL);
      if (buf == NULL)
      {
        kerror(("vme_rcc(ioctl,VMESLAVEMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }
      len = 0;
      len += sprintf(buf + len, "PCI_CSR register               = 0x%08x\n", uni->pci_csr);
      len += sprintf(buf + len, "DMA registers:\n");
      len += sprintf(buf + len, "PCI address              (DLA) = 0x%08x\n", uni->dla);
      len += sprintf(buf + len, "VME address              (DVA) = 0x%08x\n", uni->dva);
      len += sprintf(buf + len, "Byte count              (DTBC) = 0x%08x\n", uni->dtbc);
      len += sprintf(buf + len, "Chain pointer           (DCPP) = 0x%08x\n", uni->dcpp);
      data = uni->dctl;
      len += sprintf(buf + len, "Transfer control reg.   (DCTL) = 0x%08x\n", data);
      len += sprintf(buf + len, "64 bit PCI is                    %s\n", data & 0x80 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "VMEbus cycle type:               %s\n", data & 0x100 ? "Block cycles" : "Single cycles");
      len += sprintf(buf + len, "AM code tpye:                    %s\n", data & 0x1000 ? "Supervisor" : "User");
      len += sprintf(buf + len, "AM code tpye:                    %s\n", data & 0x4000 ? "Programm" : "Data");
      len += sprintf(buf + len, "VMEbus address space:            ");
      value = (data >> 16) & 0x7;
      if (value == 0) len += sprintf(buf + len, "A16\n");
      if (value == 1) len += sprintf(buf + len, "A24\n");
      if (value == 2) len += sprintf(buf + len, "A32\n");
      if (value == 3) len += sprintf(buf + len, "Reserved\n");
      if (value == 4) len += sprintf(buf + len, "Reserved\n");
      if (value == 5) len += sprintf(buf + len, "Reserved\n");
      if (value == 6) len += sprintf(buf + len, "User 1\n");
      if (value == 7) len += sprintf(buf + len, "User 2\n");
      len += sprintf(buf + len, "VMEbus max data width:           ");
      value = (data >> 22) & 0x3;
      if (value == 0) len += sprintf(buf + len, "D8\n");
      if (value == 1) len += sprintf(buf + len, "D16\n");
      if (value == 2) len += sprintf(buf + len, "D32\n");
      if (value == 3) len += sprintf(buf + len, "D64\n");
      len += sprintf(buf + len, "Transfer direction:              VMEbus %s\n", data & 0x80000000 ? "Write":  "Read");
      data = uni->dgcs;
      len += sprintf(buf + len, "General control reg.    (DGCS) = 0x%08x\n", data);
      len += sprintf(buf + len, "Interrupt on protocol error:     %s\n", data & 0x1 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt on VME error:          %s\n", data & 0x2 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt on PCI error:          %s\n", data & 0x4 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt when done:             %s\n", data & 0x8 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt when halted:           %s\n", data & 0x20 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt when stopped:          %s\n", data & 0x40 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Protocol error:                  %s\n", data & 0x100 ? "Yes" : "No");
      len += sprintf(buf + len, "VMEbus error:                    %s\n", data & 0x200 ? "Yes" : "No");
      len += sprintf(buf + len, "PCI Bus error:                   %s\n", data & 0x400 ? "Yes" : "No");
      len += sprintf(buf + len, "Transfer complete:               %s\n", data & 0x800 ? "Yes" : "No");
      len += sprintf(buf + len, "DMA halted:                      %s\n",data&0x2000?"Yes":"No");
      len += sprintf(buf + len, "DMA stopped:                     %s\n",data&0x4000?"Yes":"No");
      len += sprintf(buf + len, "DMA active:                      %s\n",data&0x8000?"Yes":"No");
      value = (data >> 16) & 0xf;
      len += sprintf(buf + len, "Minimum inter tenure gap:        ");
      if (value == 0) len += sprintf(buf + len, "0 us\n");
      else if (value == 1) len += sprintf(buf + len, "16 us\n");
      else if (value == 2) len += sprintf(buf + len, "32 us\n");
      else if (value == 3) len += sprintf(buf + len, "64 us\n");
      else if (value == 4) len += sprintf(buf + len, "128 us\n");
      else if (value == 5) len += sprintf(buf + len, "256 us\n");
      else if (value == 6) len += sprintf(buf + len, "512 us\n");
      else if (value == 7) len += sprintf(buf + len, "1024 us\n");
      else len += sprintf(buf + len, "Out of range\n");
      len += sprintf(buf + len, "Bytes transfered per bus tenure: ");
      value = (data >> 20) & 0xf;
      if (value == 0) len += sprintf(buf + len, "Until done\n");
      else if (value == 1) len += sprintf(buf + len, "256 bytes\n");
      else if (value == 2) len += sprintf(buf + len, "512 bytes\n");
      else if (value == 3) len += sprintf(buf + len, "1 Kbye\n");
      else if (value == 4) len += sprintf(buf + len, "2 Kbye\n");
      else if (value == 5) len += sprintf(buf + len, "4 Kbye\n");
      else if (value == 6) len += sprintf(buf + len, "8 Kbye\n");
      else if (value == 7) len += sprintf(buf + len, "16 Kbye\n");
      else len += sprintf(buf + len, "Out of range\n");
      len += sprintf(buf + len, "DMA chaining:                    %s\n",data & 0x8000000 ? "Yes" : "No");
      len += sprintf(buf + len, "DMA halt request:                %s\n",data & 0x20000000 ? "Halt DMA after current package" : "No");
      len += sprintf(buf + len, "DMA stop request:                %s\n",data & 0x40000000 ? "process buffered data and stop" : "No");
      len += sprintf(buf + len, "DMA go:                          %s\n",data & 0x80000000 ? "Yes" : "No");

      len += sprintf(buf + len, "\nDumping active entries in dma_donelist:\n");
      for (loop = 0; loop < VME_DMADONESIZE; loop++)
      { 
        if (dma_donelist[loop].handle != 0xffffffff && dma_donelist[loop].pid != 0)
        {
          len += sprintf(buf + len, "dma_donelist[%d].handle  = %d\n", loop, dma_donelist[loop].handle);
          len += sprintf(buf + len, "dma_donelist[%d].pid     = %d\n", loop, dma_donelist[loop].pid);
          len += sprintf(buf + len, "dma_donelist[%d].ctrl    = 0x%08x\n", loop, dma_donelist[loop].ctrl);
          len += sprintf(buf + len, "dma_donelist[%d].counter = 0x%08x\n", loop, dma_donelist[loop].counter);
          len += sprintf(buf + len, "dma_donelist[%d].timeout = %d\n\n", loop, dma_donelist[loop].timeout);
        }
      }

      if (copy_to_user((void *)arg, buf, TEXT_SIZE2*sizeof(char)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEDMADUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      kfree(buf);
      break;
    }
    
    case VMELINK:
    {
      VME_IntHandle_t *group_ptr;
      u_int i;
      int vct;

      if (copy_from_user(&pdata->VME_int_handle, (void *)arg, sizeof(VME_IntHandle_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMELINK): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMELINK): # vectors = %d\n", pdata->VME_int_handle.nvectors))
      for (i = 0; i < pdata->VME_int_handle.nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMELINK): vector # %d = 0x%8x\n", i, pdata->VME_int_handle.vector[i]))

      if (irq_level_table[pdata->VME_int_handle.level - 1] == 0)        // is this level enabled?
      {
        kdebug(("vme_rcc(ioctl,VMELINK): level %d is disabled\n", pdata->VME_int_handle.level))
        return(-VME_INTDISABLED);
      }
      
      kdebug(("vme_rcc(ioctl,VMELINK): level = %d, type = %d\n", pdata->VME_int_handle.level, pdata->VME_int_handle.type))

      if (pdata->VME_int_handle.type != irq_level_table[pdata->VME_int_handle.level - 1])        // check type
      {
        kdebug(("vme_rcc(ioctl,VMELINK): level %d, user mode = %d, VMEconfig mode = %d\n",
		   pdata->VME_int_handle.level, pdata->VME_int_handle.type,  irq_level_table[pdata->VME_int_handle.level - 1]))
        return(-VME_INTCONF);
      }

      down(&link_table.link_dsc_sem);

      for (i = 0; i < pdata->VME_int_handle.nvectors; i++)
      {
        vct = pdata->VME_int_handle.vector[i]; 
        if (link_table.link_dsc[vct].pid) 
        {
          kdebug(("vme_rcc(ioctl,VMELINK): vector busy, pid = %d\n", link_table.link_dsc[vct].pid))
          up(&link_table.link_dsc_sem);
          return(-VME_INTUSED);
        }
      }

      // allocate aux. structure for multi vector handles and fill it
      group_ptr = (VME_IntHandle_t*) NULL;
      if (pdata->VME_int_handle.nvectors > 1)
      {
        group_ptr = kmalloc(sizeof(VME_IntHandle_t), GFP_KERNEL);
        if (!group_ptr)
        {  
          up(&link_table.link_dsc_sem);
          return(-VME_ENOMEM);
        }
        group_ptr->nvectors = pdata->VME_int_handle.nvectors;
        for (i = 0; i < pdata->VME_int_handle.nvectors; i++)
        {
          group_ptr->vector[i] = pdata->VME_int_handle.vector[i];
        }
      }

      for (i = 0; i < pdata->VME_int_handle.nvectors; i++)
      {
        vct = pdata->VME_int_handle.vector[i]; 

        link_table.link_dsc[vct].pid = current->pid;		// current process
        link_table.link_dsc[vct].vct = vct;	        	// vector
        link_table.link_dsc[vct].lvl = pdata->VME_int_handle.level;    // level
        link_table.link_dsc[vct].type = pdata->VME_int_handle.type;    // type
        link_table.link_dsc[vct].pending = 0;	        	// no interrupt pending
        link_table.link_dsc[vct].total_count = 0;       	// total # interrupts
        link_table.link_dsc[vct].group_ptr = group_ptr; 	// the group vectors
        sema_init(&link_table.link_dsc[vct].sem,0); 		// initialise semaphore count
        link_table.link_dsc[vct].sig = 0;	        	// no signal
      
        pdata->link_dsc_flag[vct] = 1;			// remember which file it belongs to
      }

      up(&link_table.link_dsc_sem);
      break;
    }

    case VMEINTENABLE:
    {
      unsigned int lint_en;
      unsigned long flags;

      if (copy_from_user(&pdata->VME_IntEnable2, (void *)arg, sizeof(VME_IntEnable_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEINTENABLE): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEINTENABLE): level = %d\n", pdata->VME_IntEnable2.level))

      // ONLY allowed if RORA level
      if (irq_level_table[pdata->VME_IntEnable2.level-1] != VME_INT_RORA)
          return(-VME_LVLISNOTRORA);

      save_flags(flags);
      cli();

      lint_en = readl((char*)uni + LINT_EN);	// read enabled irq bits
      lint_en |= (1 << pdata->VME_IntEnable2.level);	// BM_LINT_VIRQxxx
      kdebug(("vme_rcc(ioctl,VMEINTENABLE): lint_en =0x %x\n", lint_en))
      writel( lint_en, (char*)uni + LINT_EN);	// write it back

      restore_flags( flags );
      break;
    }

    case VMEINTDISABLE:
    {
      unsigned int lint_en;
      unsigned long flags;

      if (copy_from_user(&pdata->VME_IntEnable3, (void *)arg, sizeof(VME_IntEnable_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEINTDISABLE): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEINTDISABLE): level = %d\n", pdata->VME_IntEnable3.level))

      save_flags(flags);
      cli();

      lint_en = readl((char*)uni + LINT_EN);	// read enabled irq bits
      lint_en &= ~(1 << pdata->VME_IntEnable3.level);	// BM_LINT_VIRQxxx
      kdebug(("vme_rcc(ioctl,VMEINTDISABLE): lint_en =0x %x\n", lint_en))
      writel( lint_en, (char*)uni + LINT_EN);	// write it back

      restore_flags( flags );
      break;
    }

    case VMEWAIT:
    {
      VME_IntHandle_t *group_ptr;
      u_int i;
      int first_vct;
      int cur_vct = 0;
      struct timer_list int_timer;
      int use_timer = 0;
      int result = 0;
      unsigned long flags;

      if (copy_from_user(&pdata->VME_WaitInt2, (void *)arg, sizeof(VME_WaitInt_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEWAIT): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEWAIT): # vectors = %d, timeout = %d\n", pdata->VME_WaitInt2.int_handle.nvectors, pdata->VME_WaitInt2.timeout))
      for (i = 0; i < pdata->VME_WaitInt2.int_handle.nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMEWAIT): vector # %d = 0x%8x\n", i, pdata->VME_WaitInt2.int_handle.vector[i]))

      first_vct = pdata->VME_WaitInt2.int_handle.vector[0];

      if (pdata->VME_WaitInt2.timeout > 0) 
      {
        init_timer(&int_timer);
        int_timer.function = vme_intTimeout;
        int_timer.data = (unsigned long)&pdata->VME_WaitInt2;
        int_timer.expires = jiffies + pdata->VME_WaitInt2.timeout;
        add_timer(&int_timer);

        use_timer = 1;
      }

      if (pdata->VME_WaitInt2.timeout != 0) 		// Wait : with timeout or not
      {
        kdebug(("vme_rcc(ioctl,VMEWAIT): Before Down, semaphore = %d\n", link_table.link_dsc[first_vct].sem.count.counter))

        // IR handled before we wait. Not quite RACE safe but only for statistics  .. 
        if (link_table.link_dsc[first_vct].sem.count.counter > 0) 
        {
          if (link_table.link_dsc[first_vct].sem.count.counter == 1)
            sem_positive[0]++;
          else
            sem_positive[1]++;
        }

        #ifdef INTDEBUG
          *marker=((current->pid) << 8) | 0x3;
        #endif

        down_interruptible(&link_table.link_dsc[first_vct].sem);	// wait ..

        #ifdef INTDEBUG
          *marker=((current->pid) << 8) | 0x4;
          *marker=first_vct;
          *marker=link_table.link_dsc[first_vct].sem.count.counter;
        #endif

        if (signal_pending(current))
        {						// interrupted system call
          kdebug(("vme_rcc(ioctl,VMEWAIT): Interrupted by signal\n"))
          result = -VME_INTBYSIGNAL;		// or ERESTARTSYS ?
        }

        else if ( (use_timer == 1) && (pdata->VME_WaitInt2.timeout == 0))
        {						// timeout
          result = -VME_TIMEOUT;
        }
      }

      pdata->VME_WaitInt2.vector = 0;      // return 0 if NO pending
      pdata->VME_WaitInt2.level = 0;
      pdata->VME_WaitInt2.type = 0;
      pdata->VME_WaitInt2.multiple = 0;

      if (result == 0)		// for all values of timeout & no early returns: get IR info
      {
        if (link_table.link_dsc[first_vct].group_ptr == NULL)	// single vector
        {
          if ( link_table.link_dsc[first_vct].pending > 0 )
          {
            pdata->VME_WaitInt2.level = link_table.link_dsc[first_vct].lvl;
            pdata->VME_WaitInt2.type = link_table.link_dsc[first_vct].type;
            pdata->VME_WaitInt2.vector = first_vct;
            pdata->VME_WaitInt2.multiple = link_table.link_dsc[first_vct].pending;    // possible RACE with ISR
            link_table.link_dsc[first_vct].pending--;         // done
          }
        }
        else		// scan vectors in group and return FIRST one pending 
			// the scan starts from the first: an issue ?
        {
          save_flags(flags);		// may not really be needed
          cli();

          group_ptr = link_table.link_dsc[first_vct].group_ptr;
          kdebug(("vme_rcc(ioctl,VMEWAIT): group pointer = 0x%p\n",group_ptr))
          for ( i = 0; i < group_ptr->nvectors; i++)
          {
            cur_vct = group_ptr->vector[i];
            kdebug(("vme_rcc(ioctl,VMEWAIT): current vector = %d\n",cur_vct))
            if ( link_table.link_dsc[cur_vct].pending > 0)
            {
              pdata->VME_WaitInt2.level = link_table.link_dsc[cur_vct].lvl;
              pdata->VME_WaitInt2.type = link_table.link_dsc[cur_vct].type;
              pdata->VME_WaitInt2.vector = cur_vct;
              pdata->VME_WaitInt2.multiple = link_table.link_dsc[cur_vct].pending;
              link_table.link_dsc[cur_vct].pending--;		// done
              break;
            }
          }
          restore_flags( flags );
        }
      }

      if (use_timer == 1)
      {
        del_timer(&int_timer);	// OK also if timer ran out
      }

      if (copy_to_user((void *)arg, &pdata->VME_WaitInt2, sizeof(VME_WaitInt_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEWAIT): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }

      return (result);

      break;	// dummy ..
    }

    case VMEREGISTERSIGNAL:
    {
      int vct;
      u_int i;

      if (copy_from_user(&pdata->VME_RegSig, (void *)arg, sizeof(VME_RegSig_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEREGISTERSIGNAL): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }

      pdata->VME_int_handle2 = pdata->VME_RegSig.int_handle;

      kdebug(("vme_rcc(ioctl,VMEREGISTERSIGNAL): # vectors = %d, signal # = %d\n", pdata->VME_int_handle2.nvectors, pdata->VME_RegSig.signum))
      for (i = 0; i < pdata->VME_int_handle2.nvectors; i++)
      {
        kdebug(("vme_rcc(ioctl,VMEREGISTERSIGNAL): vector # %d = 0x%8x\n", i, pdata->VME_int_handle2.vector[i]))
      }

      down(&link_table.link_dsc_sem);

      for (i = 0; i < pdata->VME_int_handle2.nvectors; i++)
      {
        vct = pdata->VME_int_handle2.vector[i]; 
        link_table.link_dsc[vct].sig = pdata->VME_RegSig.signum;
      }

      up(&link_table.link_dsc_sem);

      break;
    }

    case VMEINTERRUPTINFOGET:
    {
      VME_IntHandle_t *group_ptr;
      u_int i;
      int first_vct;
      int cur_vct = 0;
      unsigned long flags;

      if (copy_from_user(&pdata->VME_WaitInt3, (void *)arg, sizeof(VME_WaitInt_t)) != 0)
      {
	kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): # vectors = %d \n", pdata->VME_WaitInt3.int_handle.nvectors))
      for (i = 0; i < pdata->VME_WaitInt3.int_handle.nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): vector # %d = 0x%8x\n", i, pdata->VME_WaitInt3.int_handle.vector[i]))

      first_vct = pdata->VME_WaitInt3.int_handle.vector[0];

      pdata->VME_WaitInt3.vector = 0;      // return 0 if NO pending
      pdata->VME_WaitInt3.level = 0;
      pdata->VME_WaitInt3.type = 0;
      pdata->VME_WaitInt3.multiple = 0;

      if (link_table.link_dsc[first_vct].group_ptr == NULL)
      {
        if ( link_table.link_dsc[first_vct].pending > 0 )
        {
          pdata->VME_WaitInt3.level = link_table.link_dsc[first_vct].lvl;
          pdata->VME_WaitInt3.type = link_table.link_dsc[first_vct].type;
          pdata->VME_WaitInt3.vector = first_vct;
          pdata->VME_WaitInt3.multiple = link_table.link_dsc[first_vct].pending;	// TBD
        }
      }
      else		// scan vectors in group and return FIRST one pending 
      {
        save_flags(flags);		// may not really be needed
        cli();

        group_ptr = link_table.link_dsc[first_vct].group_ptr;
        kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): group pointer = 0x%p\n",group_ptr))
        for ( i = 0; i < group_ptr->nvectors; i++)
        {
          cur_vct = group_ptr->vector[i];
          kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): current vector = %d\n",cur_vct))
          if ( link_table.link_dsc[cur_vct].pending > 0 )
          {
            pdata->VME_WaitInt3.level = link_table.link_dsc[cur_vct].lvl;
            pdata->VME_WaitInt3.type = link_table.link_dsc[cur_vct].type;
            pdata->VME_WaitInt3.vector = cur_vct;
            pdata->VME_WaitInt3.multiple = link_table.link_dsc[cur_vct].pending;
            break;
          }
        }
        restore_flags( flags );
      }

      if (copy_to_user((void *)arg, &pdata->VME_WaitInt3, sizeof(VME_WaitInt_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }

    case VMEUNLINK:
    {
      u_int i;
      int vct, first_vector;

      if (copy_from_user(&pdata->VME_int_handle3, (void *)arg, sizeof(VME_IntHandle_t)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMEUNLINK): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEUNLINK): # vectors = %d\n", pdata->VME_int_handle3.nvectors))
      for (i = 0; i < pdata->VME_int_handle3.nvectors; i++)
      {
        kdebug(("vme_rcc(ioctl,VMEUNLINK): vector # %d = 0x%x\n", i, pdata->VME_int_handle3.vector[i]))
      }

      down(&link_table.link_dsc_sem);

      if (pdata->VME_int_handle3.nvectors > 1)
      {
        first_vector = pdata->VME_int_handle3.vector[0];
        kfree(link_table.link_dsc[first_vector].group_ptr);
        kdebug(("vme_rcc(ioctl,VMEUNLINK): first vector = %d\n", first_vector))
        kdebug(("vme_rcc(ioctl,VMEUNLINK): group_ptr = 0x%p\n", link_table.link_dsc[first_vector].group_ptr))
      }

      for (i = 0; i < pdata->VME_int_handle3.nvectors; i++)
      {
        vct = pdata->VME_int_handle3.vector[i]; 

        link_table.link_dsc[vct].pid = 0;
        link_table.link_dsc[vct].vct = 0;
        link_table.link_dsc[vct].lvl = 0;
        link_table.link_dsc[vct].pending = 0;
        link_table.link_dsc[vct].total_count = 0;
        link_table.link_dsc[vct].group_ptr = 0;
        sema_init(&link_table.link_dsc[vct].sem,0);
        link_table.link_dsc[vct].sig = 0;

        pdata->link_dsc_flag[vct] = 0;
      }
      up(&link_table.link_dsc_sem);

      break;
    }
 
    // called from vmeconfig when Universe registers are updated
    case VMEUPDATE:
    {      
      int ret, i;

      kdebug(("vme_rcc(ioctl,VMEUPDATE): Function called\n")) 
        
      ret = copy_from_user(&pdata->update_data, (void *)arg, sizeof(VME_Update_t));
      if (ret)
      {
	kerror(("vme_rcc(ioctl,VMEUPDATE): error %d from copy_from_user\n",ret))
	return(-VME_EFAULT);
      }
      
      fill_mstmap(uni->lsi0_ctl, uni->lsi0_bs, uni->lsi0_bd, uni->lsi0_to, &stmap.master[0]);
      fill_mstmap(uni->lsi1_ctl, uni->lsi1_bs, uni->lsi1_bd, uni->lsi1_to, &stmap.master[1]);
      fill_mstmap(uni->lsi2_ctl, uni->lsi2_bs, uni->lsi2_bd, uni->lsi2_to, &stmap.master[2]);
      fill_mstmap(uni->lsi3_ctl, uni->lsi3_bs, uni->lsi3_bd, uni->lsi3_to, &stmap.master[3]);
      fill_mstmap(uni->lsi4_ctl, uni->lsi4_bs, uni->lsi4_bd, uni->lsi4_to, &stmap.master[4]);
      fill_mstmap(uni->lsi5_ctl, uni->lsi5_bs, uni->lsi5_bd, uni->lsi5_to, &stmap.master[5]);
      fill_mstmap(uni->lsi6_ctl, uni->lsi6_bs, uni->lsi6_bd, uni->lsi6_to, &stmap.master[6]);
      fill_mstmap(uni->lsi7_ctl, uni->lsi7_bs, uni->lsi7_bd, uni->lsi7_to, &stmap.master[7]);

      fill_slvmap(uni->vsi0_ctl, uni->vsi0_bs, uni->vsi0_bd, uni->vsi0_to, &stmap.slave[0]);
      fill_slvmap(uni->vsi1_ctl, uni->vsi1_bs, uni->vsi1_bd, uni->vsi1_to, &stmap.slave[1]);
      fill_slvmap(uni->vsi2_ctl, uni->vsi2_bs, uni->vsi2_bd, uni->vsi2_to, &stmap.slave[2]);
      fill_slvmap(uni->vsi3_ctl, uni->vsi3_bs, uni->vsi3_bd, uni->vsi3_to, &stmap.slave[3]);
      fill_slvmap(uni->vsi4_ctl, uni->vsi4_bs, uni->vsi4_bd, uni->vsi4_to, &stmap.slave[4]);
      fill_slvmap(uni->vsi5_ctl, uni->vsi5_bs, uni->vsi5_bd, uni->vsi5_to, &stmap.slave[5]);
      fill_slvmap(uni->vsi6_ctl, uni->vsi6_bs, uni->vsi6_bd, uni->vsi6_to, &stmap.slave[6]);
      fill_slvmap(uni->vsi7_ctl, uni->vsi7_bs, uni->vsi7_bd, uni->vsi7_to, &stmap.slave[7]); 
 
      // update level table
      if (pdata->update_data.irq_mode[0])
      {
        kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode parameters are valid\n"))
        for (i = 0; i < 7; i++)	
        {
          irq_level_table[i] = pdata->update_data.irq_mode[i + 1];
          kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode for level %d = %d\n", i, pdata->update_data.irq_mode[i + 1]))
        }
        irq_sysfail =  pdata->update_data.irq_mode[8];
        kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode for SYSFAIL = %d\n", pdata->update_data.irq_mode[8]))
      }
      else
        kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode parameters are not valid\n"))
      vmetab_ok = 1;
      break;
    } 
    
    case VMESYSRST:
    {
      int regval;
      
      kdebug(("vme_rcc(ioctl,VMESYSRST): Setting SYSRESET\n"))
      regval = uni->misc_ctl;
      uni->misc_ctl = (regval | 0x00400000);
      //according to the VMEbus standard sysreset has to be active for at least 200 ms
      mdelay(200);      
      kdebug(("vme_rcc(ioctl,VMESYSRST): Clearing SYSRESET\n"))    
      uni->misc_ctl = regval;
      break;
    }  

    case VMETEST:
    {
      int ret, count;

      ret = copy_from_user(&count, (void *)arg, sizeof(int));
      if (ret)
      {
	kerror(("vme_rcc(ioctl,VMETEST): error %d from copy_from_user\n",ret))
	return(-VME_EFAULT);
      }

      count++;

      if (copy_to_user((void *)arg, &count, sizeof(int)) != 0)
      {
	kerror(("vme_rcc(ioctl,VMETEST): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc(ioctl,VMETEST): VME_ENOSYS=0x%08x\n",VME_ENOSYS))
      return(-VME_ENOSYS);
      break;
    }
  }
  return(0);
}


/******************************************************/
static void vme_rcc_vmaClose(struct vm_area_struct *vma)
/******************************************************/
{ 
  kdebug(("vme_rcc(vmaClose): mmap released\n"))
  //MOD_DEC_USE_COUNT;
}  


/********************************************************************/
static int vme_rcc_mmap(struct file *file, struct vm_area_struct *vma)
/********************************************************************/
{
  int result;
  u_int size, offset;  

  vma->vm_flags |= VM_RESERVED;

  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;

  kdebug(("vme_rcc(mmap): vma->vm_end    = 0x%08x\n", (u_int)vma->vm_end))
  kdebug(("vme_rcc(mmap): vma->vm_start  = 0x%08x\n", (u_int)vma->vm_start))
  kdebug(("vme_rcc(mmap): vma->vm_offset = 0x%08x\n", (u_int)vma->vm_pgoff << PAGE_SHIFT))
  kdebug(("vme_rcc(mmap): vma->vm_flags  = 0x%08x\n", (u_int)vma->vm_flags))

  size = vma->vm_end - vma->vm_start;

  offset = vma->vm_pgoff << PAGE_SHIFT;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,20)
  result = remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot);
#else
  result = remap_page_range(vma->vm_start, offset, size, vma->vm_page_prot);
#endif
  if (result)
  {
    kerror(("vme_rcc(mmap): function remap_page_range failed \n"))
    return(-VME_REMAP);
  }
  kdebug(("vme_rcc(mmap): vma->vm_start(2) = 0x%08x\n", (u_int)vma->vm_start))

  vma->vm_ops = &vme_vm_ops;
  //MOD_INC_USE_COUNT;      
  return(0);
}


/******************************************************************************************************/
static int vme_rcc_write_procmem(struct file *file, const char *buffer, unsigned long count, void *data)
/******************************************************************************************************/
{
  int len;
  struct vme_proc_data_t *fb_data = (struct vme_proc_data_t *)data;

  kdebug(("vme_rcc(write_procmem): vme_rcc_write_procmem called\n"))

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kdebug(("vme_rcc(write_procmem): error from copy_from_user\n"))
    return(-VME_EFAULT);
  }

  kdebug(("vme_rcc(write_procmem): len = %d\n", len))
  fb_data->value[len - 1] = '\0';
  kdebug(("vme_rcc(write_procmem): text passed = %s\n", fb_data->value))

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("vme_rcc(write_procmem): debugging enabled\n"))
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("vme_rcc(write_procmem): debugging disabled\n"))
    debug = 0;
  }

  if (!strcmp(fb_data->value, "elog"))
  {
    kdebug(("vme_rcc(write_procmem): error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(fb_data->value, "noelog"))
  {
    kdebug(("vme_rcc(write_procmem): error logging disabled\n"))
    errorlog = 0;
  }

  if (!strcmp(fb_data->value, "dec"))
  {
    kdebug(("vme_rcc(write_procmem): Use count decremented\n"))
    MOD_DEC_USE_COUNT;
  }

  if (!strcmp(fb_data->value, "inc"))
  {
    kdebug(("vme_rcc(write_procmem): Use count incremented\n"))
    MOD_INC_USE_COUNT;
  }
  
  return len;
}


/*******************************************************************************************************/
static int vme_rcc_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/*******************************************************************************************************/
{
  int nchars = 0, cnt, loop;
  unsigned int lint_stat, lint_en, lint_mask;
  static int len = 0;
  
  kdebug(("vme_rcc(vme_rcc_read_procmem): Called with buf    = 0x%08x\n", (unsigned int)buf));
  kdebug(("vme_rcc(vme_rcc_read_procmem): Called with *start = 0x%08x\n", (unsigned int)*start));
  kdebug(("vme_rcc(vme_rcc_read_procmem): Called with offset = %d\n", (unsigned int)offset));
  kdebug(("vme_rcc(vme_rcc_read_procmem): Called with count  = %d\n", count));
  
  if (offset == 0)
  {
    kdebug(("exp(exp_read_procmem): Creating text....\n"));
    len = 0;
    len += sprintf(proc_read_text + len, "RCC VMEbus driver for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);

    if (board_type == VP_PSE) len += sprintf(proc_read_text + len, "Board type: VP_PSE\n");
    if (board_type == VP_PMC) len += sprintf(proc_read_text + len, "Board type: VP_PMC\n");
    if (board_type == VP_100) len += sprintf(proc_read_text + len, "Board type: VP_100\n");
    if (board_type == VP_CP1) len += sprintf(proc_read_text + len, "Board type: VP_CP1\n");
    if (board_type == VP_110) len += sprintf(proc_read_text + len, "Board type: VP_110\n");
    if (board_type == VP_315) len += sprintf(proc_read_text + len, "Board type: VP_315\n");
    if (board_type == VP_317) len += sprintf(proc_read_text + len, "Board type: VP_317\n");
    if (board_type == VP_325) len += sprintf(proc_read_text + len, "Board type: VP_325\n");

    len += sprintf(proc_read_text + len, "\nThe Universe chip %s been initialized with vmeconfig\n", vmetab_ok ? "has" : "has NOT");

    len += sprintf(proc_read_text + len, "\n SAFE function statistics\n");
    len += sprintf(proc_read_text + len, " TXFIFO not empty before safe cycle : %d times\n", txfifo_wait_1);
    len += sprintf(proc_read_text + len, " TXFIFO not empty after safe cycle  : %d times\n", txfifo_wait_2);

    len += sprintf(proc_read_text + len, "\n VMEbus interrupts\n");
    len += sprintf(proc_read_text + len, "\n  level  state     enabled  pending  # interrupts\n");

    lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
    lint_en = readl((char*)uni + LINT_EN);	// read enabled irq bitsa

    for ( cnt = 0; cnt < 7; cnt++)
    {
      lint_mask = 1<<(cnt+1);			// BM_Lxxx_VIRQxxx

      if (irq_level_table[cnt] == VME_LEVELISDISABLED)
	len += sprintf(proc_read_text + len, "      %d  %s  %s  %s\n", cnt+1,"disabled",
                       "           ", (lint_stat & lint_mask) ? "yes":" no") ;
      else if (irq_level_table[cnt] == VME_INT_ROAK)
	len += sprintf(proc_read_text + len, "      %d  %s  %11s  %7s      %8d\n", cnt+1,"ROAK",
                       (lint_en & lint_mask) ? "yes":" no", (lint_stat & lint_mask) ? "yes":" no",
                	Interrupt_counters.virq[cnt]);
      else if (irq_level_table[cnt] == VME_INT_RORA)
	len += sprintf(proc_read_text + len, "      %d  %s  %11s  %7s      %8d\n", cnt+1,"RORA",
                       (lint_en & lint_mask) ? "yes":" no", (lint_stat & lint_mask) ? "yes":" no",
                	Interrupt_counters.virq[cnt]);
    }

    if (irq_sysfail == VME_LEVELISDISABLED)
      len += sprintf(proc_read_text + len, "SYSFAIL  %s  %s  %s\n", "disabled",
                     "           ", (lint_stat & lint_mask) ? "yes":" no") ;
    else 
      len += sprintf(proc_read_text + len, "SYSFAIL  %s  %11s  %7s      %8d\n", "RORA",
                       (lint_en & 0x4000) ? "yes":" no", (lint_stat & 0x4000) ? "yes":" no",
                      Interrupt_counters.sysfail);

    len += sprintf(proc_read_text + len, "   Bus Error                    %8d\n", berr_int_count);
    len += sprintf(proc_read_text + len, "   DMA                          %8d\n", Interrupt_counters.dma);

    len += sprintf(proc_read_text + len, "\n Link Descriptors: \n");

    for (cnt = 0; cnt< VME_VCTSIZE; cnt++) 
    {
      if (link_table.link_dsc[cnt].pid)
      {
	len += sprintf(proc_read_text + len, "\n Array index     = %02d\n", cnt);
	len += sprintf(proc_read_text + len, " Pid             = %02d\n", link_table.link_dsc[cnt].pid);
	len += sprintf(proc_read_text + len, " Vector          = %02d\n", link_table.link_dsc[cnt].vct);
	len += sprintf(proc_read_text + len, " Level           = %02d\n", link_table.link_dsc[cnt].lvl);
	len += sprintf(proc_read_text + len, " Type            = %02d\n", link_table.link_dsc[cnt].type);
	len += sprintf(proc_read_text + len, " Pending         = %02d\n", link_table.link_dsc[cnt].pending);
	len += sprintf(proc_read_text + len, " # interrupts    = %02d\n", link_table.link_dsc[cnt].total_count);
	len += sprintf(proc_read_text + len, " group pointer   = 0x%p\n", link_table.link_dsc[cnt].group_ptr);
	len += sprintf(proc_read_text + len, " Semaphore count = 0x%x\n", link_table.link_dsc[cnt].sem.count.counter);
	len += sprintf(proc_read_text + len, " Signal #        = %d\n", link_table.link_dsc[cnt].sig);
      }
    }

    len += sprintf(proc_read_text + len, " semaphore statistics\n");
    len += sprintf(proc_read_text + len, " semaphore count = 1 : %d times\n", sem_positive[0]);
    len += sprintf(proc_read_text + len, " semaphore count > 1 : %d times\n", sem_positive[1]);

    len += sprintf(proc_read_text + len, "\n BERR Links: \n");
    for (cnt = 0; cnt< VME_MAX_BERR_PROCS; cnt++) 
    {
      if (berr_proc_table.berr_link_dsc[cnt].pid)
      {
	len += sprintf(proc_read_text + len, "\n Array index     = %02d\n", cnt);
	len += sprintf(proc_read_text + len, " Pid             = %02d\n", berr_proc_table.berr_link_dsc[cnt].pid);
	len += sprintf(proc_read_text + len, " Signal #        = %d\n", berr_proc_table.berr_link_dsc[cnt].sig);
      }
    }

    len += sprintf(proc_read_text + len, "\n SYSFAIL Link: \n");
    if (sysfail_link.pid)
    {
      len += sprintf(proc_read_text + len, " Pid             = %02d\n", sysfail_link.pid);
      len += sprintf(proc_read_text + len, " Signal #        = %d\n", sysfail_link.sig);
    }

    len += sprintf(proc_read_text + len, "\n BERR Descriptor: \n");
    len += sprintf(proc_read_text + len, " VMEbus address   = 0x%8x\n", berr_dsc.vmeadd);
    len += sprintf(proc_read_text + len, " address modifier = 0x%8x\n", berr_dsc.am);
    len += sprintf(proc_read_text + len, " IACK             = %d\n", berr_dsc.iack);
    len += sprintf(proc_read_text + len, " LWORD*           = %d\n", berr_dsc.lword);
    len += sprintf(proc_read_text + len, " DS0*             = %d\n", berr_dsc.ds0);
    len += sprintf(proc_read_text + len, " DS1*             = %d\n", berr_dsc.ds1);
    len += sprintf(proc_read_text + len, " WRITE*           = %d\n", berr_dsc.wr);
    len += sprintf(proc_read_text + len, " multiple bit     = 0x%8x\n", berr_dsc.multiple);
    len += sprintf(proc_read_text + len, " bus error flag   = 0x%8x\n", berr_dsc.flag);

    len += sprintf(proc_read_text + len, "\n Block transfer status\n");
    len += sprintf(proc_read_text + len, " # of unsuccessful semaphore requests: %d\n",dma_sem_busy);
    len += sprintf(proc_read_text + len, " List of completed DMA transactions:\n");
    len += sprintf(proc_read_text + len, " process ID |     handle |   DMA ctrl | DMA counter\n");

    for (loop = 0; loop < VME_DMADONESIZE; loop++)
    {
      if (dma_donelist[loop].pid != 0)
      {
	len += sprintf(proc_read_text + len, " 0x%08x |", dma_donelist[loop].pid);
	len += sprintf(proc_read_text + len, " 0x%08x |", dma_donelist[loop].handle);
	len += sprintf(proc_read_text + len, " 0x%08x |", dma_donelist[loop].ctrl);
	len += sprintf(proc_read_text + len, "  0x%08x\n", dma_donelist[loop].counter);
      }
    }

    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/vme_rcc', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug   -> Enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug -> Disable debugging\n");
    len += sprintf(proc_read_text + len, "elog    -> Log errors to /var/log/messages\n");
    len += sprintf(proc_read_text + len, "noelog  -> Do not log errors to /var/log/messages\n");
    len += sprintf(proc_read_text + len, "dec     -> Decrement module use count\n");
    len += sprintf(proc_read_text + len, "inc     -> Increment module use count\n");
  }
  kdebug(("vme_rcc(vme_rcc_read_procmem): number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("vme_rcc(vme_rcc_read_procmem): min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("vme_rcc(vme_rcc_read_procmem): returning *start   = 0x%08x\n", (unsigned int)*start));
  kdebug(("vme_rcc(vme_rcc_read_procmem): returning nchars   = %d\n", nchars));
  return(nchars);
  
}


/*******************/
int init_module(void)
/*******************/
{
  u_int i;
  int result, loop;
  u_char reg;

  SET_MODULE_OWNER(&fops);
  
  kerror(("vme_rcc(init_module): Error debug is active\n"))

  result = init_universe();
  if (result)
  {
    kerror(("vme_rcc(init_module): init_universe() failed\n"))
    return(result);
  }

  // Read the board identification
  outb(BID1, CMOSA);
  reg = inb(CMOSD);
  kdebug(("vme_rcc(init_module): BID1 = 0x%02x\n", reg))
  
  if (!(reg&0x80))
  {
    kerror(("vme_rcc(init_module): Unable to determine board type\n"))
    return(-VME_UNKNOWN_BOARD);
  }

  outb(BID2, CMOSA);
  reg = inb(CMOSD);
  kdebug(("vme_rcc(init_module): BID2 = 0x%02x\n", reg))

  reg &= 0x1f;  // Mask board ID bits
  board_type = VP_UNKNOWN;
       if (reg == 2) board_type = VP_PSE;  // VP-PSE
  else if (reg == 3) board_type = VP_PSE;  // VP-PSE
  else if (reg == 4) board_type = VP_PSE;  // VP-PSE
  else if (reg == 5) board_type = VP_PMC;  // VP-PMC
  else if (reg == 6) board_type = VP_CP1;  // VP-CP1
  else if (reg == 7) board_type = VP_100;  // VP-100
  else if (reg == 8) board_type = VP_110;  // VP-110
  else if (reg == 11) board_type = VP_315; // VP-315
  else if (reg == 13) board_type = VP_317; // VP-317
  else if (reg == 14) board_type = VP_325; // VP-325
  if (board_type == VP_PSE)
    kdebug(("vme_rcc(init_module): Board type = VP-PSE\n"))
  if (board_type == VP_PMC)
    kdebug(("vme_rcc(init_module): Board type = VP-PMC\n"))
  if (board_type == VP_100)
    kdebug(("vme_rcc(init_module): Board type = VP-100\n"))
  if (board_type == VP_CP1)
    kdebug(("vme_rcc(init_module): Board type = VP-CP1\n"))
  if (board_type == VP_110)
    kdebug(("vme_rcc(init_module): Board type = VP-110\n"))
  if (board_type == VP_315)
    kdebug(("vme_rcc(init_module): Board type = VP-315\n"))
  if (board_type == VP_317)
    kdebug(("vme_rcc(init_module): Board type = VP-317\n"))
  if (board_type == VP_325)
    kdebug(("vme_rcc(init_module): Board type = VP-325\n"))

  if (!board_type)
  {
    kerror(("vme_rcc(init_module): Unable to determine board type(2). Board seems to be of type %d\n", reg))
    return(-VME_UNKNOWN_BOARD);
  } 
      
  if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_325)
  {
    // Enable VMEbus BERR capturing
    outb(0x1, BERR_VP100);
  }

  init_cct_berr();	// enable CCT bus errors

  result = register_chrdev(vmercc_major, "vme_rcc", &fops); 
  if (result < 1)
  {
    kerror(("vme_rcc(init_module): register VME RCC driver failed.\n"))
    return(-VME_EIO);
  }
  vmercc_major = result;

  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("vme_rcc(init_module): error from kmalloc\n"));
    return(-EFAULT);
  }

  vme_rcc_file = create_proc_entry("vme_rcc", 0644, NULL);
  if (vme_rcc_file == NULL)
  {
    kdebug(("vme_rcc(init_module): error from call to create_proc_entry\n"))
    return (-ENOMEM);
  }

  strcpy(vme_proc_data.name, "vme_rcc");
  strcpy(vme_proc_data.value, "vme_rcc");
  vme_rcc_file->data = &vme_proc_data;
  vme_rcc_file->read_proc = vme_rcc_read_procmem;
  vme_rcc_file->write_proc = vme_rcc_write_procmem;
  vme_rcc_file->owner = THIS_MODULE;

  //initialize the DMA semaphore
  sema_init(&dma_semaphore, 1);
  
  //initialize the Link Descriptor protection semaphore
  sema_init(&link_table.link_dsc_sem, 1);

  //Initialise the watchdog timer for DMA transactions
  init_timer(&dma_timer);
  
  //Initialize the list of completed DMA transaction
  for (loop = 0; loop < VME_DMADONESIZE; loop++)
  {
    dma_donelist[loop].handle = 0xffffffff;
    dma_donelist[loop].pid = 0;
  }

  //Initialize the list of active master mappings
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
  {
    master_map_table.map[loop].pid = 0;
    master_map_table.map[loop].vaddr = 0;
  }
  sema_init(&master_map_table.sem, 1);

  // reset link and BERR tables
  for (i = 0; i < VME_VCTSIZE; i++)
  {
    link_table.link_dsc[i].pid = 0;
    link_table.link_dsc[i].vct = 0;
    link_table.link_dsc[i].lvl = 0;
    link_table.link_dsc[i].pending = 0;
    link_table.link_dsc[i].total_count = 0;
    link_table.link_dsc[i].group_ptr = 0;
    sema_init(&link_table.link_dsc[i].sem, 0);
    link_table.link_dsc[i].sig = 0;
  }

  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    berr_proc_table.berr_link_dsc[i].pid =  0;
    berr_proc_table.berr_link_dsc[i].sig =  0;
    sema_init(&berr_proc_table.proc_sem, 1);
  }

  berr_dsc.vmeadd = 0;
  berr_dsc.am = 0;
  berr_dsc.iack = 0;
  berr_dsc.lword = 0;
  berr_dsc.ds0 = 0;
  berr_dsc.ds1 = 0;
  berr_dsc.wr = 0;

  berr_dsc.multiple = 0;
  berr_dsc.flag = 0;

  sysfail_link.pid =  0;
  sysfail_link.sig =  0;
  sema_init(&sysfail_link.proc_sem, 1);

  kdebug(("vme_rcc(init_module): driver loaded; major device number = %d\n", vmercc_major))
  return(0);
}


/***********************/
void cleanup_module(void)
/***********************/
{
  cleanup_universe();

  if (unregister_chrdev(vmercc_major, "vme_rcc") != 0) 
    kdebug(("vme_rcc(cleanup_module): cleanup_module failed\n"))

  remove_proc_entry("vme_rcc", NULL);
  kfree(proc_read_text);

  kdebug(("vme_rcc(cleanup_module): driver removed\n"))
}

/******************************************************************************
*
* vme_intTimeout 
*
* Interrupt timeout function. Called when an interrupt fails to occur following
* a call to VME_InterruptWait()
*
******************************************************************************/
static void vme_intTimeout(unsigned long arg)
{
  VME_WaitInt_t *iPtr;
  int first_vct;

  iPtr = (VME_WaitInt_t *) arg;

  kdebug(("vme_rcc(vme_intTimeout): timeout = %d\n",iPtr->timeout))

  iPtr->timeout = 0; // zero to indicate timeout has occurred

  first_vct = iPtr->int_handle.vector[0];
  kdebug(("vme_rcc(vme_intTimeout): first vector = 0x%x\n",first_vct))

  up(&link_table.link_dsc[first_vct].sem);	// wake up WAIT
}

/******************************************************************************
*
* vme_intSysfailTimeout 
*
* Interrupt timeout function. Called when an interrupt fails to occur following
* a call to VME_SysfailInterruptWait()
*
******************************************************************************/
static void vme_intSysfailTimeout(unsigned long arg)
{
  VME_WaitInt_t *iPtr;

  iPtr = (VME_WaitInt_t *) arg;

  kdebug(("vme_rcc(vme_intSysfailTimeout): timeout = %d\n",iPtr->timeout))

  iPtr->timeout = 0; // zero to indicate timeout has occurred

  up(&sysfail_link.sem);			// wake up WAIT
}


/*******************************************/
static void vme_dmaTimeout(unsigned long arg)
/*******************************************/
{  
  //MJ:  It is very unclear if there will ever be a time-out
  //     This function should be removed unless we find a good reason for keeping it

  #ifdef VMEDEBUG 
    *marker=0x80000000 | ((current->pid) << 8) | 0x70; 
  #endif
      
  current_dmahandle.ctrl    = uni->dgcs & 0xf00;
  current_dmahandle.counter = uni->dtbc;
  current_dmahandle.timeout = 0;

  kdebug(("vme_rcc(vme_dmaTimeout): current_dmahandle.ctrl    = 0x%08x\n", current_dmahandle.ctrl))
  kdebug(("vme_rcc(vme_dmaTimeout): current_dmahandle.counter = 0x%08x\n", current_dmahandle.counter))
  
  //time-out: reset the DMA controller
  uni->dctl = 0;
  uni->dtbc = 0;
  uni->dla  = 0;
  uni->dva  = 0;
  uni->dcpp = 0;
  uni->dgcs = 0x6f00;
  
  #ifdef VMEDEBUG 
    *marker=0x80000000 | ((current->pid) << 8) | 0x71; 
  #endif
      
  if (dma_donelist[current_dmahandle.index].handle != 0xffffffff && dma_donelist[current_dmahandle.index].pid != 0) 
    kerror(("vme_rcc(vme_dmaTimeout): ERROR: Entry in dma_donelist is not empty\n"))
 
  kdebug(("vme_rcc(vme_dmaTimeout): entering data into array at index %d\n", current_dmahandle.index))
  dma_donelist[current_dmahandle.index].handle  = current_dmahandle.handle;
  dma_donelist[current_dmahandle.index].pid     = current_dmahandle.pid;
  dma_donelist[current_dmahandle.index].ctrl    = current_dmahandle.ctrl;
  dma_donelist[current_dmahandle.index].counter = current_dmahandle.counter;
  dma_donelist[current_dmahandle.index].timeout = 0;  //time out
   
  up(&dma_semaphore); //DMA reset, we can release the lock        
      
  #ifdef VMEDEBUG 
    *marker=0x80000000 | ((current->pid) << 8) | 0x72; 
  #endif
}


/*******************************/
static void vme_dma_handler(void)
/*******************************/
{  
  #ifdef VMEDEBUG 
    *marker=((current->pid) << 8) | 0xee; 
  #endif
      
  current_dmahandle.ctrl    = uni->dgcs & 0xf00;
  current_dmahandle.counter = uni->dtbc;
  
  kdebug(("vme_rcc(vme_dma_handler): current_dmahandle.ctrl    = 0x%08x\n", current_dmahandle.ctrl))
  kdebug(("vme_rcc(vme_dma_handler): current_dmahandle.counter = 0x%08x\n", current_dmahandle.counter))
      
  if (dma_donelist[current_dmahandle.index].handle != 0xffffffff && dma_donelist[current_dmahandle.index].pid != 0) 
    kerror(("vme_rcc(vme_dma_handler): ERROR: Entry in dma_donelist is not empty\n"))
    
  kdebug(("vme_rcc(vme_dma_handler): entering data into array at index %d\n", current_dmahandle.index))
  kdebug(("vme_rcc(vme_dma_handler): current_dmahandle.handle = %d\n", current_dmahandle.handle))
  kdebug(("vme_rcc(vme_dma_handler): current_dmahandle.pid    = %d\n", current_dmahandle.pid))
  dma_donelist[current_dmahandle.index].handle  = current_dmahandle.handle;
  dma_donelist[current_dmahandle.index].pid     = current_dmahandle.pid;
  dma_donelist[current_dmahandle.index].ctrl    = current_dmahandle.ctrl;
  dma_donelist[current_dmahandle.index].counter = current_dmahandle.counter;
  dma_donelist[current_dmahandle.index].timeout = 1;  //normal termination, no time out 

  up(&dma_semaphore); //EOT, we can release the lock        
}


/*****************************/
static void init_cct_berr(void)
/*****************************/
{
  u_char reg;

  reg = inb(BERR_INT_PORT);
  kdebug(("vme_rcc(init_cct_berr): CCT BERR register = 0x%x\n", reg))

  //Clear the error flag 
  reg &= ~BERR_INT_MASK;
  outb(reg, BERR_INT_PORT);
  
  // Enable the BERR interrupt
  reg |= BERR_INT_ENABLE;
  outb(reg, BERR_INT_PORT);
  
  berr_interlock = 0;   // For cards with shared interrupts such as the VP325
  kdebug(("vme_rcc(init_cct_berr): CCT BERR register = 0x%x\n", reg))
}


/*****************************/
static void mask_cct_berr(void)
/*****************************/
{
  u_char reg;

  berr_interlock = 1;   // For cards with shared interrupts such as the VP325
  reg = inb(BERR_INT_PORT);
  reg &= ~BERR_INT_ENABLE;
  outb(reg, BERR_INT_PORT);

  kdebug(("vme_rcc(mask_cct_berr): CCT BERR register = 0x%x\n", reg))
}


/******************************************************************************
*
* cct_berrInt
*
* VME bus error interrupt function for boards which have the hardware support.
* e.g VP PMC/C1x and VP CPI/Pxx boards
*
* Do nothing if the BERR was caused by a DMA operation
*
* If there is a BERR interrupt: 
* If the BERR descriptor is empty, fill it and set the count = 1, else just count++
* re-enable the latching of the BERR registers in the Universe for evry interrupt.
*
******************************************************************************/
static int cct_berrInt(void)
{
  int result = 0;
  u_char reg;
  u_int data;

  reg = inb(BERR_INT_PORT);
  kdebug(("vme_rcc(cct_berrInt): CCT BERR register = 0x%x\n", reg))

  // was there a DMA BERR
  if ((uni->dgcs & 0x700))
  {
    kerror(("vme_rcc(cct_berrInt): DMA BERR detected\n"))
    reg &= ~BERR_INT_MASK;		        // clear VME bus error
    outb(reg, BERR_INT_PORT);

    // fill the dma_done list
    current_dmahandle.ctrl    = uni->dgcs & 0xf00;
    current_dmahandle.counter = uni->dtbc;
    current_dmahandle.timeout = 0;

    kdebug(("vme_rcc(cct_berrInt): current_dmahandle.ctrl    = 0x%08x\n", current_dmahandle.ctrl))
    kdebug(("vme_rcc(cct_berrInt): current_dmahandle.counter = 0x%08x\n", current_dmahandle.counter))
 
    //error: reset the DMA controller
    uni->dctl=0;
    uni->dtbc=0;
    uni->dla =0;
    uni->dva =0;
    uni->dcpp=0;
    uni->dgcs=0x6f00;
    
    if (berr_dsc.flag == 0)       // copy info from Universe into BERR descriptor
      { 
        if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_325)
          read_berr_capture();
        else
        {
          berr_dsc.vmeadd = 0xffffffff;
          berr_dsc.am = 0xffffffff;
          berr_dsc.iack = 0xffffffff;
          berr_dsc.multiple = 0xffffffff;
          berr_dsc.lword = 0xffffffff;
          berr_dsc.ds0 = 0xffffffff;
          berr_dsc.ds1 = 0xffffffff;
          berr_dsc.wr = 0xffffffff;
        }
        // log the error
        kerror(("vme_rcc(cct_berrInt): bus error on VMEbus block transfer, current pid = %d\n", current->pid))
      }
          
    if (dma_donelist[current_dmahandle.index].handle != 0xffffffff && dma_donelist[current_dmahandle.index].pid != 0) 
      kerror(("vme_rcc(cct_berrInt): ERROR: Entry in dma_donelist is not empty\n"))

    kdebug(("vme_rcc(cct_berrInt): entering data into array at index %d\n", current_dmahandle.index))
    dma_donelist[current_dmahandle.index].handle  = current_dmahandle.handle;
    dma_donelist[current_dmahandle.index].pid     = current_dmahandle.pid;
    dma_donelist[current_dmahandle.index].ctrl    = current_dmahandle.ctrl;
    dma_donelist[current_dmahandle.index].counter = current_dmahandle.counter;
    dma_donelist[current_dmahandle.index].timeout = 1;

    up(&dma_semaphore); //Error handled, DMA reset, we can release the lock

    return(result);
  }
  
  if ( (reg & BERR_INT_MASK) != 0 )		// if VME bus error has occurred
  {
    data = readl((char*)uni + PCI_CSR);		// error with coupled cycle ?
    kdebug(("vme_rcc(cct_berrInt): PCI_CSR = 0x%x\n", data))
    if (data & 0x08000000)			// check S_TA bit
    {
      if (berr_dsc.flag == 0)		        // copy info from Universe into BERR descriptor
      {
        if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_325)
          read_berr_capture();
        else
        {
          berr_dsc.vmeadd = 0xffffffff;
          berr_dsc.am = 0xffffffff;
          berr_dsc.iack = 0xffffffff;
          berr_dsc.multiple = 0xffffffff;
          berr_dsc.lword = 0xffffffff;
          berr_dsc.ds0 = 0xffffffff;
          berr_dsc.ds1 = 0xffffffff;
          berr_dsc.wr = 0xffffffff;
        }
        // log the error
        kerror(("vme_rcc(cct_berrInt): bus error on coupled VMEbus cycle, current pid = %d\n", current->pid))
        // send signals, if anybody registered
        send_berr_signals();

        berr_dsc.flag = 1;		// reset by BusErrorInfoGet or RegisterSignal
      }
      writel(data | 0x08000000, (char*)uni + PCI_CSR);	// unlock error in Universe
    }

    data = readl((char*)uni + V_AMERR);		// error with posted cycle ?
    kdebug(("vme_rcc(cct_berrInt): V_AMERR = 0x%x\n", data))
    if (data & 0x00800000)			// V_STAT
    {
      if (berr_dsc.flag == 0)		        // copy info from Universe into BERR descriptor
      {
        if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_325)
          read_berr_capture();
        else
        {
          berr_dsc.am = (data >> 26) & 0x3f;
          berr_dsc.multiple = (data >> 24) & 0x1;
          berr_dsc.vmeadd = readl((char*)uni + VAERR);
          berr_dsc.iack = (data >> 25) & 0x1;
          berr_dsc.lword = 0xffffffff;
          berr_dsc.ds0 = 0xffffffff;
          berr_dsc.ds1 = 0xffffffff;
          berr_dsc.wr = 0xffffffff;
        }
        kdebug(("vme_rcc(cct_berrInt): VAERR = 0x%x\n", berr_dsc.vmeadd))

        // log the error
        kerror(("vme_rcc(cct_berrInt): VMEbus error: address = 0x%8x, AM code = 0x%8x\n", berr_dsc.vmeadd, berr_dsc.am))
        // send signals, if anybody registered
        send_berr_signals();
 
        berr_dsc.flag = 1;	// reset by BusErrorInfoGet or RegisterSignal
      }
      writel(0x00800000, (char*)uni + V_AMERR);	// unlock error
    }
    
    reg &= ~BERR_INT_MASK;		        // clear VME bus error
    outb(reg, BERR_INT_PORT);
    result = 1;		                        // BERR
    berr_int_count++;
  }
  return(result);
}


/***********************************/
static void sysfail_irq_handler(void)
/***********************************/
{
  unsigned int lint_en;

  // The SYSFAIL interupt is active as long as the SYSFAIL signal is asserted on the bus
  // In order to prevent a deadlock we disable the interrupt here and provide an ioctl() to re-enable it
  lint_en = readl((char*)uni + LINT_EN);		// read enabled irq bits
  kdebug(("vme_rcc(sysfail_irq_handler): lint_en = 0x%x\n", lint_en))
  lint_en &= 0xffffbfff;				
  kdebug(("vme_rcc(sysfail_irq_handler): lint_en after masking = 0x%x\n", lint_en))
  writel(lint_en, (char*)uni + LINT_EN);	 	//disable SYSFAIL

  if (sysfail_link.pid == 0)	// not linked: "spurious" interrupt
  {
    kerror(("vme_rcc(sysfail_irq_handler): SYSFAIL interrupt not linked\n"))
    kerror(("vme_rcc(sysfail_irq_handler): SYSFAIL interrupt disabled\n"))
  }
  else
  {
    if (sysfail_link.sig > 0)
    {
      kdebug(("vme_rcc(sysfail_irq_handler): pid = %d, signal = %d\n", sysfail_link.pid, sysfail_link.sig))
      kill_proc (sysfail_link.pid, sysfail_link.sig, 1);
    }
    else
      up(&sysfail_link.sem);
  } 
}


/************************************/
static void vme_irq_handler(int level)
/************************************/
{
  int statid;
  int vector, first_vector;
  unsigned int lint_en;
  VME_IntHandle_t* group_ptr;

  #ifdef VMEDEBUG 
    *marker=((current->pid) << 8) | 0x40; 
  #endif

  Interrupt_counters.virq[level-1]++;

  statid = readl((char*)uni + STATID + 4 * level);

  if (statid & 0x00000100) // bus error during IACK cycle
    kerror(("vme_rcc(vme_irq_handler): bus error during IACK cycle on level %d\n", level))
  else
  {
    vector = statid & 0x000000FF;
    kdebug(("vme_rcc(vme_irq_handler): vector = 0x%x\n",vector))

    if (link_table.link_dsc[vector].pid == 0)	// not linked: "spurious" interrupt
      kerror(("vme_rcc(vme_irq_handler): interrupt with vector %d not linked\n", vector))
    else		// handle it
    {
      if (irq_level_table[level-1] == VME_INT_RORA)
      {
        lint_en = readl((char*)uni + LINT_EN);		// read enabled irq bits
        kdebug(("vme_rcc(vme_irq_handler): level = %d, lint_en = 0x%x\n", level, lint_en))
        lint_en &= ~(1<<level);				// BM_LINT_VIRQxxx
        kdebug(("vme_rcc(vme_irq_handler): lint_en after masking = 0x%x\n", lint_en))
        writel( lint_en, (char*)uni + LINT_EN);	 	//disable  SHOULD BE MARKED??
      }

      link_table.link_dsc[vector].lvl = level;	// store in link descriptor
      link_table.link_dsc[vector].pending++;
      link_table.link_dsc[vector].total_count++;

      // signal FIRST(common) semaphore in a group
      // for signals: use signal in the link descriptor for "vector"
      group_ptr = link_table.link_dsc[vector].group_ptr;
      if (group_ptr == NULL)
      {
        first_vector = vector;
      }
      else 
      {
        first_vector = group_ptr->vector[0];
      }
      kdebug(("vme_rcc(vme_irq_handler): first vector = 0x%x\n",first_vector))

      if (link_table.link_dsc[vector].sig > 0)
      {
        kdebug(("vme_rcc(vme_irq_handler): pid = %d, signal = %d\n", link_table.link_dsc[vector].pid, link_table.link_dsc[vector].sig))
        kill_proc (link_table.link_dsc[vector].pid, link_table.link_dsc[vector].sig,1);
      }
      else
      {
        up(&link_table.link_dsc[first_vector].sem);
        #ifdef INTDEBUG
          *marker=((current->pid) << 8) | 0x2;
          *marker=first_vector;
        #endif
      }
    } 
  }
  #ifdef VMEDEBUG 
    *marker=((current->pid) << 8) | 0x4f; 
  #endif
}

// This function handles Universe Interrupts & CCT direct BUS error interrupts.
// The latter are handled first and if found, the function returns.
/****************************************************************************/
static void universe_irq_handler (int irq, void *dev_id, struct pt_regs *regs)
/****************************************************************************/
{
  unsigned int lint_stat, lint_estat, lint_en;

  #ifdef VMEDEBUG 
    *marker=0x10000000 | (current->pid); 
  #endif

  lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
  lint_en = readl((char*)uni + LINT_EN);	// read enabled irq bits
  lint_estat = lint_stat & lint_en;		// and look only at enabled irqs
  kdebug(("vme_rcc(universe_irq_handler): lint_estat = 0x%x\n", lint_estat))

  if (!berr_interlock && cct_berrInt())	// first check and handle CCT VMEbus errors
  {
    return;
  }

  //handle all the interrupts sequentially

  if (lint_estat & BM_LINT_VIRQ7)
  {
    vme_irq_handler(7);
  }
  if (lint_estat & BM_LINT_VIRQ6)
  {
     vme_irq_handler(6);
  }
  if (lint_estat & BM_LINT_VIRQ5)
  {
    vme_irq_handler(5);
  }
  if (lint_estat & BM_LINT_VIRQ4)
  {
    vme_irq_handler(4);
  }
  if (lint_estat & BM_LINT_VIRQ3)
  {
    vme_irq_handler(3);
  }
  if (lint_estat & BM_LINT_VIRQ2)
  {
    vme_irq_handler(2);
  }
  if (lint_estat & BM_LINT_VIRQ1)
  {
    vme_irq_handler(1);
  }
  
  if (lint_estat & BM_LINT_SYSFAIL)
  {
    Interrupt_counters.sysfail++;    
    sysfail_irq_handler();
  }
  
  // ERROR CHECKING
  if (lint_estat & BM_LINT_VOWN)
  {
    Interrupt_counters.vown++;
  }
  if (lint_estat & BM_LINT_ACFAIL)
  {
    Interrupt_counters.acfail++;
  }
  if (lint_estat & BM_LINT_SW_INT)
  {
    Interrupt_counters.sw_int++;
  }
  if (lint_estat & BM_LINT_SW_IACK)
  {
    Interrupt_counters.sw_iack++;
  }
  if (lint_estat & BM_LINT_VERR)
  {
    Interrupt_counters.verr++;
  }
  if (lint_estat & BM_LINT_LERR)
  {
    Interrupt_counters.lerr++;
  }
  if (lint_estat & BM_LINT_DMA)
  {
    #ifdef VMEDEBUG 
      *marker=0x30000000 | (current->pid); 
    #endif
    Interrupt_counters.dma++;
    vme_dma_handler();
  }
    
  #ifdef VMEDEBUG 
    *marker=0x40000000 | (current->pid); 
  #endif
  
  writel(lint_estat, (char*)uni + LINT_STAT);   // clear only the enabled bits
}

/****************************/
static int init_universe(void)
/****************************/
{
  struct pci_dev *uni_dev = NULL;		// "soft" Universe structure
  unsigned char pci_bus, pci_device_fn, pci_revision;
  unsigned short pci_vendor_id;
  unsigned int pci_ioaddr;
  int result;
	  
  uni_dev = pci_find_device(PCI_VENDOR_ID_TUNDRA, PCI_DEVICE_ID_TUNDRA_CA91C042, uni_dev);
  if (uni_dev == NULL)
  {
    kerror(("vme_rcc(init_universe): No Universe found\n"))
    return(-VME_ENOSYS);
  }
 
  pci_bus = uni_dev->bus->number;
  pci_device_fn = uni_dev->devfn;
  pci_ioaddr = uni_dev->resource[0].start;

  // get revision directly from the Tundra
  pci_read_config_byte(uni_dev, PCI_REVISION_ID, &pci_revision);

  kdebug(("vme_rcc(init_universe): Universe found: \n"))
  kdebug(("vme_rcc(init_universe): bus = %x device = %x revision = %x address = 0x%08x\n", pci_bus, pci_device_fn, pci_revision, pci_ioaddr))

  if (pci_revision < 1)
  {
    kerror(("vme_rcc(init_universe): Illegal Universe Revision\n"))
    return(-VME_ILLREV);
  }

  if ((pci_ioaddr & PCI_BASE_ADDRESS_SPACE) == PCI_BASE_ADDRESS_SPACE_IO)
  {
    kerror(("vme_rcc(init_universe): The device uses io addresses\n"))
    kerror(("vme_rcc(init_universe): But this driver is made for memory mapped access\n"))
    return(-VME_EIO);
  }

  uni = (universe_regs_t *)ioremap(pci_ioaddr & PCI_BASE_ADDRESS_MEM_MASK, 4*1024);
  if (uni == NULL)
  {
    kerror(("vme_rcc(init_universe): error from ioremap\n"))
    return(-VME_EFAULT);
  }

  pci_vendor_id = readw((char*)uni + PCI_VENDOR_ID);
  kdebug(("vme_rcc(init_universe): Testing ioremap -> VDID = 0x%08x\n", pci_vendor_id))
  if (pci_vendor_id != PCI_VENDOR_ID_TUNDRA)
  {
    return(-VME_IOREMAP);
  }

  pci_read_config_dword(uni_dev, PCI_INTERRUPT_LINE, &vme_irq_line);
  kdebug(("vme_rcc(init_universe): interrupt pin = %d, interrupt line = %d \n", (vme_irq_line & 0x0000ff00)>>8, vme_irq_line & 0x000000ff))

  vme_irq_line &= 0x000000FF;
	
  result = request_irq(vme_irq_line, universe_irq_handler,
		       SA_SHIRQ, "vme_rcc", universe_irq_handler);	// Rubini p.275 & CCT
  if (result)
  {
    kerror(("vme_rcc(init_universe): request_irq failed on IRQ = %d\n", vme_irq_line))
    return(-VME_REQIRQ);
  }

  kdebug(("vme_rcc(init_universe): request_irq OK\n"))

  // Clear and enable the DMA interrupt
  uni->lint_stat |= 0x100;
  uni->lint_en |= 0x100;

  // Read the static master and slave mappings once
  fill_mstmap(uni->lsi0_ctl, uni->lsi0_bs, uni->lsi0_bd, uni->lsi0_to, &stmap.master[0]);
  fill_mstmap(uni->lsi1_ctl, uni->lsi1_bs, uni->lsi1_bd, uni->lsi1_to, &stmap.master[1]);
  fill_mstmap(uni->lsi2_ctl, uni->lsi2_bs, uni->lsi2_bd, uni->lsi2_to, &stmap.master[2]);
  fill_mstmap(uni->lsi3_ctl, uni->lsi3_bs, uni->lsi3_bd, uni->lsi3_to, &stmap.master[3]);
  fill_mstmap(uni->lsi4_ctl, uni->lsi4_bs, uni->lsi4_bd, uni->lsi4_to, &stmap.master[4]);
  fill_mstmap(uni->lsi5_ctl, uni->lsi5_bs, uni->lsi5_bd, uni->lsi5_to, &stmap.master[5]);
  fill_mstmap(uni->lsi6_ctl, uni->lsi6_bs, uni->lsi6_bd, uni->lsi6_to, &stmap.master[6]);
  fill_mstmap(uni->lsi7_ctl, uni->lsi7_bs, uni->lsi7_bd, uni->lsi7_to, &stmap.master[7]);

  fill_slvmap(uni->vsi0_ctl, uni->vsi0_bs, uni->vsi0_bd, uni->vsi0_to, &stmap.slave[0]);
  fill_slvmap(uni->vsi1_ctl, uni->vsi1_bs, uni->vsi1_bd, uni->vsi1_to, &stmap.slave[1]);
  fill_slvmap(uni->vsi2_ctl, uni->vsi2_bs, uni->vsi2_bd, uni->vsi2_to, &stmap.slave[2]);
  fill_slvmap(uni->vsi3_ctl, uni->vsi3_bs, uni->vsi3_bd, uni->vsi3_to, &stmap.slave[3]);
  fill_slvmap(uni->vsi4_ctl, uni->vsi4_bs, uni->vsi4_bd, uni->vsi4_to, &stmap.slave[4]);
  fill_slvmap(uni->vsi5_ctl, uni->vsi5_bs, uni->vsi5_bd, uni->vsi5_to, &stmap.slave[5]);
  fill_slvmap(uni->vsi6_ctl, uni->vsi6_bs, uni->vsi6_bd, uni->vsi6_to, &stmap.slave[6]);
  fill_slvmap(uni->vsi7_ctl, uni->vsi7_bs, uni->vsi7_bd, uni->vsi7_to, &stmap.slave[7]);       

#if defined (VMEDEBUG) || defined (INTDEBUG)
  marker = (unsigned int *)ioremap(0x80008000, 1024);
  *marker=1;
#endif

  return(0);
}	


/*******************************/
static int cleanup_universe(void)
/*******************************/
{
  u_int i;
  unsigned int reg;

  for (i=0;i<7;i++)				// restore initial enable bits from vmetab 
  {
    kdebug(("vme_rcc(cleanup_universe): irq_level_table[%d] =0x%x\n", i, irq_level_table[i]))
    if ( irq_level_table[i] != VME_LEVELISDISABLED )
    {
      reg = readl((char*)uni + LINT_EN);	// read enabled irq bits
      kdebug(("vme_rcc(cleanup_universe): reg =0x%x\n", reg))
      reg |= (1<<(i+1)); 			// BM_LINT_VIRQxxx
      kdebug(("vme_rcc(cleanup_universe): reg =0x%x\n", reg))
      writel( reg, (char*)uni + LINT_EN);
    }
  }

  kdebug(("vme_rcc(cleanup_universe): irqsysfail = 0x%x\n", irq_sysfail))
  if ( irq_sysfail != VME_LEVELISDISABLED )
  {
    reg = readl((char*)uni + LINT_EN);	// read enabled irq bits
    kdebug(("vme_rcc(cleanup_universe): reg =0x%x\n", reg))
    reg |= 0x4000; 			// SYSFAIL
    kdebug(("vme_rcc(cleanup_universe): reg =0x%x\n", reg))
    writel( reg, (char*)uni + LINT_EN);
  }

  free_irq(vme_irq_line, universe_irq_handler);
  kdebug(("vme_rcc(cleanup_universe): interrupt line %d released\n", vme_irq_line))
 
  iounmap((void *)uni);

  return(0);
}


/*************************************************************************************/
static void fill_mstmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap)
/*************************************************************************************/
{
  if (d3 == 0) 
    d3 = 0xffffffff;
  d2 = d2 & 0xfffff000;
  d4 = d4 & 0xfffff000;
  mstslvmap->vbase = d2 + d4;
  mstslvmap->vtop = d3 + d4;
  mstslvmap->pbase = d2;
  mstslvmap->ptop = d3;
  mstslvmap->am = (d1 >> 16) & 0x7;   //0 = A16, 1 = A24, 2 = A32
  mstslvmap->options = d1 & 0x0000f000;
  mstslvmap->enab = (d1 >> 31) & 0x1;
  mstslvmap->wp = (d1 >> 30) & 0x1;
  mstslvmap->rp = 0;
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->vbase   = 0x%08x @  0x%08x\n", mstslvmap->vbase, (u_int) &mstslvmap->vbase))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->vtop    = 0x%08x @  0x%08x\n", mstslvmap->vtop, (u_int) &mstslvmap->vtop))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->am      = %d @  0x%08x\n", mstslvmap->am, (u_int) &mstslvmap->am))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->options = %d @  0x%08x\n", mstslvmap->options, (u_int) &mstslvmap->options))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->enab    = %d @  0x%08x\n", mstslvmap->enab, (u_int) &mstslvmap->enab))
}


/*************************************************************************************/
static void fill_slvmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap)
/*************************************************************************************/
{
  d2 = d2 & 0xfffff000;
  d4 = d4 & 0xfffff000;
  mstslvmap->vbase = d2;
  mstslvmap->vtop = d3;
  mstslvmap->pbase = d2 + d4;
  mstslvmap->ptop = d3 + d4;
  mstslvmap->space = d1 & 0x3;
  mstslvmap->am = (d1 >> 16) & 0x7;   //0 = A16, 1 = A24, 2 = A32
  mstslvmap->enab = (d1 >> 31) & 0x1;
  mstslvmap->wp = (d1 >> 30) & 0x1;
  mstslvmap->rp = (d1 >> 29) & 0x1;
}


/*********************************/
static void send_berr_signals(void)
/*********************************/
{
  u_int i;

  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    if (berr_proc_table.berr_link_dsc[i].pid)
    {
      kdebug(("vme_rcc(send_berr_signals): index = %d, pid (current)= %d, signal = %d\n", i, berr_proc_table.berr_link_dsc[i].pid, berr_proc_table.berr_link_dsc[i].sig))
      kill_proc(berr_proc_table.berr_link_dsc[i].pid, berr_proc_table.berr_link_dsc[i].sig,1);
    }
  }
}


/*********************************************************/
static int berr_check(u_int *addr, u_int *multi, u_int *am)
/*********************************************************/
{ 
  //This function is to look for single cycle related bus errors
  //It does not handle BERRs of DMAs or IACK cycles
  unsigned int data;

  kdebug(("vme_rcc(berr_check): called\n"))

  //lets see if it there was an error with a coupled cycle
  data = uni->pci_csr;                 //read the target abort status bit
  if (data & 0x08000000)               //check S_TA bit
  { 
    uni->pci_csr = data | 0x08000000;  //clear the bit
    *addr = 0xffffffff;
    *multi = 0xffffffff;
    *am = 0xffffffff;
    kdebug(("vme_rcc(berr_check): BERR on coupled cycle detected\n"))
    return(VME_BUSERROR);              //bus error
  }

  //lets see if it there was a VMEbus error with a posted cycle
  data = uni->v_amerr;
  if (data & 0x00800000)
  {
    *am = (data >> 26) & 0x3f;
    *multi = (data >> 24) & 0x1;
    *addr = uni->vaerr;
    uni->v_amerr = 0x00800000;
    kdebug(("vme_rcc(berr_check): BERR on coupled posted detected\n"))
    return(VME_BUSERROR);
  }
  else
  {
    kdebug(("vme_rcc(berr_check): No BERR detected\n"))
    return(0);                //no error
  }
}


/*********************************/
static void read_berr_capture(void)
/*********************************/
{  
  u_char reg, berr_info[16];
  u_int data, loop;
  
  reg = inb(BERR_VP100);
  if (reg & 0x80)
  {
  
    berr_dsc.vmeadd = 0xffffffff;
    berr_dsc.am = 0xffffffff;
    berr_dsc.iack = 0xffffffff;
    berr_dsc.multiple = 0xffffffff;
    berr_dsc.lword = 0xffffffff;
    berr_dsc.ds0 = 0xffffffff;
    berr_dsc.ds1 = 0xffffffff;
    berr_dsc.wr = 0xffffffff;
    kerror(("vme_rcc(read_berr_capture): The BERR capture logic was busy. Information could not be read\n"))
    return;
  }
 
  // Reset the read sequence
  outb(0x2, BERR_VP100);

  // Read the raw data
  for(loop = 0; loop < 16; loop++)
    berr_info[loop] = inb(BERR_VP100) & 0xf;
  
  // Assemble the values
  berr_dsc.vmeadd = (berr_info[0] << 28) +
                    (berr_info[1] << 24) +
                    (berr_info[2] << 20) +
                    (berr_info[3] << 16) +
                    (berr_info[4] << 12) +
                    (berr_info[5] << 8) +
                    (berr_info[6] << 4) +
                    (berr_info[7] & 0xe);
  berr_dsc.am = ((berr_info[8] & 0x3) << 4) + berr_info[9];
  berr_dsc.multiple = 0xffffffff;
  berr_dsc.lword = berr_info[7] & 0x1;
  berr_dsc.ds0 = (berr_info[8] & 0x4) >> 2;
  berr_dsc.ds1 = (berr_info[8] & 0x8) >> 3;
  berr_dsc.wr = (berr_info[10] & 0x8) >> 3;
  
  data = readl((char*)uni + V_AMERR);		// error with IACK cycle ?
  kdebug(("vme_rcc(read_berr_capture): V_AMERR = 0x%x\n", data))
  if (data & 0x00800000)			// V_STAT
    berr_dsc.iack = (data >> 25) & 0x1;
  else
    berr_dsc.iack = 0xffffffff;

  kdebug(("vme_rcc(read_berr_capture): berr_dsc.vmeadd = 0x%08x\n", berr_dsc.vmeadd))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.am     = 0x%02x\n", berr_dsc.am))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.iack   = 0x%02x\n", berr_dsc.iack))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.lword  = %d\n", berr_dsc.lword))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.ds0    = %d\n", berr_dsc.ds0))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.ds1    = %d\n", berr_dsc.ds1))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.write  = %d\n", berr_dsc.wr))

  // Reset the capture buffer
  outb(0x4, BERR_VP100);

  //Reenable the capture
  outb(0x1, BERR_VP100);
}




