// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_interrupt_test.c					*/
/*									*/
/* This is the interrupt test program for the RCC VMEbus library	*/
/*									*/
/* 02. Nov. 01  JOP  created ( compatible with vme_rcc_test.c )		*/
/* 18. Apr. 02  JOP  ROAK / RORA / Enable / Disable			*/
/* 27. May. 02  JOP  Modify interrupt API				*/
/*									*/
/**************** C 2001 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"

#define VME_VECTOR_DEF 130
#define VME_LEVEL_DEF 3

#define SIGNUM 42               // Qed RT signal

enum
{
  VME_OPEN_F = 1,
  VME_LINK_F,
  VME_ENABLE_F,
  VME_WAIT_F,
  VME_REGSIG_F,
  VME_UNLINK_F,
  VME_CLOSE_F,
  VME_SYSFAILLINK_F,
  VME_SYSFAILREGISTER_F,
  VME_SYSFAILWAIT_F,
  VME_SYSFAILPOLL_F,
  VME_SYSFAILREENABLE_F,
  VME_SYSFAILUNLINK_F,
  VME_EXIT_F = 100
};


// Prototypes
int mainhelp(void);
int func_menu(void);

static struct sigaction sa;

/**********************************************************/
void sysfail_signal_handler(int signum)
/**********************************************************/
{

  printf(" \n sysfail_signal_handler; signal # = %d\n",signum);

}


/************/
int main(void)
/************/
{
  int fun=1;

  sigemptyset(&sa.sa_mask) ;
  sa.sa_flags = 0;
  sa.sa_handler =  sysfail_signal_handler;
// dont block any signal in intercept handler
  if (sigaction( SIGNUM, &sa, NULL) < 0 ) {
      perror(" sigaction SIGNUM ");
      exit(1);
  }

  printf("\n\n\nThis is the interrupt test program for the RCC VMEbus library\n");
  printf("=============================================================\n");

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                        2 Test functions\n");  
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
  }

  return(0);  
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact jorgen.petersen@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}

/*****************/
int func_menu(void)
/*****************/
{
  int fun=1;
  int i;
  int int_handle;
  int timeout, signum;
  int quit_flag = 0;
  VME_ErrorCode_t ret;
  VME_InterruptList_t irq_list;
  VME_InterruptInfo_t ir_info;
  int flag;

  quit_flag = 0;

  do
  {

    printf("\n=========================================================================\n");
    printf("\n");
    printf("Select a function of the API:\n");
    printf(" VME_Open                            : %d\n", VME_OPEN_F);
    printf(" VME_InterruptLink                   : %d\n", VME_LINK_F);
    printf(" VME_InterruptReenable               : %d\n", VME_ENABLE_F);
    printf(" VME_InterruptWait                   : %d\n", VME_WAIT_F);
    printf(" VME_RegisterSignal                  : %d\n", VME_REGSIG_F);
    printf(" VME_InterruptUnLink                 : %d\n", VME_UNLINK_F);
    printf(" VME_SysfailInterruptLink            : %d\n", VME_SYSFAILLINK_F);
    printf(" VME_SysfailInterruptRegisterSignal  : %d\n", VME_SYSFAILREGISTER_F);
    printf(" VME_SysfailInterruptWait            : %d\n", VME_SYSFAILWAIT_F);
    printf(" VME_SysfailInterruptPoll            : %d\n", VME_SYSFAILPOLL_F);
    printf(" VME_SysfailInterruptReenable        : %d\n", VME_SYSFAILREENABLE_F);
    printf(" VME_SysfailInterruptUnlink          : %d\n", VME_SYSFAILUNLINK_F);
    printf(" VME_Close                           : %d\n", VME_CLOSE_F);
    printf(" EXIT                                : %d\n", VME_EXIT_F);

    printf("Your choice ");
    fun = getdecd(fun);

    switch (fun) 
    {
      case VME_OPEN_F :

        ret=VME_Open();
        if (ret != VME_SUCCESS)
          VME_ErrorPrint(ret);

        break;

      case VME_CLOSE_F :
        ret = VME_Close();
        if (ret != VME_SUCCESS)
          VME_ErrorPrint(ret);

        break;

      case VME_LINK_F :

        printf("# interrupts = ");
        irq_list.number_of_items = getdecd(1);
        for (i=0; i < irq_list.number_of_items; i++)
        {
          printf("vector = ");
          irq_list.list_of_items[i].vector = getdecd(VME_VECTOR_DEF+i);
          printf("level = ");
          irq_list.list_of_items[i].level = getdecd(VME_LEVEL_DEF);
          printf("interrupt type ( ROAK = %2d, RORA = %2d) = ",VME_INT_ROAK, VME_INT_RORA);
          irq_list.list_of_items[i].type = getdecd(VME_INT_ROAK);
        }
        
        ret = VME_InterruptLink(&irq_list, &int_handle);
        if (ret != VME_SUCCESS)
          VME_ErrorPrint(ret);
        else
          printf(" IRQ Handle = %d\n", int_handle);

        break;

      case VME_ENABLE_F :

        printf("interrupt handle = ");
        int_handle = getdec();

        ret = VME_InterruptReenable(int_handle);
        if (ret != VME_SUCCESS)
          VME_ErrorPrint(ret);

        break;

      case VME_WAIT_F :

        printf("interrupt handle = ");
        int_handle = getdec();

        printf("timeout (in ms) = ");
        timeout = getdecd(10000);
        
        ret = VME_InterruptWait(int_handle, timeout, &ir_info);
        if (ret != VME_SUCCESS)
          VME_ErrorPrint(ret);

        printf(" interrupt level = %d\n", ir_info.level);
        printf(" interrupt type  = %d\n", ir_info.type);
        printf(" interrupt vector = %d\n", ir_info.vector);

        break;

      case VME_REGSIG_F :

        printf("interrupt handle = ");
        int_handle = getdec();
  
        printf("signal # = ");
        signum = getdecd(42);	// real-time signal  FIXME
        
        ret = VME_InterruptRegisterSignal(int_handle, signum);
        if (ret != VME_SUCCESS)
  	VME_ErrorPrint(ret);
  
        break;

      case VME_UNLINK_F :

        printf("interrupt handle = ");
        int_handle = getdecd(int_handle);
        
        ret = VME_InterruptUnlink(int_handle);
        if (ret != VME_SUCCESS)
  	VME_ErrorPrint(ret);

        break;

      case VME_SYSFAILLINK_F:
        ret = VME_SysfailInterruptLink();
        if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);
       break;

      case VME_SYSFAILREGISTER_F:
        printf("signal # = ");
        signum = getdecd(42);	// real-time signal  FIXME
        ret = VME_SysfailInterruptRegisterSignal(signum);
        if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);

        break;

      case VME_SYSFAILWAIT_F:
        printf("timeout (in ms) = ");
        timeout = getdecd(10000);
        ret =  VME_SysfailInterruptWait(timeout);
        if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);

        printf("\n SYSFAIL interrupt \n");

       break;

      case VME_SYSFAILPOLL_F:
        ret =  VME_SysfailPoll(&flag);
        if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);
        else
        {
          if (flag)
            printf("The SYSFAIL is active\n");
          else
            printf("The SYSFAIL is inactive\n");
        }

       break;

      case VME_SYSFAILREENABLE_F:
        ret =  VME_SysfailInterruptReenable();
        if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);

       break;

      case VME_SYSFAILUNLINK_F:
        ret = VME_SysfailInterruptUnlink();
        if (ret != VME_SUCCESS)
	  VME_ErrorPrint(ret);

        break;

      case VME_EXIT_F :

        quit_flag = 1;

        break;

      default :

        printf(" not yet implemented .. \n");

    }

  } while (quit_flag == 0);

  return(0);
}
