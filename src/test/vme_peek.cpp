// $Id: 
/************************************************************************/
/*									*/
/* File: vme_peek.c							*/
/*									*/
/* Execute one VMEbus read cycle and print the data 			*/
/*									*/
/* 17. Jul. 06  MAJO  created						*/
/*									*/
/**************** C 2006 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSGetInput/get_input.h"


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
  u_int ret, vmeaddr, vmedata;
  u_char cdata;
  u_short sdata;
  int handle, amcode, dsize;

  if ((argc == 4) && (sscanf(argv[3], "%d", &amcode) == 1)) {argc--;} else {amcode = 2;}
  if ((argc == 3) && (sscanf(argv[2], "%d", &dsize) == 1)) {argc--;} else {dsize = 4;}
  if ((argc == 2) && (sscanf(argv[1], "%x", &vmeaddr) == 1)) {argc--;} else {vmeaddr = 0x0;}
  if (argc != 1) 
  {
    printf("Use:\nvme_peek <Address><Data size><AM code>\n");
    printf("Address   = The hexadecimal VMEbus address                     Default: 0x0\n");
    printf("Data size = The number of bytes to read (1=D8, 2=D16, 4=D32)   Default: 4\n");
    printf("AM code   = The AM code (0=A16, 1=A24, 2=A32)                  Default: 2\n");
    printf("\n"); 
    exit(0);
  }

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }

  master_map.vmebus_address   = vmeaddr;
  master_map.window_size      = 0x10;
  master_map.address_modifier = amcode;
  master_map.options          = 0;
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }

  if (dsize == 1)
  {
    ret = VME_ReadSafeUChar(handle, 0, &cdata);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(1);
    }
    vmedata = cdata;
  }  
  
  if (dsize == 2)
  {
    ret = VME_ReadSafeUShort(handle, 0, &sdata);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(1);
    }
    vmedata = sdata;
  }  
  
  if (dsize == 4)
  {
    ret = VME_ReadSafeUInt(handle, 0, &vmedata);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(1);
    }
  }

  printf("0x%08x\n", vmedata);

  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }
  exit(vmedata);
}
