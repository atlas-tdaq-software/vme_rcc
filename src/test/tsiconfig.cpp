// $Id$
/**************************************************************************************/
/*  This is the "tsiconfig" program to initialise the VMEbus interface	              */
/*  of a Tsi148 based card. 						              */
/*									              */
/*  Author: Markus Joos, CERN						              */
/*									              */
/*  26.5.2016  Automatic PCI address allocation added by Benedikt Muessig, GBS-SHA    */
/******* C 2016  A nickel program worth a dime ****************************************/   

#include <stdio.h>             
#include <stdlib.h>             
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/file.h>
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "vme_rcc/vme_rcc.h"
#include "vme_rcc/tsi148.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"

//types
typedef struct 
{
  u_int64_t vbase;  //VMEbus base address
  u_int64_t pbase;  //PCI base address
  u_int size;       //size of window
  u_int vmespace;   //0=A16  1=A24  2=A32
  u_int enable;     //0=disable decoder  1=enable decoder
  u_int decoder;    //0=decoder 0, etc.
} slavemap;

typedef struct
{
  u_int64_t vbase;  //VMEbus base address
  u_int64_t pbase;  //PCI base address
  u_int size;       //size of window*/ 
  u_int vmespace;   //0=A16  1=A24  2=A32  5=CR/CSR 8=USER1 9=USER2 10=USER1 11=USER2
  u_int supervisor; //1=supervisor AM  0=user AM
  u_int program;    //1=program AM  0=data AM
  u_int enable;     //0=disable decoder  1=enable decoder
  u_int decoder;    //Number of map decoder
} mastermap;

typedef struct
{
  mastermap mmap0;          //master map decoder #0
  mastermap mmap1;          //master map decoder #1
  mastermap mmap2;          //master map decoder #2
  mastermap mmap3;          //master map decoder #3
  mastermap mmap4;          //master map decoder #4
  mastermap mmap5;          //master map decoder #5
  mastermap mmap6;          //master map decoder #6
  mastermap mmap7;          //master map decoder #7
  slavemap smap0;           //slave map decoder #0
  slavemap smap1;           //slave map decoder #1
  slavemap smap2;           //slave map decoder #2
  slavemap smap3;           //slave map decoder #3
  slavemap smap4;           //slave map decoder #4
  slavemap smap5;           //slave map decoder #5
  slavemap smap6;           //slave map decoder #6
  slavemap smap7;           //slave map decoder #7
  u_int reqlvl;             //bus request level: 0=BR0  1=BR1  2=BR2  3=BR3
  u_int fair;               //0=fair arbit. disabled  1=fair arbit. enabled
  u_int relmode;            //release mode
  u_int vmeto;              //0=disabled  1=16us  2=32us  3=64us  4=128us  5=256us  6=512us  7=1024us 8=2048us
  u_int arbto;              //0=disabled  1=16us  
  u_int arbmode;            //0=Round robin  1=Priority
  u_int full_init;          //0=only base initialisation  1=full initialisation of Tsi148
  u_int irqs;               //mask for VMEbus interrupts
  u_int irq_mode[9];        //VME_INT_RORA or VME_INT_ROAK or VME_LEVELISDISABLED
  u_int ms;                 //master byte swapping: 0=disabled  1=enabled
  u_int ss;                 //slave byte swapping: 0=disabled  1=enable
  u_int fs;                 //fast byte swapping: 0=disabled  1=enable  NOTE: fast swapping is incompatible with D8 cycles
  u_int64_t high_map_base;  //base address of non prefetchable PCI memory above the 4 GB limit
  u_int64_t high_map_size;  //size of non prefetchable PCI memory above the 4 GB limit
  u_int high_map_bridge;    //Occurrence number of the 12d8:e130 bridge in front of the Tsi148. Only relevant if PMCs are installed
} tsipara;


#define VME_MAP_WINDOW_SIZE 0x180000000

//globals
tsi148_regs_t *tsi;
tsipara params;
int verbose, utype, parfile, btype = -1, bridge_occn = -1;
char parfile_name[MAXPATHLEN];
char *tsip, captxtbuf[50];
u_long tsibase;
unsigned long long pci_addr_base = 0, pci_addr_offset = 0;

int open_tsi(void);
int close_tsi(void);
int set_slave_map(slavemap smap);
int set_master_map(mastermap mmap);
int set_fair(u_int fair);
int set_high_map(u_int64_t hm_base, u_int64_t hm_size, u_int bridge);
int set_req_lvl(u_int level);
int set_rel_mode(u_int mode);
int set_vme_to(u_int to);
int set_arb_to(u_int to);
int set_arb_mode(u_int mode);
int set_irq(u_int mask);
int set_swap(u_int ms, u_int ss, u_int fs);
int full_initialisation(u_int mode);
int get_high_map(u_int64_t *hm_base, u_int64_t *hm_size, u_int *bridge);
int get_slave_map();
int get_master_map();
int get_fair(u_int *fair);
int get_req_lvl(u_int *level);
int get_rel_mode(u_int *mode);
int get_vme_to(u_int *to);
int get_arb_to(u_int *to);
int get_arb_mode(u_int *mode); 
int get_irq(u_int *mask, u_int *irq_mode);
int get_swap(u_int *ms, u_int *ss, u_int *fs); 
int set_default_param(tsipara *pa);
int update_all(tsipara pa);
int dump_parameters(tsipara pa);
int exands();
int help();
int dettype();
int setdebug(void);
void dump_mmap_decoder(u_int nr, mastermap mmap);
void dump_smap_decoder(u_int nr, slavemap smap);
int pciaddr_finder_init(void);
unsigned long long pciaddr_finder_getaddr(u_int *size);
int pci_addr_finder_checkautomode(tsipara pa);


/*********************/
u_int bswap(u_int word)
/*********************/
{
  return ((word & 0xFF) << 24) + ((word & 0xFF00) << 8) + ((word & 0xFF0000) >> 8) + ((word & 0xFF000000) >> 24);
}


/*****************************/
int main(int argc,char *argv[])
/*****************************/
{
  u_int res, fun;
  int acti;

  if (argc != 3)
  {
    printf("Wrong number of parameters!\n");
    printf("Use: tsiconfig  -i <file name> to intercatively prepare the configuration file\n");
    printf("Use: tsiconfig  -a <file name> to upload the configuration file\n");
    exit(0);
  }
  else
    if ((acti = getopt(argc, argv, "aid")) == -1)
    {
      printf("Invalid option\n");
      exit(-1);
    }
 
  if (sscanf(argv[2], "%s", parfile_name) != 1)
  {
    printf("Cannot read the file name!\n");
    exit(-1);
  }

  if (acti == 'a' || acti == 'd')
  {
    if (acti == 'd')
    {
      DF::GlobalDebugSettings::setup(20, 0);
      verbose = 1;
    }
    else
    {
      DF::GlobalDebugSettings::setup(5, DFDB_VMERCC);
      verbose = 0;
    }
    
    parfile = open(parfile_name, O_RDONLY, 0666);
    open_tsi();
    dettype();
    res = read(parfile, &params, sizeof(tsipara));
    if (res != sizeof(tsipara))
    {
      printf("The file '%s' seems to be new or corrupted.\n", parfile_name);
      close_tsi();
      exit(-1);
    }
    update_all(params);
    printf("tsiconfig: VMEbus interface initialised\n");
    close_tsi();
    exit(0);
  }
  else
    parfile = open(parfile_name, O_RDWR | O_CREAT, 0666);

  if (parfile == 0) 
  {
    printf("Cannot open parameter file\n");
    exit(-1);
  }

  open_tsi();
  dettype();

  //read parameters from file
  res = read(parfile, &params, sizeof(tsipara));
  if (res != sizeof(tsipara))
  {  //failed to read parameters -> file new or currupted
    set_default_param(&params);
    printf("The file '%s' seems to be new or corrupted. \n", parfile_name);
    printf("Select '0' to quit if you do not want to modify it.\n");
  }

  fun = 1;

  printf("==========================================================================================\n");
  printf("IMPORTANT: If you are using PCI addresses above 0xffffffff and manual mode for your master\n");
  printf("           mappings, or if you are switching to automatic mode from an older file,\n");
  printf("           you have to manually register the PCI map window base address with option 12.\n");
  printf("==========================================================================================\n");
  printf("\n");

  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                             2 Dump all parameters\n");
    printf("   3 Set slave map decoder            4 Set master map decoder\n");
    printf("   5 Set VMEbus request level         6 Set VMEbus release mode\n");
    printf("   7 Set VMEbus time-out              8 Set Arbitration time-out\n");
    printf("   9 Set arbitration mode            10 Set FAIR arbitration mode\n");
    printf("  11 Set interrupt mask              12 Set non prefetchable PCI window\n");
    printf("  13 Set byte swapping\n");
    printf("  15 Set default parameters          16 Update Tsi148 registers\n");
    printf("  17 Exit and save                   18 Set debug parameters\n");
    printf("   0 Quit\n");                   
    printf("Your choice: ");                 
    fun = getdecd(fun);
    if (fun == 1) help();
    if (fun == 2) dump_parameters(params);
    if (fun == 3) get_slave_map();
    if (fun == 4) get_master_map();
    if (fun == 5) get_req_lvl(&params.reqlvl);  
    if (fun == 6) get_rel_mode(&params.relmode);
    if (fun == 7) get_vme_to(&params.vmeto);
    if (fun == 8) get_arb_to(&params.arbto);
    if (fun == 9) get_arb_mode(&params.arbmode);
    if (fun == 10) get_fair(&params.fair);
    if (fun == 11) get_irq(&params.irqs, &params.irq_mode[0]);
    if (fun == 12) get_high_map(&params.high_map_base, &params.high_map_size, &params.high_map_bridge);
    if (fun == 13) get_swap(&params.ms,&params.ss, &params.fs);
    if (fun == 15) set_default_param(&params);
    if (fun == 16) update_all(params);
    if (fun == 17) exands();
    if (fun == 18) setdebug();
  }
  close_tsi();
  res = close(parfile);
  if(res)
    printf("Parameter file closed with error\n");
  exit(0);
}


/****************/
int setdebug(void)
/****************/
{
  static u_int dblevel = 0, dbpackage = DFDB_VMERCC;
  
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/***********/
int dettype()
/***********/
{
  int id, i, found = 0;
  u_int ret;
  u_long virtaddr;
  u_char *id_ptr;

  id = tsi->noswap_devi_veni;  
  if (id != 0x014810e3)
  {
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    printf("ERROR: This is not a Tsi148 based SBC\n");
    printf("Chip ID = 0x%08x\n", id);
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    exit(-1);
  }
  
  id = tsi->noswap_clas_revi & 0xff;        //mask Revision ID   
  if (id == 1)
  {
    utype = 1;                              //Tsi I
  }
  else
  {
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    printf("WARNING: The revision of the Tsi148 chip is unknown\n");
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    utype = 1;
  }
    
  ret = IO_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
 
  ret = IO_PCIMemMap(0xe0000, 0x20000, &virtaddr);
  if (ret != IO_RCC_SUCCESS)
    rcc_error_print(stdout, ret);

  id_ptr = (u_char *)virtaddr;

  for (i = 0; (i + 7) < 0x20000; i++)
  {
    if ((id_ptr[0] == '$') && (id_ptr[1] == 'S') && (id_ptr[2] == 'H') && (id_ptr[3] == 'A') && (id_ptr[4] == 'A') && (id_ptr[5] == 'N') && (id_ptr[6] == 'T') && (id_ptr[7] == 'I'))
    {
      found = 1;
      if (id_ptr[8] == 1 && id_ptr[9] == 2 && id_ptr[10] == 28 && id_ptr[11] == 0) 
      {
        btype = 1;   //VP717
	printf("Board type: VP717\n");
      }
      if (id_ptr[8] == 1 && id_ptr[9] == 2 && id_ptr[10] == 31 && id_ptr[11] == 0) 
      {
        btype = 2;   //VP917
	printf("Board type: VP917\n");
      }
      if (id_ptr[8] == 2 && id_ptr[9] == 2 && id_ptr[10] == 31 && id_ptr[11] == 0) 
      {
        btype = 2;   //VP917
	printf("Board type: VP917\n");
      }
    }
    if (found)
      break;
    id_ptr++;
  }
  
  if (btype != 1 && btype != 2)
  {
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    printf("ERROR: This is neither a VP717 nor a VP917\n");
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    exit(-1);
  }
  
  
  ret = IO_Close();
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-5);
  }
  
  return(0);
}


/********/
int help()
/********/
{
  printf("********************************************************************************\n");
  printf("Background:\n");
  printf("Before any single cycle access to VMEbus can be made it is necessary to program a map\n"); 
  printf("decoder of the Tsi148 chip with appropriate parameters. The generation of block\n");
  printf("transfers is independent from the map decoders. Most drivers for the Tsi148 chip allow\n");
  printf("to dynamically program a map decoder from user programs my means of a function call. We\n");
  printf("have decided not to do it that way but to provide a special application (this one) for\n"); 
  printf("that purpose. The master and slave mapping functions of the vme_rcc library only check\n");
  printf("if a mapping with the requested parameters is supported by the static set-up of the\n");
  printf("Tsi148 chip and return the corresponding addresses or an error message. The main reason\n"); 
  printf("for this approach is that the Tsi148 has only 8 map decoders. In systems with more\n");
  printf("than 8 slaves it is necessary to group slaves and program the map decoders accordingly.\n");
  printf("Functions for dynamic mappings bear the risk of run time errors when all map decoders\n"); 
  printf("are used. With tsiconfig the users has to lay out the VMEbus address space before the\n"); 
  printf("application can be started and is therefore guaranteed not to get run time\n");
  printf("errors.\n");
  
  printf("Currently tsiconfig requires the user to enter a PCI address range for each master\n");
  printf("mapping. It is important that these ranges do neither overlap nor conflict with address\n"); 
  printf("ranges used by PCI devices or other kernel resources (check with: more /proc/iomem)\n");
      
  printf("Modes of operation:\n");
  printf("Tsiconfig can be used in two ways:\n");
  printf("1) tsiconfig -i <parfile>\n");
  printf("This is the interactive mode. If <parfile> contains a valid set of parameters\n");
  printf("initialised by a privious session these will be loaded into memory. Otherwise\n");
  printf("default parameters will be used. During the session, all modifications will be\n");
  printf("made in memory.  Only if the save and exit function is called will they be\n");
  printf("copied to the file. The data file is not human readable.\n");
  printf("\n");
  printf("2) tsiconfig -a <parfile>\n");
  printf("This is the automatic mode. <parfile> has to be a valid parameter file.\n");
  printf("The parameters from <parfile> will be loaded iautomatically into to registers\n");
  printf("of the Tsi148 chip. Use tsiconfig this way during system boot.\n");
  printf("\n");
  printf("\n");
  printf("Description of the menu points (interactive mode only):\n");
  printf("1)\n");
  printf("Display this text.\n");
  printf("\n");
  printf("2)\n");
  printf("This function dumps the contents of the parameter structure.\n");
  printf("\n");
  printf("3..13)\n");
  printf("These functions allow to specify parameters for the Tsi148 registers. The \n");
  printf("values entered will only be copied into a structure in memory. The set-up of\n");
  printf("the VMEbus interface will not be updated.\n");
  printf("\n");
  printf("15)\n");
  printf("This function allows to delete all setings and to initialise the parameter\n");
  printf("structure with default values.\n");
  printf("\n");
  printf("16)\n");
  printf("Once all parameters have been defined with the functions 3..13 this function\n");
  printf("has to be called to actually program the registers of the VMEbus interface.\n");
  printf("\n");
  printf("17)\n");
  printf("As the name says: copy the parameter structure to the data file and exit.\n");
  printf("\n");
  printf("18)\n");
  printf("For experts only. Enable debugging messages from the vme_rcc library\n");
  printf("\n");
  printf("0)\n");
  printf("Quits the program without saving back the parameters to the file.\n");
  printf("\n");
  printf("********************************************************************************\n");
  return(0);
}


/**********************************************/
void dump_mmap_decoder(u_int nr, mastermap mmap)
/**********************************************/
{
  printf("Master map decoder %d Decoder number  = %d\n", nr, mmap.decoder);
  printf("Master map decoder %d Enabled         = %s\n", nr, mmap.enable?"Yes":"No");
  printf("Master map decoder %d VME base address= 0x%016llx\n", nr, (unsigned long long)mmap.vbase);

  printf("Master map decoder %d PCI base address= ", nr);
  if (!mmap.pbase && mmap.enable) printf("[AUTO]\n"); else printf("0x%16llx\n", (unsigned long long)mmap.pbase);

  printf("Master map decoder %d Window size     = 0x%08x\n", nr, mmap.size);
  printf("Master map decoder %d VME space       = ", nr);
  if (mmap.vmespace == 0) printf("A16\n");
  if (mmap.vmespace == 1) printf("A24\n");
  if (mmap.vmespace == 2) printf("A32\n"); 
  if (mmap.vmespace == 5) printf("CR/CSR\n"); 
  if (mmap.vmespace == 8) printf("USER1\n"); 
  if (mmap.vmespace == 9) printf("USER2\n"); 
  if (mmap.vmespace == 10) printf("USER3\n"); 
  if (mmap.vmespace == 11) printf("USER4\n"); 
  printf("Master map decoder %d AM code         = ", nr);
  if (mmap.supervisor == 1 && mmap.program == 1) printf("Supervisor / Program\n\n");
  if (mmap.supervisor == 0 && mmap.program == 1) printf("User / Program\n\n");
  if (mmap.supervisor == 1 && mmap.program == 0) printf("Supervisor / Data\n\n");
  if (mmap.supervisor == 0 && mmap.program == 0) printf("User / Data\n\n");
}


/*********************************************/
void dump_smap_decoder(u_int nr, slavemap smap)
/*********************************************/
{
  printf("Slave map decoder %d Decoder number  = %d\n", nr, smap.decoder);
  printf("Slave map decoder %d Enabled         = %s\n", nr, smap.enable?"Yes":"No");
  printf("Slave map decoder %d VME base address= 0x%016llx\n", nr, (unsigned long long)smap.vbase);
  printf("Slave map decoder %d PCI base address= 0x%016llx\n", nr, (unsigned long long)smap.pbase);
  printf("Slave map decoder %d Window size     = 0x%08x\n", nr, smap.size);
  printf("Slave map decoder %d VME space       = ", nr);
  if (smap.vmespace == 0) printf("A16\n");
  if (smap.vmespace == 1) printf("A24\n");
  if (smap.vmespace == 2) printf("A32\n");
}


/*****************************/
int dump_parameters(tsipara pa)
/*****************************/
{
  int res = pci_addr_finder_checkautomode(pa);

  if(res < -1) 
    printf("%d MANUALLY configured master map(s) enabled.\n\n", -(res + 1));
  else if (res == -1) 
    printf("No master maps enabled.\n\n");
  else if (res == 0) 
    printf("Warning, invalid configuration! Mixed auto and manual PCI addressing modes.\n\n");
  else
    printf("%d automatically configured master map(s) enabled.\n\n", res);

  printf("High map base                        = ");
  if (pa.high_map_base)
    printf("0x%016lx\n", pa.high_map_base);
  else
    printf("[AUTO]\n");

  printf("High map size (bytes)                = 0x%016lx%s\n", pa.high_map_size, pa.high_map_base ? "" : " [AUTO]");
  printf("High map bridge                      = ");

  if (pa.high_map_base)
    printf("%d\n", pa.high_map_bridge);
  else
    printf("[AUTO]\n\n");

  dump_mmap_decoder(0, pa.mmap0);
  dump_mmap_decoder(1, pa.mmap1);
  dump_mmap_decoder(2, pa.mmap2);
  dump_mmap_decoder(3, pa.mmap3);
  dump_mmap_decoder(4, pa.mmap4);
  dump_mmap_decoder(5, pa.mmap5);
  dump_mmap_decoder(6, pa.mmap6);
  dump_mmap_decoder(7, pa.mmap7);
  
  dump_smap_decoder(0, pa.smap0);
  dump_smap_decoder(1, pa.smap1);
  dump_smap_decoder(2, pa.smap2);
  dump_smap_decoder(3, pa.smap3);
  dump_smap_decoder(4, pa.smap4);
  dump_smap_decoder(5, pa.smap5);
  dump_smap_decoder(6, pa.smap6);
  dump_smap_decoder(7, pa.smap7);

  printf("\nVMEbus request level          = %d\n", pa.reqlvl);
  printf("Fair arbitration              = %s\n", pa.fair?"Yes":"No");
  printf("VMEbus release mode           = %d\n", pa.relmode);
 
  printf("VMEbus time out               = ");
  if (pa.vmeto == 0) printf("8 us\n");
  if (pa.vmeto == 1) printf("16 us\n");
  if (pa.vmeto == 2) printf("32 us\n");
  if (pa.vmeto == 3) printf("64 us\n");
  if (pa.vmeto == 4) printf("128 us\n");
  if (pa.vmeto == 5) printf("256 us\n");
  if (pa.vmeto == 6) printf("512 us\n");
  if (pa.vmeto == 7) printf("1024 us\n");
  if (pa.vmeto == 8) printf("2048 us\n");
  if (pa.vmeto == 15) printf("disabled\n");
  printf("VMEbus arbitration time out   = ");
  if (pa.arbto == 0) printf("Disabled\n");
  if (pa.arbto == 1) printf("16 us\n");
  printf("VMEbus arbitration mode       = %s\n", pa.arbmode?"Priority":"Round robin");
  printf("VMEbus interrupt level 1 is %s", (pa.irqs & 0x02)?"enabled":"masked");
  if (pa.irqs & 0x02)
  {
    printf("  Mode = ");
         if (pa.irq_mode[1] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[1] == VME_INT_ROAK) printf("ROAK\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");
      
  printf("VMEbus interrupt level 2 is %s", (pa.irqs & 0x04)?"enabled":"masked");
  if (pa.irqs & 0x04)
  {
    printf("  Mode = ");
    if      (pa.irq_mode[2] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[2] == VME_INT_ROAK) printf("ROAK\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 3 is %s", (pa.irqs & 0x08)?"enabled":"masked");
  if (pa.irqs & 0x08)
  {
    printf("  Mode = ");
         if (pa.irq_mode[3] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[3] == VME_INT_ROAK) printf("ROAK\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 4 is %s", (pa.irqs & 0x10)?"enabled":"masked");
  if (pa.irqs & 0x10)
  {
    printf("  Mode = ");
         if (pa.irq_mode[4] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[4] == VME_INT_ROAK) printf("ROAK\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 5 is %s", (pa.irqs & 0x20)?"enabled":"masked");
  if (pa.irqs & 0x20)
  {
    printf("  Mode = ");
         if (pa.irq_mode[5] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[5] == VME_INT_ROAK) printf("ROAK\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 6 is %s", (pa.irqs & 0x40)?"enabled":"masked");
  if (pa.irqs & 0x40)
  {
    printf("  Mode = ");
         if (pa.irq_mode[6] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[6] == VME_INT_ROAK) printf("ROAK\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 7 is %s", (pa.irqs & 0x80)?"enabled":"masked");
  if (pa.irqs & 0x80)
  {
    printf("  Mode = ");
         if (pa.irq_mode[7] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[7] == VME_INT_ROAK) printf("ROAK\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");
    
  printf("SYSFAIL interrupt is %s", (pa.irqs & 0x200)?"enabled":"masked");
  if (pa.irqs & 0x200)
  {
    printf("  Mode = ");
         if (pa.irq_mode[8] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[8] == VME_INT_ROAK) printf("ROAK (ATTENTION: The SYSFAIL interrup should be of type RORA)\n");
    else                                     printf("Undefined\n");
  }
  else
    printf("\n");  

  printf("Master byte swapping          = %s\n", pa.ms?"enabled":"disabled");
  printf("Slave byte swapping           = %s\n", pa.ss?"enabled":"disabled");
  printf("Fast byte swapping            = %s\n", pa.fs?"enabled":"disabled");

  return(0);
}


/**********/
int exands()
/**********/
{
  int res = pci_addr_finder_checkautomode(params);

  printf("Saving configuration.\n");
  if (res < -1) 
    printf("Notice: %d MANUALLY configured master map(s) enabled.\n", -(res + 1));
  else if (res == -1) 
    printf("Notice: No master maps enabled.\n");
  else if (res == 0) 
    printf("Error, invalid configuration! Mixed auto and manual PCI addressing modes.\n");
  else
    printf("Notice: %d automatically configured master map(s) enabled.\n", res);

  res = lseek(parfile , 0 , 0);
  if (res)
    printf("Cannot set file pointer to beginning of file\n");

  res = write(parfile, &params, sizeof(tsipara));
  if (res != sizeof(tsipara))
    printf("Cannot write to file\n");

  res = close(parfile);
  if (res)
    printf("Parameter file closed with error\n");

  close_tsi();
  exit(0);
}


/********************************/
int set_default_param(tsipara *pa)
/********************************/
{
  pa->mmap0.vbase      = 0;
  pa->mmap0.pbase      = 0;
  pa->mmap0.size       = 0; 
  pa->mmap0.vmespace   = 2;
  pa->mmap0.enable     = 0; 
  pa->mmap0.decoder    = 0;
  pa->mmap0.supervisor = 0;
  pa->mmap0.program    = 0;
 
  pa->mmap1.vbase      = 0;
  pa->mmap1.pbase      = 0;
  pa->mmap1.size       = 0; 
  pa->mmap1.vmespace   = 2; 
  pa->mmap1.enable     = 0;  
  pa->mmap1.decoder    = 1; 
  pa->mmap1.supervisor = 0;
  pa->mmap1.program    = 0;
  
  pa->mmap2.vbase      = 0;
  pa->mmap2.pbase      = 0;
  pa->mmap2.size       = 0; 
  pa->mmap2.vmespace   = 2; 
  pa->mmap2.enable     = 0;  
  pa->mmap2.decoder    = 2; 
  pa->mmap2.supervisor = 0;
  pa->mmap2.program    = 0;
  
  pa->mmap3.vbase      = 0;
  pa->mmap3.pbase      = 0;
  pa->mmap3.size       = 0; 
  pa->mmap3.vmespace   = 2; 
  pa->mmap3.enable     = 0;  
  pa->mmap3.decoder    = 3; 
  pa->mmap3.supervisor = 0;
  pa->mmap3.program    = 0;
  
  pa->mmap4.vbase      = 0;
  pa->mmap4.pbase      = 0;
  pa->mmap4.size       = 0;    
  pa->mmap4.vmespace   = 2;
  pa->mmap4.enable     = 0;  
  pa->mmap4.decoder    = 4;
  pa->mmap4.supervisor = 0;
  pa->mmap4.program    = 0;

  pa->mmap5.vbase      = 0;
  pa->mmap5.pbase      = 0;
  pa->mmap5.size       = 0;
  pa->mmap5.vmespace   = 2;
  pa->mmap5.enable     = 0;
  pa->mmap5.decoder    = 5;
  pa->mmap5.supervisor = 0;
  pa->mmap5.program    = 0;

  pa->mmap6.vbase      = 0;
  pa->mmap6.pbase      = 0;
  pa->mmap6.size       = 0;
  pa->mmap6.vmespace   = 2;
  pa->mmap6.enable     = 0;
  pa->mmap6.decoder    = 6;
  pa->mmap6.supervisor = 0;
  pa->mmap6.program    = 0;

  pa->mmap7.vbase      = 0;
  pa->mmap7.pbase      = 0;
  pa->mmap7.size       = 0;
  pa->mmap7.vmespace   = 2;
  pa->mmap7.enable     = 0;
  pa->mmap7.decoder    = 7;
  pa->mmap7.supervisor = 0;
  pa->mmap7.program    = 0;

  pa->smap0.vbase     = 0;
  pa->smap0.pbase     = 0;
  pa->smap0.size      = 0;
  pa->smap0.vmespace  = 2;
  pa->smap0.enable    = 0;
  pa->smap0.decoder   = 0;

  pa->smap1.vbase     = 0;
  pa->smap1.pbase     = 0;
  pa->smap1.size      = 0; 
  pa->smap1.vmespace  = 2; 
  pa->smap1.enable    = 0;   
  pa->smap1.decoder   = 1; 

  pa->smap2.vbase     = 0;
  pa->smap2.pbase     = 0;
  pa->smap2.size      = 0; 
  pa->smap2.vmespace  = 2; 
  pa->smap2.enable    = 0;   
  pa->smap2.decoder   = 2; 

  pa->smap3.vbase     = 0;
  pa->smap3.pbase     = 0;
  pa->smap3.size      = 0; 
  pa->smap3.vmespace  = 2; 
  pa->smap3.enable    = 0;   
  pa->smap3.decoder   = 3; 

  pa->smap4.vbase     = 0;
  pa->smap4.pbase     = 0;
  pa->smap4.size      = 0;    
  pa->smap4.vmespace  = 2; 
  pa->smap4.enable    = 0;   
  pa->smap4.decoder   = 4; 

  pa->smap5.vbase     = 0;
  pa->smap5.pbase     = 0;
  pa->smap5.size      = 0; 
  pa->smap5.vmespace  = 2;
  pa->smap5.enable    = 0;   
  pa->smap5.decoder   = 5;  

  pa->smap6.vbase     = 0;
  pa->smap6.pbase     = 0;
  pa->smap6.size      = 0; 
  pa->smap6.vmespace  = 2;
  pa->smap6.enable    = 0;   
  pa->smap6.decoder   = 6;  

  pa->smap7.vbase     = 0;
  pa->smap7.pbase     = 0;
  pa->smap7.size      = 0; 
  pa->smap7.vmespace  = 2;
  pa->smap7.enable    = 0;   
  pa->smap7.decoder   = 7;  

  pa->reqlvl      = 3;
  pa->fair        = 0;
  pa->relmode     = 0;
  pa->vmeto       = 8;
  pa->arbto       = 0;
  pa->arbmode     = 1;
  pa->full_init   = 1; //Initialise also registers like pci latency counter or interrupts
  pa->irqs        = 0;
  pa->irq_mode[0] = 1; //Valid
  pa->irq_mode[1] = VME_LEVELISDISABLED;
  pa->irq_mode[2] = VME_LEVELISDISABLED;
  pa->irq_mode[3] = VME_LEVELISDISABLED;
  pa->irq_mode[4] = VME_LEVELISDISABLED;
  pa->irq_mode[5] = VME_LEVELISDISABLED;
  pa->irq_mode[6] = VME_LEVELISDISABLED;
  pa->irq_mode[7] = VME_LEVELISDISABLED;
  pa->irq_mode[8] = VME_LEVELISDISABLED;
  pa->ms          = 1;
  pa->ss          = 0;
  pa->fs          = 0;
  
  pa->high_map_base   = 0;
  pa->high_map_size   = VME_MAP_WINDOW_SIZE;
  pa->high_map_bridge = 1;

  return(0);
}


/************************/
int update_all(tsipara pa)
/************************/
{
  int ret, all_ok = 1;
  u_int status;

  ret = pci_addr_finder_checkautomode(pa);
  
  if (ret < -1) 
    printf("Notice: Enabling MANUAL mode, as %d MANUALLY configured master map(s) were found.\n", -(ret + 1));
  else if (ret == -1) 
    printf("Notice: No master maps enabled.\n");
  else if (ret == 0) 
  {
    printf("Invalid configuration! For automatic mode to work, all PCI addresses have to be set to zero!\n");
    printf("Hint: To use manual mode, disable all unused master maps and set the PCI addresses to a non-zero value.\n");
    exit(-1);
  } 
  else if ((!bridge_occn && !pa.high_map_base) || !pci_addr_base)
  {
    printf("Notice: Enabling automatic mode, as %d automatically configured master map(s) were found.\n", ret);
    if (pciaddr_finder_init())
      exit(-1);
  } 
  else 
  {
    printf("Oops.");
    exit(-1);
  }
 
  printf("update_all called\n");
  ret = set_high_map(pa.high_map_base ? pa.high_map_base : pci_addr_base, pa.high_map_size, pa.high_map_bridge ? pa.high_map_bridge : bridge_occn);
  if (ret != 0) 
  {
    printf("update_all(0) returns %d\n", ret);
    return(-1);
  }
  
  if(!pa.mmap0.pbase && pa.mmap0.enable) { if(!(pa.mmap0.pbase = pciaddr_finder_getaddr(&(pa.mmap0.size)))) exit(-1); }
  if(!pa.mmap1.pbase && pa.mmap1.enable) { if(!(pa.mmap1.pbase = pciaddr_finder_getaddr(&(pa.mmap1.size)))) exit(-1); }
  if(!pa.mmap2.pbase && pa.mmap2.enable) { if(!(pa.mmap2.pbase = pciaddr_finder_getaddr(&(pa.mmap2.size)))) exit(-1); }
  if(!pa.mmap3.pbase && pa.mmap3.enable) { if(!(pa.mmap3.pbase = pciaddr_finder_getaddr(&(pa.mmap3.size)))) exit(-1); }
  if(!pa.mmap4.pbase && pa.mmap4.enable) { if(!(pa.mmap4.pbase = pciaddr_finder_getaddr(&(pa.mmap4.size)))) exit(-1); }
  if(!pa.mmap5.pbase && pa.mmap5.enable) { if(!(pa.mmap5.pbase = pciaddr_finder_getaddr(&(pa.mmap5.size)))) exit(-1); }
  if(!pa.mmap6.pbase && pa.mmap6.enable) { if(!(pa.mmap6.pbase = pciaddr_finder_getaddr(&(pa.mmap6.size)))) exit(-1); }
  if(!pa.mmap7.pbase && pa.mmap7.enable) { if(!(pa.mmap7.pbase = pciaddr_finder_getaddr(&(pa.mmap7.size)))) exit(-1); }

  ret = set_master_map(pa.mmap0);
  if (ret != 0) { all_ok = 0; printf("update_all(1) returns %d\n", ret); }
  ret = set_master_map(pa.mmap1);
  if (ret != 0) { all_ok = 0; printf("update_all(2) returns %d\n", ret); }
  ret = set_master_map(pa.mmap2);
  if (ret != 0) { all_ok = 0; printf("update_all(3) returns %d\n", ret); }
  ret = set_master_map(pa.mmap3);
  if (ret != 0) { all_ok = 0; printf("update_all(4) returns %d\n", ret); }
  ret = set_master_map(pa.mmap4);
  if (ret != 0) { all_ok = 0; printf("update_all(15) returns %d\n", ret); }
  ret = set_master_map(pa.mmap5);
  if (ret != 0) { all_ok = 0; printf("update_all(16) returns %d\n", ret); }
  ret = set_master_map(pa.mmap6);
  if (ret != 0) { all_ok = 0; printf("update_all(17) returns %d\n", ret); }
  ret = set_master_map(pa.mmap7);
  if (ret != 0) { all_ok = 0; printf("update_all(18) returns %d\n", ret); }

  ret = set_slave_map(pa.smap0);
  if (ret != 0) { all_ok = 0; printf("update_all(5) returns %d\n", ret); }
  ret = set_slave_map(pa.smap1);
  if (ret != 0) { all_ok = 0; printf("update_all(6) returns %d\n", ret); }
  ret = set_slave_map(pa.smap2);
  if (ret != 0) { all_ok = 0; printf("update_all(7) returns %d\n", ret); }
  ret = set_slave_map(pa.smap3);
  if (ret != 0) { all_ok = 0; printf("update_all(8) returns %d\n", ret); }
  ret = set_slave_map(pa.smap4);
  if (ret != 0) { all_ok = 0; printf("update_all(19) returns %d\n", ret); }
  ret = set_slave_map(pa.smap5);
  if (ret != 0) { all_ok = 0; printf("update_all(20) returns %d\n", ret); }
  ret = set_slave_map(pa.smap6);
  if (ret != 0) { all_ok = 0; printf("update_all(21) returns %d\n", ret); }
  ret = set_slave_map(pa.smap7);
  if (ret != 0) { all_ok = 0; printf("update_all(22) returns %d\n", ret); }

  ret = set_fair(pa.fair);
  if (ret != 0) { all_ok = 0; printf("update_all(9) returns %d\n", ret); }
  ret = set_req_lvl(pa.reqlvl);
  if (ret != 0) { all_ok = 0; printf("update_all(10) returns %d\n", ret); }
  ret = set_rel_mode(pa.relmode);
  if (ret != 0) { all_ok = 0; printf("update_all(11) returns %d\n", ret); }
  ret = set_vme_to(pa.vmeto);
  if (ret != 0) { all_ok = 0; printf("update_all(12) returns %d\n", ret); }
  ret = set_arb_to(pa.arbto);
  if (ret != 0) { all_ok = 0; printf("update_all(13) returns %d\n", ret); }
  ret = set_arb_mode(pa.arbmode); 
  if (ret != 0) { all_ok = 0; printf("update_all(14) returns %d\n", ret); }
  ret = full_initialisation(pa.full_init);
  if (ret != 0) { all_ok = 0; printf("update_all(15) returns %d\n", ret); }
  ret = set_irq(pa.irqs);
  if (ret != 0) { all_ok = 0; printf("update_all(16) returns %d\n", ret); }
  ret = set_swap(pa.ms, pa.ss, pa.fs);
  if (ret != 0) { all_ok = 0; printf("update_all(17) returns %d\n", ret); }

  if(!all_ok) 
  {
    printf("Your configuration file contains errors. The Tsi148 chip is not properly programmed!\n");  
    exit(-1);
  } 
  else
    printf("Tsi148 registers programmed\n");

  status = VME_Update(&pa.irq_mode[0]);
  if (status)
  {
    VME_ErrorPrint(status);
    exit(-1);
  }  

  printf("VME_RCC driver synchronized\n");
  return(0);
}


/*****************/
int get_slave_map()
/*****************/
{
  static int decoder = 0;
  slavemap *smap;

  printf("Enter number of map decoder        <0..7> ");
  decoder = getdecd(decoder);
  if (decoder > 7) decoder = 7;
  if (decoder < 0) decoder = 0;

  if (decoder == 0) smap = &params.smap0;
  if (decoder == 1) smap = &params.smap1;
  if (decoder == 2) smap = &params.smap2;
  if (decoder == 3) smap = &params.smap3;
  if (decoder == 4) smap = &params.smap4;
  if (decoder == 5) smap = &params.smap5;
  if (decoder == 6) smap = &params.smap6;
  if (decoder == 7) smap = &params.smap7;

  smap->decoder = decoder;
  printf("Enable map decoder            <0=no 1=yes> ");
  smap->enable = getdecd((int)smap->enable);
  printf("smap->enable is %d\n", smap->enable);

  if (smap->enable)
  {
    printf("Select VMEbus base address                   ");
    smap->vbase = gethexdll(smap->vbase);

    printf("Select PCI base address                      ");
    smap->pbase = gethexdll(smap->pbase);

    printf("Select the window size (bytes)               ");
    smap->size = gethexd(smap->size);

    printf("Select address space <0=A16, 1=A24, 2=A32>   ");
    smap->vmespace = getdecd((int)smap->vmespace);
  }
  return(0);
}
 
 
/******************/
int get_master_map()
/******************/
{
  static int decoder = 0;
  int res;
  mastermap *mmap;

  printf("Enter number of map decoder        <0..7> ");
  decoder = getdecd(decoder);
  if (decoder > 7) decoder = 7;
  if (decoder < 0) decoder = 0;

  if (decoder == 0) mmap = &params.mmap0;
  if (decoder == 1) mmap = &params.mmap1;
  if (decoder == 2) mmap = &params.mmap2;
  if (decoder == 3) mmap = &params.mmap3;
  if (decoder == 4) mmap = &params.mmap4;
  if (decoder == 5) mmap = &params.mmap5;
  if (decoder == 6) mmap = &params.mmap6;
  if (decoder == 7) mmap = &params.mmap7;

  res = pci_addr_finder_checkautomode(params);

  mmap->decoder = decoder;
  printf("Enable map decoder            <0=no 1=yes> ");
  mmap->enable = getdecd((int)mmap->enable);
 
  if (mmap->enable)
  {
    printf("Select VMEbus base address                          ");
    mmap->vbase = gethexdll(mmap->vbase);

    if(res < 1) 
    {
      if(res == -1) 
      {
	printf("NEW: vmeconfig is now able to allocate PCI addresses for you\n");
	printf("If you want to control the PCI<->VME mapping yourself you just have\n");
	printf("to enter the PCI address for the master mapping below as you did in\n");
	printf("the past.\n");
	printf("If you want to use the new automatic allocation you have to enter a\n");
	printf("PCI base address of \"0\" for all master map decoders.\n");
	printf("The PCI addresses will be allocated automatically when the <vmetab>\n");
	printf("file gets loaded into the H/W at boot time. You can check the result\n");
	printf("with function 2/2 of tsiscope.\n");
	printf("In case of an error during the address allocation\n");
	printf("the file /proc/vme_rcc will tell you that the Tsi148 chip has not\n");
	printf("been initialized. The error messages can be found in the boot log\n");
	printf("file.\n");
      }
      printf("Select PCI base address                             ");
      mmap->pbase = gethexdll(mmap->pbase);
    }
    
    printf("Select the window size (bytes)                      ");
    mmap->size = gethexd(mmap->size);

    printf("Select address space\n");
    printf("  <0=A16, 1=A24, 2=A32, 5=CR/CSR, 8=USER1, 9=USER2, 10=USER3, 11=USER4> ");
    mmap->vmespace = getdecd((int)mmap->vmespace);

    printf("Select cycle type            <0=User, 1=Supervisor> ");
    mmap->supervisor = getdecd((int)mmap->supervisor);

    printf("Select cycle type               <0=Data, 1=Program> ");
    mmap->program = getdecd((int)mmap->program);    
  }
  return(0);
} 


/*****************************************/
int get_swap(u_int *ms,u_int *ss,u_int *fs)
/*****************************************/
{
  printf("Enable master byte swapping <0=no 1=yes> "); 
  *ms = getdecd((int)*ms);
  printf("Enable slave byte swapping  <0=no 1=yes> "); 
  *ss = getdecd((int)*ss);
  printf("Enable fast byte swapping   <0=no 1=yes> "); 
  *fs = getdecd((int)*fs);

  return(0);
}


/*********************************************************************/ 
int get_high_map(u_int64_t *hm_base, u_int64_t *hm_size, u_int *bridge)
/*********************************************************************/ 
{

  printf("The TSI148 is able to use PCI addresses above the 4GB limit for VMEbus mappings.\n");
  printf("First you have to find a free address range. Check ""dmesg"" and look for ""E820""\n");
  printf("Select a free address range above 0x1.0000.0000 and check with ""more /proc/iomem"" \n");
  printf("and ""lspci -vvxxx"" if this range is not used by any other device.\n\n");
  printf("NEW: You can let the program figure this out for you by entering a PCI base address of\n");
  printf("     zero.\n\n");

  printf("Enter the PCI base address of the mapping window: "); 
  *hm_base = gethexdll(*hm_base);

  if(*hm_base) 
  {
    printf("Enter the size of the mapping window: "); 
    *hm_size = gethexdll(*hm_size);

    printf("\nIn order to allow high PCI addresses to be used a PCI bridge has to be programmed.\n");
    printf("tsiconfig will take care of that but needs to know which bridge to program.\n");
    printf("Use lspci to look for devices with DID/VID = 12d8:e130 and locate the one in front of the Tsi148.\n");
    printf("Enter the occurrence number (1,2,3) of that bridge.\n");
    printf("If no PMCs are installed just enter 1\n\n");

    printf("Enter the bridge occurrence number: "); 
    *bridge = getdecd(*bridge);
  } 
  else 
  {
    *hm_size = VME_MAP_WINDOW_SIZE;
    *bridge = 0;
    printf("Set to automatic configuration!\n");
  }

  return(0);
}


/***********************/ 
int get_fair(u_int *fair)
/***********************/
{
  printf("Select fair arbitration <0=no 1=yes> "); 
  *fair = getdecd((int)*fair);
  return(0);
}


/***************************/ 
int get_req_lvl(u_int *level)
/***************************/ 
{
  printf("Select bus request level <0..3> ");
  *level = getdecd((int)*level);
  return(0);
}


/***************************/ 
int get_rel_mode(u_int *mode)
/***************************/ 
{
  printf("Select release mode <0=RWD, 1, 2 or 3> ");
  *mode = getdecd((int)*mode);
  return(0);
}


/***********************/ 
int get_vme_to(u_int *to)
/***********************/ 
{
  printf("Select VMEbus time-out\n");
  printf("<0=8us 1=16us 2=32us 3=64us 4=128us 5=256us 6=512us 7=1024us 8-2048us 15=disabled> ");
  *to = getdecd((int)*to);
  return(0);
}


/***********************/ 
int get_arb_to(u_int *to)
/***********************/ 
{
  printf("Select arbitration time-out <0=disabled 1=16us> ");
  *to = getdecd((int)*to);
  return(0);
}


/***************************/ 
int get_arb_mode(u_int *mode)
/***************************/ 
{
  printf("Select release mode <1=Round Robin 0=Priority> ");
  *mode = getdecd((int)*mode);
  return(0);
} 


/***************************************/ 
int get_irq(u_int *mask, u_int *irq_mode)
/***************************************/ 
{
  u_int value;

  printf("Enable interrupts on level 1 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 1) & 0x01);
  if (value) *mask |= 0x02; else *mask &= 0xfffffffd;
  if (!value)
    irq_mode[1] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[1] = getdecd((int)irq_mode[1]);
  }  

  printf("Enable interrupts on level 2 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 2) & 0x1);
  if (value) *mask |= 0x04; else *mask &= 0xfffffffb;
  if (!value)
    irq_mode[2] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[2] = getdecd((int)irq_mode[2]);
  }
    
  printf("Enable interrupts on level 3 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 3) & 0x1);
  if (value) *mask |= 0x08; else *mask &= 0xfffffff7;
  if (!value)
    irq_mode[3] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[3] = getdecd((int)irq_mode[3]);
  }
    
  printf("Enable interrupts on level 4 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 4) & 0x1);
  if (value) *mask |= 0x10; else *mask &= 0xffffffef;
  if (!value)
    irq_mode[4] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[4] = getdecd((int)irq_mode[4]);
  }
    
  printf("Enable interrupts on level 5 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 5) & 0x1);
  if (value) *mask |= 0x20; else *mask &= 0xffffffdf;
  if (!value)
    irq_mode[5] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[5] = getdecd((int)irq_mode[5]);
  }
    
  printf("Enable interrupts on level 6 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 6) & 0x1);
  if (value) *mask |= 0x40; else *mask &= 0xffffffbf;
  if (!value)
    irq_mode[6] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[6] = getdecd((int)irq_mode[6]);
  }
    
  printf("Enable interrupts on level 7 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 7) & 0x1);
  if (value) *mask |= 0x80; else *mask &= 0xffffff7f;
  if (!value)
    irq_mode[7] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[7] = getdecd((int)irq_mode[7]);
  }

  printf("Enable SYSFAIL interrupt (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 9) & 0x1);
  if (value) *mask |= 0x200; else *mask &= 0xfffffdff;
  if (!value)
    irq_mode[8] = VME_LEVELISDISABLED;
  else
    irq_mode[8] = VME_INT_RORA;

  irq_mode[0] = 1; //Valid
  return(0);
}

/********************************/
int set_master_map(mastermap mmap)
/********************************/
{
  u_int ctld;
  u_int64_t pci_top, vmeoffset;
   
  /*check the input*/
  if (mmap.decoder > 7) return(-1);
  if (mmap.vmespace == 3 || mmap.vmespace == 6 || mmap.vmespace == 7 || mmap.vmespace > 11) return(-3);
  mmap.size &= 0xffff0000;  //All decoders have a 64kB granularity
  if (mmap.enable && mmap.size == 0)
  {
    mmap.size = 0x10000; 
    printf("Master map decoder %d: Size set to 0x10000\n", mmap.decoder);
  }

  ctld = (0x40 | (mmap.enable << 31) | mmap.vmespace | (mmap.supervisor << 5) | (mmap.program << 4));
  pci_top     = mmap.pbase + mmap.size - 1;  //If we want to mag e.g. 0x10000 bytes to PCI address 0xd0000000 the top address has to be 0xd00f0000. As the 16 low significant bits are "don't care" this is equivalent to 0xd00fffff
  vmeoffset   = mmap.vbase - mmap.pbase;
  
  if (!mmap.enable)
  {
    pci_top = 0;
    mmap.pbase = 0;
    vmeoffset = 0;
    ctld = 0;
  }
    
  if (verbose)
  {
    printf("mmap.decoder = %d\n", mmap.decoder);
    printf("ctld         = 0x%08x\n", ctld);
  }

  if (mmap.decoder == 0)
  {
    tsi->otat0  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau0 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal0 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau0 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal0 = bswap(pci_top & 0xffffffff);
    tsi->otofu0 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl0 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs0  = 0;
    tsi->otat0  = bswap(ctld);
  }
  if (mmap.decoder == 1)
  {
    tsi->otat1  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau1 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal1 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau1 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal1 = bswap(pci_top & 0xffffffff);
    tsi->otofu1 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl1 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs1  = 0;
    tsi->otat1  = bswap(ctld);
  }
  if (mmap.decoder == 2)
  {
    tsi->otat2  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau2 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal2 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau2 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal2 = bswap(pci_top & 0xffffffff);
    tsi->otofu2 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl2 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs2  = 0;
    tsi->otat2  = bswap(ctld);
  }
  if (mmap.decoder == 3)
  {
    tsi->otat3  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau3 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal3 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau3 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal3 = bswap(pci_top & 0xffffffff);
    tsi->otofu3 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl3 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs3  = 0;
    tsi->otat3  = bswap(ctld);
  }
  if (mmap.decoder == 4)
  {
    tsi->otat4  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau4 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal4 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau4 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal4 = bswap(pci_top & 0xffffffff);
    tsi->otofu4 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl4 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs4  = 0;
    tsi->otat4  = bswap(ctld);
  }
  if (mmap.decoder == 5)
  {
    tsi->otat5  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau5 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal5 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau5 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal5 = bswap(pci_top & 0xffffffff);
    tsi->otofu5 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl5 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs5  = 0;
    tsi->otat5  = bswap(ctld);
  }
  if (mmap.decoder == 6)
  {
    tsi->otat6  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau6 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal6 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau6 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal6 = bswap(pci_top & 0xffffffff);
    tsi->otofu6 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl6 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs6  = 0;
    tsi->otat6  = bswap(ctld);
  }
  if (mmap.decoder==7)
  {
    tsi->otat7  = 0;                                           //disable the decoder for the time we reprogram it
    tsi->otsau7 = bswap((u_int)((u_int64_t)mmap.pbase >> 32));
    tsi->otsal7 = bswap(mmap.pbase & 0xffffffff);
    tsi->oteau7 = bswap((u_int)((u_int64_t)pci_top >> 32));
    tsi->oteal7 = bswap(pci_top & 0xffffffff);
    tsi->otofu7 = bswap((u_int)((u_int64_t)vmeoffset >> 32));
    tsi->otofl7 = bswap(vmeoffset & 0xffffffff);
    tsi->otbs7  = 0;
    tsi->otat7  = bswap(ctld);
  }
  return(0);
}


/******************************/
int set_slave_map(slavemap smap)
/******************************/
{
  u_int ctld;
  unsigned long long offset;
  
  //check the input
  if (smap.decoder > 7) return(-1);
  if (smap.vmespace > 2) return(-4);
  if (smap.enable && smap.size == 0)
  {
    smap.size = 0x1000;
    printf("Slave map decoder %d: Size set to 0x1000\n", smap.decoder);
  }

  if(smap.vmespace == 0)
    smap.vbase &= 0xfffffff0;
  if(smap.vmespace == 1)
    smap.vbase &= 0xfffff000;
  if(smap.vmespace == 2)
    smap.vbase &= 0xffff0000;

  ctld = (0x78f | (smap.enable << 31) | (smap.vmespace << 4) );

  if (smap.decoder == 0)
  {
    tsi->itsau0 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal0 = bswap(smap.vbase);
    tsi->iteau0 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal0 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu0 = bswap(offset >> 32);
    tsi->itofl0 = bswap(offset & 0xffffffff);
    tsi->itat0  = bswap(ctld);
  }
  if (smap.decoder == 1)
  {
    tsi->itsau1 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal1 = bswap(smap.vbase);
    tsi->iteau1 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal1 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu1 = bswap(offset >> 32);
    tsi->itofl1 = bswap(offset & 0xffffffff);
    tsi->itat1  = bswap(ctld);
  } 
  if (smap.decoder == 2)
  {
    tsi->itsau2 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal2 = bswap(smap.vbase);
    tsi->iteau2 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal2 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu2 = bswap(offset >> 32);
    tsi->itofl2 = bswap(offset & 0xffffffff);
    tsi->itat2  = bswap(ctld);
  } 
  if (smap.decoder == 3)
  {
    tsi->itsau3 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal3 = bswap(smap.vbase);
    tsi->iteau3 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal3 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu3 = bswap(offset >> 32);
    tsi->itofl3 = bswap(offset & 0xffffffff);
    tsi->itat3  = bswap(ctld);
  } 
  if (smap.decoder == 4)
  {
    tsi->itsau4 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal4 = bswap(smap.vbase);
    tsi->iteau4 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal4 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu4 = bswap(offset >> 32);
    tsi->itofl4 = bswap(offset & 0xffffffff);
    tsi->itat4  = bswap(ctld);
  }
  if (smap.decoder == 5)
  {
    tsi->itsau5 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal5 = bswap(smap.vbase);
    tsi->iteau5 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal5 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu5 = bswap(offset >> 32);
    tsi->itofl5 = bswap(offset & 0xffffffff);
    tsi->itat5  = bswap(ctld);
  }
  if (smap.decoder == 6)
  {
    tsi->itsau6 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal6 = bswap(smap.vbase);
    tsi->iteau6 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal6 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu6 = bswap(offset >> 32);
    tsi->itofl6 = bswap(offset & 0xffffffff);
    tsi->itat6  = bswap(ctld);
  }
  if (smap.decoder == 7)
  {
    tsi->itsau7 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->itsal7 = bswap(smap.vbase);
    tsi->iteau7 = 0;            //64-bit VMEbus addresses not yet supported
    tsi->iteal7 = bswap(smap.vbase + smap.size);
    offset = smap.pbase - smap.vbase;
    tsi->itofu7 = bswap(offset >> 32);
    tsi->itofl7 = bswap(offset & 0xffffffff);
    tsi->itat7  = bswap(ctld);
  }
  return(0);
}


/******************************************************************/
int set_high_map(u_int64_t hm_base, u_int64_t hm_size, u_int bridge)
/******************************************************************/
{ 
  IO_ErrorCode_t ret;
  u_int h1, h2;
  u_char b1, b2, b3, b4;
  u_int config_data;
  u_int64_t hm_limit;
  
  if (hm_base == 0)  //High PCI address range not used. Nothing to be done
    return(0);
  
  if(hm_base < 0x100000000LLU)
  {
    printf("ERROR: High memory range starts below 4 GB boundary\n");
    return(-3);
  }
  
  ret = IO_Open();
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  if(btype == 1)
  {
    ret = IO_PCIDeviceLink(0x8086, 0x3b42, 1, &h1);
    if (ret != IO_RCC_SUCCESS)
    {
      rcc_error_print(stdout, ret);
      return(-2);
    }
  }
  else
  {
    ret = IO_PCIDeviceLink(0x8086, 0x1e10, 1, &h1);
    if (ret != IO_RCC_SUCCESS)
    {
      rcc_error_print(stdout, ret);
      return(-2);
    }
  }
  
  ret = IO_PCIDeviceLink(0x12d8, 0xe130, bridge, &h2);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-2);
  }

  hm_limit = hm_base + hm_size;
  
  printf("hm_base  = 0x%016lx\n", (long unsigned int) hm_base);
  printf("hm_limit = 0x%016lx\n", (long unsigned int) hm_limit);
  
  b1 = ((hm_base >> 16) & 0xf0) + 1;
  b2 = (hm_base >> 24) & 0xff;
  b3 = ((hm_limit >> 16) & 0xf0) + 1;
  b4 = (hm_limit >> 24) & 0xff;
  config_data = (b4 << 24) + (b3 << 16) +(b2 << 8) + b1;
  
  printf("b1 = 0x%02x, b2 = 0x%02x, b3 = 0x%02x, b4 = 0x%02x\n", b1, b2, b3, b4);
  printf("config_data = 0x%08x\n", config_data);

  ret = IO_PCIConfigWriteUInt(h1, 0x24, config_data);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  ret = IO_PCIConfigWriteUInt(h2, 0x24, config_data);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  config_data = hm_base >> 32;
  printf("config_data = 0x%08x\n", config_data);

  ret = IO_PCIConfigWriteUInt(h1, 0x28, config_data);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  ret = IO_PCIConfigWriteUInt(h2, 0x28, config_data);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  config_data = hm_limit >> 32;
  printf("config_data = 0x%08x\n", config_data);

  ret = IO_PCIConfigWriteUInt(h1, 0x2c, config_data);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  ret = IO_PCIConfigWriteUInt(h2, 0x2c, config_data);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  ret = IO_PCIDeviceUnlink(h1);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-3);
  }

  ret = IO_PCIDeviceUnlink(h2);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-4);
  }

  ret = IO_Close();
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    return(-5);
  }

  return(0);
}


/**********************/
int set_fair(u_int fair)
/**********************/
{
  //0=not fair 1=fair
  if (fair == 1)
  {
    tsi->vmctrl |= bswap(0x00000004);
    return(0);
  }
  else if (fair == 0)
  {
    tsi->vmctrl &= bswap(0xfffffffb);
    return(0);
  }
  else
    return(-1);
}


/**************************/
int set_req_lvl(u_int level)
/**************************/
{
  //check input
  if (level > 3) return(-1);

  tsi->vmctrl &= bswap(0xfffffffc);
  tsi->vmctrl |= bswap(level);
  return(0);
}


/**************************/
int set_rel_mode(u_int mode)
/**************************/
{
  if (mode > 3) return(-1);
  
  tsi->vmctrl &= bswap(0xffffffe7);
  tsi->vmctrl |= bswap(mode << 3);
  
  //Set some other useful bits
  tsi->vmctrl &= bswap(0xfffff8ff);
  tsi->vmctrl |= bswap(0x700);
  
  return(0);
}


/**********************/
int set_vme_to(u_int to)
/**********************/
{
  //check input
  if(to > 9) return(-1);

  tsi->vctrl &= bswap(0xfffffff0);
  tsi->vctrl |= bswap(to);
  return(0);
}


/**************************/
int set_arb_mode(u_int mode)
/**************************/
{
  //1=round robin  0=priority
  if (mode == 0)
  {
    tsi->vctrl |= bswap(0x00000040);
    return(0);
  }
  else if (mode == 1)
  {
    tsi->vctrl &= bswap(0xffffffbf);
    return(0);
  }
  else
    return(-1);
}


/**************************************/
int set_swap(u_int ms,u_int ss,u_int fs)
/**************************************/
{
  u_int ret, data;

  data = (ms << 3);
  data |= (ss << 4);
  data |= (fs << 5);
  ret = VME_CCTSetSwap(data);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  return(0);
}


/*********************/
int set_irq(u_int mask)
/*********************/
{
  u_int value;

  value = bswap(tsi->inten);
  value &= 0xfffffd01;    //mask all interrupts
  value |= mask;          //enable selected interrupts
  if (verbose)
    printf("Setting inten and inteo to 0x%08x\n", value); 
  
  tsi->inten = bswap(value);
  tsi->inteo = bswap(value);
  return(0);
}


/*********************************/
int full_initialisation(u_int mode)
/*********************************/
{
  //0=only base initialisation   1=full initialisation
  if (mode == 0)
    return(0);  //nothing to do
  else if(mode == 1)
  {
    //generic stuff for all Tsi148 based cards
    //Set latency timer to maximum
    tsi->noswap_head_mlat_clsz |= 0x0000f800;

    //Enable PCI master
    tsi->noswap_stat_cmmd |= 0x00000004;

    return(0);
  }
  else
    return(-1);
}


/**********************/
int set_arb_to(u_int to)
/**********************/
{
  //check input
  if (to > 1) 
    return(-1);

  tsi->vctrl &= bswap(0xffffff7f);
  tsi->vctrl |= bswap(to << 7);
  return(0);
}


/****************/
int open_tsi(void)
/****************/
{
  u_int ret;

  //Get a virtual address for the Tsi148 registers
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_Tsi148Map(&tsibase);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  tsi = (tsi148_regs_t *)tsibase;
  return(0);
}


/*****************/
int close_tsi(void)
/*****************/
{
  u_int ret;

  ret = VME_Tsi148Unmap(tsibase);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  return(0);
} 


/***************************/
int pciaddr_finder_init(void)
/***************************/
{
  FILE *fp;
  char pci_dev_sig[10];
  
  bridge_occn = -1;

  fp = popen("lspci -n | grep \"12d8:e130\\|10e3:0148\" | cut -s -d \" \" -f 3", "r");

  if (!fp) 
  {
    printf("Couldn't execute command: '%s'\n", "lspci -n | grep \"12d8:e130\\|10e3:0148\" | cut -s -d \" \" -f 3");
    return 1;
  }

  while(true) 
  {
    if (fscanf(fp, "%9s", pci_dev_sig) < 1)  //Look for the signature of a PCI device
      break;

    if (!strcmp(pci_dev_sig, "12d8:e130"))   //Is the signature that of a bridge chip? Count it!
      bridge_occn--;
    else if (!strcmp(pci_dev_sig, "10e3:0148"))  //If we find  the signature of the TIS we stop counting bridges
    {
      bridge_occn = -bridge_occn - 1;
      break;
    }
  }
  
  if (bridge_occn < 0) 
  {
    printf("TSI PCI signature not found! Are you sure there is a TSI VME-Bus Bridge installed?\n");
    return 2;
  } 
  else if (!bridge_occn) 
  {
    printf("Couldn't find any PCI bridges!\n");
    return 3;
  }
  
  printf("Found TSI PCI signature!\n");
  pclose(fp);
  
  fp = popen("tail -n 1 /proc/iomem | cut -d \"-\" -f 2", "r");
  if (!fp) 
  {
    printf("Couldn't execute command: '%s'\n", "tail -n 1 /proc/iomem | cut -d \"-\" -f 2");
    return 4;
  }

  fscanf(fp, "%16llx%*[\t ]:", &pci_addr_base);
  pclose(fp);
  
  if (pci_addr_base & 0xFFFFF) 
  {
    pci_addr_base += 0x100000; // add 1 MB
    pci_addr_base &= ~0xFFFFF; // round to full MB
  }

  printf("Successfully determined address (%llx) and bridge-occurence-number (%d)\n", pci_addr_base, bridge_occn);
  pci_addr_offset = 0;

  return 0;
}


/****************************************************/
unsigned long long pciaddr_finder_getaddr(u_int *size)
/****************************************************/
{
  if(!*size) 
  {
    printf("Size parameter out of range. Size cannot be zero!\n");
    return 0;
  }

  if (*size & 0xFFFF) 
  {
    printf("Warning: Automatically adjusted size from 0x%x ", *size);
    printf("to 0x%x, to match the 64 KB resolution!\n", (*size = (*size + 0x10000) & ~0xFFFF));
  }

  if (pci_addr_offset + *size > VME_MAP_WINDOW_SIZE) 
  {
    printf("Size parameter out of range. Cannot allocate 0x%x bytes. Free PCI space exhausted!\n", *size);
    return 0; 
  }

  unsigned long long ret_addr = pci_addr_base + pci_addr_offset;
  pci_addr_offset += *size;

  printf("Successfully allocated 0x%x bytes to PCI Address 0x%llx!\n", *size, ret_addr);
  return ret_addr;
}


/*******************************************/
int pci_addr_finder_checkautomode(tsipara pa)
/*******************************************/
{
  int autoi = 0, enabi = 0;

  if(pa.mmap0.enable) { enabi++; if(!pa.mmap0.pbase) autoi++; }
  if(pa.mmap1.enable) { enabi++; if(!pa.mmap1.pbase) autoi++; }
  if(pa.mmap2.enable) { enabi++; if(!pa.mmap2.pbase) autoi++; }
  if(pa.mmap3.enable) { enabi++; if(!pa.mmap3.pbase) autoi++; }
  if(pa.mmap4.enable) { enabi++; if(!pa.mmap4.pbase) autoi++; }
  if(pa.mmap5.enable) { enabi++; if(!pa.mmap5.pbase) autoi++; }
  if(pa.mmap6.enable) { enabi++; if(!pa.mmap6.pbase) autoi++; }
  if(pa.mmap7.enable) { enabi++; if(!pa.mmap7.pbase) autoi++; }

  if(autoi) 
  {
    if (autoi == enabi)
      return autoi;
    else
      return 0; // can't be here. This return is just for completeness  
  } 
  else 
  {
    if (!enabi)  
      return -1; // no auto or manual mmaps's enabled
    else
      return -(enabi + 1); // manual mode enabled
  }
}
