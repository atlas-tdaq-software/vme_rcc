// $Id$
/************************************************************************/
/*									*/
/* File: vme_rod_emul_test.c						*/
/*									*/
/* This program talks to a ROD emulator					*/
/* It reacts to interrupts from the ROD and reads a buffer from the 	*/
/* ROD using a DMA transfer. The size of the transfer is given by the 	*/
/* first word of the buffer (simulating a register)			*/
/* In addition, the data is checked					*/
/*									*/
/* 18. Dec. 01  J.O.Petersen created					*/
/* 29. May. 02  adapt to new IR API					*/
/*									*/
/**************** *******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "rcc_time_stamp/tstamp.h"
#include "cmem_rcc/cmem_rcc.h"

#define ROD_BUFFER_MAX_SIZE 0x10000
#define ROD_INTERRUPT 130
#define ROD_INTERRUPT_LEVEL 3
#define ROD_NO_VECTORS 1
#define ROD_INTTIMEOUT 20000
#define ROD_DMATIMEOUT 1000

/************/
int main(void)
/************/
{
  VME_ErrorCode_t v_ret;
  u_int err_code;
  char ichstr[10];
  int i;
  u_int no_rod_event;

  int sc_handle;
  VME_BlockTransferList_t rlist;
  int cmem_desc;
  unsigned long cmem_desc_uaddr, cmem_desc_paddr, j;
  volatile u_int* d_ptr;
  u_int event_size, event_data;
  u_int rod_vmebus_add;
  VME_MasterMap_t master_map;

  int int_handle;
  int no_timeouts;
  VME_InterruptList_t irq_list;
  VME_InterruptInfo_t ir_info;

  int no_events;
 
  tstamp ts1;
  tstamp ts2;
  float delta;
  float mbyte, mbytepersec, usecpercall;
 
  err_code = ts_open(1, TS_DUMMY);
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = CMEM_Open();
  if (err_code) 
    rcc_error_print(stdout, err_code);
 
  // allocate the receive buffer   
  err_code = CMEM_SegmentAllocate(ROD_BUFFER_MAX_SIZE, (char *)"DMA_BUFFER", &cmem_desc);
  if (err_code) 
    rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
  if (err_code)
    rcc_error_print(stdout, err_code);
 
  v_ret = VME_Open();
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);
 
  // single cycle master access to the ROD memory   
  printf("VMEbus address of ROD memory: ");
  rod_vmebus_add = gethex();

  master_map.vmebus_address = rod_vmebus_add;
  master_map.window_size = ROD_BUFFER_MAX_SIZE;
  master_map.address_modifier = VME_A32;
  master_map.options = 0x0;		// FIXME

  v_ret = VME_MasterMap(&master_map, &sc_handle);
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);
  else
    printf("Handle = %d\n", sc_handle);
  
  // prepare the block transfer list 
  rlist.number_of_items = 1;
  rlist.list_of_items[0].vmebus_address = rod_vmebus_add;
  rlist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  printf("PCI address of data buffer is 0x%016lx\n", cmem_desc_paddr);
  // D64 requires byte swapping by SW ..
  rlist.list_of_items[0].control_word = VME_DMA_D32R;

  // now the interrupts

  irq_list.number_of_items = 1;
  printf("vector = ");
  irq_list.list_of_items[0].vector = getdecd(ROD_INTERRUPT);
  irq_list.list_of_items[0].level = ROD_INTERRUPT_LEVEL;
  irq_list.list_of_items[0].type = VME_INT_ROAK;

  v_ret = VME_InterruptLink(&irq_list, &int_handle);
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);
  else
    printf(" IRQ Handle = %d\n", int_handle);

  printf(" data check (y/n) ");
  getstrd(ichstr,(char *)"y");

  printf("# events = ");
  no_events = getdecd(1000);
  no_timeouts = 0;
  no_rod_event = 0;

  for (i=0; i < no_events; i++)
  {

    no_timeouts = 0;
    v_ret = VME_InterruptWait(int_handle, ROD_INTTIMEOUT, &ir_info);
    if (v_ret != VME_SUCCESS)
    {
      if (v_ret == VME_TIMEOUT)
        no_timeouts++;
      VME_ErrorPrint(v_ret);
    }

    if (i == 0)
      ts_clock(&ts1);

    if (no_timeouts == 0)	// no timeout
    {

      if (ir_info.vector != irq_list.list_of_items[0].vector)
      {
        printf(" illegal vector = %d\n",ir_info.vector);
      }

      // read first word to get size
      v_ret = VME_ReadSafeUInt(sc_handle, 0, &event_size);  
      if (v_ret != VME_SUCCESS)
        VME_ErrorPrint(v_ret);

      if (event_size > ROD_BUFFER_MAX_SIZE)
      {
        printf(" illegal event size = %d\n",event_size);
      }

      // read second word to get (fixed) event pattern
      v_ret = VME_ReadSafeUInt(sc_handle, 0x4, &event_data);  
      if (v_ret != VME_SUCCESS)
        VME_ErrorPrint(v_ret);

      rlist.list_of_items[0].size_requested = event_size;
	
      if (event_size == 0)
      {
	printf("event_size = 0\n");
	v_ret = VME_WriteSafeUInt(sc_handle, 0, 0x11222211);  
	exit(0);
      }

      // Read buffer TO = 1 sec.
      v_ret = VME_BlockTransfer(&rlist, ROD_DMATIMEOUT);
      if (v_ret != VME_SUCCESS)
        VME_ErrorPrint(v_ret);      

      if (ichstr[0] == 'y')    // check data
      {

        d_ptr = (unsigned int*)cmem_desc_uaddr;
  
        for (j = 2; j < (event_size / sizeof(int) - 1); j++)
        {
          if (d_ptr[j] != event_data)
          {
            printf(" word # %ld, data = 0x%x, expected 0x%8x\n",j,d_ptr[i],event_data);
            break;
            
          }
        }

        if (d_ptr[event_size / sizeof(int) - 1] != no_rod_event)
        {
          printf(" last word = 0x%8x instead of 0x%8x\n", d_ptr[event_size/sizeof(int) - 1], no_rod_event);
          v_ret = VME_WriteSafeUInt(sc_handle, 0, 0x12345678);  
          exit(0);
        }
      }

      no_rod_event++;

// write first word to ACK
      v_ret = VME_WriteSafeUInt(sc_handle, 0, 0);  
      if (v_ret != VME_SUCCESS)
        VME_ErrorPrint(v_ret);
    }

  }

  ts_clock(&ts2);
  delta = ts_duration(ts1,ts2);

  printf(" # ROD events = %d\n",no_rod_event);

  printf(" # seconds     = %6.2f\n",delta);
  usecpercall = (delta/no_rod_event)*1000000;
  printf(" usecs/message = %6.2f\n",usecpercall);

  mbyte = ((float)(no_rod_event)*event_size)/(1024*1024);
  mbytepersec =  mbyte/delta;
  printf(" Mbyte/sec     = %6.2f\n",mbytepersec);


  printf(" clean & quit\n");

  v_ret = VME_InterruptUnlink(int_handle);
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);

  v_ret = VME_MasterUnmap(sc_handle);
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);

  v_ret = VME_Close();
  if (v_ret != VME_SUCCESS)
    VME_ErrorPrint(v_ret);

  err_code = CMEM_SegmentFree(cmem_desc);
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = CMEM_Close();
  if (err_code)
    rcc_error_print(stdout, err_code);

  err_code = ts_close(TS_DUMMY);
  if (err_code)
    rcc_error_print(stdout, err_code);


}
