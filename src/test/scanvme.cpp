// $Id$
/************************************************************************/
/*  scanvme: A program by M. Joos CERN,EP/ESS/OS 			*/
/*									*/
/*  The main purpose of scanvme is to scan the whole 4 GBytes		*/
/*  of VME address space for slaves present. This is done by trying 	*/
/*  to read a char, short or int. Hence, only slaves with readable 	*/
/*  registers can be found.						*/
/*									*/
/*  8.06.94   MAJO  created						*/
/* 07.03.02   MAJO  Ported to VME_RCC driver for CCT SBCs		*/
/*****C 2010 - A nickel program worth a dime*****************************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "vme_rcc/universe.h"

#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

//prototypes
int open_universe();
int close_universe();
int init_universe();

//globals
volatile universe_regs_t *uni;
int amcode = 0, space = 32, bw = 32;
u_int pci_base = 0xf1000000, pci_address, lsi0ctl, lsi1ctl, lsi2ctl, lsi3ctl, lsi3bs, lsi3bd, lsi3to, misc_ctl_data;
u_int lsi4ctl, lsi5ctl, lsi6ctl, lsi7ctl;
u_int step = 0x100000, dblevel = 0, dbpackage = DFDB_VMERCC, start_offset = 0;
int handle, *pci, closeit;
u_long vme_address;
  
  
/*****************/
int open_universe()
/*****************/
{ 
  u_int ret;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  ret = VME_UniverseMap(&vme_address);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  uni = (universe_regs_t *)vme_address; 
  return(0);
}



/*****************/
int init_universe()
/*****************/
{
  u_int ldata;

  //disable lsi0,1,2,4,5,6,7
  lsi0ctl = uni->lsi0_ctl;
  lsi1ctl = uni->lsi1_ctl;
  lsi2ctl = uni->lsi2_ctl;
  lsi4ctl = uni->lsi4_ctl;
  lsi5ctl = uni->lsi5_ctl;
  lsi6ctl = uni->lsi6_ctl;
  lsi7ctl = uni->lsi7_ctl;
  uni->lsi0_ctl = 0;
  uni->lsi1_ctl = 0;
  uni->lsi2_ctl = 0;
  uni->lsi4_ctl = 0;
  uni->lsi5_ctl = 0;
  uni->lsi6_ctl = 0;
  uni->lsi7_ctl = 0;

  //save lsi3
  lsi3ctl = uni->lsi3_ctl;
  lsi3bs = uni->lsi3_bs;
  lsi3bd = uni->lsi3_bd;  
  lsi3to = uni->lsi3_to;
  
  //Set bus time-out to 16 us
  misc_ctl_data = uni->misc_ctl;
  ldata = misc_ctl_data & 0x0fffffff;
  ldata = ldata | 0x10000000;
  uni->misc_ctl = ldata;
  printf("VMEbus time-out temporarily reduced to 16 us to speed up scanning process\n");
  return(0);
}


/******************/
int close_universe()
/******************/
{
  u_int ret;
  u_int dummy[8];

  if (closeit)
  {
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
  }
  
  dummy[0] = 0; //IRQ modes are not valid

  //restore lsi0,1,2,3,4,5,6,7
  uni->lsi0_ctl = lsi0ctl;
  uni->lsi1_ctl = lsi1ctl;
  uni->lsi2_ctl = lsi2ctl;
  uni->lsi4_ctl = lsi4ctl;
  uni->lsi5_ctl = lsi5ctl;
  uni->lsi6_ctl = lsi6ctl;
  uni->lsi7_ctl = lsi7ctl;

  uni->lsi3_ctl = lsi3ctl;
  uni->lsi3_bs = lsi3bs;
  uni->lsi3_bd = lsi3bd;
  uni->lsi3_to = lsi3to;

  //Reset bus time-out to original value
  uni->misc_ctl = misc_ctl_data;

  ret = VME_UniverseUnmap(vme_address);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  // Update the driver
  ret = VME_Update(dummy);
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }  
    
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  return(0);
}


/*****************************/
void SigHandler(int /*signum*/)
/*****************************/
{
  printf("Aaarrrgghhhhh....\n");
  close_universe();
  exit(-1);
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-s x: Step size                          -> Default: 0x" << std::hex << step << std::dec << std::endl;
  std::cout << "-o x: Start offset                       -> Default: 0x" << std::hex << start_offset << std::dec << std::endl;
  std::cout << "-a x: Address width                      -> Default: " << space << std::endl;
  std::cout << "       0 = CR/CSR" << std::endl;
  std::cout << "      16 = A16" << std::endl;
  std::cout << "      24 = A24" << std::endl;
  std::cout << "      32 = A32" << std::endl;
  std::cout << "-d x: Data width                         -> Default: " << bw << std::endl;
  std::cout << "       8 = D8" << std::endl;
  std::cout << "      16 = D16" << std::endl;
  std::cout << "      32 = D32" << std::endl;  
  std::cout << "-p x: PCI base (for the map decoder)     -> Default: 0x" << std::hex << pci_base << std::dec << std::endl;
  std::cout << "-c x: AM code                            -> Default: " << amcode << std::endl;
  std::cout << "      0 = User" << std::endl;
  std::cout << "      1 = Supervisor" << std::endl;
  std::cout << "-l x: Debug level                        -> Default: " << dblevel << std::endl;
  std::cout << "-D x: Debug package                      -> Default: " << dbpackage << std::endl;
  std::cout << std::endl;
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  int c, stat;
  u_int ret, ii;
  u_char cdata;
  u_short sdata;
  u_int scan_offset, idata;
  u_int loop, dummy[8];
  struct sigaction sa;
  VME_MasterMap_t master_map;

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigHandler;
  stat = sigaction(SIGINT, &sa, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", stat);
    exit(-1);
  }

  dummy[0] = 0; //IRQ modes are not valid
  
  static struct option long_options[] = {{"DVS", no_argument, NULL, '1'}}; 
  while ((c = getopt_long(argc, argv, "s:o:a:d:p:c:l:D:", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(-1);
      break;

    case 's': sscanf(optarg, "%x", &step);         break;
    case 'o': sscanf(optarg, "%x", &start_offset); break;
    case 'a': space = atoi(optarg);                break;
    case 'd': bw = atoi(optarg);                   break;
    case 'p': sscanf(optarg, "%x", &pci_base);     break;
    case 'c': amcode = atoi(optarg);               break;
    case 'l': dblevel = atoi(optarg);              break;
    case 'D': dbpackage = atoi(optarg);            break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (-1);
    }
    
  if (space != 0 && space != 16 && space != 24 && space != 32) 
    space = 32;
  if (bw != 8 && bw != 16 && bw != 32) 
    bw = 8;
  if (amcode != 0 && amcode != 1)
    amcode = 0;
  if (step > 0x01000000)
    step = 0x01000000;

  DF::GlobalDebugSettings::setup(dblevel, dbpackage);

  printf("==============================\n");
  printf("step size        = 0x%08x\n", step);
  printf("space            = A%d\n", space);
  printf("bus width        = D%d\n", bw);
  printf("AM code          = %s\n", (amcode == 1)?"User":"Data");
  printf("==============================\n");
  printf("PCI base address = 0x%08x\n", pci_base);
  printf("------------------------------\n\n");

  open_universe();
  init_universe();
  closeit = 1;
 
  if (space == 0)
  {
    // Generate a mapping
    uni->lsi3_ctl = 0x80850000;
    uni->lsi3_bs = pci_base;
    uni->lsi3_bd = pci_base + 0x01000000;
    uni->lsi3_to = (0xffffffff - pci_base) + 1;
       
    // Update the driver
    ret = VME_Update(dummy);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    // Attach process to the mapping
    master_map.vmebus_address  = 0x0;
    master_map.window_size = 0x01000000;
    master_map.address_modifier = VME_CRCSR;
    master_map.options = 0;
    
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    for (ii = start_offset; ii < 0x1000000; ii += step)
    {
      if (bw == 8)
        ret = VME_ReadSafeUChar(handle, ii+3, &cdata);
      if (bw == 16)
        ret = VME_ReadSafeUShort(handle, ii, &sdata);
      if (bw == 32)
        ret = VME_ReadSafeUInt(handle, ii, &idata);
        
      if (!ret) 
        printf("Got response from address 0x%06x\n", ii);
    }
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
    closeit = 0;
  }
 
 
  if (space == 16)
  {
    // Generate a mapping
    if (amcode == 0) uni->lsi3_ctl = 0x80800000;
    if (amcode == 1) uni->lsi3_ctl = 0x80801000;
    uni->lsi3_bs = pci_base;
    uni->lsi3_bd = pci_base + 0x01000000;
    uni->lsi3_to = (0xffffffff - pci_base) + 1;
    
    // Update the driver
    ret = VME_Update(dummy);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    // Attach process to the mapping
    master_map.vmebus_address  = 0x0;
    master_map.window_size = 0x01000000;
    master_map.address_modifier = VME_A16;
    if (amcode == 0) master_map.options = 0;
    if (amcode == 1) master_map.options = VME_AM_SUPERVISOR;
    
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    for (ii = start_offset; ii < 0x10000; ii += step)
    {      
      if (bw == 8)
        ret = VME_ReadSafeUChar(handle, ii, &cdata);
      if (bw == 16)
        ret = VME_ReadSafeUShort(handle, ii, &sdata);
      if (bw == 32)
        ret = VME_ReadSafeUInt(handle, ii, &idata);
        
      if (!ret) 
        printf("Got response from address 0x%06x\n", ii);

    }
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
    closeit = 0;
  }

  if (space == 24)
  {
    // Generate a mapping in the Universe
    if (amcode == 0) uni->lsi3_ctl = 0x80810000;
    if (amcode == 1) uni->lsi3_ctl = 0x80811000;
    uni->lsi3_bs = pci_base; 
    uni->lsi3_bd = pci_base + 0x01000000; 
    uni->lsi3_to = (0xffffffff - pci_base) + 1;
    
    // Update the driver
    ret = VME_Update(dummy);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    // Attach process to the mapping
    master_map.vmebus_address  = 0x0;
    master_map.window_size = 0x01000000;
    master_map.address_modifier = VME_A24;
    if (amcode == 0) master_map.options = 0;
    if (amcode == 1) master_map.options = VME_AM_SUPERVISOR;
    
    ret = VME_MasterMap(&master_map, &handle);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
   
    for (ii = start_offset; ii < 0x1000000; ii += step)
    {      
      if (bw == 8)
        ret = VME_ReadSafeUChar(handle, ii, &cdata);
      if (bw == 16)
        ret = VME_ReadSafeUShort(handle, ii, &sdata);
      if (bw == 32)
        ret = VME_ReadSafeUInt(handle, ii, &idata);
        
      if (!ret) 
        printf("Got response from address 0x%06x\n", ii);
    }
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
    closeit = 0;
  }

  if (space == 32)
  {
    for(loop = 0; loop < 0x100; loop++)
    {
      if ((start_offset >> 24) > loop)    
        continue;

      // Generate a mapping in the Universe
      if (amcode == 0) uni->lsi3_ctl = 0x80820000;
      if (amcode == 1) uni->lsi3_ctl = 0x80821000;
      uni->lsi3_bs = pci_base;
      uni->lsi3_bd = pci_base + 0x01000000;
      uni->lsi3_to = ((0xffffffff - pci_base) + 1) + (loop << 24);
      
      // Update the driver
      ret = VME_Update(dummy);
      if (ret)
      {
        VME_ErrorPrint(ret);
        exit(-1);
      }  

      // Attach process to the mapping
      master_map.vmebus_address  = loop << 24;
      master_map.window_size = 0x01000000;
      master_map.address_modifier = VME_A32;
      if (amcode == 0) master_map.options = 0;
      if (amcode == 1) master_map.options = VME_AM_SUPERVISOR;

      ret = VME_MasterMap(&master_map, &handle);
      if (ret)
      {
        VME_ErrorPrint(ret);
        exit(-1);
      }  

      scan_offset = 0;
      if ((start_offset >> 24) == loop)    
        scan_offset = start_offset & 0x00ffffff;
	
      for (ii = scan_offset; ii < 0x1000000; ii += step)
      {
        if (bw == 8)
          ret = VME_ReadSafeUChar(handle, ii, &cdata);
        if (bw == 16)
          ret = VME_ReadSafeUShort(handle, ii, &sdata);
        if (bw == 32)
          ret = VME_ReadSafeUInt(handle, ii, &idata);

        if (!ret) 
          printf("Got response from address 0x%08x\n", ii + (loop << 24));
      }    
      ret = VME_MasterUnmap(handle);
      if (ret)
        VME_ErrorPrint(ret);
      closeit = 0;
    }
  }

  close_universe();
  exit(0);
}

