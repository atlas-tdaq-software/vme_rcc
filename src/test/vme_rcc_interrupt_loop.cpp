// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_interrupt_loop.c					*/
/*									*/
/* Loop on interrupts							*/
/*									*/
/* 07. Nov. 01  JOP  created 						*/
/* 29. May. 02  JOP  adapt to new IR API 				*/
/*									*/
/**************** C 2001 - A nickel program worth a dime ****************/

#include <stdio.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"

#define VME_VECTOR_DEF 130
#define VME_LEVEL_DEF 3
#define VME_INTTIMEOUT 1 

/************/
int main(void)
/************/
{
  int i;
  int int_handle;
  int  nloops;
  VME_ErrorCode_t ret;
  VME_InterruptList_t irq_list;
  VME_InterruptInfo_t ir_info;

  ret=VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("# interrupts = ");
  irq_list.number_of_items = getdecd(1);
  for (i=0; i < irq_list.number_of_items; i++)
  {
    printf("vector = ");
    irq_list.list_of_items[i].vector = getdecd(VME_VECTOR_DEF+i);
    printf("level = ");
    irq_list.list_of_items[i].level = getdecd(VME_LEVEL_DEF);
    printf("interrupt type ( ROAK = %2d, RORA = %2d) = ",VME_INT_ROAK, VME_INT_RORA);
    irq_list.list_of_items[i].type = getdecd(VME_INT_ROAK);
  }

  ret = VME_InterruptLink(&irq_list, &int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf(" IRQ Handle = %d\n", int_handle);

  printf("# loops = ");
  nloops = getdecd(10);

  for (i=0; i < nloops; i++) {
        
    ret = VME_InterruptWait(int_handle, VME_INTTIMEOUT, &ir_info);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
  }

  ret = VME_InterruptUnlink(int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

}
