#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "cmem_rcc/cmem_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"

//globals
tstamp ts1, ts2;
float delta;
int cmem_desc, cont;
unsigned long cmem_desc_uaddr, cmem_desc_paddr;
VME_BusErrorInfo_t g_bus_err_info;

//Prototypes
void SigQuitHandler(int signum);
void SigBusHandler(int signum);
void dmarw(void);
void scrw(void);
void mastermap(void);
void vmebench(void);
void setdebug(void);
int stress_menu(void);
int mainhelp(void);
int func_menu(void);
int bench_menu(void);
int test_menu(void);
int swapping(void);


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  int stat;
  struct sigaction sa;
  int loop, dmar_desc;
  VME_BlockTransferList_t rlist;
  u_long dmar_desc_uaddr, dmar_desc_paddr;
  u_int dmasize, ret, dmadirection = 0;

  if ((argc == 2) && (sscanf(argv[1], "%u", &dmadirection) == 1)) {argc--;} else {dmadirection = 0;}

  if (dmadirection == 0)
    printf("This process will do DMA reads\n");
  else
    printf("This process will do DMA writes\n");
  
  DF::GlobalDebugSettings::setup(5, 102);

  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler; 
  stat = sigaction(SIGQUIT, &sa, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", stat);
    exit(-1);
  }
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = CMEM_Open();
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit(-3);
  } 
    
  ret = CMEM_SegmentAllocate(0x10000, (char *)"DMA_READ_BUFFER", &dmar_desc);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit (-1);
  }
  
  ret = CMEM_SegmentPhysicalAddress(dmar_desc, &dmar_desc_paddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit (-1);
  }

  ret = CMEM_SegmentVirtualAddress(dmar_desc, &dmar_desc_uaddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit (-1);
  }

  dmasize = 0x20;
    
  cont = 1;
  printf("=====================================\n");
  printf("Test running. Press <ctrl+\\> to stop\n");
  printf("=====================================\n");
  while(cont)
  {
    dmasize += 0x20;
    if (dmasize > 0x4000)
      dmasize = 0x20;
    rlist.number_of_items = 1;
    rlist.list_of_items[0].vmebus_address = 0x08000000;
    rlist.list_of_items[0].system_iobus_address = dmar_desc_paddr;
    rlist.list_of_items[0].size_requested = dmasize;
    
    if (dmadirection == 0)
      rlist.list_of_items[0].control_word = VME_DMA_D32R;
    else
      rlist.list_of_items[0].control_word = VME_DMA_D32W;

    //Read or write buffer
    ret = VME_BlockTransfer(&rlist, 1000);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);      
    if (ret == VME_DMAERR)
    {
      for (loop = 0; loop < rlist.number_of_items; loop++)
      {
        printf("Element:         %d\n", loop);
        printf("Status:          %d\n", rlist.list_of_items[loop].status_word);
        printf("Bytes remaining: %d\n", rlist.list_of_items[loop].size_remaining);
      }
    }
  }
   
  ret = CMEM_SegmentFree(dmar_desc);
  if (ret) 
    rcc_error_print(stdout, ret);

  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  return(0);  
}


/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
  cont = 0;
}















