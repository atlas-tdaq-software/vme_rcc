// $Id: 
/************************************************************************/
/*									*/
/* File: geo_peek.c							*/
/*									*/
/* Execute one VMEbus read cycle to a CR/CSR register and print the data*/
/*									*/
/* 17. Jul. 06  MAJO  created						*/
/*									*/
/**************** C 2006 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSGetInput/get_input.h"


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  u_int ret, vmeaddr, vmedata;
  int slot, dsize, awidth;

  if ((argc == 4) && (sscanf(argv[3], "%d", &dsize) == 1)) {argc--;} else {dsize = 4;}
  if ((argc == 3) && (sscanf(argv[2], "%x", &vmeaddr) == 1)) {argc--;} else {vmeaddr = 0x0;}
  if ((argc == 2) && (sscanf(argv[1], "%d", &slot) == 1)) {argc--;} else {slot = 2;}
  if (argc != 1) 
  {
    printf("Use:\nvme_peek <slot><Address><Data size>\n");
    printf("slot      = The slot number of the slave module                           Default: 2\n");
    printf("Address   = The hexadecimal VMEbus address offset within the CR/CSR space Default: 0x0\n");
    printf("Data size = The number of bytes to read (1=D8, 2=D16, 4=D32)              Default: 4\n");
    printf("\n"); 
    exit(0);
  }

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }
  
  if (dsize == 1) awidth = 0;
  if (dsize == 2) awidth = 1;
  if (dsize == 4) awidth = 2;
  
  ret = VME_ReadCRCSR(slot, ((dsize << 28) | (awidth << 24) | (vmeaddr & 0xfffff)), &vmedata);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  printf("0x%08x\n", vmedata);

  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  exit(vmedata);
}
