// $Id: 
/************************************************************************/
/*									*/
/* File: single_cycles.c						*/
/*									*/
/* This is a S/W example for the RCC VMEbus library			*/
/*									*/
/* 16. Sept. 03  MAJO  created						*/
/*									*/
/**************** C 2011 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include "DFDebug/DFDebug.h"
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSGetInput/get_input.h"


/* Globals*/
static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
static u_int data = 0, ddir = 1, dtype = 3;
u_int *lptr, ldata = 0;
u_short *sptr, sdata = 0;
u_char *bptr, bdata = 0;
u_int ret;
u_long vbase;
int handle, mode, slow;


/**************************/
void *cycles(void *threadid)
/**************************/
{
  int tid;
  u_int offset, pattern;
  tid = (uintptr_t)threadid;
  
  printf("It's me, thread #%d!\n", tid);

  if (tid == 1)
  {
    offset = 0;
    pattern = 0x111;
  }
  else
  {
    offset = 0x10;
    pattern = 0x222;
  }

  ret = VME_WriteSafeUInt(handle, offset, pattern);
  if (ret != VME_SUCCESS) 
    VME_ErrorPrint(ret);
    
  while(1)
  {
    if (slow == 1)
      sleep(1);
    ret = VME_ReadSafeUInt(handle, offset, &ldata);
    if (ret != VME_SUCCESS) 
      VME_ErrorPrint(ret);
    if(ldata != pattern)
      printf("T%d: Data error: Expected=0x%08x  Read=0x%08x", tid, pattern, ldata);	
  } 
}



/************/
int main(void)
/************/
{
  printf("This program does the absolute minimum to generate single VMEbus cycles\n");
  printf("It does not handle bus errors. For it to work you have to have initialized\n");
  printf("the VMEbus interface with `vmeconfig -i vematb` (this may be automatic)\n"); 

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  printf("Enter mode (0=classic  1=RC? debug  2=threads): ");
  mode = getdecd(1);

  if (mode == 1)
  {
    u_int indata;
    
    printf("VMEbus address       : ");
    master_map.vmebus_address = gethexd(master_map.vmebus_address);
    printf("Data                 : ");
    indata = getdecd(123);
    printf("Insert sleeps (1=yes): ");
    slow = getdecd(1);

    if (slow == 1)
      DF::GlobalDebugSettings::setup(20, 102);
    else
      DF::GlobalDebugSettings::setup(5, 102);
    
    master_map.window_size = 0x100;
    master_map.address_modifier = VME_A32;
    master_map.options = 0;
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }
    
    ret = VME_WriteSafeUInt(handle, 0, indata);
    if (ret != VME_SUCCESS) 
      VME_ErrorPrint(ret);
    while(1)
    {
      if (slow == 1)
        sleep(1);
      ret = VME_ReadSafeUInt(handle, 0, &ldata);
      if (ret != VME_SUCCESS) 
        VME_ErrorPrint(ret);
      if(ldata != indata)
        printf("Data error: Expected=0x%08x  Read=0x%08x", indata, ldata);	
    }  
  }
  
  else if (mode == 2)
  {
    pthread_t threads[2];
    int rc, t0 = 0, t1 = 1;
    
    printf("VMEbus address       : ");
    master_map.vmebus_address = gethexd(master_map.vmebus_address);
    printf("Insert sleeps (1=yes): ");
    slow = getdecd(1);

    if (slow == 1)
      DF::GlobalDebugSettings::setup(20, 102);
    else
      DF::GlobalDebugSettings::setup(5, 102);

    master_map.window_size = 0x100;
    master_map.address_modifier = VME_A32;
    master_map.options = 0;
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }

    printf("In main: creating thread\n");
    rc = pthread_create(&threads[0], NULL, cycles, (void *)&t0);
    if (rc)
    {
      printf("ERROR; return code from pthread_create() is %d\n", rc);
      exit(-1);
    }

    printf("In main: creating thread 1\n");
    rc = pthread_create(&threads[1], NULL, cycles, (void *)&t1);
    if (rc)
    {
      printf("ERROR; return code from pthread_create() is %d\n", rc);
      exit(-1);
    }

    pthread_exit(NULL);
  }
  
  else
  {
    printf("VMEbus address                        ");
    master_map.vmebus_address = gethexd(master_map.vmebus_address);
    printf("Window size                           ");
    master_map.window_size = gethexd(master_map.window_size);
    printf("Address modifier (%d=A24, %d=A32, %d=CR/CSR, %d=USER1, %d=USER2) ",VME_A24, VME_A32, VME_CRCSR, VME_USER1, VME_USER2);
    master_map.address_modifier = gethexd(master_map.address_modifier);
    master_map.options = 0;
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }

    ret = VME_MasterMapVirtualLongAddress(handle, &vbase);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }

    printf("Define the transfer direction (1=read  0=write): ");
    ddir = getdecd(ddir);

    printf("Select the transfer width (1=D8  2=D16  3=D32): "); 
    dtype = getdecd(dtype);

    if (dtype == 3) lptr = (u_int *)vbase;
    if (dtype == 2) sptr = (u_short *)vbase;
    if (dtype == 1) bptr = (u_char *)vbase;

    if (!ddir)
    {
      printf("Enter the data to write: ");
      data = gethexd(data);

      if (dtype == 1) *bptr = data & 0xff;
      if (dtype == 2) *sptr = data & 0xffff;
      if (dtype == 3) *lptr = data;
    }
    else
    {
      if (dtype == 1) {bdata = *bptr; printf("Data read = 0x%2x\n", bdata);}
      if (dtype == 2) {sdata = *sptr; printf("Data read = 0x%4x\n", sdata);}
      if (dtype == 3) {ldata = *lptr; printf("Data read = 0x%8x\n", ldata);}
    }

    ret = VME_MasterUnmap(handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }
  }
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
}
