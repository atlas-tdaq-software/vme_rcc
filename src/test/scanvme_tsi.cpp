// $Id: scanvme_tsi.cpp 91533 2010-04-30 09:38:54Z joos $
/************************************************************************/
/*  scanvme_tsi: A program by M. Joos CERN,PH/ESE/BE 			*/
/*									*/
/*  The main purpose of scanvme is to scan the whole 4 GBytes		*/
/*  of VME address space for slaves present. This is done by trying 	*/
/*  to read a char, short or int. Hence, only slaves with readable 	*/
/*  registers can be found.						*/
/*									*/
/*  8.10.10   MAJO  created						*/
/*****C 2010 - A nickel program worth a dime*****************************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "vme_rcc/tsi148.h"

#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

//prototypes
int open_tsi();
int close_tsi();
int init_tsi();
u_int bswap(u_int word);

//globals
volatile tsi148_regs_t *tsi;
int amcode = 0, space = 32, bw = 32;
unsigned long long pci_base = 0x60000000ll;
u_int vctrl;
u_long tsibase;
u_int otat1, otat2, otat3, otat4, otat5, otat6, otat7;
u_int otsau0, otsal0, oteau0, oteal0, otofu0, otofl0, otbs0, otat0;
u_int step = 0x100000, dblevel = 0, dbpackage = DFDB_VMERCC, start_offset = 0;
int handle, *pci, closeit;
  
  
/*********************/
u_int bswap(u_int word)
/*********************/
{
  return ((word & 0xFF) << 24) + ((word & 0xFF00) << 8) + ((word & 0xFF0000) >> 8) + ((word & 0xFF000000) >> 24);
}

/************/
int open_tsi()
/************/
{ 
  u_int ret;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  ret = VME_Tsi148Map(&tsibase);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  tsi = (tsi148_regs_t *)tsibase; 
  return(0);
}



/************/
int init_tsi()
/************/
{
  u_int ldata;

  //disable map decoders 1 to 7
  otat1 = tsi->otat1;
  otat2 = tsi->otat2;
  otat3 = tsi->otat3;
  otat4 = tsi->otat4;
  otat5 = tsi->otat5;
  otat6 = tsi->otat6;
  otat7 = tsi->otat7;
  tsi->otat1 = 0;
  tsi->otat2 = 0;
  tsi->otat3 = 0;
  tsi->otat4 = 0;
  tsi->otat5 = 0;
  tsi->otat6 = 0;
  tsi->otat7 = 0;

  //save map decoder 0
  otsau0 = tsi->otsau0;
  otsal0 = tsi->otsal0;
  oteau0 = tsi->oteau0;
  oteal0 = tsi->oteal0;
  otofu0 = tsi->otofu0;
  otofl0 = tsi->otofl0;
  otbs0 = tsi->otbs0;
  otat0 = tsi->otat0;
    
  //Set bus time-out to 8 us
  vctrl = bswap(tsi->vctrl);
  ldata = vctrl & 0xfffffff0;
  tsi->vctrl = bswap(ldata);
  printf("VMEbus time-out temporarily reduced to 8 us to speed up scanning process\n");
  return(0);
}


/*************/
int close_tsi()
/*************/
{
  u_int ret;
  u_int dummy[8];

  if (closeit)
  {
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
  }
  
  dummy[0] = 0; //IRQ modes are not valid

  //restore lsi0,1,2,3,4,5,6,7
  tsi->otat1 = otat1;
  tsi->otat2 = otat2;
  tsi->otat3 = otat3;
  tsi->otat4 = otat4;
  tsi->otat5 = otat5;
  tsi->otat6 = otat6;
  tsi->otat7 = otat7;
 
  tsi->otsau0 = otsau0;
  tsi->otsal0 = otsal0;
  tsi->oteau0 = oteau0;
  tsi->oteal0 = oteal0;
  tsi->otofu0 = otofu0;
  tsi->otofl0 = otofl0;
  tsi->otbs0 = otbs0;
  tsi->otat0 = otat0;
    
  //Reset bus time-out to original value
  tsi->vctrl = vctrl;

  // Update the driver
  ret = VME_Update(dummy);
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }  

  ret = VME_Tsi148Unmap(tsibase);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  return(0);
}


/*****************************/
void SigHandler(int /*signum*/)
/*****************************/
{
  printf("Aaarrrgghhhhh....\n");
  close_tsi();
  exit(-1);
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-h  : Display this help text" << std::endl;
  std::cout << "-s x: Step size                          -> Default: 0x" << std::hex << step << std::dec << std::endl;
  std::cout << "-o x: Start offset                       -> Default: 0x" << std::hex << start_offset << std::dec << std::endl;
  std::cout << "-a x: Address width                      -> Default: " << space << std::endl;
  std::cout << "       0 = CR/CSR" << std::endl;
  std::cout << "      16 = A16" << std::endl;
  std::cout << "      24 = A24" << std::endl;
  std::cout << "      32 = A32" << std::endl;
  std::cout << "      99 = USER1" << std::endl;
  std::cout << "-d x: Data width                         -> Default: " << bw << std::endl;
  std::cout << "       8 = D8" << std::endl;
  std::cout << "      16 = D16" << std::endl;
  std::cout << "      32 = D32" << std::endl;  
  std::cout << "-p x: PCI base (for the map decoder)     -> Default: 0x" << std::hex << pci_base << std::dec << std::endl;
  std::cout << "-c x: AM code                            -> Default: " << amcode << std::endl;
  std::cout << "      0 = User" << std::endl;
  std::cout << "      1 = Supervisor" << std::endl;
  std::cout << "-l x: Debug level                        -> Default: " << dblevel << std::endl;
  std::cout << "-D x: Debug package                      -> Default: " << dbpackage << std::endl;
  std::cout << std::endl;
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  int c, stat;
  u_int ret, ii;
  u_char cdata;
  u_short sdata;
  u_int scan_offset, idata;
  u_int loop, dummy[8];
  struct sigaction sa;
  VME_MasterMap_t master_map;
  unsigned long long pci_offset;
  
  
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigHandler;
  stat = sigaction(SIGINT, &sa, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", stat);
    exit(-1);
  }

  dummy[0] = 0; //IRQ modes are not valid
  
  static struct option long_options[] = {{"DVS", no_argument, NULL, '1'}}; 
  while ((c = getopt_long(argc, argv, "hs:o:a:d:p:c:l:D:", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(-1);
      break;

    case 's': sscanf(optarg, "%x", &step);         break;
    case 'o': sscanf(optarg, "%x", &start_offset); break;
    case 'a': space = atoi(optarg);                break;
    case 'd': bw = atoi(optarg);                   break;
    case 'p': sscanf(optarg, "%llx", &pci_base);   break;
    case 'c': amcode = atoi(optarg);               break;
    case 'l': dblevel = atoi(optarg);              break;
    case 'D': dbpackage = atoi(optarg);            break;
    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (-1);
    }
    
  if (space != 0 && space != 16 && space != 24 && space != 32 && space != 99) 
    space = 32;
  if (bw != 8 && bw != 16 && bw != 32) 
    bw = 8;
  if (amcode != 0 && amcode != 1)
    amcode = 0;
  if (step > 0x01000000)
    step = 0x01000000;

  DF::GlobalDebugSettings::setup(dblevel, dbpackage);

  printf("==============================\n");
  printf("step size        = 0x%08x\n", step);
  printf("space            = A%d\n", space);
  printf("bus width        = D%d\n", bw);
  printf("AM code          = %s\n", (amcode == 1)?"User":"Data");
  printf("==============================\n");
  printf("PCI base address = 0x%016llx\n", pci_base);
  printf("------------------------------\n\n");

  open_tsi();
  init_tsi();
  closeit = 1;
 
  if (space == 0)  //CR/CSR
  {
    // Generate a mapping
    pci_offset = (0xffffffffffffffffll - pci_base) + 1;
    if (dblevel == 20)
      printf("pci_offset = 0x%016llx\n", pci_offset);

    tsi->otsau0 = bswap(pci_base >> 32);
    tsi->otsal0 = bswap(pci_base & 0xffffffff);
    tsi->oteau0 = bswap((pci_base + 0x01000000) >> 32);
    tsi->oteal0 = bswap((pci_base + 0x01000000) & 0xffffffff);
    tsi->otofu0 = bswap(pci_offset >> 32);
    tsi->otofl0 = bswap(pci_offset & 0xffffffff);
    tsi->otbs0 = 0;
    tsi->otat0 =  bswap(0x80000045);
       
    // Update the driver
    ret = VME_Update(dummy);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    // Attach process to the mapping
    master_map.vmebus_address  = 0x0;
    master_map.window_size = 0x01000000;
    master_map.address_modifier = VME_CRCSR;
    master_map.options = 0;
    
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    for (ii = start_offset; ii < 0x1000000; ii += step)
    {
      if (bw == 8)
        ret = VME_ReadSafeUChar(handle, ii+3, &cdata);
      if (bw == 16)
        ret = VME_ReadSafeUShort(handle, ii, &sdata);
      if (bw == 32)
        ret = VME_ReadSafeUInt(handle, ii, &idata);
        
      if (!ret) 
        printf("Got response from address 0x%06x\n", ii);
    }
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
    closeit = 0;
  }
 
 
  if (space == 16)  //A16
  {
    // Generate a mapping

    pci_offset = (0xffffffffffffffffll - pci_base) + 1;
    if (dblevel == 20)
      printf("pci_offset = 0x%016llx\n", pci_offset);

    tsi->otsau0 = bswap(pci_base >> 32);
    tsi->otsal0 = bswap(pci_base & 0xffffffff);
    tsi->oteau0 = bswap((pci_base + 0x01000000) >> 32);
    tsi->oteal0 = bswap((pci_base + 0x01000000) & 0xffffffff);
    tsi->otofu0 = bswap(pci_offset >> 32);
    tsi->otofl0 = bswap(pci_offset & 0xffffffff);
    tsi->otbs0 = 0;
    
    if (amcode == 0)
      tsi->otat0 =  bswap(0x80000040);
    else
      tsi->otat0 =  bswap(0x80000060);
    
    // Update the driver
    ret = VME_Update(dummy);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    // Attach process to the mapping
    master_map.vmebus_address  = 0x0;
    master_map.window_size = 0x01000000;
    master_map.address_modifier = VME_A16;
    if (amcode == 0) master_map.options = 0;
    if (amcode == 1) master_map.options = VME_AM_SUPERVISOR;
    
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    for (ii = start_offset; ii < 0x10000; ii += step)
    {      
      if (bw == 8)
        ret = VME_ReadSafeUChar(handle, ii, &cdata);
      if (bw == 16)
        ret = VME_ReadSafeUShort(handle, ii, &sdata);
      if (bw == 32)
        ret = VME_ReadSafeUInt(handle, ii, &idata);
        
      if (!ret) 
        printf("Got response from address 0x%06x\n", ii);

    }
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
    closeit = 0;
  }

  if (space == 24)  //A24
  {
    pci_offset = (0xffffffffffffffffll - pci_base) + 1;
    if (dblevel == 20)
      printf("pci_offset = 0x%016llx\n", pci_offset);

    tsi->otsau0 = bswap(pci_base >> 32);
    tsi->otsal0 = bswap(pci_base & 0xffffffff);
    tsi->oteau0 = bswap((pci_base + 0x01000000) >> 32);
    tsi->oteal0 = bswap((pci_base + 0x01000000) & 0xffffffff);
    tsi->otofu0 = bswap(pci_offset >> 32);
    tsi->otofl0 = bswap(pci_offset & 0xffffffff);
    tsi->otbs0 = 0;
    
    if (amcode == 0)
      tsi->otat0 =  bswap(0x80000041);
    else
      tsi->otat0 =  bswap(0x80000061);
    
    // Update the driver
    ret = VME_Update(dummy);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
    
    // Attach process to the mapping
    master_map.vmebus_address  = 0x0;
    master_map.window_size = 0x01000000;
    master_map.address_modifier = VME_A24;
    if (amcode == 0) master_map.options = 0;
    if (amcode == 1) master_map.options = VME_AM_SUPERVISOR;
    
    ret = VME_MasterMap(&master_map, &handle);
    if (ret)
    {
      VME_ErrorPrint(ret);
      exit(-1);
    }  
   
    for (ii = start_offset; ii < 0x1000000; ii += step)
    {      
      if (bw == 8)
        ret = VME_ReadSafeUChar(handle, ii, &cdata);
      if (bw == 16)
        ret = VME_ReadSafeUShort(handle, ii, &sdata);
      if (bw == 32)
        ret = VME_ReadSafeUInt(handle, ii, &idata);
        
      if (!ret) 
        printf("Got response from address 0x%06x\n", ii);
    }
    ret = VME_MasterUnmap(handle);
    if (ret)
      VME_ErrorPrint(ret);
    closeit = 0;
  }

  if (space == 32)  //A32
  {
    for(loop = 0; loop < 0x100; loop++)
    {
      if ((start_offset >> 24) > loop)    
        continue;

      pci_offset = (0xffffffffffffffffll - pci_base) + 1 + (loop << 24);
      if (dblevel == 20)
        printf("pci_offset = 0x%016llx\n", pci_offset);

      tsi->otsau0 = bswap(pci_base >> 32);
      tsi->otsal0 = bswap(pci_base & 0xffffffff);
      tsi->oteau0 = bswap((pci_base + 0x01000000) >> 32);
      tsi->oteal0 = bswap((pci_base + 0x01000000) & 0xffffffff);
      tsi->otofu0 = bswap(pci_offset >> 32);
      tsi->otofl0 = bswap(pci_offset & 0xffffffff);
      tsi->otbs0 = 0;

      if (amcode == 0)
	tsi->otat0 =  bswap(0x80000042);
      else
	tsi->otat0 =  bswap(0x80000062);

      // Update the driver
      ret = VME_Update(dummy);
      if (ret)
      {
        VME_ErrorPrint(ret);
        exit(-1);
      }  

      // Attach process to the mapping
      master_map.vmebus_address  = loop << 24;
      master_map.window_size = 0x01000000;
      master_map.address_modifier = VME_A32;
      if (amcode == 0) master_map.options = 0;
      if (amcode == 1) master_map.options = VME_AM_SUPERVISOR;

      ret = VME_MasterMap(&master_map, &handle);
      if (ret)
      {
        VME_ErrorPrint(ret);
        exit(-1);
      }  

      scan_offset = 0;
      if ((start_offset >> 24) == loop)    
        scan_offset = start_offset & 0x00ffffff;
	
      for (ii = scan_offset; ii < 0x1000000; ii += step)
      {
        if (bw == 8)
          ret = VME_ReadSafeUChar(handle, ii, &cdata);
        if (bw == 16)
          ret = VME_ReadSafeUShort(handle, ii, &sdata);
        if (bw == 32)
          ret = VME_ReadSafeUInt(handle, ii, &idata);

        if (!ret) 
          printf("Got response from address 0x%08x\n", ii + (loop << 24));
      }    
      ret = VME_MasterUnmap(handle);
      if (ret)
        VME_ErrorPrint(ret);
      closeit = 0;
    }
  }

  if (space == 99)  //USER1
  {
    for(loop = 0; loop < 0x100; loop++)
    {
      if ((start_offset >> 24) > loop)    
        continue;

      pci_offset = (0xffffffffffffffffll - pci_base) + 1 + (loop << 24);
      if (dblevel == 20)
        printf("pci_offset = 0x%016llx\n", pci_offset);

      tsi->otsau0 = bswap(pci_base >> 32);
      tsi->otsal0 = bswap(pci_base & 0xffffffff);
      tsi->oteau0 = bswap((pci_base + 0x01000000) >> 32);
      tsi->oteal0 = bswap((pci_base + 0x01000000) & 0xffffffff);
      tsi->otofu0 = bswap(pci_offset >> 32);
      tsi->otofl0 = bswap(pci_offset & 0xffffffff);
      tsi->otbs0 = 0;

      if (amcode == 0)
	tsi->otat0 =  bswap(0x80000048);
      else
	tsi->otat0 =  bswap(0x80000068);

      // Update the driver
      ret = VME_Update(dummy);
      if (ret)
      {
        VME_ErrorPrint(ret);
        exit(-1);
      }  

      // Attach process to the mapping
      master_map.vmebus_address  = loop << 24;
      master_map.window_size = 0x01000000;
      master_map.address_modifier = 8;
      if (amcode == 0) master_map.options = 0;
      if (amcode == 1) master_map.options = VME_AM_SUPERVISOR;

      ret = VME_MasterMap(&master_map, &handle);
      if (ret)
      {
        VME_ErrorPrint(ret);
        exit(-1);
      }  

      scan_offset = 0;
      if ((start_offset >> 24) == loop)    
        scan_offset = start_offset & 0x00ffffff;
	
      for (ii = scan_offset; ii < 0x1000000; ii += step)
      {
        if (bw == 8)
          ret = VME_ReadSafeUChar(handle, ii, &cdata);
        if (bw == 16)
          ret = VME_ReadSafeUShort(handle, ii, &sdata);
        if (bw == 32)
          ret = VME_ReadSafeUInt(handle, ii, &idata);

        if (!ret) 
          printf("Got response from address 0x%08x\n", ii + (loop << 24));
      }    
      ret = VME_MasterUnmap(handle);
      if (ret)
        VME_ErrorPrint(ret);
      closeit = 0;
    }
  }
  close_tsi();
  exit(0);
}

