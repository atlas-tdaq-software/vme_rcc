#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <tmgr/tmresult.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "io_rcc/io_rcc.h"

using namespace daq::tmgr;

//Macros
#define ISOK if (ret) {rcc_error_print(stdout, ret); exit(TmUnresolved);}

// general defines
#define TRUE                            1
#define FALSE                           0
#define PCI_CFG_COMMAND                 0x04

// Max1617a Defines
#define MX_ADDR				0x18		// Max1617a Address (CPU)
#define MX_ADDR2			0x1a		// Max1617a Address (battery)
#define MX_ADDR_315			0x52		// Max6656 Address (CPU)
#define MX_ADDR2_315			0x53		// Max6656 Address (battery)

#define MX_CMD_RLTS			0x00		// Read Local Temp 
#define MX_CMD_RRTE			0x01		// Read Remote Temp 
#define MX_CMD_RSL			0x02		// Read Status 
#define MX_CMD_RCL			0x03		// Read Config 
#define MX_CMD_RCRA			0x04		// Read Conversion Rate 
#define MX_CMD_RLHN			0x05		// Read Local T-High 
#define MX_CMD_RLLI			0x06		// Read Local T-Low 
#define MX_CMD_RRHI			0x07		// Read Remote T-High 
#define MX_CMD_RRLS			0x08		// Read Local T-Low/

// SMBus
#define SMB_HST_STS			0x00		// SMBus register offsets 
#define SMB_HST_CNT			0x02
#define SMB_HST_CMD			0x03
#define SMB_HST_ADD			0x04
#define SMB_HST_DAT0			0x05
#define SMB_HST_DAT1			0x06
#define PM_BASE_ADDR			0x40		// Power Mngmnt config space, base address 
#define PM_REG_MISC			0x80		// Power Mngmnt config space, I/O enable 
#define SMB_BASE_ADDR			0x90		// SMBus config space, base address 
#define SMB_BASE_ADDR_315		0x20		// SMBus config space, base address 
#define SMB_BDATA			0x08		// Data xfr - Byte
#define SMB_WDATA			0x0C		// Data xfr - Word

// Errors
#define ERROR              	        -1
#define E_OK                    	0
#define E__REMOTE_OPEN			0x421		// Remote sensor: open circut 
#define E__REMOTE_SHORT			0x422		// Remote sensor: short circut OR Remote sensor: 0^C 
#define E__REMOTE_VCC			0x423		// Remote sensor: Connected to Vcc OR Remote & Local sensors: 127^C 

//CCT special registers
#define CMOSA                           0x70
#define CMOSD                           0x71
#define BID1                            0x35
#define BID2                            0x36

// CCT Definitions for VP417 
#define SMBUS_BAR                       0x8000fb20
#define SMBUS_CTRL_REG                  0xcf8
#define SMBUS_DATA_REG                  0xcfc
#define HOST_STATUS_REG                 adt7461_base
#define HOST_CTRL_REG                   adt7461_base + 2
#define HOST_CMD_REG                    adt7461_base + 3
#define HOST_ADDRESS_REG                adt7461_base + 4
#define HOST_DATA_REG                   adt7461_base + 5
#define ADT7461_ADDR                    0x98               // ADT7461 I2C address (0x4C << 1
#define ADT7461_EXT_OFFSET              64                 // ADT7461 temperature range offset. BIOS programs ADT7461 in 'extended' mode for negative temperature support
#define ADT7461_LT                      0x00
#define ADT7461_RT_H                    0x01
#define ADT7461_RT_L                    0x10
#define ADT7461_RT_OFFSET_H             0x11
#define ADT7461_RT_OFFSET_L             0x12
#define ADT7461_RMID                    0xfe
#define ADT7461_RDR                     0xff


// Globals
static u_short wSmbusBase;				// base address of SMBus controller regs 
static u_short wPmGpoBase;				// base address of power management 
u_int btype, smb_base, mxaddr, mxaddr2, dvs_mode, handle;
int adt7461_base = 0, temperature, tmax = 0;

// Function/procedure prototypes 
void vSMBusInit(void);
void vSMBusClose(void);
int wSMBusHRead	(u_char bAddr, u_char bCmd, u_short *pwData, u_char bType, u_short wTimeout);
int wSMBusHWrite(u_char bAddr, u_char bCmd, u_short *pwData, u_char bType, u_short wTimeout);
int wSMBusExec(u_short wTimeout);
int wMxReadData(int *mt);
int wMxFaultCheck(void);
int check_board_type(void);
int adt7461_get_base_addr(void);
int adt7461_write_reg(int reg, int value);
int adt7461_read_reg(int reg, int *value);


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-m x   : specify the max. allowed temperature. Default = 0 deg.C" << std::endl;
  std::cout << "--DVS  : Run in DVS mode" << std::endl;
  std::cout << std::endl; 
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  u_int ret;
  int manuf = 0, stepping = 0, rc, local_temp = 0, remote_temp_h = 0, tmp, remote_temp_l = 0;
  int c, wErr, maxtemp;
  static struct option long_options[] = {"DVS", no_argument, NULL, '1'};
  const char *filename = "/proc/vme_rcc";

  dvs_mode = 0;
  while ((c = getopt_long(argc, argv, "m:1", long_options, NULL)) != -1)
  {
    switch (c) 
    {
      case '1':
	dvs_mode = 1;
	break;

      case 'm':
        tmax = atoi(optarg);
        break;

      default:
        std::cout << "Invalid option " << c << std::endl;
        std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
        usage();
        exit (TmUnresolved);
    }
  } 
     
  if (dvs_mode)
  {
    if (access(filename, F_OK) == -1 )
      switch (errno)
      {
        case ENOENT:
          std::cerr << "The machine is most likely not a VP110/VP315/VP317. The file " << filename << " does not exist." << std::endl;
          exit(TmPass);
    	default:
          std::cerr << "Error:: Got '" << strerror(errno) << "' error trying to access file "<< filename << "." << std::endl;
          exit(TmUnresolved);
       }
    else 
       std::cout << "The machine is probably a VP110/VP315/VP317" << std::endl;
  } 
  
  ret = IO_Open();
  ISOK

  wErr = check_board_type();
  if (wErr)
  {
    std::cout << "The board you are running this program on is not a VP110/VP315/VP317/VP417" << std::endl;
    exit (TmUnresolved);
  }

  if(btype == 3)
  {
    adt7461_base = adt7461_get_base_addr();        //Find base address of SMBUS Controller

    rc = adt7461_read_reg(ADT7461_RMID, &manuf);
    if(!rc)
      std::cout << "Manufacturer ID 0x" << HEX(manuf) << std::endl;
    else
    {
      std::cout << "Error returned from adt7461_read_reg" << std::endl;
      exit (TmUnresolved);
    }

    rc = adt7461_read_reg(ADT7461_RDR, &stepping);
    if(!rc)
      std::cout << "Stepping 0x" << HEX(stepping) << std::endl;
    else
    {
      std::cout << "Error returned from adt7461_read_reg" << std::endl;
      exit (TmUnresolved);
    }

    rc = adt7461_read_reg(ADT7461_LT, &local_temp);
    if(!rc)
      std::cout << "PCB (local) temp = " << local_temp - ADT7461_EXT_OFFSET << " C" << std::endl;
    else
    {
      std::cout << "Error returned from adt7461_read_reg" << std::endl;
      exit (TmUnresolved);
    }

    tmax = local_temp - ADT7461_EXT_OFFSET;

    // Check offset, should be set by BIOS from CPU Dothan thermal diode register
    rc = 0;
    rc += adt7461_read_reg(ADT7461_RT_OFFSET_H, &remote_temp_h);
    rc += adt7461_read_reg(ADT7461_RT_OFFSET_L, &remote_temp_l);

    if(!rc)
      if((remote_temp_h) || (remote_temp_l))
	std::cout << "Thermal diode correction " << remote_temp_h << " / " << remote_temp_l << " applied" << std::endl;

    rc = 0;
    rc += adt7461_read_reg(ADT7461_RT_H, &remote_temp_h);
    rc += adt7461_read_reg(ADT7461_RT_L, &remote_temp_l);
    if(!rc)
    {
      // Only the top 2 bits of 'low' byte are used. This gives a .25 degC resolution to the fractional part of the remote temperature
      tmp = (remote_temp_l >> 6) * 25;
      std::cout << "CPU (remote) temp " << remote_temp_h - ADT7461_EXT_OFFSET << "." << tmp << " C" << std::endl;
    }
    else
    {
      std::cout << "Error returned from adt7461_read_reg" << std::endl;
      exit (TmUnresolved);
    }

    temperature = remote_temp_h - ADT7461_EXT_OFFSET;
    if (temperature > tmax)
      tmax = temperature;
  }
  else
  {
    vSMBusInit();

    wErr = wMxFaultCheck();
    switch (wErr)
    {
      case E_OK:
	printf("Diagnostics passed.\n");
	break;
      case E__REMOTE_OPEN:
	printf("Warning: CPU diode is open-circuit\n");
	break;
      case E__REMOTE_SHORT:
	printf("Warning: CPU diode may be short circuited\n");
	break;
      case E__REMOTE_VCC:
	printf("Warning: CPU diode may be connected to Vcc\n");
	break;
      default:
       printf("Warning: unknown error\n");
       //MJ: disabled  return (TmUnresolved);
    }

    wErr = wMxReadData(&maxtemp);
    if (wErr != E_OK)
    { 
      printf("Error received from wMxReadData()\n");
      return (TmUnresolved);
    }

    vSMBusClose();     
  }
  
  ret = IO_Close();
  ISOK

  if (dvs_mode)
  {
    std::cout << "tmax = " << tmax << std::endl;
    if (maxtemp > tmax)
      exit(TmFail);
    else
      exit(TmPass);
  }
  else
  {
    exit(maxtemp);
  }
}


/*****************************/
int adt7461_get_base_addr(void)
/*****************************/
{
  u_int ret, value;
  
  ret = IO_IOPokeUInt(SMBUS_CTRL_REG, SMBUS_BAR);    //Read base address value from register
  ISOK
   
  ret = IO_IOPeekUInt(SMBUS_DATA_REG, &value);  
  ISOK

  value &= 0xfffffffe;                               //Remove I/O type bit

  std::cout << "ADT7461 base address is 0x" << HEX(value) << std::endl;

  return value;
}


/***************************************/
int adt7461_write_reg(int reg, int value)
/***************************************/
{
  u_int ret;
  u_char tmp;
  int retval = 0, done = 0;

  ret = IO_IOPokeUChar(HOST_STATUS_REG, 0x1e);           //Clear status bits 
  ISOK

  ret = IO_IOPokeUChar(HOST_CMD_REG, reg);               //Offset to write
  ISOK

  ret = IO_IOPokeUChar(HOST_ADDRESS_REG, ADT7461_ADDR);  //Address to write
  ISOK

  ret = IO_IOPokeUChar(HOST_DATA_REG, value);            //Data to write
  ISOK

  ret = IO_IOPokeUChar(HOST_CTRL_REG, 0x48);             //Byte protocol & start command
  ISOK
  
  while(!done)
  {
    ret = IO_IOPeekUChar(HOST_STATUS_REG, &tmp);         //Read host status
    ISOK
    
    if(tmp & 0x1e)
    {
      ret = IO_IOPokeUChar(HOST_STATUS_REG, tmp);        //Clear status bits
      ISOK

      if(tmp & 2)                                        //The status bits tell us if the write is done.
        retval = 0;                                      //Write success
      else
      {
        std::cout << "Error: 0x" << HEX(tmp) << std::endl;
	retval = -1;
      }
      done = 1;
    }
  }
  return retval;
}


/***************************************/
int adt7461_read_reg(int reg, int *value)
/***************************************/
{
  u_int ret;
  u_char tmp;
  int retval = 0, done = 0;

  ret = IO_IOPokeUChar(HOST_STATUS_REG, 0x1e);       //Clear status bits
  ISOK

  ret = IO_IOPokeUChar(HOST_CMD_REG, reg);           //Offset to read
  ISOK

  tmp = ADT7461_ADDR | 0x01;   // & set 'read' bit
  ret = IO_IOPokeUChar(HOST_ADDRESS_REG, tmp);       //Address to read
  ISOK

  ret = IO_IOPokeUChar(HOST_CTRL_REG, 0x48);         //Byte protocol & start command
  ISOK

  while(!done)
  {
    ret = IO_IOPeekUChar(HOST_STATUS_REG, &tmp);     //Read host status
    ISOK
    if(tmp & 0x1e)
    {
      ret = IO_IOPokeUChar(HOST_STATUS_REG, tmp);    //Clear status bits
      ISOK

      if(tmp & 2)                                    //The status bits give us a clue as to whether the data can be read
      {
	ret = IO_IOPeekUChar(HOST_DATA_REG, &tmp);   //Read data
	ISOK
	 *value = tmp;
      }
      else
      {
        std::cout << "Error: 0x" << HEX(tmp) << std::endl;
        retval = -1;
      }
      done = 1;
    }
  }
  return retval;
}


/*******************/
void vSMBusInit(void)
/*******************/
{
  u_int ret;
  u_short wTemp;
    
  if (btype == 1)
    ret = IO_PCIDeviceLink(0x1166, 0x0201, 1, &handle);
  else
    ret = IO_PCIDeviceLink(0x8086, 0x25a4, 1, &handle);
  ISOK
  
  // Get the SMBus base address 
  ret = IO_PCIConfigReadUShort(handle, smb_base, &wSmbusBase);
  ISOK
  wSmbusBase &= 0xFFFE;

  // Get power management GPO address 
  ret = IO_PCIConfigReadUShort(handle, PM_BASE_ADDR, &wPmGpoBase);
  ISOK
  wPmGpoBase = (wPmGpoBase & 0xFFFE) + 0x34;

  // Enable SMBus registers in PIIX4 
  ret = IO_PCIConfigReadUShort(handle, PCI_CFG_COMMAND, &wTemp);
  ISOK
    
  ret = IO_PCIConfigWriteUShort(handle, PCI_CFG_COMMAND, wTemp | 0x1);
  ISOK

  // Enable PM I/O registers in PIIX4 
  if(btype == 1)
  {
    ret = IO_PCIConfigReadUShort(handle, PM_REG_MISC, &wTemp);
    ISOK

    ret = IO_PCIConfigWriteUShort(handle, PM_REG_MISC, wTemp | 0x1);
    ISOK
  }
  else
  {    
    ret = IO_PCIConfigReadUShort(handle, 0x40, &wTemp);
    ISOK

    ret = IO_PCIConfigWriteUShort(handle, 0x40, wTemp | 0x1);
    ISOK
  }

  return;
}


/********************/
void vSMBusClose(void)
/********************/
{
  u_int ret;
  
  ret = IO_PCIDeviceUnlink(handle);
  ISOK
}


/*********************/
int wMxFaultCheck(void)
/*********************/
{
  u_short wTemp;
  int wErr;
  
  //Check fault conditions on max 1617a

  wErr = wSMBusHRead(mxaddr, MX_CMD_RSL, &wTemp, SMB_BDATA, 1000);
  if (wErr != E_OK)
  {
    printf("wMxFaultCheck: error 1\n");
    return (wErr);
  }  
  
  if (wTemp & 0x02)
  {
    printf("wMxFaultCheck: error 2\n");
    return (E__REMOTE_OPEN);
  }  
		
  wErr = wSMBusHRead(mxaddr, MX_CMD_RRTE, &wTemp, SMB_BDATA, 1000);
  if (wErr != E_OK) 
  {
    printf("wMxFaultCheck: error 3\n");
    return (wErr);
  }  
  if (wTemp == 0)
  {
    printf("wMxFaultCheck: error 6\n");
    return (E__REMOTE_SHORT);
  }  
  else if (wTemp == 127)
  {
    wErr = wSMBusHRead(mxaddr, MX_CMD_RLTS, &wTemp, SMB_BDATA, 1000);
    if (wErr != E_OK)
    { 
     printf("wMxFaultCheck: error 4\n");
     return (wErr);
    }  
    if (wTemp == 127)
    {
      printf("wMxFaultCheck: error 5\n");
      return(E__REMOTE_VCC);
    }  
  }
  return (E_OK);
}



/*****************************************************************************************/
int wSMBusHRead(u_char bAddr, u_char bCmd, u_short *pwData, u_char bType, u_short wTimeout)
/*****************************************************************************************/
//bAddr:	7 bit address
//bCmd:		Command to send
//pwData:	For Data read
//bType:	Type of xfr  Ie. SMB_BDATA or SMB_WDATA
//wTimeout:	Timeout in 100mS blocks
{
  int ret, wStatus;
  u_char bTemp;

  //Read data from a HOST device on the SMBus
  if (bAddr > 0x7F)    // Bad address? 
  {
    printf("wSMBusHRead - Error: bAddr = 0x%08x\n", bAddr); 
    return ERROR;      //(E__SMB_BAD_ADDR);
  }
  
  /*** set up the registers ***/
  ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_STS, &bTemp);      // clear status bits 
  ISOK

  ret = IO_IOPokeUChar(wSmbusBase + SMB_HST_STS, bTemp | 0x1E);
  ISOK

  ret = IO_IOPokeUChar(wSmbusBase + SMB_HST_CNT, bType);	// Set mode 
  ISOK

  if (btype == 1)
    ret = IO_IOPokeUChar(wSmbusBase + SMB_HST_ADD, (bAddr << 1)+1);	// Read from device 
  else
    ret = IO_IOPokeUChar(wSmbusBase + SMB_HST_ADD, (bAddr | 1));	// Read from device 
  ISOK

  ret = IO_IOPokeUChar(wSmbusBase + SMB_HST_CMD, bCmd);		// Command to send 
  ISOK

  wStatus = wSMBusExec(wTimeout);				// Execute command 
  if (wStatus == E_OK)
  {
    if (bType == SMB_WDATA)					// Word of data 
    {
      ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_DAT1, &bTemp);  // Read MSB 
      ISOK
      *pwData = (u_short) bTemp << 8;				// Move to MSB 
      ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_DAT0, &bTemp);  // Read LSB 
      ISOK
      *pwData += bTemp;
    }
    else if (bType == SMB_BDATA)				// Byte of data 
    {
      ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_DAT0, &bTemp);  // Read LSB 
      ISOK
      *pwData = bTemp;
    }
    else							// Fall over! 
      wStatus = ERROR;//E__SMB_BAD_TYPE;
  }
  return (wStatus);
} 


/******************************/
int wSMBusExec(u_short wTimeout)
/******************************/
// wTimeout: Timeout in 100mS blocks
{
  u_char bTemp;
  int ret;
 
  //  Execute current queued command
  ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_CNT, &bTemp); // Start process 
  ISOK

  ret = IO_IOPokeUChar(wSmbusBase + SMB_HST_CNT, bTemp | 0x40);
  ISOK

  /*** Wait for a termination event:  Success, Fail, Timeout ***/
  do 
  {
    usleep(100000);					// 100ms delay 
    wTimeout--;
    ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_STS, &bTemp); // Get status 
    ISOK
  } while ((wTimeout > 0) && ((bTemp & 0x1E) == 0));	// While !Timeout and !Finished 

  /*** Deal with the outcome ***/
  if (wTimeout == 0)					// It timed out 
  {
    ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_CNT, &bTemp);  // Cause process to 'FAIL' 
    ISOK
  
    ret = IO_IOPokeUChar(wSmbusBase + SMB_HST_CNT, bTemp | 0x02);
    ISOK
   
    printf("Timeout\n");
    return ERROR; //(E__TIMEOUT);
  }
  else							// It terminated 
  {
    ret = IO_IOPeekUChar(wSmbusBase + SMB_HST_STS, &bTemp);    // Get termination condition 
    ISOK
    switch(bTemp & 0x1e)  //0x1e mask Proposed by CCT
    {
      case 0x02:  return E_OK;
      case 0x04:  printf("E__SMB_READ_DERR\n"); return ERROR; //(E__SMB_READ_DERR);
      case 0x08:  printf("E__SMB_READ_CLSN\n"); return ERROR; //(E__SMB_READ_CLSN);
      case 0x10:  printf("E__SMB_READ_FAIL\n"); return ERROR; //(E__SMB_READ_FAIL);
    }
    return ERROR;					// Catch all 
  }
  return(E_OK);
}


/**********************/
int wMxReadData(int *mt)
/**********************/
{
  u_char bTemp1, bTemp2, bTemp3;
  u_short wErr;
  u_short wRTemp;	// Remote Temp 
  u_short wLTemp;	// Local Temp  
  u_short wLHi;		// Local Hi    
  u_short wLLo;		//   ~   Lo    
  u_short wRHi;		// Remote Hi   
  u_short wRLo;		//   ~    Lo   
  u_short wCnv;		// Conv. rate  
  u_short wCfg;		// Config reg. 
  u_short wSts;		// Status reg. 
  int t1, t2, t3;

  wErr = wSMBusHRead (mxaddr, MX_CMD_RLHN, &wLHi, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RLLI, &wLLo, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RRHI, &wRHi, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RRLS, &wRLo, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RCRA, &wCnv, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RCL, &wCfg, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RSL, &wSts, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RRTE, &wRTemp, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);
  wErr = wSMBusHRead (mxaddr, MX_CMD_RLTS, &wLTemp, SMB_BDATA, 1000);
  if (wErr != E_OK) return (wErr);

  bTemp1 = (wCfg & 0x80) >> 7;		// Alert 
  bTemp2 = (wCfg & 0x40) >> 6;		// standby 
  printf("ALERT mask   : %d          S/w standby  : %d\n", bTemp1, bTemp2);

  bTemp1 = (wSts & 0x80) >> 7;		// Busy 
  switch (wCnv)
  {
    case 0:	
      printf("Conv. rate   : 0.0625Hz   Chip busy    : %d\n", bTemp1);
      break;
    case 1:	
      printf("Conv. rate   : 0.125Hz    Chip busy    : %d\n", bTemp1);
      break;
    case 2:	
      printf("Conv. rate   : 0.25Hz     Chip busy    : %d\n", bTemp1);
      break;
    case 3:	
      printf("Conv. rate   : 0.5Hz      Chip busy    : %d\n", bTemp1);
      break;
    case 4:	
      printf("Conv. rate   : 1Hz        Chip busy    : %d\n", bTemp1);
      break;
    case 5:	
      printf("Conv. rate   : 2Hz        Chip busy    : %d\n", bTemp1);
      break;
    case 6:	
      printf("Conv. rate   : 4Hz        Chip busy    : %d\n", bTemp1);
      break;
    case 7:	
      printf("Conv. rate   : 8Hz        Chip busy    : %d\n", bTemp1);
      break;
  }
	
  bTemp1 = (wCfg & 0x04) >> 2;		// Open 
  bTemp2 = '0';				// Short 
  bTemp3 = '0';				// Vcc 
  if (wRTemp==0)
    bTemp2 = '?';
  else if (wRTemp==127)
  {
    if (wLTemp==127)
      bTemp3 = '?';
  }
  printf("CPU OPEN     : %d          CPU SHORT    : %c      CPU Vcc    : %c\n", bTemp1, bTemp2, bTemp3);
	
  bTemp1 = ' ';				// Local "-" 
  bTemp2 = ' ';				// Remote "-" 
  if (wLTemp > 127)
  {	
    wLTemp -= 128;
    bTemp1 = '-';
  }
  if (wRTemp > 127)
  {	
    wRTemp -= 128;
    bTemp2 = '-';
  }
  t1 = wLTemp;
  t2 = wRTemp;

  if (btype == 1)
  {
    printf("Temperature at bottom of card        : %c%3d^C\n", bTemp1, wLTemp); 
    printf("CPU temperature                      : %c%3d^C\n", bTemp2, wRTemp);
  
    wErr = wSMBusHRead (mxaddr2, MX_CMD_RLTS, &wLTemp, SMB_BDATA, 1000);
    if (wErr != E_OK) return (wErr);
    bTemp1 = ' ';				// Local "-" 
    if (wLTemp > 127)
    {	
      wLTemp -= 128;
      bTemp1 = '-';
    }
    t3 = wLTemp;
    printf("Temperature at top of card           : %c%3d^C\n", bTemp1, wLTemp);
  }
  else
  {
    printf("Board temperature        : %c%3d^C\n", bTemp1, wLTemp); 
    printf("CPU temperature          : %c%3d^C\n", bTemp2, wRTemp);
  }

  // alarms 
  bTemp1 = FALSE;
  printf("Alarms triggered:\n");
  if (wSts & 0x40)				// LH 
  {
    printf("*** Ambient High Threshold ***\n");
    bTemp1 = TRUE;
  }
  if (wSts & 0x20)				// LL 
  {
    printf("*** Ambient Low Threshold ***\n");
    bTemp1 = TRUE;
  }
  if (wSts & 0x10)				// RH 
  {
    printf("*** CPU High Threshold ***\n");
    bTemp1 = TRUE;
  }
  if (wSts & 0x08)				// RL 
  {
    printf("*** CPU Low Threshold ***\n");
    bTemp1 = TRUE;
  }
  if (bTemp1 == FALSE)
    printf("None\n");


  if ((t1 > t2) && (t1 > t3))
    *mt = t1;
  else if (t2 > t3)
    *mt = t2;
  else
    *mt = t3;  

  return(E_OK);

} // wMxReadData() 


/************************/
int check_board_type(void)
/************************/
{
  int ret;
  u_char d1;

  // Read the board identification
  ret = IO_IOPokeUChar(CMOSA, BID1);
  ISOK

  ret = IO_IOPeekUChar(CMOSD, &d1);
  ISOK

  if (!(d1&0x80))
  {
    printf("Unable to determine board type\n");
    return(-1);
  }
  
  ret = IO_IOPokeUChar(CMOSA, BID2);
  ISOK

  ret = IO_IOPeekUChar(CMOSD, &d1);
  ISOK

  d1 &= 0x1f;  // Mask board ID bits
  if (d1 == 8)
  {  
    btype = 1;
    smb_base = SMB_BASE_ADDR;
    mxaddr  = MX_ADDR;
    mxaddr2 = MX_ADDR2;
    printf("This is a VP110\n");
    return(0);
  }
  
  else if (d1 == 11)
  {  
    btype = 2;
    smb_base = SMB_BASE_ADDR_315;
    mxaddr  = MX_ADDR_315;
    mxaddr2 = MX_ADDR2_315;
    printf("This is a VP315\n");
    return(0);
  }  

  else if (d1 == 13)
  {  
    btype = 2;
    smb_base = SMB_BASE_ADDR_315;
    mxaddr  = MX_ADDR_315;
    mxaddr2 = MX_ADDR2_315;
    printf("This is a VP317 or VP417\n");
    return(0);
  } 
  
  else if (d1 == 23)
  {  
    btype = 3;
    printf("This is a VP317 or a VP417\n");
    return(0);
  } 
  
  else
  {  
    printf("This is not a VP110/VP315/VP317/VP417\n");
    return(-1);
  }
}








