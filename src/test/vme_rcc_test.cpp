// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_test.c							*/
/*									*/
/* This is the test program for the RCC VMEbus library			*/
/*									*/
/* 26. Oct. 01  MAJO  created						*/
/* 26. Nov. 01  JOP   add bus error functions				*/
/*									*/
/**************** C 2011 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "vme_rcc/tsi148.h"
#include "cmem_rcc/cmem_rcc.h"
#include "io_rcc/io_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"

//globals
tstamp ts1, ts2;
float delta;
int cmem_desc, cont;
unsigned long cmem_desc_uaddr, cmem_desc_paddr;
VME_BusErrorInfo_t g_bus_err_info;
static int signal_pending = 0;      // flag : 0 or 1
static struct sigaction sa_new, sa_old;

//Constants
#define SIGNUM 42 	  	    // Qed RT signal

//Prototypes
void SigQuitHandler(int signum);
void SigBusHandler(int signum);
void signal_handler(int signum);
void dmarw(void);
void scrw(void);
void mastermap(void);
void vmebench(void);
void vmebenchdma(void);
void setdebug(void);
void data_integrity(void);
void slave717(void);
void newdma(void);
void newdma2(void);
int stress_menu(void);
int mainhelp(void);
int func_menu(void);
int bench_menu(void);
int test_menu(void);
int swapping(void);
int dmatest(void);
int irqtest(void);
u_int bswap(u_int word);


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  int fun = 1;
  unsigned int ret;
  int stat;
  struct sigaction sa, sa2;

  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler; 
  stat = sigaction(SIGQUIT, &sa, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", stat);
    exit(-1);
  }

  sigemptyset(&sa2.sa_mask); 
  sa2.sa_flags = 0;
  sa2.sa_handler = SigBusHandler; 
  stat = sigaction(SIGBUS, &sa2, NULL);
  if (stat < 0)
  {
    printf("Cannot install SIGBUS handler (error=%d)\n", stat);
    exit(-1);
  }
  
  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-2);
  }

  ret = CMEM_Open();
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit(-3);
  } 

  ret = CMEM_SegmentAllocate(0x100000, (char *)"DMA_BUFFER", &cmem_desc);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit(-4);
  }

  ret = CMEM_SegmentPhysicalAddress(cmem_desc, &cmem_desc_paddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit(-4);
  }

  ret = CMEM_SegmentVirtualAddress(cmem_desc, &cmem_desc_uaddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    exit(-4);
  }

  DF::GlobalDebugSettings::setup(5, DFDB_VMERCC);

  printf("\n\n\nThis is the test program for the RCC VMEbus library \n");
  printf("====================================================\n");
  printf("DMA buffer (0x10000 bytes) starts at PCI address 0x%08lx\n", cmem_desc_paddr);

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                           2 Test functions\n");  
    printf("   3 Benchmarks                     4 Stress tests\n");
    printf("   5 Functional tests               6 Set debug parameters\n");
    printf("   7 Direct DMA test (Tsi148 only)  8 Interrupt test\n");
    printf("   9 New DMA test                  10 New DMA test 2\n");
    printf("   0 Exit\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
    if (fun == 3) bench_menu();
    if (fun == 4) stress_menu();
    if (fun == 5) test_menu();
    if (fun == 6) setdebug();
    if (fun == 7) dmatest();
    if (fun == 8) irqtest();
    if (fun == 9) newdma();
    if (fun == 10) newdma2();
  }

  ret = CMEM_SegmentFree(cmem_desc);
  if (ret) 
    rcc_error_print(stdout, ret);

  ret = CMEM_Close();
  if (ret) 
    rcc_error_print(stdout, ret);

  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  return(0);  
}


/*****************************/
void signal_handler(int signum)
/*****************************/
{
  printf("signal_handler: IRQ signal\n");

  if (signum == SIGNUM)
  {
    printf("signal_handler: This is the expected signal\n");
    signal_pending = 1;
  }
  else 
    printf("ERROR: unexpected signal\n");
}


/***************/
int irqtest(void)
/***************/
{
  VME_InterruptList_t irq_list;
  VME_InterruptInfo_t ir_info;
  u_int ret, level, vector, type;
  int int_handle;
  
  ret = VME_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }

  printf("ROAK or RORA interrupt (ROAK = %2d  RORA = %2d)", VME_INT_ROAK, VME_INT_RORA);  
  type = getdecd(VME_INT_ROAK);

  printf("Interrupt level = ");
  level = getdecd(3);

  printf("Interrupt vector = ");
  vector = getdecd(3);
  
  irq_list.number_of_items = 1;	
  irq_list.list_of_items[0].vector = vector;
  irq_list.list_of_items[0].level = level;
  irq_list.list_of_items[0].type = type;

  ret = VME_InterruptLink(&irq_list, &int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf(" Interrupt Handle = %d\n", int_handle);
        
  //First test the semaphore
  printf("Generate first interrupt\n");
  ret = VME_InterruptWait(int_handle, -1, &ir_info);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("after Wait, level = %d, type = %d, vector = %d\n", ir_info.level, ir_info.type, ir_info.vector);
   
  //Now test the signal
  sigemptyset(&sa_new.sa_mask);
  sa_new.sa_flags = 0;
  sa_new.sa_handler = signal_handler;
  // dont block any signal in intercept handler
  if (sigaction(SIGNUM, &sa_new, &sa_old) < 0) 
  {
    perror(" sigaction SIGNUM ");
    return(1);
  }
    
  ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("Generate second interrupt\n");
  signal_pending = 0;
  while(!signal_pending)
  {
    printf("Waiting...\n");
    sleep(1);
  }

  ret = VME_InterruptInfoGet(int_handle, &ir_info);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  printf("After InfoGet, level = %d, type = %d, vector = %d\n", ir_info.level, ir_info.type, ir_info.vector);

  //remove signal handler
  if (sigaction(SIGNUM, &sa_old, (struct sigaction *)NULL))
  {
    perror("sigaction");
  } 
  
  ret = VME_InterruptUnlink(int_handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  return(0);
}


/***************/
int dmatest(void)
/***************/
{
  VME_TsiDmaChain_t *dmachain;
  u_int data, ret;
  u_long tsibase;
  tsi148_regs_t *tsi;
 
  ret = IO_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  ret = VME_Tsi148Map(&tsibase);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  tsi = (tsi148_regs_t *)tsibase;

  dmachain = (VME_TsiDmaChain_t *)cmem_desc_uaddr;

  dmachain->dsal = bswap((cmem_desc_paddr + 0x100) & 0xffffffff);         
  dmachain->dsau = bswap((cmem_desc_paddr + 0x100) >> 32);         
  dmachain->ddal = bswap(0x08000000);         
  dmachain->ddau = 0;         
  dmachain->ddat = bswap(0x10000142);         
  dmachain->dsat = 0;         
  dmachain->dnlal = bswap(1);        
  dmachain->dnlau = 0;        
  dmachain->ddbs = 0;         
  dmachain->dcnt = bswap(0x100);         
  
  printf("dsal  is 0x%08x at 0x%016lx\n", bswap(dmachain->dsal), (unsigned long)&dmachain->dsal);         
  printf("dsau  is 0x%08x at 0x%016lx\n", bswap(dmachain->dsau), (unsigned long)&dmachain->dsau);         
  printf("ddal  is 0x%08x at 0x%016lx\n", bswap(dmachain->ddal), (unsigned long)&dmachain->ddal);         
  printf("ddau  is 0x%08x at 0x%016lx\n", bswap(dmachain->ddau), (unsigned long)&dmachain->ddau);         
  printf("ddat  is 0x%08x at 0x%016lx\n", bswap(dmachain->ddat), (unsigned long)&dmachain->ddat);         
  printf("dsat  is 0x%08x at 0x%016lx\n", bswap(dmachain->dsat), (unsigned long)&dmachain->dsat);         
  printf("dnlal is 0x%08x at 0x%016lx\n", bswap(dmachain->dnlal), (unsigned long)&dmachain->dnlal);        
  printf("dnlau is 0x%08x at 0x%016lx\n", bswap(dmachain->dnlau), (unsigned long)&dmachain->dnlau);        
  printf("ddbs  is 0x%08x at 0x%016lx\n", bswap(dmachain->ddbs), (unsigned long)&dmachain->ddbs);         
  printf("dcnt  is 0x%08x at 0x%016lx\n", bswap(dmachain->dcnt), (unsigned long)&dmachain->dcnt);         
 
  printf("PCI address = 0x%016lx\n", cmem_desc_paddr);
 
  data = bswap(cmem_desc_paddr & 0xffffffff);
  tsi->dnlal0 = data;
  printf("dnlal0 is 0x%08x, data = 0x%08x\n", bswap(tsi->dnlal0), data);         
  
  data = bswap(cmem_desc_paddr >> 32);
  tsi->dnlau0 = data;
  printf("dnlau0 is 0x%08x, data = 0x%08x\n", bswap(tsi->dnlau0), data); 
          
  tsi->dctl0 = bswap(0x02007070);

  data = bswap(tsi->dsta0);
  while(data & 0x01000000)
  {
    printf("dsta0 = 0x%08x\n", data);
    data = bswap(tsi->dsta0);
  }
  printf("Transfer done. dsta0 = 0x%08x\n", data);

  sleep(5);
  printf("dnlal0 is 0x%08x\n", bswap(tsi->dnlal0));         
  printf("dnlau0 is 0x%08x\n", bswap(tsi->dnlau0)); 

  VME_Tsi148Unmap(tsibase);

  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  IO_Close();

  return(0);
}


/*****************/
void setdebug(void)
/*****************/
{
  static unsigned int dblevel = 20, dbpackage = DFDB_VMERCC;
  
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}


/*******************/
int stress_menu(void)
/*******************/
{
int fun =1 ;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function :\n");
    printf("   1 DMA read/write    2 Single cycle read/write\n");
    printf("   3 VME_MasterMap     4 VP717 slave test\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);  
  if (fun == 1) dmarw();
  if (fun == 2) scrw();
  if (fun == 3) mastermap();
  if (fun == 4) slave717();
  }
  printf("=========================================================================\n\n"); 
  return(0);
}


/*****************/
void slave717(void)
/*****************/
{
  static VME_MasterMap_t master_map = {0x01233210, 0x1000, VME_A32, 0};
  int handle;
  u_int loop, rdata, ret;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  printf("VMEbus address                  ");
  master_map.vmebus_address = gethexd(master_map.vmebus_address);

  master_map.window_size = 0x1000;
  master_map.address_modifier = VME_A32;
  master_map.options = 0;
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf("Handle = %d\n", handle);
      
  cont = 1;
  printf("=====================================\n");
  printf("Test running. Press <ctrl+\\> to stop\n");
  printf("=====================================\n");
  while(cont)
  {
      for (loop = 0; loop < 0x10; loop++)
      {
        ret = VME_ReadSafeUInt(handle, loop * 4, &rdata);  
        if (ret != VME_SUCCESS)
          VME_ErrorPrint(ret);
      }
  }
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf("Handle = %d unmapped\n", handle);  
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
}


/*****************/
int test_menu(void)
/*****************/
{
int fun =1 ;

  printf("\n=========================================================================\n");
  while (fun != 0)
  {
    printf("\n");
    printf("Select a function :\n");
    printf("   1 Byte swapping\n");
    printf("   2 Data integrity\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) swapping();
    if (fun == 2) data_integrity();
  }
  printf("=========================================================================\n\n");
  return(0);
}
 

/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
cont=0;
//debug(("SigQuitHandler: ctrl+// received\n"));
}

// Normally it's not recommended to do too much in a signal handler ..
/********************************/
void SigBusHandler(int /*signum*/)
/********************************/
{
  VME_ErrorCode_t ret;

  printf(" SigBusHandler: SIGBUS received\n");

  ret = VME_BusErrorInfoGet(&g_bus_err_info);
  if (ret == VME_SUCCESS)
  {
    printf(" Bus Error Occured:\n");
    printf(" VMEbus address = 0x%8x\n", g_bus_err_info.vmebus_address);
    printf(" VMEbus AM      = 0x%8x\n", g_bus_err_info.address_modifier);
    printf(" VMEbus IACK    = %d\n", g_bus_err_info.iack);
    printf(" VMEbus LWORD*  = %d\n", g_bus_err_info.lword);
    printf(" VMEbus DS0*    = %d\n", g_bus_err_info.ds0);
    printf(" VMEbus DS1*    = %d\n", g_bus_err_info.ds1);
    printf(" VMEbus WRITE*  = %d\n", g_bus_err_info.wr);
    printf(" # bus errors   = 0x%8x\n", g_bus_err_info.multiple);
  }
  else
  {
    printf(" No Bus Error Information\n");
  }

}


/****************/
int swapping(void)
/****************/
{
  VME_BlockTransferList_t rlist;
  static VME_MasterMap_t master_map = {0x00000000, 0x1000, VME_A32, 0};
  static u_int vmeaddr = 0x00000000;
  int dmar_desc, handle;
  u_long dmar_desc_uaddr, dmar_desc_paddr;
  u_int ret, *rptr;
  u_int idata;
  u_short sdata;
  u_char cdata;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  ret = CMEM_SegmentAllocate(0x10000, (char*)"DMA_READ_BUFFER", &dmar_desc);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return(0);
  }
  
  ret = CMEM_SegmentPhysicalAddress(dmar_desc, &dmar_desc_paddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return(0);
  }

  ret = CMEM_SegmentVirtualAddress(dmar_desc, &dmar_desc_uaddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return(0);
  }

  printf("VMEbus address ");
  vmeaddr = gethexd(vmeaddr);

  master_map.vmebus_address = vmeaddr;
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  //Initialize the memory
  ret = VME_WriteSafeUInt(handle, 0, 0x11223344);
  if (ret != VME_SUCCESS) VME_ErrorPrint(ret);
  ret = VME_WriteSafeUInt(handle, 0x4, 0x55667788);
  if (ret != VME_SUCCESS) VME_ErrorPrint(ret);
  ret = VME_WriteSafeUInt(handle, 0x8, 0x99aabbcc);
  if (ret != VME_SUCCESS) VME_ErrorPrint(ret);
  ret = VME_WriteSafeUInt(handle, 0xc, 0xddeeff00);
  if (ret != VME_SUCCESS) VME_ErrorPrint(ret);


  ret = VME_ReadSafeUInt(handle, 0, &idata);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  printf("First D32 single read cycle returns:  0x%08x\n", idata);
  ret = VME_ReadSafeUInt(handle, 4, &idata);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  printf("Second D32 single read cycle returns: 0x%08x\n", idata);

  ret = VME_ReadSafeUShort(handle, 0, &sdata);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  printf("First D16 single read cycle returns:  0x%08x\n", sdata);
  ret = VME_ReadSafeUShort(handle, 2, &sdata);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  printf("Second D16 single read cycle returns: 0x%08x\n", sdata);

  ret = VME_ReadSafeUChar(handle, 0, &cdata);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  printf("First D8 single read cycle returns:  0x%08x\n", cdata);
  ret = VME_ReadSafeUChar(handle, 1, &cdata);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  printf("Second D8 single read cycle returns: 0x%08x\n", cdata);

  rlist.number_of_items = 1;
  rlist.list_of_items[0].vmebus_address = vmeaddr;
  rlist.list_of_items[0].system_iobus_address = dmar_desc_paddr;
  rlist.list_of_items[0].size_requested = 0x100;
  rlist.list_of_items[0].control_word = VME_DMA_D32R;

  //Read buffer
  ret = VME_BlockTransfer(&rlist, 1000);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);      

  printf("Dumping result of D32 read\n");
  rptr = (u_int *) dmar_desc_uaddr;
  printf("Offset 0x00 = 0x%08x\n", *rptr++);
  printf("Offset 0x04 = 0x%08x\n", *rptr++);
  printf("Offset 0x08 = 0x%08x\n", *rptr++);
  printf("Offset 0x0c = 0x%08x\n", *rptr++);

  rlist.number_of_items = 1;
  rlist.list_of_items[0].vmebus_address = vmeaddr;
  rlist.list_of_items[0].system_iobus_address = dmar_desc_paddr;
  rlist.list_of_items[0].size_requested = 0x100;
  rlist.list_of_items[0].control_word = VME_DMA_D64R;
    
  //Read buffer
  ret = VME_BlockTransfer(&rlist, 1000);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);  
     
  printf("Dumping result of D64 read\n");
  rptr = (u_int *) dmar_desc_uaddr;
  printf("Offset 0x00 = 0x%08x\n", *rptr++);
  printf("Offset 0x04 = 0x%08x\n", *rptr++);
  printf("Offset 0x08 = 0x%08x\n", *rptr++);
  printf("Offset 0x0c = 0x%08x\n", *rptr++);

  ret = CMEM_SegmentFree(dmar_desc);
  if (ret) 
    rcc_error_print(stdout, ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  return(0);
}


/***************/
void newdma(void)
/***************/
{  
  static int n_req = 1, n_words = 4;
  int spar, dmas_done, dmaw_desc;
  VME_BlockTransferList_t wlist;
  u_long dmaw_desc_uaddr, dmaw_desc_paddr;
  u_int ret, upar, *dptr;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  ret = CMEM_SegmentAllocate(0x10000, (char *)"DMA_WRITE_BUFFER", &dmaw_desc);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  ret = CMEM_SegmentPhysicalAddress(dmaw_desc, &dmaw_desc_paddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  ret = CMEM_SegmentVirtualAddress(dmaw_desc, &dmaw_desc_uaddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  printf("Enter number of requests: (0 = run forever)");
  n_req = getdecd(n_req);
  printf("Enter number of words: ");
  n_words = getdecd(n_words);  

  wlist.number_of_items = 1;
  wlist.list_of_items[0].vmebus_address = 0x08000000;
  wlist.list_of_items[0].system_iobus_address = dmaw_desc_paddr;
  wlist.list_of_items[0].size_requested = n_words * 4;
  wlist.list_of_items[0].control_word = VME_DMA_D32W;
  
  cont = 1;
  dmas_done = 0;
  if (!n_req)
  {
    printf("=====================================\n");
    printf("Test running. Press <ctrl+\\> to stop\n");
    printf("=====================================\n");
  }

  while(cont)
  {
    dptr = (u_int *)dmaw_desc_uaddr;
    *dptr++ = dmas_done;
    *dptr++ = dmas_done;
    *dptr++ = dmas_done;
    *dptr++ = dmas_done;

    printf("starting DMA %d\n", dmas_done);
    ret = VME_BlockTransfer(&wlist, 100);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);      
      ret = VME_BlockTransferStatus(&wlist, 0, &upar);
      printf("The error code is 0x%08x\n", upar);

      ret = VME_BlockTransferRemaining(&wlist, 0, &spar);
      printf("Bytes remaining: 0x%08x\n", spar);
      
      exit(-1);
    }
    dmas_done++;  

    if (dmas_done == n_req)
      break;
    if (dmas_done % 1000 == 0)
       printf("%d DMAs done\n", dmas_done);
  }
   
  ret = CMEM_SegmentFree(dmaw_desc);
  if (ret) 
    rcc_error_print(stdout, ret);
    
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
}


/****************/
void newdma2(void)
/****************/
{  
  static int loop, iloop, n_req = 9, n_words = 40;
  int dmaw_desc, handle[100];
  VME_BlockTransferList_t wlist;
  u_long dmaw_desc_uaddr, dmaw_desc_paddr;
  u_int ret;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  ret = CMEM_SegmentAllocate(0x10000, (char *)"DMA_WRITE_BUFFER", &dmaw_desc);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  ret = CMEM_SegmentPhysicalAddress(dmaw_desc, &dmaw_desc_paddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  ret = CMEM_SegmentVirtualAddress(dmaw_desc, &dmaw_desc_uaddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  printf("Enter number of parallel requests: ");
  n_req = getdecd(n_req);
  printf("Enter number of words: ");
  n_words = getdecd(n_words);  


  for(loop = 0; loop < n_req;  loop++)
  {
    wlist.number_of_items = 1;
    wlist.list_of_items[0].vmebus_address = 0x08000000;
    wlist.list_of_items[0].system_iobus_address = dmaw_desc_paddr;
    wlist.list_of_items[0].size_requested = n_words;
    wlist.list_of_items[0].control_word = VME_DMA_D32W;
    
    printf("starting DMA %d\n", loop);
    ret = VME_BlockTransferInit(&wlist, &handle[loop]);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
    else
      printf("Handle = %d\n", handle[loop]);
      
    ret = VME_BlockTransferStart(handle[loop]);       
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);    
  }
  
  printf("Continue....... ");
  getdecd(0);      

  for(loop = 0; loop < n_req;  loop++)
  {  
    ret = VME_BlockTransferWait(handle[loop], 100, &wlist);      
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);
    if (ret != VME_SUCCESS)
    {
      for (iloop = 0; iloop < wlist.number_of_items; iloop++)
      {
        printf("Element:         %d\n", iloop);
        printf("Status:          %d\n", wlist.list_of_items[iloop].status_word);
        printf("Bytes remaining: %d\n", wlist.list_of_items[iloop].size_remaining);
      }
    } 
  }
   
  ret = CMEM_SegmentFree(dmaw_desc);
  if (ret) 
    rcc_error_print(stdout, ret);
    
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
}



/**************/
void dmarw(void)
/**************/
{  
  int loop, size, dmar_desc, dmaw_desc;
  VME_BlockTransferList_t wlist, rlist;
  u_long dmar_desc_uaddr, dmar_desc_paddr, dmaw_desc_uaddr, dmaw_desc_paddr;
  u_int wdata = 0, ret, *rptr, *wptr;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  ret = CMEM_SegmentAllocate(0x10000, (char *)"DMA_WRITE_BUFFER", &dmaw_desc);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  ret = CMEM_SegmentPhysicalAddress(dmaw_desc, &dmaw_desc_paddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  ret = CMEM_SegmentVirtualAddress(dmaw_desc, &dmaw_desc_uaddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }
 
  ret = CMEM_SegmentAllocate(0x10000, (char *)"DMA_READ_BUFFER", &dmar_desc);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }
  
  ret = CMEM_SegmentPhysicalAddress(dmar_desc, &dmar_desc_paddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  ret = CMEM_SegmentVirtualAddress(dmar_desc, &dmar_desc_uaddr);
  if (ret) 
  {
    rcc_error_print(stdout, ret);
    return;
  }

  wlist.number_of_items = 1;
  printf("Enter VMEbus address ");
  wlist.list_of_items[0].vmebus_address = gethexd(0x0);
  wlist.list_of_items[0].system_iobus_address = dmaw_desc_paddr;
  printf("Size in bytes (max. = 0x10000) ");
  wlist.list_of_items[0].size_requested = gethexd(0x1000);
  size = wlist.list_of_items[0].size_requested >> 2;  //convert from bytes to words

  printf("DMA Control word:\n");
  printf("D32 = 0x%08x\n", VME_DMA_D32W);
  printf("D64 = 0x%08x\n", VME_DMA_D64W);
  printf("Your choice ");
  wlist.list_of_items[0].control_word = gethexd(VME_DMA_D32W);
  
  rlist.number_of_items = 1;
  rlist.list_of_items[0].vmebus_address = wlist.list_of_items[0].vmebus_address;
  rlist.list_of_items[0].system_iobus_address = dmar_desc_paddr;
  rlist.list_of_items[0].size_requested = wlist.list_of_items[0].size_requested;
  if (wlist.list_of_items[0].control_word == VME_DMA_D32W)
    rlist.list_of_items[0].control_word = VME_DMA_D32R;
  else
    rlist.list_of_items[0].control_word = VME_DMA_D64R;
  
  cont = 1;
  printf("=====================================\n");
  printf("Test running. Press <ctrl+\\> to stop\n");
  printf("=====================================\n");
  while(cont)
  {
    //Initialize write buffer
    wptr = (u_int *)dmaw_desc_uaddr;
    for (loop = 0; loop < size; loop++)
      *wptr++ = wdata++;
    
    //Write buffer
    ret = VME_BlockTransfer(&wlist, 1000);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);      
    if (ret == VME_DMAERR)
    {
      for (loop = 0; loop < wlist.number_of_items; loop++)
      {
        printf("Element:         %d\n", loop);
        printf("Status:          %d\n", wlist.list_of_items[loop].status_word);
        printf("Bytes remaining: %d\n", wlist.list_of_items[loop].size_remaining);
      }
    }

    //Read buffer
    ret = VME_BlockTransfer(&rlist, 1000);
    if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);      
    if (ret == VME_DMAERR)
    {
      for (loop = 0; loop < rlist.number_of_items; loop++)
      {
        printf("Element:         %d\n", loop);
        printf("Status:          %d\n", rlist.list_of_items[loop].status_word);
        printf("Bytes remaining: %d\n", rlist.list_of_items[loop].size_remaining);
      }
    }
  
    //Compare the buffers
    wptr = (u_int *)dmaw_desc_uaddr;
    rptr = (u_int *)dmar_desc_uaddr;
    for (loop = 0; loop < size; loop++)
      if (*wptr != *rptr)
      {
        printf("Data mismatch: offset: 0x%08x  Written: 0x%08x  Read: 0x%08x\n", loop * 4, *wptr, *rptr);
        wptr++;
        rptr++;
      }
  }
   
  ret = CMEM_SegmentFree(dmaw_desc);
  if (ret) 
    rcc_error_print(stdout, ret);
    
  ret = CMEM_SegmentFree(dmar_desc);
  if (ret) 
    rcc_error_print(stdout, ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
}


/******************/
void mastermap(void)
/******************/
{
  static VME_MasterMap_t master_map = {0x00000000, 0x1000, VME_A32, 0};
  static int imap = 0, mapsize = 0x1000;
  int handle, mapping = 0;
  u_int ret;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  printf("Identical mappings (1=yes  0=no): ");
  imap = getdecd(imap);
 
  printf("Enter the window size per mapping: ");
  mapsize = gethexd(mapsize);
 
  while(mapping < 200)
  {
    if (imap)
      master_map.vmebus_address = 0x1000 * mapping;
    master_map.window_size = mapsize;
    ret = VME_MasterMap(&master_map, &handle);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      break;
    }
    else
      printf("Handle = %d for mapping %d\n", handle, mapping);
    mapping++;  
  }  
   
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
}


/*************/
void scrw(void)
/*************/
{
  static VME_MasterMap_t master_map = {0x01233210, 0x1000, VME_A32, 0};
  static int dowrite = 1, doread = 1;
  int handle;
  u_int ret, wdata, rdata;
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  printf("VMEbus address                  ");
  master_map.vmebus_address = gethexd(master_map.vmebus_address);
  printf("Window size                     ");
  master_map.window_size = gethexd(master_map.window_size);
  printf("Address modifier (%d=A24, %d=A32) ",VME_A24,VME_A32);
  master_map.address_modifier = gethexd(master_map.address_modifier);
  printf("Options                         ");
  master_map.options = gethexd(master_map.options);
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf("Handle = %d\n", handle);
    
  printf("Generate read cycles (1=yes  0=no) ");
  doread = getdecd(doread);
  printf("Generate write cycles (1=yes  0=no) ");
  dowrite = getdecd(dowrite);
      
  cont = 1;
  wdata = 0;
  printf("=====================================\n");
  printf("Test running. Press <ctrl+\\> to stop\n");
  printf("=====================================\n");
  while(cont)
  {
    if (dowrite)
    {
      ret = VME_WriteSafeUInt(handle, 0, wdata);   
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }
    
    if (doread)
    {  
      ret = VME_ReadSafeUInt(handle, 0, &rdata);  
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }
      
    if (doread && dowrite && (wdata != rdata))
      printf("Data mismatch: Written: 0x%08x  Read: 0x%08x\n", wdata, rdata);

    wdata++;
  }
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  else
    printf("Handle = %d unmapped\n", handle);  
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
}


/******************/
int bench_menu(void)
/******************/
{
  int loop, loop2, fun = 2, ncalls[5] = {1, 3, 10, 100, 1000};
  unsigned int ret;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function :\n");
    printf("   1 Measure ioctl overhead\n");
    printf("   2 Measure VMEbus single cycles\n");
    printf("   3 Measure VMEbus block transfers\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);  
    if (fun == 1)
    {
      printf("\n=========================================================================\n");
      ret = VME_Open();
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);

      printf("\nMeasuring delay of ts_clock: ");
      ret = ts_clock(&ts1);
      if (ret)
	rcc_error_print(stdout, ret);
      ret = ts_clock(&ts2);
      if (ret)
	rcc_error_print(stdout, ret);
      delta = ts_duration(ts1, ts2);
      printf(" measured delay = %d nsecs\n", (unsigned int)(delta * 1000000000.0));   

      for (loop = 0; loop < 5; loop++)
      {  
	printf("%5d call(s) to VME_test:   ", ncalls[loop]);
	ret = ts_clock(&ts1);
	if (ret)
	  rcc_error_print(stdout, ret);
	for (loop2 = 0; loop2 < ncalls[loop]; loop2++)
          ret = VME_test();
	ret = ts_clock(&ts2);
	if (ret)
	  rcc_error_print(stdout, ret);
	delta = ts_duration(ts1, ts2);
	printf(" measured delay per call = %d nsecs\n", (unsigned int)(delta * 1000000000.0 / (float)ncalls[loop]));   
      }

      ret = VME_Close();
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      printf("=========================================================================\n\n");  
    }
    if (fun == 2) vmebench(); 
    if (fun == 3) vmebenchdma(); 
  }
  printf("=========================================================================\n\n"); 
  return(0);
}


/***********************/
void data_integrity(void)
/***********************/
{
  VME_MasterMap_t master_map;
  u_int nloops = 1, loop1, loop2, vbase = 0x08000000;
  volatile u_int   *iptr, idatain, idataout;
  volatile u_short *sptr, sdatain, sdataout;
  volatile u_char  *cptr, cdatain, cdataout;
  u_long value;
  int handle;
  VME_ErrorCode_t ret;

  printf("For this test you need a A32/D32/MBLT64 VMEbus slave modules with at least 64 KB RAM\n");
  printf("Enter the VMEbus base address of the slave: ");
  vbase = gethexd(vbase);
 
  printf("Enter the number of loops: ");
  nloops = gethexd(nloops);
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }

  master_map.vmebus_address = vbase;
  master_map.window_size = 0x1000;
  master_map.address_modifier = 2;
  master_map.options = 0;
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
 
  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
  
  cptr = (u_char *)value; 
  for (loop1 = 0; loop1 < nloops; loop1++)
  {
    for (loop2 = 0; loop2 < 8; loop2++)
    {
      cdatain = 1 << loop2;
      *cptr = cdatain;
      cdataout = *cptr;
      if (cdataout != cdatain)
        printf("dataout = 0x%08x, datain = 0x%08x\n", cdataout, cdatain);
	
      cdatain = ~(1 << loop2);
      *cptr = cdatain;
      cdataout = *cptr;
      if (cdataout != cdatain)
        printf("dataout = 0x%08x, datain = 0x%08x\n", cdataout, cdatain);	
    }
  }

  sptr = (u_short *)value; 
  for (loop1 = 0; loop1 < nloops; loop1++)
  {
    for (loop2 = 0; loop2 < 16; loop2++)
    {
      sdatain = 1 << loop2;
      *sptr = sdatain;
      sdataout = *sptr;
      if (sdataout != sdatain)
        printf("dataout = 0x%08x, datain = 0x%08x\n", sdataout, sdatain);
	
      sdatain = ~(1 << loop2);
      *sptr = sdatain;
      sdataout = *sptr;
      if (sdataout != sdatain)
        printf("dataout = 0x%08x, datain = 0x%08x\n", sdataout, sdatain);	
    }
  }

  iptr = (u_int *)value; 
  for (loop1 = 0; loop1 < nloops; loop1++)
  {
    for (loop2 = 0; loop2 < 32; loop2++)
    {
      idatain = 1 << loop2;
      *iptr = idatain;
      idataout = *iptr;
      if (idataout != idatain)
        printf("dataout = 0x%08x, datain = 0x%08x\n", idataout, idatain);
	
      idatain = ~(1 << loop2);
      *iptr = idatain;
      idataout = *iptr;
      if (idataout != idatain)
        printf("dataout = 0x%08x, datain = 0x%08x\n", idataout, idatain);	
    }
  }

  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
}


/*****************/
void vmebench(void)
/*****************/
{
  VME_MasterMap_t master_map;
  u_int loop, vbase = 0x08000000, data;
  volatile u_int *ptr;
  u_long value;
  int handle;
  VME_ErrorCode_t ret;
  float mbps;

  printf("For this test you need a A32/D32/MBLT64 VMEbus slave modules with at least 64 KB RAM\n");
  printf("Enter the VMEbus base address of the slave: 0x");
  vbase = gethexd(vbase);
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }

  master_map.vmebus_address = vbase;
  master_map.window_size = 0x1000;
  master_map.address_modifier = 2;
  master_map.options = 0;
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
  
  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
    
  printf("Safe cycles:\n");
  ts_clock(&ts1);
  for(loop = 0; loop < 256; loop++)
    ret = VME_ReadSafeUInt(handle, 0, &data);
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 1024.0);
  printf("Safe VMEbus A32/D32 read:  Duration = %f us; Performance = %f MB/s\n", delta * 1000000.0 / 256.0, mbps);
  
  ts_clock(&ts1);
  for(loop = 0; loop < 256; loop++)
    ret = VME_WriteSafeUInt(handle, 0, 0x12345678);
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 1024.0);
  printf("Safe VMEbus A32/D32 write:  Duration = %f us; Performance = %f MB/s\n", delta * 1000000.0 / 256.0, mbps);
  
  printf("Fast cycles:\n");
  ptr = (u_int *)value;
  ts_clock(&ts1);
  for(loop = 0; loop < 256; loop++)
    data = *ptr;
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 1024.0);
  printf("Fast VMEbus A32/D32 read:  Duration = %f us; Performance = %f MB/s\n", delta * 1000000.0 / 256.0, mbps);

  ts_clock(&ts1);
  for(loop = 0; loop < 256; loop++)
    *ptr = 0x12345678;
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 1024.0);
  printf("Fast VMEbus A32/D32 write:  Duration = %f us; Performance = %f MB/s\n", delta * 1000000.0 / 256.0, mbps);

  ts_clock(&ts1);
  for(loop = 0; loop < 256; loop++)
    *ptr = 0x12345678;
  data = *ptr;                   //This read cycle has to wait until all writes have passed
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 1024.0);
  printf("Fast VMEbus A32/D32 write with flush:  Duration = %f us; Performance = %f MB/s\n", delta * 1000000.0 / 256.0, mbps);

  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
}


/********************/
void vmebenchdma(void)
/********************/
{
  int loop;
  u_int vbase = 0x08000000;
  VME_ErrorCode_t ret;
  float mbps;
  VME_BlockTransferList_t blist;

  printf("For this test you need a A32/D32/MBLT64 VMEbus slave modules with at least 64 KB RAM\n");
  printf("Enter the VMEbus base address of the slave: 0x");
  vbase = gethexd(vbase);
  
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }

  printf("Next: VMEbus A32/D32 DMA write [8 bytes]\n");
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 8;
  blist.list_of_items[0].control_word         = VME_DMA_D32W;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    for (loop = 0; loop < blist.number_of_items; loop++)
    {
      printf("Element:         %d\n", loop);
      printf("Status:          %d\n", blist.list_of_items[loop].status_word);
      printf("Bytes remaining: %d\n", blist.list_of_items[loop].size_remaining);
    }
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 8.0 / (delta * 1024.0 * 1024.0);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D32 DMA write [8 bytes]: %f MB/s\n", mbps);
     
  printf("Press return to continue. Next: VMEbus A32/D32 DMA write [64 Kb]");  
  getdecd(0);
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 0x10000;
  blist.list_of_items[0].control_word         = VME_DMA_D32W;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 16.0);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D32 DMA write [64 Kb]: %f MB/s\n", mbps);     

  printf("Press return to continue. Next: VMEbus A32/D32 DMA read [8 bytes]");  
  getdecd(0);
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 8;
  blist.list_of_items[0].control_word         = VME_DMA_D32R;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 8.0 / (delta * 1024.0 * 1024.0);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D32 DMA read [8 bytes]:  %f MB/s\n", mbps);
  
  printf("Press return to continue. Next: VMEbus A32/D32 DMA read [64 Kb]");  
  getdecd(0);
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 0x10000;
  blist.list_of_items[0].control_word         = VME_DMA_D32R;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 16.0);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D32 DMA read [64 Kb]:  %f MB/s\n", mbps);

  printf("Press return to continue. Next: VMEbus A32/D64 DMA write [16 bytes]");  
  getdecd(0);
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 16;
  blist.list_of_items[0].control_word         = VME_DMA_D64W;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 16.0 / (delta * 1024.0 * 1024);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D64 DMA write [16 bytes]: %f MB/s\n", mbps);
 
  printf("Press return to continue. Next: VMEbus A32/D64 DMA write [64 kb]");  
  getdecd(0);
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 0x10000;
  blist.list_of_items[0].control_word         = VME_DMA_D64W;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 16.0);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D64 DMA write [64 kb]: %f MB/s\n", mbps);

  printf("Press return to continue. Next: VMEbus A32/D64 DMA read [16 bytes]");  
  getdecd(0);
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 16;
  blist.list_of_items[0].control_word         = VME_DMA_D64R;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 16.0 / (delta * 1024.0 * 1024.0);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D64 DMA read [16 bytes]:  %f MB/s\n", mbps);

  printf("Press return to continue. Next: VMEbus A32/D64 DMA read [64 kb]");  
  getdecd(0);
  blist.number_of_items                       = 1;
  blist.list_of_items[0].vmebus_address       = vbase;
  blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
  blist.list_of_items[0].size_requested       = 0x10000;
  blist.list_of_items[0].control_word         = VME_DMA_D64R;
  ts_clock(&ts1);
  ret = VME_BlockTransfer(&blist, 1000);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }	     
  ts_clock(&ts2);
  delta = ts_duration(ts1, ts2);
  mbps = 1.0 / (delta * 16.0);
  printf("Duration = %f us\n", delta * 1000000.0);
  printf("VMEbus A32/D64 DMA read [64 kb]:  %f MB/s\n", mbps);


  printf("Press return to continue. Next: Scan of VMEbus A32/D64 DMA read");  
  getdecd(0);

  for (loop = 0x200; loop <= 0x10000; loop += 0x200)
  {
    blist.number_of_items                       = 1;
    blist.list_of_items[0].vmebus_address       = vbase;
    blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
    blist.list_of_items[0].size_requested       = loop;
    blist.list_of_items[0].control_word         = VME_DMA_D64R;
    ts_clock(&ts1);
    ret = VME_BlockTransfer(&blist, 1000);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      return;
    }	     
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    mbps = loop / (delta * 1024.0 * 1024.0);
    printf("Duration = %f us\n", delta * 1000000.0);
    printf("VMEbus A32/D64 DMA read [0x%04x bytes]:  %f MB/s\n", loop, mbps);
  }
 
  printf("Press return to continue. Next: Scan of VMEbus A32/D64 DMA write");
  getdecd(0);

  for (loop = 0x200; loop <= 0x10000; loop += 0x200)
  {
    blist.number_of_items                       = 1;
    blist.list_of_items[0].vmebus_address       = vbase;
    blist.list_of_items[0].system_iobus_address = cmem_desc_paddr;
    blist.list_of_items[0].size_requested       = loop;
    blist.list_of_items[0].control_word         = VME_DMA_D64W;
    ts_clock(&ts1);
    ret = VME_BlockTransfer(&blist, 1000);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      return;
    }
    ts_clock(&ts2);
    delta = ts_duration(ts1, ts2);
    mbps = loop / (delta * 1024.0 * 1024.0);
    printf("Duration = %f us\n", delta * 1000000.0);
    printf("VMEbus A32/D64 DMA write [0x%04x bytes]:  %f MB/s\n", loop, mbps);
  }

 
  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
}


/*****************/
int func_menu(void)
/*****************/
{
  int fun = 1;
  VME_ErrorCode_t ret, ret2;
  static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
  static VME_SlaveMap_t slave_map = {0x0, 0x1000, VME_A32, 0};
  static VME_BlockTransferList_t blist;
  static int awidth = 0, slot = 1, nbytes = 1, time_out = 1000, handle = 0, position = 0;
  static u_int ldata = 0, offset = 0x0, ct1 = 0, ct2 = 0;
  static u_short sdata = 0;
  static u_char bdata = 0;
  static u_int dma_number_of_items = 1, dma_vmebus_address = 0x00000000, dma_system_iobus_address = 0, dma_size_requested = 0x1000, dma_control_word = VME_DMA_D32R; 
  u_int *ptr, value;
  u_long lvalue;
  u_short *sptr;
  int iloop, enumb, rest, signum;
  char stext[20],etext[400];
  VME_BusErrorInfo_t bus_error_info;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:                                                              \n");
    printf("   1 VME_Open                         2 VME_Close                                          \n");
    printf("   3 VME_MasterMap                    4 VME_MasterMapDump           5 VME_MasterUnmap      \n");
    printf("   6 VME_SlaveMap                     7 VME_SlaveMapDump            8 VME_SlaveUnmap       \n");
    printf("   9 VME_MasterMapVirtualLongAddress 10 VME_SlaveMapVmebusAddress                          \n");
    printf("  11 VME_ReadSafeUInt                12 VME_ReadSafeUShort         13 VME_ReadSafeUChar    \n");
    printf("  14 VME_WriteSafeUInt               15 VME_WriteSafeUShort        16 VME_WriteSafeUChar   \n");
    printf("  17 VME_ReadFastUInt                18 VME_ReadFastUShort         19 VME_ReadFastUChar    \n");
    printf("  20 VME_WriteFastUInt               21 VME_WriteFastUShort        22 VME_WriteFastUChar   \n");    
    printf("  23 VME_BlockTransferInit           24 VME_BlockTransferStart     25 VME_BlockTransferWait\n");
    printf("  26 VME_BlockTransferEnd            27 VME_BlockTransferDump      28 VME_BlockTransfer    \n");
    printf("  29 VME_BlockTransferStatus         30 VME_BlockTransferRemaining                         \n");
    printf("  31 VME_ReadCRCSR                   32 VME_WriteCRCSR                                     \n");
    printf("  33 VME_BusErrorRegisterSignal      34 VME_BusErrorInfoGet                                \n");
    printf("  35 VME_ErrorPrint                  36 VME_ErrorString            37 VME_ErrorNumber      \n");
    printf("  38 VME_UniverseMap                 39 VME_Tsi148Map                                      \n");
    printf("  40 VME_SysfailSet                  41 VME_SysfailReset           42 VME_SysfailPoll      \n");
    printf("  43 VME_SendSysreset                                                                      \n");
    printf("  -----------------------------------------------------------------------------------------\n");
    printf("  94 CR/CSR dump                     95 VME_test                                           \n");
    printf("  96 peek                            97 poke                                               \n");
    printf("  98 speek                           99 spoke                                              \n");
    printf("   0 Exit                                                                              \n");
    printf("Your choice ");
    fun = getdecd(fun);  

    if(fun == 1)
    {
      ret = VME_Open();
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 2) 
    {
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 3)
    {
      printf("VMEbus address                        ");
      master_map.vmebus_address = gethexd(master_map.vmebus_address);
      printf("Window size                           ");
      master_map.window_size = gethexd(master_map.window_size);
      printf("Address modifier (%d=A24, %d=A32, %d=CR/CSR, %d=USER1, %d=USER2) ",VME_A24, VME_A32, VME_CRCSR, VME_USER1, VME_USER2);
      master_map.address_modifier = gethexd(master_map.address_modifier);
      printf("cycle type (1) (%d=supervisor  0=user) ", VME_AM_SUPERVISOR);
      ct1 = getdecd(ct1);
      printf("cycle type (2) (%d=program  0=data)    ", VME_AM_PROGRAM);
      ct2 = getdecd(ct2);
      master_map.options = ct1 | ct2;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("Handle = %d\n", handle);
    }
    
    if (fun == 4)
    {
      ret = VME_MasterMapDump();
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 5)
    {
      printf("Handle of mapping to be closed ");
      handle = getdecd(handle);
      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 6)
    {    
      printf("PCI MEM address                 ");
      slave_map.system_iobus_address = gethexd(slave_map.system_iobus_address);
      printf("Window size                     ");
      slave_map.window_size = gethexd(slave_map.window_size);
      printf("Address modifier (%d=A24, %d=A32) ",VME_A24, VME_A32);
      slave_map.address_width = gethexd(slave_map.address_width);
      printf("Options                         ");
      slave_map.options = gethexd(slave_map.options);
      ret = VME_SlaveMap(&slave_map, &handle);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("Handle = %d\n", handle);
    }  
    
    if (fun == 7)
    {
      ret = VME_SlaveMapDump();
      if(ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 8)
    {
      printf("Handle of mapping to be closed ");
      handle = getdecd(handle);
      ret = VME_SlaveUnmap(handle);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 9)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      ret = VME_MasterMapVirtualLongAddress(handle, &lvalue);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("The virtual address is 0x%016lx\n", lvalue);
    }
    
    if (fun == 10)
    {    
      printf("Handle of slave mapping to be used ");
      handle = getdecd(handle);
      ret = VME_SlaveMapVmebusAddress(handle, &value);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("The VMEbus address is 0x%08x\n", value);
    }
    
    if (fun == 11)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      ret = VME_ReadSafeUInt(handle, offset, &ldata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("Data read=0x%08x\n", ldata);
    }
    
    if (fun == 12)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      ret = VME_ReadSafeUShort(handle, offset, &sdata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("Data read=0x%04x\n", sdata);
    }
    
    if (fun == 13)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      ret = VME_ReadSafeUChar(handle, offset, &bdata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("Data read=0x%02x\n", bdata);
    } 
    
    if (fun == 14)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      printf("Data to be written ");
      ldata = gethexd(ldata);
      ret = VME_WriteSafeUInt(handle, offset, ldata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 15)
      {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      printf("Data to be written ");
      sdata = gethexd((unsigned int)sdata);
      ret = VME_WriteSafeUShort(handle, offset, sdata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 16)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      printf("Data to be written ");
      bdata = gethexd((unsigned int)bdata);
      ret = VME_WriteSafeUChar(handle, offset, bdata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    }
    
    if (fun == 17)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      VME_ReadFastUInt(handle ,offset, &ldata);
      printf("Data read=0x%08x\n", ldata);
    }
    
    if (fun == 18)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      VME_ReadFastUShort(handle, offset, &sdata);
      printf("Data read=0x%04x\n", sdata);
    }
    
    if (fun == 19)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      VME_ReadFastUChar(handle, offset, &bdata);
      printf("Data read=0x%02x\n", bdata);
    } 
    
    if (fun == 20)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      printf("Data to be written ");
      ldata = gethexd(ldata);
      VME_WriteFastUInt(handle, offset, ldata);
    }
    
    if (fun == 21)
      {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      printf("Data to be written ");
      sdata = gethexd((unsigned int)sdata);
      VME_WriteFastUShort(handle, offset, sdata);
    }
    
    if (fun == 22)
    {
      printf("Handle of master mapping to be used ");
      handle = getdecd(handle);
      printf("Address offset ");
      offset = gethexd(offset);
      printf("Data to be written ");
      bdata = gethexd((unsigned int)bdata);
      VME_WriteFastUChar(handle, offset, bdata);
    }
    
    if (fun == 23)
    {    
      printf("Number of list elements ");
      dma_number_of_items = getdecd(dma_number_of_items);
      blist.number_of_items = dma_number_of_items;
      for (iloop = 0; iloop < blist.number_of_items; iloop++)
      {
	printf("Enter parameters for element %d\n",iloop);
	printf("VMEbus address ");
	dma_vmebus_address = gethexd(dma_vmebus_address);
	blist.list_of_items[iloop].vmebus_address = dma_vmebus_address;

	printf("PCI address ");
	dma_system_iobus_address = gethexd(dma_system_iobus_address);
	blist.list_of_items[iloop].system_iobus_address = dma_system_iobus_address;

	printf("Size (in bytes) ");
	dma_size_requested = gethexd(dma_size_requested);
	blist.list_of_items[iloop].size_requested = dma_size_requested;

	printf("Control word:\n");
	printf("D32 write = 0x%08x\n", VME_DMA_D32W);
	printf("D32 read  = 0x%08x\n", VME_DMA_D32R);
	printf("D64 write = 0x%08x\n", VME_DMA_D64W);
	printf("D64 read  = 0x%08x\n", VME_DMA_D64R);
	printf("Your choice ");
	dma_control_word = gethexd(dma_control_word);
	blist.list_of_items[iloop].control_word = dma_control_word;
      }
      ret = VME_BlockTransferInit(&blist, &handle);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      else
	printf("Handle = %d\n", handle);
    }
    
    if (fun == 24)
    {  
      printf("Enter the number of the handle ");
      handle = getdecd(handle);   
      ret = VME_BlockTransferStart(handle);       
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);    
    }
    
    if (fun == 25)
    {     
      printf("Enter the number of the handle ");
      handle = getdecd(handle);
      printf("Enter the time-out ");
      time_out = getdecd(time_out);
      ret = VME_BlockTransferWait(handle, time_out, &blist);      
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
      if ((ret == VME_DMAERR) | (ret == VME_SUCCESS))
      {
        for (iloop = 0; iloop < blist.number_of_items; iloop++)
        {
          printf("Element:         %d\n", iloop);
          printf("Status:          %d\n", blist.list_of_items[iloop].status_word);
          printf("Bytes remaining: %d\n", blist.list_of_items[iloop].size_remaining);
        }
      } 
    }
    
    if (fun == 26)
    {    
      printf("Enter the number of the handle ");
      handle = getdecd(handle);   
      ret = VME_BlockTransferEnd(handle);       
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);     
    }
    
    if (fun == 27)
    {     
      ret = VME_BlockTransferDump();      
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);
    } 
       
    if (fun == 28)
    {    
      printf("Number of list elements ");
      dma_number_of_items = getdecd(dma_number_of_items);
      blist.number_of_items = dma_number_of_items;
      for (iloop = 0; iloop < blist.number_of_items; iloop++)
      {
	printf("Enter parameters for element %d\n",iloop);
	printf("VMEbus address ");
	dma_vmebus_address = gethexd(dma_vmebus_address);
	blist.list_of_items[iloop].vmebus_address = dma_vmebus_address;

	printf("PCI address ");
	dma_system_iobus_address = gethexd(cmem_desc_paddr);
	blist.list_of_items[iloop].system_iobus_address = gethexd(dma_system_iobus_address);

	printf("Size (in bytes) ");
	dma_size_requested = gethexd(dma_size_requested);
	blist.list_of_items[iloop].size_requested = dma_size_requested;

	printf("Control word:\n");
	printf("D16 write       = 0x%08x\n", VME_DMA_D16W);
	printf("D16 read        = 0x%08x\n", VME_DMA_D16R);
	printf("D32 write       = 0x%08x\n", VME_DMA_D32W);
	printf("D32 read        = 0x%08x\n", VME_DMA_D32R);
	printf("D64 write       = 0x%08x\n", VME_DMA_D64W);
	printf("D64 read        = 0x%08x\n", VME_DMA_D64R);
        printf("D32 FIFO write  = 0x%08x\n", VME_FIFO_DMA_D32W);
        printf("D32 FIFO read   = 0x%08x\n", VME_FIFO_DMA_D32R);
	printf("Your choice ");
	dma_control_word = gethexd(dma_control_word);
	blist.list_of_items[iloop].control_word = dma_control_word;
      }      
      printf("Enter the time-out ");
      time_out = getdecd(time_out);
      ret = VME_BlockTransfer(&blist, time_out);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret);      
      if (ret == VME_DMAERR || ret == VME_SUCCESS)
      {
        for (iloop = 0; iloop < blist.number_of_items; iloop++)
        {
          printf("Element:         %d\n", iloop);
          printf("Status:          0x%08x\n", blist.list_of_items[iloop].status_word);
          printf("Bytes remaining: 0x%08x\n", blist.list_of_items[iloop].size_remaining);
        }
      } 
    }
    
    if (fun == 29)
    {
      printf("This function must only be called after the execution of a block transfer\n");
      printf("Enter the position of the element in the block transfer list (0..N-1) ");
      position = getdecd(position);   
      ret = VME_BlockTransferStatus(&blist, position, &ret2);
      printf("The error code is 0x%08x\n", ret2);
    }
    
    if (fun == 30)
    { 
      printf("This function must only be called after the execution of a block transfer\n");
      printf("Enter the position of the element in the block transfer list (0..N-1) ");
      position = getdecd(position);   
      ret = VME_BlockTransferRemaining(&blist, position, &rest);
      printf("Bytes remaining: 0x%08x\n", rest);
    }
    
    if (fun == 31)
    {
      printf("Slot number            ");
      slot = getdecd(slot);
      printf("Address offset         ");
      offset = gethexd(offset);
      printf("Number of bytes (1..4) ");
      nbytes = getdecd(nbytes);
      printf("Data width (0=D8, 1=D16, 2=D32) ");
      awidth = getdecd(awidth);
      ret = VME_ReadCRCSR(slot, ((nbytes << 28) | (awidth << 24) | (offset & 0xfffff)), &ldata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret); 
      else
        printf("Data read = 0x%08x\n", ldata);
    } 
    
    if (fun == 32)
    {
      printf("Slot number            ");
      slot = getdecd(slot);
      printf("Address offset         ");
      offset = gethexd(offset);
      printf("Number of bytes (1..4) ");
      nbytes = getdecd(nbytes);
      printf("Data width (0=D8, 1=D16, 2=D32) ");
      awidth = getdecd(awidth);
      printf("Data                   ");
      ldata = gethexd(ldata);      
      ret = VME_WriteCRCSR(slot, ((nbytes << 28) | (awidth << 24) | (offset & 0xfffff)), ldata);
      if (ret != VME_SUCCESS)
	VME_ErrorPrint(ret); 
      else
        printf("Data written\n");
    }
    
    if (fun == 33)
    {
      printf("signal # ( 0 to unregister) = ");
      signum = getdecd(SIGBUS);

      ret = VME_BusErrorRegisterSignal(signum);
      if (ret != VME_SUCCESS)
      VME_ErrorPrint(ret);

    }
    
    if (fun == 34)
    {
      ret = VME_BusErrorInfoGet(&bus_error_info);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      else
      {
        printf(" VMEbus address = 0x%8x\n", bus_error_info.vmebus_address);
        printf(" VMEbus AM code = 0x%8x\n", bus_error_info.address_modifier);
        printf(" VMEbus LWORD*  = %d\n", bus_error_info.lword);
        printf(" VMEbus DS0*    = %d\n", bus_error_info.ds0);
        printf(" VMEbus DS1*    = %d\n", bus_error_info.ds1);
        printf(" VMEbus WRITE*  = %d\n", bus_error_info.wr);
        printf(" # bus errors   = %d\n", bus_error_info.multiple);
      }

    }
    
    if (fun == 35)
    {
      printf("Printing error VME_NOTKNOWN\n");
      VME_ErrorPrint(VME_NOTKNOWN);
    }
    
    if (fun == 36)
    {
      printf("Converting error VME_NOTKNOWN to string\n");
      VME_ErrorString(VME_NOTKNOWN, etext);
      printf("String is:\n %s \n", etext);
    }
    
    if (fun == 37)
    {
      VME_ErrorNumber(VME_NOTKNOWN, &enumb);
      printf("Error number of error VME_NOTKNOWN is %d\n", enumb);
    }  
      
    if (fun == 38)
    {
      ret = VME_UniverseMap(&lvalue);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      else
        printf("Virtual address of Universe registers = 0x%016lx\n", lvalue);
    }
    
    if (fun == 39)
    {
      ret = VME_Tsi148Map(&lvalue);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      else
        printf("Virtual address of Tsi148 registers = 0x%016lx\n", lvalue);
    }
    
    if (fun == 40)
    {
      ret = VME_SysfailSet();
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      else
        printf("The SYSFAIL is now active\n");
    }
    
    if (fun == 41)
    {
      ret = VME_SysfailReset();
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      else
        printf("The SYSFAIL is now inactive\n");
    }
    
    if (fun == 42)
    { 
      int flag;
      
      ret = VME_SysfailPoll(&flag);
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      else
      {
        if (flag)
          printf("The SYSFAIL is active\n");
        else
          printf("The SYSFAIL is inactive\n");      
      }
    } 

    if (fun == 43)
    {
      printf("Sending a 200 ms SYSRESET...\n");
      ret = VME_SendSysreset();     
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
      printf("Done.\n"); 
    }    

    if (fun == 94)
    {
      printf("Slot number            ");
      slot = getdecd(slot);
      
      ret = VME_ReadCRCSR(slot, CR_LENGTH, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CR: Length of ROM    = %s\n", ret ? "Bus error" : stext);
      
      ret = VME_ReadCRCSR(slot, CR_CRACCESSWIDTH, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CR: CR access width  = %s\n", ret ? "Bus error" : stext);
      
      ret = VME_ReadCRCSR(slot, CR_CSRACCESSWIDTH, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CR: CSR access width = %s\n", ret ? "Bus error" : stext);
      
      ret = VME_ReadCRCSR(slot, CR_MANUFID, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CR: Manufacturer ID  = %s\n", ret ? "Bus error" : stext);
      
      ret = VME_ReadCRCSR(slot, CR_BOARDID, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CR: Board ID         = %s\n", ret ? "Bus error" : stext);
            
      ret = VME_ReadCRCSR(slot, CR_REVID, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CR: Revision ID      = %s\n", ret ? "Bus error" : stext);
                  
      ret = VME_ReadCRCSR(slot, CR_ASCII1, &ldata);
      sprintf(stext, "%c", ldata);
      printf("CR: Character C      = %s\n", ret ? "Bus error" : stext);
      
      ret = VME_ReadCRCSR(slot, CR_ASCII2, &ldata);
      sprintf(stext, "%c", ldata);
      printf("CR: Character R      = %s\n", ret ? "Bus error" : stext);
      
      ret = VME_ReadCRCSR(slot, CSR_BAR, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CSR: BAR             = %s\n", ret ? "Bus error" : stext);
      
      ret = VME_ReadCRCSR(slot, CSR_ADER0, &ldata);
      sprintf(stext, "0x%08x", ldata);
      printf("CSR: ADER0           = %s\n", ret ? "Bus error" : stext);
    }
    
    if (fun == 95)
    {
      ret = VME_test();
      if (ret != VME_SUCCESS)
        VME_ErrorPrint(ret);
    }
    
    if (fun == 96)
    {
      printf("Virtual address ");
      lvalue = gethexd(lvalue);
      ptr = (u_int *)lvalue;
      ldata = *ptr;
      printf("Data read = 0x%08x\n", ldata);
    }
    
    if (fun == 97)
    {
      printf("Virtual address ");
      lvalue = gethexd(lvalue);
      printf("Data to be written ");
      ldata = gethexd(ldata);
      ptr = (u_int *)lvalue;
      *ptr = ldata;
      printf("Data written\n");
    } 
     
    if (fun == 98)
    {
      printf("Virtual address ");
      lvalue = gethexd(lvalue);
      sptr = (u_short *)lvalue;
      sdata = *sptr;
      printf("Data read = 0x%04x\n", sdata);
    }
    
    if (fun == 99)
    {
      printf("Virtual address ");
      lvalue = gethexd(lvalue);
      printf("Data to be written ");
      sdata = gethexd(sdata);
      sptr = (u_short *)lvalue;
      *sptr = sdata;
      printf("Data written\n");
    }
  }
  printf("=========================================================================\n\n");
  return(0);
}


/*********************/
u_int bswap(u_int word)
/*********************/
{
  return ((word & 0xFF) << 24) + ((word & 0xFF00) << 8) + ((word & 0xFF0000) >> 8) + ((word & 0xFF000000) >> 24);
}










