// $Id: 
/************************************************************************/
/*									*/
/* File: vme_poke.c							*/
/*									*/
/* Execute one VMEbus write cycle		 			*/
/*									*/
/* 17. Jul. 06  MAJO  created						*/
/*									*/
/**************** C 2006 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSGetInput/get_input.h"


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
  u_int ret, vmeaddr, vmedata;
  int handle, amcode, dsize;

  if ((argc == 5) && (sscanf(argv[4], "%d", &amcode) == 1)) {argc--;} else {amcode = 2;}
  if ((argc == 4) && (sscanf(argv[3], "%d", &dsize) == 1)) {argc--;} else {dsize = 4;}
  if ((argc == 3) && (sscanf(argv[2], "%x", &vmedata) == 1)) {argc--;} else {vmedata = 0x0;}
  if ((argc == 2) && (sscanf(argv[1], "%x", &vmeaddr) == 1)) {argc--;} else {vmeaddr = 0x0;}
  if (argc != 1) 
  {
    printf("Use:\nvme_poke <Address><Data><Data size><AM code>\n");
    printf("Address   = The hexadecimal VMEbus address                     Default: 0x0\n");
    printf("Data      = The hexadecimal data to be sent                    Default: 0x0\n");
    printf("Data size = The number of bytes to write (1=D8, 2=D16, 4=D32)  Default: 4\n");
    printf("AM code   = The AM code (0=A16, 1=A24, 2=A32)                  Default: 2\n");
    printf("\n"); 
    exit(0);
  }

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }

  master_map.vmebus_address   = vmeaddr;
  master_map.window_size      = 0x10;
  master_map.address_modifier = amcode;
  master_map.options          = 0;
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }

  if (dsize == 1)
  {
    ret = VME_WriteSafeUChar(handle, 0, vmedata);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(1);
    }
  }
  
  if (dsize == 2)
  {
    ret = VME_WriteSafeUShort(handle, 0, vmedata);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(1);
    }
  }
  
  if (dsize == 4)
  {
    ret = VME_WriteSafeUInt(handle, 0, vmedata);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      exit(1);
    }
  }

  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(1);
  }
  exit(0);
}
