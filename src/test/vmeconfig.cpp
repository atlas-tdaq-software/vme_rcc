// $Id$
/**************************************************************************************/
/*  This is the "vmeconfig" program to initialise the VMEbus interface	              */
/*  of a Universe based card. 						              */
/*									              */
/*  Author: Markus Joos, CERN						              */
/*									              */
/*  26.5.2016  Automatic PCI address allocation added by Benedikt Muessig, GBS-SHA    */
/*									              */
/********************** C 2016  A nickel program worth a dime *************************/   

#include <stdio.h>             
#include <stdlib.h>             
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/file.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "vme_rcc/universe.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"

#ifndef BSWAP

//macros
#ifdef __powerpc__              //MJ: Why PPC?
#define BSWAP(x) bswap(x)
#else
#define BSWAP(x) (x)
#endif

#endif // !BSWAP


//types
typedef struct 
{
  unsigned int vbase;      //VMEbus base address
  unsigned int pbase;      //PCI base address
  unsigned int size;       //size of window
  unsigned int wpost;      //1=use write posting  0=no write posting
  unsigned int rprefetch;  //1=use read prefetching  0=no read prefetching
  unsigned int vmespace;   //0=A16  1=A24  2=A32
  unsigned int pcispace;   //0=MEM  1=I/O  2=configuration
  unsigned int enable;     //0=disable decoder  1=enable decoder
  unsigned int decoder;    //0=decoder 0  1=decoder 1  2=decoder 2  3=decoder 3
} slavemap;

typedef struct
{
  unsigned int vbase;      //VMEbus base address
  unsigned int pbase;      //PCI base address
  unsigned int size;       //size of window*/ 
  unsigned int wpost;      //1=use write posting  0=no write posting
  unsigned int vmespace;   //0=A16  1=A24  2=A32  5=CR/CSR  6=USER1  7=USER2
  unsigned int supervisor; //1=supervisor AM  0=user AM
  unsigned int program;    //1=program AM  0=data AM
  unsigned int enable;     //0=disable decoder  1=enable decoder
  unsigned int decoder;    //NUmber of map decoder: Universe I: 0..3   Universe II: 0..7
} mastermap;

typedef struct
{
  mastermap mmap0;          //master map decoder #0
  mastermap mmap1;          //master map decoder #1
  mastermap mmap2;          //master map decoder #2
  mastermap mmap3;          //master map decoder #3
  mastermap mmap4;          //master map decoder #4 Universe II only
  mastermap mmap5;          //master map decoder #5 Universe II only
  mastermap mmap6;          //master map decoder #6 Universe II only
  mastermap mmap7;          //master map decoder #7 Universe II only
  slavemap smap0;           //slave map decoder #0
  slavemap smap1;           //slave map decoder #1
  slavemap smap2;           //slave map decoder #2
  slavemap smap3;           //slave map decoder #3
  slavemap smap4;           //slave map decoder #4 Universe II only
  slavemap smap5;           //slave map decoder #5 Universe II only
  slavemap smap6;           //slave map decoder #6 Universe II only
  slavemap smap7;           //slave map decoder #7 Universe II only
  unsigned int reqlvl;      //bus request level: 0=BR0  1=BR1  2=BR2  3=BR3
  unsigned int fair;        //0=fair arbit. disabled  1=fair arbit. enabled
  unsigned int relmode;     //0=RWD  1=ROR
  unsigned int vmeto;       //0=disabled  1=16us  2=32us  3=64us  4=128us  5=256us  6=512us  7=1024us
  unsigned int arbto;       //0=disabled  1=16us  2=256us
  unsigned int arbmode;     //0=Round robin  1=Priority
  unsigned int full_init;   //0=only base initialisation  1=full initialisation of Universe
  unsigned int irqs;        //mask for VMEbus interrupts
  unsigned int irq_mode[9]; //VME_INT_RORA or VME_INT_ROAK or VME_LEVELISDISABLED
  unsigned int ms;          //master byte swapping: 0=disabled  1=enabled
  unsigned int ss;          //slave byte swapping: 0=disabled  1=enable
  unsigned int fs;          //fast byte swapping: 0=disabled  1=enable
  unsigned int useram1;     //user defined AM code 1
  unsigned int useram2;     //user defined AM code 2
} unipara;


//globals
universe_regs_t *uni;
unipara params;
int utype, parfile;
char parfile_name[MAXPATHLEN];
char *unip, captxtbuf[50];
u_long unibase, pci_addr_base = 0, pci_addr_size = 0, pci_addr_offset = 0;

int open_universe();
int close_universe();
int set_slave_map(slavemap smap);
int set_master_map(mastermap mmap);
int set_fair(unsigned int fair);
int set_req_lvl(unsigned int level);
int set_rel_mode(unsigned int mode);
int set_vme_to(unsigned int to);
int set_arb_to(unsigned int to);
int set_arb_mode(unsigned int mode);
int set_irq(unsigned int mask);
int set_swap(unsigned int ms, unsigned int ss, unsigned int fs);
int set_useram(unsigned int am1, unsigned int am2);
int full_initialisation(unsigned int mode);
int get_slave_map();
int get_master_map();
int get_fair(unsigned int *fair);
int get_req_lvl(unsigned int *level);
int get_rel_mode(unsigned int *mode);
int get_vme_to(unsigned int *to);
int get_arb_to(unsigned int *to);
int get_arb_mode(unsigned int *mode); 
int get_irq(unsigned int *mask, unsigned int *irq_mode);
int get_swap(unsigned int *ms, unsigned int *ss, unsigned int *fs); 
int get_useram(unsigned int *am1, unsigned int *am2);
int set_default_param(unipara *pa);
int update_all(unipara pa);
int dump_parameters(unipara pa);
int exands();
int help();
int dettype();
int setdebug(void);
int pciaddr_finder_init(void);
u_long pciaddr_finder_getaddr(u_int *size);
int pci_addr_finder_checkautomode(unipara pa);


/*****************************/
int main(int argc,char *argv[])
/*****************************/
{
  unsigned int res, fun;
  int acti;

  if (argc != 3)
  {
    printf("Wrong number of parameters!\n");
    printf("Use: vmeconfig  -i <file name> to interactively configure the VME\n");
    printf("Use: vmeconfig  -a <file name> to automaticaly configure the VME\n");
    exit(0);
  }
  else
    if ((acti = getopt(argc, argv, "ai")) == -1)
    {
      printf("Invalid option\n");
      exit(-1);
    }
 
  if (sscanf(argv[2], "%s", parfile_name) != 1)
  {
    printf("Cannot read the file name!\n");
    exit(-1);
  }

  if (acti == 'a')
  {
    parfile = open(parfile_name, O_RDONLY, 0666);
    open_universe();
    dettype();
    res = read(parfile, &params, sizeof(unipara));
    if (res != sizeof(unipara))
    {
      printf("The file '%s' seems to be new or corrupted.\n", parfile_name);
      close_universe();
      exit(-1);
    }
    
    update_all(params);
    printf("vmeconfig: VMEbus interface initialised\n");
    close_universe();
    exit(-1);
  }
  else
    parfile = open(parfile_name, O_RDWR | O_CREAT, 0666);

  if (parfile == 0) 
  {
    printf("Cannot open parameter file\n");
    exit(-1);
  }

  open_universe();
  dettype();

  //read parameters from file
  res = read(parfile, &params, sizeof(unipara));
  if (res != sizeof(unipara))
  {  //failed to read parameters -> file new or currupted
    set_default_param(&params);
    printf("The file '%s' seems to be new or corrupted. \n", parfile_name);
    printf("Select '0' to quit if you do not want to modify it.\n");
  }

  fun = 1;

  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                             2 Dump all parameters\n");
    printf("   3 Set slave map decoder            4 Set master map decoder\n");
    printf("   5 Set VMEbus request level         6 Set VMEbus release mode\n");
    printf("   7 Set VMEbus time-out              8 Set Arbitration time-out\n");
    printf("   9 Set arbitration mode            10 Set FAIR arbitration mode\n");
    printf("  11 Set interrupt mask              12 Set user AM codes\n");
    printf("  13 Set byte swapping\n");
    printf("  15 Set default parameters          16 Update Universe registers\n");
    printf("  17 Exit and save                   18 Set debug parameters\n");
    printf("   0 Quit\n");                   
    printf("Your choice: ");                 
    fun = getdecd(fun);
    if (fun == 1) help();
    if (fun == 2) dump_parameters(params);
    if (fun == 3) get_slave_map();
    if (fun == 4) get_master_map();
    if (fun == 5) get_req_lvl(&params.reqlvl);  
    if (fun == 6) get_rel_mode(&params.relmode);
    if (fun == 7) get_vme_to(&params.vmeto);
    if (fun == 8) get_arb_to(&params.arbto);
    if (fun == 9) get_arb_mode(&params.arbmode);
    if (fun == 10) get_fair(&params.fair);
    if (fun == 11) get_irq(&params.irqs, &params.irq_mode[0]);
    if (fun == 12) get_useram(&params.useram1, &params.useram2);
    if (fun == 13) get_swap(&params.ms,&params.ss, &params.fs);
    if (fun == 15) set_default_param(&params);
    if (fun == 16) update_all(params);
    if (fun == 17) exands();
    if (fun == 18) setdebug();
  }
  close_universe();
  res = close(parfile);
  if(res)
    printf("Parameter file closed with error\n");
  exit(0);
}


/****************/
int setdebug(void)
/****************/
{
  static unsigned int dblevel = 0, dbpackage = DFDB_VMERCC;
  
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/***********/
int dettype()
/***********/
{
  int id;
  id = BSWAP(uni->pci_class) & 0xff;        //mask Revision ID   

  if (id == 0)
    utype = 1;                              //Universe I
  else if (id == 1)
    utype = 2;                              //Universe II
  else if (id == 2)
    utype = 2;                              //Universe IIb
  else
  {
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    printf("WARNING: The revision of the Universe chip is unknown\n");
    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");

    utype = 1;
  }

  return(0);
}


/********/
int help()
/********/
{
  printf("********************************************************************************\n");
  
  printf("Background:\n");
  printf("Before any single cycle access to VMEbus can be made it is necessary to program a map\n"); 
  printf("decoder of the Universe chip with appropriate parameters. The generation of block\n");
  printf("transfers is independent from the map decoders. Most drivers for the Universe chip allow\n");
  printf("to dynamically program a map decoder from user programs my means of a function call. We\n");
  printf("have decided not to do it that way but to provide a special application (this one) for\n"); 
  printf("that purpose. The master and slave mapping functions of the vme_rcc library only check\n");
  printf("if a mapping with the requested parameters is supported by the static set-up of the\n");
  printf("Universe chip and return the corresponding addresses or an error message. The main reason\n"); 
  printf("for this approach is that the Universe has only 8 map decoders. In systems with more\n");
  printf("than 8 slaves it is necessary to group slaves and program the map decoders accordingly.\n");
  printf("Functions for dynamic mappings bear the risk of run time errors when all map decoders\n"); 
  printf("are used. With vmeconfig the users has to lay out the VMEbus address space before the\n"); 
  printf("application can be started and is therefore guaranteed not to get run time\n");
  printf("errors.\n");
  
  printf("Currently vmeconfig requires the user to enter a PCI address range for each master\n");
  printf("mapping. It is important that these ranges do neither overlap nor conflict with address\n"); 
  printf("ranges used by PCI devices or other kernel resources (check with: more /proc/iomem)\n");
      
  printf("Modes of operation:\n");
  printf("Vmeconfig can be used in two ways:\n");
  printf("1) vmeconfig -i <parfile>\n");
  printf("This is the interactive mode. If <parfile> contains a valid set of parameters\n");
  printf("initialised by a privious session these will be loaded into memory. Otherwise\n");
  printf("default parameters will be used. During the session, all modifications will be\n");
  printf("made in memory.  Only if the save and exit function is called will they be\n");
  printf("copied to the file. The data file is not human readable.\n");
  printf("\n");
  printf("2) vmeconfig -a <parfile>\n");
  printf("This is the automatic mode. <parfile> has to be a valid parameter file.\n");
  printf("The parameters from <parfile> will be loaded iautomatically into to registers\n");
  printf("of the Universe chip. Use vmeconfig this way during system boot.\n");
  printf("\n");
  printf("\n");
  printf("Description of the menu points (interactive mode only):\n");
  printf("1)\n");
  printf("Display this text.\n");
  printf("\n");
  printf("2)\n");
  printf("This function dumps the contents of the parameter structure.\n");
  printf("\n");
  printf("3..13)\n");
  printf("These functions allow to specify parameters for the Universe registers. The \n");
  printf("values entered will only be copied into a structure in memory. The set-up of\n");
  printf("the VMEbus interface will not be updated. There is one special mode hidded in\n");
  printf("function 3 (Set slave map decoder): Selecting map decoder=0 and PCI address=1\n");
  printf("enables the auto-map feature. This tells vmeconfig (when used in the automatic mode)\n");
  printf("to read information about the Big Physical Area from /proc/cmem_rcc and\n");
  printf("/proc/bigphysarea and to slave map the full BPA to the selected VMEbus address.\n");
  printf("\n");
  printf("15)\n");
  printf("This function allows to delete all setings and to initialise the parameter\n");
  printf("structure with default values.\n");
  printf("\n");
  printf("16)\n");
  printf("Once all parameters have been defined with the functions 3..13 this function\n");
  printf("has to be called to actually program the registers of the VMEbus interface.\n");
  printf("\n");
  printf("17)\n");
  printf("As the name says: copy the parameter structure to the data file and exit.\n");
  printf("\n");
  printf("18)\n");
  printf("For experts only. Enable debugging messages from the vme_rcc library\n");
  printf("\n");
  printf("0)\n");
  printf("Quits the program without saving back the parameters to the file.\n");
  printf("\n");
  printf("Special functionality\n");
  printf("=====================\n");
  printf("Slave map decoder 0 supports a special mode to map the BPA buffer. See above\n");
  printf("********************************************************************************\n");
  return(0);
}


/*****************************/
int dump_parameters(unipara pa)
/*****************************/
{
  int res = pci_addr_finder_checkautomode(pa);

  if(res < -1) 
    printf("%d MANUALLY configured master map(s) enabled.\n\n", -(res + 1));
  else if (res == -1) 
    printf("No master maps enabled.\n\n");
  else if (res == 0) 
    printf("Warning, invalid configuration! Mixed auto and manual PCI addressing modes.\n\n");
  else
    printf("%d automatically configured master map(s) enabled.\n\n", res);

  printf("Master map decoder 0 Decoder number  = %d\n", pa.mmap0.decoder);
  printf("Master map decoder 0 Enabled         = %s\n", pa.mmap0.enable?"Yes":"No");
  printf("Master map decoder 0 VME base address= 0x%08x\n", pa.mmap0.vbase); 
  printf("Master map decoder 0 PCI base address= ");
  if(!pa.mmap0.pbase && pa.mmap0.enable) printf("[AUTO]\n"); else printf("0x%08x\n", pa.mmap0.pbase);
  printf("Master map decoder 0 Window size     = 0x%08x\n", pa.mmap0.size);
  printf("Master map decoder 0 Write posting   = %s\n", pa.mmap0.wpost?"Yes":"No");
  printf("Master map decoder 0 VME space       = ");
  if (pa.mmap0.vmespace == 0) printf("A16\n");
  if (pa.mmap0.vmespace == 1) printf("A24\n");
  if (pa.mmap0.vmespace == 2) printf("A32\n"); 
  if (pa.mmap0.vmespace == 5) printf("CR/CSR\n"); 
  if (pa.mmap0.vmespace == 6) printf("USER1\n"); 
  if (pa.mmap0.vmespace == 7) printf("USER2\n"); 
  printf("Master map decoder 0 AM code         = ");
  if (pa.mmap0.supervisor == 1 && pa.mmap0.program == 1) printf("Supervisor / Program\n\n");
  if (pa.mmap0.supervisor == 0 && pa.mmap0.program == 1) printf("User / Program\n\n");
  if (pa.mmap0.supervisor == 1 && pa.mmap0.program == 0) printf("Supervisor / Data\n\n");
  if (pa.mmap0.supervisor == 0 && pa.mmap0.program == 0) printf("User / Data\n\n");

  printf("Master map decoder 1 Decoder number  = %d\n", pa.mmap1.decoder);
  printf("Master map decoder 1 Enabled         = %s\n", pa.mmap1.enable?"Yes":"No");
  printf("Master map decoder 1 VME base address= 0x%08x\n", pa.mmap1.vbase);
  printf("Master map decoder 1 PCI base address= ");
  if(!pa.mmap1.pbase && pa.mmap1.enable) printf("[AUTO]\n"); else printf("0x%08x\n", pa.mmap1.pbase);
  printf("Master map decoder 1 Window size     = 0x%08x\n", pa.mmap1.size);
  printf("Master map decoder 1 Write posting   = %s\n", pa.mmap1.wpost?"Yes":"No");
  printf("Master map decoder 1 VME space       = ");
  if (pa.mmap1.vmespace == 0) printf("A16\n");     
  if (pa.mmap1.vmespace == 1) printf("A24\n");
  if (pa.mmap1.vmespace == 2) printf("A32\n");
  if (pa.mmap1.vmespace == 5) printf("CR/CSR\n");
  if (pa.mmap1.vmespace == 6) printf("USER1\n"); 
  if (pa.mmap1.vmespace == 7) printf("USER2\n"); 
  printf("Master map decoder 1 AM code         = ");
  if (pa.mmap1.supervisor == 1 && pa.mmap1.program == 1) printf("Supervisor / Program\n\n");
  if (pa.mmap1.supervisor == 0 && pa.mmap1.program == 1) printf("User / Program\n\n");
  if (pa.mmap1.supervisor == 1 && pa.mmap1.program == 0) printf("Supervisor / Data\n\n");
  if (pa.mmap1.supervisor == 0 && pa.mmap1.program == 0) printf("User / Data\n\n");
  
  printf("Master map decoder 2 Decoder number  = %d\n", pa.mmap2.decoder);
  printf("Master map decoder 2 Enabled         = %s\n", pa.mmap2.enable?"Yes":"No");
  printf("Master map decoder 2 VME base address= 0x%08x\n", pa.mmap2.vbase);
  printf("Master map decoder 2 PCI base address= ");
  if(!pa.mmap2.pbase && pa.mmap2.enable) printf("[AUTO]\n"); else printf("0x%08x\n", pa.mmap2.pbase);
  printf("Master map decoder 2 Window size     = 0x%08x\n", pa.mmap2.size);
  printf("Master map decoder 2 Write posting   = %s\n", pa.mmap2.wpost?"Yes":"No");
  printf("Master map decoder 2 VME space       = ");
  if (pa.mmap2.vmespace == 0) printf("A16\n");     
  if (pa.mmap2.vmespace == 1) printf("A24\n");
  if (pa.mmap2.vmespace == 2) printf("A32\n");
  if (pa.mmap2.vmespace == 5) printf("CR/CSR\n");
  if (pa.mmap2.vmespace == 6) printf("USER1\n"); 
  if (pa.mmap2.vmespace == 7) printf("USER2\n"); 
  printf("Master map decoder 2 AM code         = ");
  if (pa.mmap2.supervisor == 1 && pa.mmap2.program == 1) printf("Supervisor / Program\n\n");
  if (pa.mmap2.supervisor == 0 && pa.mmap2.program == 1) printf("User / Program\n\n");
  if (pa.mmap2.supervisor == 1 && pa.mmap2.program == 0) printf("Supervisor / Data\n\n");
  if (pa.mmap2.supervisor == 0 && pa.mmap2.program == 0) printf("User / Data\n\n");
  
  printf("Master map decoder 3 Decoder number  = %d\n", pa.mmap3.decoder);
  printf("Master map decoder 3 Enabled         = %s\n", pa.mmap3.enable?"Yes":"No");
  printf("Master map decoder 3 VME base address= 0x%08x\n", pa.mmap3.vbase);
  printf("Master map decoder 3 PCI base address= ");
  if(!pa.mmap3.pbase && pa.mmap3.enable) printf("[AUTO]\n"); else printf("0x%08x\n", pa.mmap3.pbase);
  printf("Master map decoder 3 Window size     = 0x%08x\n", pa.mmap3.size);
  printf("Master map decoder 3 Write posting   = %s\n", pa.mmap3.wpost?"Yes":"No");
  printf("Master map decoder 3 VME space       = ");
  if (pa.mmap3.vmespace == 0) printf("A16\n");     
  if (pa.mmap3.vmespace == 1) printf("A24\n");
  if (pa.mmap3.vmespace == 2) printf("A32\n");
  if (pa.mmap3.vmespace == 5) printf("CR/CSR\n");
  if (pa.mmap3.vmespace == 6) printf("USER1\n"); 
  if (pa.mmap3.vmespace == 7) printf("USER2\n"); 
  printf("Master map decoder 3 AM code         = ");
  if (pa.mmap3.supervisor == 1 && pa.mmap3.program == 1) printf("Supervisor / Program\n\n");
  if (pa.mmap3.supervisor == 0 && pa.mmap3.program == 1) printf("User / Program\n\n");
  if (pa.mmap3.supervisor == 1 && pa.mmap3.program == 0) printf("Supervisor / Data\n\n");
  if (pa.mmap3.supervisor == 0 && pa.mmap3.program == 0) printf("User / Data\n\n");
  
  if (utype == 2)
  {
    printf("Master map decoder 4 Decoder number  = %d\n" ,pa.mmap4.decoder);
    printf("Master map decoder 4 Enabled         = %s\n", pa.mmap4.enable?"Yes":"No");
    printf("Master map decoder 4 VME base address= 0x%08x\n", pa.mmap4.vbase);
    printf("Master map decoder 4 PCI base address= ");
    if(!pa.mmap4.pbase && pa.mmap4.enable) printf("[AUTO)]\n"); else printf("0x%08x\n", pa.mmap4.pbase);
    printf("Master map decoder 4 Window size     = 0x%08x\n", pa.mmap4.size); 
    printf("Master map decoder 4 Write posting   = %s\n", pa.mmap4.wpost?"Yes":"No");
    printf("Master map decoder 4 VME space       = ");
    if (pa.mmap4.vmespace == 0) printf("A16\n");  
    if (pa.mmap4.vmespace == 1) printf("A24\n");  
    if (pa.mmap4.vmespace == 2) printf("A32\n");  
    if (pa.mmap4.vmespace == 5) printf("CR/CSR\n");
    if (pa.mmap4.vmespace == 6) printf("USER1\n"); 
    if (pa.mmap4.vmespace == 7) printf("USER2\n"); 
    printf("Master map decoder 4 AM code         = ");
    if (pa.mmap4.supervisor == 1 && pa.mmap4.program == 1) printf("Supervisor / Program\n\n");
    if (pa.mmap4.supervisor == 0 && pa.mmap4.program == 1) printf("User / Program\n\n");
    if (pa.mmap4.supervisor == 1 && pa.mmap4.program == 0) printf("Supervisor / Data\n\n");
    if (pa.mmap4.supervisor == 0 && pa.mmap4.program == 0) printf("User / Data\n\n");
  
    printf("Master map decoder 5 Decoder number  = %d\n", pa.mmap5.decoder);
    printf("Master map decoder 5 Enabled         = %s\n", pa.mmap5.enable?"Yes":"No");
    printf("Master map decoder 5 VME base address= 0x%08x\n", pa.mmap5.vbase);
    printf("Master map decoder 5 PCI base address= ");
    if(!pa.mmap5.pbase && pa.mmap5.enable) printf("[AUTO]\n"); else printf("0x%08x\n", pa.mmap5.pbase);
    printf("Master map decoder 5 Window size     = 0x%08x\n", pa.mmap5.size); 
    printf("Master map decoder 5 Write posting   = %s\n", pa.mmap5.wpost?"Yes":"No");
    printf("Master map decoder 5 VME space       = ");
    if (pa.mmap5.vmespace == 0) printf("A16\n");  
    if (pa.mmap5.vmespace == 1) printf("A24\n");  
    if (pa.mmap5.vmespace == 2) printf("A32\n");  
    if (pa.mmap5.vmespace == 5) printf("CR/CSR\n");
    if (pa.mmap5.vmespace == 6) printf("USER1\n"); 
    if (pa.mmap5.vmespace == 7) printf("USER2\n"); 
    printf("Master map decoder 5 AM code         = ");
    if (pa.mmap5.supervisor == 1 && pa.mmap5.program == 1) printf("Supervisor / Program\n\n");
    if (pa.mmap5.supervisor == 0 && pa.mmap5.program == 1) printf("User / Program\n\n");
    if (pa.mmap5.supervisor == 1 && pa.mmap5.program == 0) printf("Supervisor / Data\n\n");
    if (pa.mmap5.supervisor == 0 && pa.mmap5.program == 0) printf("User / Data\n\n");
    
    printf("Master map decoder 6 Decoder number  = %d\n", pa.mmap6.decoder);
    printf("Master map decoder 6 Enabled         = %s\n", pa.mmap6.enable?"Yes":"No");
    printf("Master map decoder 6 VME base address= 0x%08x\n", pa.mmap6.vbase);
    printf("Master map decoder 6 PCI base address= ");
    if(!pa.mmap6.pbase && pa.mmap6.enable) printf("[AUTO]\n"); else printf("0x%08x\n", pa.mmap6.pbase);
    printf("Master map decoder 6 Window size     = 0x%08x\n", pa.mmap6.size); 
    printf("Master map decoder 6 Write posting   = %s\n", pa.mmap6.wpost?"Yes":"No");
    printf("Master map decoder 6 VME space       = ");
    if (pa.mmap6.vmespace == 0) printf("A16\n");  
    if (pa.mmap6.vmespace == 1) printf("A24\n");  
    if (pa.mmap6.vmespace == 2) printf("A32\n");  
    if (pa.mmap6.vmespace == 5) printf("CR/CSR\n");
    if (pa.mmap6.vmespace == 6) printf("USER1\n"); 
    if (pa.mmap6.vmespace == 7) printf("USER2\n"); 
    printf("Master map decoder 6 AM code         = ");
    if (pa.mmap6.supervisor == 1 && pa.mmap6.program == 1) printf("Supervisor / Program\n\n");
    if (pa.mmap6.supervisor == 0 && pa.mmap6.program == 1) printf("User / Program\n\n");
    if (pa.mmap6.supervisor == 1 && pa.mmap6.program == 0) printf("Supervisor / Data\n\n");
    if (pa.mmap6.supervisor == 0 && pa.mmap6.program == 0) printf("User / Data\n\n");
    
    printf("Master map decoder 7 Decoder number  = %d\n", pa.mmap7.decoder);
    printf("Master map decoder 7 Enabled         = %s\n", pa.mmap7.enable?"Yes":"No");
    printf("Master map decoder 7 VME base address= 0x%08x\n", pa.mmap7.vbase);
    printf("Master map decoder 7 PCI base address= ");
    if(!pa.mmap7.pbase && pa.mmap7.enable) printf("[AUTO]\n"); else printf("0x%08x\n", pa.mmap7.pbase);
    printf("Master map decoder 7 Window size     = 0x%08x\n", pa.mmap7.size); 
    printf("Master map decoder 7 Write posting   = %s\n", pa.mmap7.wpost?"Yes":"No");
    printf("Master map decoder 7 VME space       = ");
    if (pa.mmap7.vmespace == 0) printf("A16\n");  
    if (pa.mmap7.vmespace == 1) printf("A24\n");  
    if (pa.mmap7.vmespace == 2) printf("A32\n"); 
    if (pa.mmap7.vmespace == 5) printf("CR/CSn\n"); 
    if (pa.mmap7.vmespace == 6) printf("USER1\n\n"); 
    if (pa.mmap7.vmespace == 7) printf("USER2\n");
    printf("Master map decoder 7 AM code         = ");
    if (pa.mmap7.supervisor == 1 && pa.mmap7.program == 1) printf("Supervisor / Program\n\n");
    if (pa.mmap7.supervisor == 0 && pa.mmap7.program == 1) printf("User / Program\n\n");
    if (pa.mmap7.supervisor == 1 && pa.mmap7.program == 0) printf("Supervisor / Data\n\n");
    if (pa.mmap7.supervisor == 0 && pa.mmap7.program == 0) printf("User / Data\n\n"); 
  } 

  printf("Slave map decoder 0 Decoder number  = %d\n", pa.smap0.decoder);
  printf("Slave map decoder 0 Enabled         = %s\n", pa.smap0.enable?"Yes":"No");
  printf("Slave map decoder 0 VME base address= 0x%08x\n", pa.smap0.vbase);
  if (pa.smap0.pbase == 1)
    printf("Slave map decoder 0 PCI base address= Auto-map BPA\n");
  else

  printf("Slave map decoder 0 PCI base address= 0x%08x\n", pa.smap0.pbase);
  printf("Slave map decoder 0 Window size     = 0x%08x\n", pa.smap0.size);
  printf("Slave map decoder 0 Write posting   = %s\n", pa.smap0.wpost?"Yes":"No");
  printf("Slave map decoder 0 Read prefetching= %s\n", pa.smap0.rprefetch?"Yes":"No");
  printf("Slave map decoder 0 VME space       = ");
  if (pa.smap0.vmespace == 0) printf("A16\n");
  if (pa.smap0.vmespace == 1) printf("A24\n");
  if (pa.smap0.vmespace == 2) printf("A32\n");
  printf("Slave map decoder 0 PCI space       = ");
  if (pa.smap0.pcispace == 0) printf("MEM\n\n");
  if (pa.smap0.pcispace == 1) printf("I/O\n\n");
  if (pa.smap0.pcispace == 2) printf("Configuration\n\n");

  printf("Slave map decoder 1 Decoder number  = %d\n", pa.smap1.decoder);    
  printf("Slave map decoder 1 Enabled         = %s\n", pa.smap1.enable?"Yes":"No");
  printf("Slave map decoder 1 VME base address= 0x%08x\n", pa.smap1.vbase);

  printf("Slave map decoder 1 PCI base address= 0x%08x\n", pa.smap1.pbase);
  printf("Slave map decoder 1 Window size     = 0x%08x\n", pa.smap1.size);
  printf("Slave map decoder 1 Write posting   = %s\n", pa.smap1.wpost?"Yes":"No");
  printf("Slave map decoder 1 Read prefetching= %s\n", pa.smap1.rprefetch?"Yes":"No");
  printf("Slave map decoder 1 VME space       = ");
  if (pa.smap1.vmespace == 0) printf("A16\n"); 
  if (pa.smap1.vmespace == 1) printf("A24\n");
  if (pa.smap1.vmespace == 2) printf("A32\n");   
  printf("Slave map decoder 1 PCI space       = ");
  if (pa.smap1.pcispace == 0) printf("MEM\n\n");
  if (pa.smap1.pcispace == 1) printf("I/O\n\n");
  if (pa.smap1.pcispace == 2) printf("Configuration\n\n");

  printf("Slave map decoder 2 Decoder number  = %d\n", pa.smap2.decoder);    
  printf("Slave map decoder 2 Enabled         = %s\n", pa.smap2.enable?"Yes":"No");
  printf("Slave map decoder 2 VME base address= 0x%08x\n", pa.smap2.vbase);

  printf("Slave map decoder 2 PCI base address= 0x%08x\n", pa.smap2.pbase);
  printf("Slave map decoder 2 Window size     = 0x%08x\n", pa.smap2.size);
  printf("Slave map decoder 2 Write posting   = %s\n", pa.smap2.wpost?"Yes":"No");
  printf("Slave map decoder 2 Read prefetching= %s\n", pa.smap2.rprefetch?"Yes":"No");
  printf("Slave map decoder 2 VME space       = ");
  if (pa.smap2.vmespace == 0) printf("A16\n"); 
  if (pa.smap2.vmespace == 1) printf("A24\n");
  if (pa.smap2.vmespace == 2) printf("A32\n");   
  printf("Slave map decoder 2 PCI space       = ");
  if (pa.smap2.pcispace == 0) printf("MEM\n\n");
  if (pa.smap2.pcispace == 1) printf("I/O\n\n");
  if (pa.smap2.pcispace == 2) printf("Configuration\n\n");

  printf("Slave map decoder 3 Decoder number  = %d\n", pa.smap3.decoder);    
  printf("Slave map decoder 3 Enabled         = %s\n", pa.smap3.enable?"Yes":"No");
  printf("Slave map decoder 3 VME base address= 0x%08x\n", pa.smap3.vbase);
  printf("Slave map decoder 3 PCI base address= 0x%08x\n", pa.smap3.pbase);
  printf("Slave map decoder 3 Window size     = 0x%08x\n", pa.smap3.size);
  printf("Slave map decoder 3 Write posting   = %s\n", pa.smap3.wpost?"Yes":"No");
  printf("Slave map decoder 3 Read prefetching= %s\n", pa.smap3.rprefetch?"Yes":"No");
  printf("Slave map decoder 3 VME space       = ");
  if (pa.smap3.vmespace == 0) printf("A16\n"); 
  if (pa.smap3.vmespace == 1) printf("A24\n");
  if (pa.smap3.vmespace == 2) printf("A32\n");   
  printf("Slave map decoder 3 PCI space       = ");
  if (pa.smap3.pcispace == 0) printf("MEM\n\n");
  if (pa.smap3.pcispace == 1) printf("I/O\n\n");
  if (pa.smap3.pcispace == 2) printf("Configuration\n\n");

  if(utype==2)
  {
    printf("Slave map decoder 4 Decoder number  = %d\n", pa.smap4.decoder);
    printf("Slave map decoder 4 Enabled         = %s\n", pa.smap4.enable?"Yes":"No");
    printf("Slave map decoder 4 VME base address= 0x%08x\n", pa.smap4.vbase);
    printf("Slave map decoder 4 PCI base address= 0x%08x\n", pa.smap4.pbase);
    printf("Slave map decoder 4 Window size     = 0x%08x\n", pa.smap4.size);
    printf("Slave map decoder 4 Write posting   = %s\n", pa.smap4.wpost?"Yes":"No");
    printf("Slave map decoder 4 Read prefetching= %s\n", pa.smap4.rprefetch?"Yes":"No");
    printf("Slave map decoder 4 VME space       = ");
    if (pa.smap4.vmespace == 0) printf("A16\n");   
    if (pa.smap4.vmespace == 1) printf("A24\n");
    if (pa.smap4.vmespace == 2) printf("A32\n");   
    printf("Slave map decoder 4 PCI space       = ");
    if (pa.smap4.pcispace == 0) printf("MEM\n\n"); 
    if (pa.smap4.pcispace == 1) printf("I/O\n\n");
    if (pa.smap4.pcispace == 2) printf("Configuration\n\n");

    printf("Slave map decoder 5 Decoder number  = %d\n", pa.smap5.decoder);
    printf("Slave map decoder 5 Enabled         = %s\n", pa.smap5.enable?"Yes":"No");
    printf("Slave map decoder 5 VME base address= 0x%08x\n", pa.smap5.vbase);
    printf("Slave map decoder 5 PCI base address= 0x%08x\n", pa.smap5.pbase);
    printf("Slave map decoder 5 Window size     = 0x%08x\n", pa.smap5.size);
    printf("Slave map decoder 5 Write posting   = %s\n", pa.smap5.wpost?"Yes":"No");
    printf("Slave map decoder 5 Read prefetching= %s\n", pa.smap5.rprefetch?"Yes":"No");
    printf("Slave map decoder 5 VME space       = ");
    if (pa.smap5.vmespace == 0) printf("A16\n"); 
    if (pa.smap5.vmespace == 1) printf("A24\n");
    if (pa.smap5.vmespace == 2) printf("A32\n");
    printf("Slave map decoder 5 PCI space       = ");
    if (pa.smap5.pcispace == 0) printf("MEM\n\n");
    if (pa.smap5.pcispace == 1) printf("I/O\n\n");
    if (pa.smap5.pcispace == 2) printf("Configuration\n\n");

    printf("Slave map decoder 6 Decoder number  = %d\n", pa.smap6.decoder);
    printf("Slave map decoder 6 Enabled         = %s\n", pa.smap6.enable?"Yes":"No");
    printf("Slave map decoder 6 VME base address= 0x%08x\n", pa.smap6.vbase);
    printf("Slave map decoder 6 PCI base address= 0x%08x\n", pa.smap6.pbase);
    printf("Slave map decoder 6 Window size     = 0x%08x\n", pa.smap6.size);
    printf("Slave map decoder 6 Write posting   = %s\n", pa.smap6.wpost?"Yes":"No");
    printf("Slave map decoder 6 Read prefetching= %s\n", pa.smap6.rprefetch?"Yes":"No");
    printf("Slave map decoder 6 VME space       = ");
    if (pa.smap6.vmespace == 0) printf("A16\n"); 
    if (pa.smap6.vmespace == 1) printf("A24\n");
    if (pa.smap6.vmespace == 2) printf("A32\n");
    printf("Slave map decoder 6 PCI space       = ");
    if (pa.smap6.pcispace == 0) printf("MEM\n\n");
    if (pa.smap6.pcispace == 1) printf("I/O\n\n");
    if (pa.smap6.pcispace == 2) printf("Configuration\n\n");

    printf("Slave map decoder 7 Decoder number  = %d\n", pa.smap7.decoder);
    printf("Slave map decoder 7 Enabled         = %s\n", pa.smap7.enable?"Yes":"No");
    printf("Slave map decoder 7 VME base address= 0x%08x\n", pa.smap7.vbase);
    printf("Slave map decoder 7 PCI base address= 0x%08x\n", pa.smap7.pbase);
    printf("Slave map decoder 7 Window size     = 0x%08x\n", pa.smap7.size);
    printf("Slave map decoder 7 Write posting   = %s\n", pa.smap7.wpost?"Yes":"No");
    printf("Slave map decoder 7 Read prefetching= %s\n", pa.smap7.rprefetch?"Yes":"No");
    printf("Slave map decoder 7 VME space       = ");
    if (pa.smap7.vmespace == 0) printf("A16\n"); 
    if (pa.smap7.vmespace == 1) printf("A24\n");
    if (pa.smap7.vmespace == 2) printf("A32\n");
    printf("Slave map decoder 7 PCI space       = ");
    if (pa.smap7.pcispace == 0) printf("MEM\n\n");
    if (pa.smap7.pcispace == 1) printf("I/O\n\n");
    if (pa.smap7.pcispace == 2) printf("Configuration\n\n");
  }

  printf("VMEbus request level          = %d\n", pa.reqlvl);
  printf("Fair arbitration              = %s\n", pa.fair?"Yes":"No");
  printf("VMEbus release mode           = %s\n", pa.relmode?"ROR":"RWD");
  printf("VMEbus time out               = ");
  if (pa.vmeto == 0) printf("Disabled\n");
  if (pa.vmeto == 1) printf("16 us\n");
  if (pa.vmeto == 2) printf("32 us\n");
  if (pa.vmeto == 3) printf("64 us\n");
  if (pa.vmeto == 4) printf("128 us\n");
  if (pa.vmeto == 5) printf("256 us\n");
  if (pa.vmeto == 6) printf("512 us\n");
  if (pa.vmeto == 7) printf("1024 us\n");
  printf("VMEbus arbitration time out   = ");
  if (pa.arbto == 0) printf("Disabled\n");
  if (pa.arbto == 1) printf("16 us\n");
  if (pa.arbto == 2) printf("256 us\n");
  printf("VMEbus arbitration mode       = %s\n", pa.arbmode?"Priority":"Round robin");
  printf("VMEbus interrupt level 1 is %s", (pa.irqs&0x02)?"enabled":"masked");
  if (pa.irqs&0x02)
  {
    printf("  Mode = ");
    if (pa.irq_mode[1] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[1] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");
      
  printf("VMEbus interrupt level 2 is %s", (pa.irqs&0x04)?"enabled":"masked");
  if (pa.irqs&0x04)
  {
    printf("  Mode = ");
    if (pa.irq_mode[2] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[2] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 3 is %s", (pa.irqs&0x08)?"enabled":"masked");
  if (pa.irqs&0x08)
  {
    printf("  Mode = ");
    if (pa.irq_mode[3] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[3] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 4 is %s", (pa.irqs&0x10)?"enabled":"masked");
  if (pa.irqs&0x10)
  {
    printf("  Mode = ");
    if (pa.irq_mode[4] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[4] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 5 is %s", (pa.irqs&0x20)?"enabled":"masked");
  if (pa.irqs&0x20)
  {
    printf("  Mode = ");
    if (pa.irq_mode[5] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[5] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 6 is %s", (pa.irqs&0x40)?"enabled":"masked");
  if (pa.irqs&0x40)
  {
    printf("  Mode = ");
    if (pa.irq_mode[6] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[6] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");
  
  printf("VMEbus interrupt level 7 is %s", (pa.irqs&0x80)?"enabled":"masked");
  if (pa.irqs&0x80)
  {
    printf("  Mode = ");
    if (pa.irq_mode[7] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[7] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");
    
  printf("SYSFAIL interrupt %s", (pa.irqs&0x4000)?"enabled":"masked");
  if (pa.irqs&0x4000)
  {
    printf("  Mode = ");
    if (pa.irq_mode[8] == VME_INT_RORA) printf("RORA\n");
    else if (pa.irq_mode[8] == VME_INT_ROAK) printf("ROAK\n");
    else printf("Undefined\n");
  }
  else
    printf("\n");  

  printf("Master byte swapping          = %s\n", pa.ms?"enabled":"disabled");
  printf("Slave byte swapping           = %s\n", pa.ss?"enabled":"disabled");
  printf("Fast byte swapping            = %s\n", pa.fs?"enabled":"disabled");

  printf("User AM code 1                = 0x%02x\n", pa.useram1);
  printf("User AM code 2                = 0x%02x\n", pa.useram2);

  return(0);
}


/**********/
int exands()
/**********/
{
  int res = pci_addr_finder_checkautomode(params);

  printf("Saving configuration.\n");
  if(res < -1) 
    printf("Notice: %d MANUALLY configured master map(s) enabled.\n", -(res + 1));
  else if(res == -1) 
    printf("Notice: No master maps enabled.\n");
  else if(res == 0) 
    printf("Error, invalid configuration! Mixed auto and manual PCI addressing modes.\n");
  else
    printf("Notice: %d automatically configured master map(s) enabled.\n", res);

  res = lseek(parfile , 0 , 0);
  if (res)
    printf("\nCannot set file pointer to beginning of file\n");

  res = write(parfile, &params, sizeof(unipara));
  if (res != sizeof(unipara))
    printf("\nCannot write to file\n");

  res = close(parfile);
  if (res)
    printf("\nParameter file closed with error\n");

  close_universe();
  exit(0);
}


/********************************/
int set_default_param(unipara *pa)
/********************************/
{
  pa->mmap0.vbase      = 0;
  pa->mmap0.pbase      = 0;
  pa->mmap0.size       = 0; 
  pa->mmap0.wpost      = 0;
  pa->mmap0.vmespace   = 2;
  pa->mmap0.enable     = 0; 
  pa->mmap0.decoder    = 0;
  pa->mmap0.supervisor = 0;
  pa->mmap0.program    = 0;
 
  pa->mmap1.vbase      = 0;
  pa->mmap1.pbase      = 0;
  pa->mmap1.size       = 0; 
  pa->mmap1.wpost      = 0;
  pa->mmap1.vmespace   = 2; 
  pa->mmap1.enable     = 0;  
  pa->mmap1.decoder    = 1; 
  pa->mmap1.supervisor = 0;
  pa->mmap1.program    = 0;
  
  pa->mmap2.vbase      = 0;
  pa->mmap2.pbase      = 0;
  pa->mmap2.size       = 0; 
  pa->mmap2.wpost      = 0;
  pa->mmap2.vmespace   = 2; 
  pa->mmap2.enable     = 0;  
  pa->mmap2.decoder    = 2; 
  pa->mmap2.supervisor = 0;
  pa->mmap2.program    = 0;
  
  pa->mmap3.vbase      = 0;
  pa->mmap3.pbase      = 0;
  pa->mmap3.size       = 0; 
  pa->mmap3.wpost      = 0;
  pa->mmap3.vmespace   = 2; 
  pa->mmap3.enable     = 0;  
  pa->mmap3.decoder    = 3; 
  pa->mmap3.supervisor = 0;
  pa->mmap3.program    = 0;
  
  if(utype==2)
  {
    pa->mmap4.vbase      = 0;
    pa->mmap4.pbase      = 0;
    pa->mmap4.size       = 0;    
    pa->mmap4.wpost      = 0;  
    pa->mmap4.vmespace   = 2;
    pa->mmap4.enable     = 0;  
    pa->mmap4.decoder    = 4;
    pa->mmap4.supervisor = 0;
    pa->mmap4.program    = 0;
  
    pa->mmap5.vbase      = 0;
    pa->mmap5.pbase      = 0;
    pa->mmap5.size       = 0;
    pa->mmap5.wpost      = 0;
    pa->mmap5.vmespace   = 2;
    pa->mmap5.enable     = 0;
    pa->mmap5.decoder    = 5;
    pa->mmap5.supervisor = 0;
    pa->mmap5.program    = 0;
  
    pa->mmap6.vbase      = 0;
    pa->mmap6.pbase      = 0;
    pa->mmap6.size       = 0;
    pa->mmap6.wpost      = 0;
    pa->mmap6.vmespace   = 2;
    pa->mmap6.enable     = 0;
    pa->mmap6.decoder    = 6;
    pa->mmap6.supervisor = 0;
    pa->mmap6.program    = 0;
  
    pa->mmap7.vbase      = 0;
    pa->mmap7.pbase      = 0;
    pa->mmap7.size       = 0;
    pa->mmap7.wpost      = 0;
    pa->mmap7.vmespace   = 2;
    pa->mmap7.enable     = 0;
    pa->mmap7.decoder    = 7;
    pa->mmap7.supervisor = 0;
    pa->mmap7.program    = 0;
  }

  pa->smap0.vbase     = 0;
  pa->smap0.pbase     = 0;
  pa->smap0.size      = 0;
  pa->smap0.wpost     = 0;
  pa->smap0.rprefetch = 0;
  pa->smap0.vmespace  = 2;
  pa->smap0.pcispace  = 0;
  pa->smap0.enable    = 0;
  pa->smap0.decoder   = 0;

  pa->smap1.vbase     = 0;
  pa->smap1.pbase     = 0;
  pa->smap1.size      = 0; 
  pa->smap1.wpost     = 0;
  pa->smap1.rprefetch = 0;
  pa->smap1.vmespace  = 2; 
  pa->smap1.pcispace  = 0;
  pa->smap1.enable    = 0;   
  pa->smap1.decoder   = 1; 

  pa->smap2.vbase     = 0;
  pa->smap2.pbase     = 0;
  pa->smap2.size      = 0; 
  pa->smap2.wpost     = 0;
  pa->smap2.rprefetch = 0;
  pa->smap2.vmespace  = 2; 
  pa->smap2.pcispace  = 0;
  pa->smap2.enable    = 0;   
  pa->smap2.decoder   = 2; 

  pa->smap3.vbase     = 0;
  pa->smap3.pbase     = 0;
  pa->smap3.size      = 0; 
  pa->smap3.wpost     = 0;
  pa->smap3.rprefetch = 0;
  pa->smap3.vmespace  = 2; 
  pa->smap3.pcispace  = 0;
  pa->smap3.enable    = 0;   
  pa->smap3.decoder   = 3; 

  if(utype==2)
  {
    pa->smap4.vbase     = 0;
    pa->smap4.pbase     = 0;
    pa->smap4.size      = 0;    
    pa->smap4.wpost     = 0;    
    pa->smap4.rprefetch = 0;
    pa->smap4.vmespace  = 2; 
    pa->smap4.pcispace  = 0; 
    pa->smap4.enable    = 0;   
    pa->smap4.decoder   = 4; 

    pa->smap5.vbase     = 0;
    pa->smap5.pbase     = 0;
    pa->smap5.size      = 0; 
    pa->smap5.wpost     = 0;
    pa->smap5.rprefetch = 0;
    pa->smap5.vmespace  = 2;
    pa->smap5.pcispace  = 0; 
    pa->smap5.enable    = 0;   
    pa->smap5.decoder   = 5;  

    pa->smap6.vbase     = 0;
    pa->smap6.pbase     = 0;
    pa->smap6.size      = 0; 
    pa->smap6.wpost     = 0;
    pa->smap6.rprefetch = 0;
    pa->smap6.vmespace  = 2;
    pa->smap6.pcispace  = 0; 
    pa->smap6.enable    = 0;   
    pa->smap6.decoder   = 6;  

    pa->smap7.vbase     = 0;
    pa->smap7.pbase     = 0;
    pa->smap7.size      = 0; 
    pa->smap7.wpost     = 0;
    pa->smap7.rprefetch = 0;
    pa->smap7.vmespace  = 2;
    pa->smap7.pcispace  = 0; 
    pa->smap7.enable    = 0;   
    pa->smap7.decoder   = 7;  
  }
  pa->reqlvl    = 3;
  pa->fair      = 0;
  pa->relmode   = 0;
  pa->vmeto     = 5;
  pa->arbto     = 2;
  pa->arbmode   = 0;
  pa->full_init = 1; //Initialise also registers like pci latency counter or interrupts
  pa->irqs      = 0;
  pa->irq_mode[0] = 1; //Valid
  pa->irq_mode[1] = VME_LEVELISDISABLED;
  pa->irq_mode[2] = VME_LEVELISDISABLED;
  pa->irq_mode[3] = VME_LEVELISDISABLED;
  pa->irq_mode[4] = VME_LEVELISDISABLED;
  pa->irq_mode[5] = VME_LEVELISDISABLED;
  pa->irq_mode[6] = VME_LEVELISDISABLED;
  pa->irq_mode[7] = VME_LEVELISDISABLED;
  pa->irq_mode[8] = VME_LEVELISDISABLED;
  pa->ms        = 1;
  pa->ss        = 0;
  pa->fs        = 0;
  pa->useram1   = 0x10;
  pa->useram2   = 0x20;
  return(0);
}


/************************/
int update_all(unipara pa)
/************************/
{
  int ret, all_ok = 1;
  u_int status;

  ret = pci_addr_finder_checkautomode(pa);
  
  if (ret < -1) 
    printf("Notice: Enabling MANUAL mode, as %d MANUALLY configured master map(s) were found.\n", -(ret + 1));
  else if (ret == -1) 
    printf("Notice: No master maps enabled.\n");
  else if (ret == 0) 
  {
    printf("Invalid configuration! For automatic mode to work, all PCI addresses have to be set to zero!\n");
    printf("Hint: To use manual mode, disable all unused master maps and set the PCI addresses to a non-zero value.\n");
    exit(-1);
  } 
  else 
  {
    printf("Notice: Enabling automatic mode, as %d automatically configured master map(s) were found.\n", ret);
    if (pciaddr_finder_init())
      exit(-1);
  }
  
  if(!pa.mmap0.pbase && pa.mmap0.enable) { if(!(pa.mmap0.pbase = pciaddr_finder_getaddr(&(pa.mmap0.size)))) exit(-1); }
  if(!pa.mmap1.pbase && pa.mmap1.enable) { if(!(pa.mmap1.pbase = pciaddr_finder_getaddr(&(pa.mmap1.size)))) exit(-1); }
  if(!pa.mmap2.pbase && pa.mmap2.enable) { if(!(pa.mmap2.pbase = pciaddr_finder_getaddr(&(pa.mmap2.size)))) exit(-1); }
  if(!pa.mmap3.pbase && pa.mmap3.enable) { if(!(pa.mmap3.pbase = pciaddr_finder_getaddr(&(pa.mmap3.size)))) exit(-1); }
  if(utype == 2) 
  {
    if(!pa.mmap4.pbase && pa.mmap4.enable) { if(!(pa.mmap4.pbase = pciaddr_finder_getaddr(&(pa.mmap4.size)))) exit(-1); }
    if(!pa.mmap5.pbase && pa.mmap5.enable) { if(!(pa.mmap5.pbase = pciaddr_finder_getaddr(&(pa.mmap5.size)))) exit(-1); }
    if(!pa.mmap6.pbase && pa.mmap6.enable) { if(!(pa.mmap6.pbase = pciaddr_finder_getaddr(&(pa.mmap6.size)))) exit(-1); }
    if(!pa.mmap7.pbase && pa.mmap7.enable) { if(!(pa.mmap7.pbase = pciaddr_finder_getaddr(&(pa.mmap7.size)))) exit(-1); }
  }

  ret = set_master_map(pa.mmap0);
  if (ret != 0) { all_ok = 0; printf("update_all(1) returns %d\n", ret); }
  ret = set_master_map(pa.mmap1);
  if (ret != 0) { all_ok = 0; printf("update_all(2) returns %d\n", ret); }
  ret = set_master_map(pa.mmap2);
  if (ret != 0) { all_ok = 0; printf("update_all(3) returns %d\n", ret); }
  ret = set_master_map(pa.mmap3);
  if (ret != 0) { all_ok = 0; printf("update_all(4) returns %d\n", ret); }
  if (utype == 2)
  {
    ret = set_master_map(pa.mmap4);
    if (ret != 0) { all_ok = 0; printf("update_all(15) returns %d\n", ret); }
    ret = set_master_map(pa.mmap5);
    if (ret != 0) { all_ok = 0; printf("update_all(16) returns %d\n", ret); }
    ret = set_master_map(pa.mmap6);
    if (ret != 0) { all_ok = 0; printf("update_all(17) returns %d\n", ret); }
    ret = set_master_map(pa.mmap7);
    if (ret != 0) { all_ok = 0; printf("update_all(18) returns %d\n", ret); }
  }

  ret = set_slave_map(pa.smap0);
  if (ret != 0) { all_ok = 0; printf("update_all(5) returns %d\n", ret); }
  ret = set_slave_map(pa.smap1);
  if (ret != 0) { all_ok = 0; printf("update_all(6) returns %d\n", ret); }
  ret = set_slave_map(pa.smap2);
  if (ret != 0) { all_ok = 0; printf("update_all(7) returns %d\n", ret); }
  ret = set_slave_map(pa.smap3);
  if (ret != 0) { all_ok = 0; printf("update_all(8) returns %d\n", ret); }
  if (utype == 2)
    {
    ret = set_slave_map(pa.smap4);
    if (ret != 0) { all_ok = 0; printf("update_all(19) returns %d\n", ret); }
    ret = set_slave_map(pa.smap5);
    if (ret != 0) { all_ok = 0; printf("update_all(20) returns %d\n", ret); }
    ret = set_slave_map(pa.smap6);
    if (ret != 0) { all_ok = 0; printf("update_all(21) returns %d\n", ret); }
    ret = set_slave_map(pa.smap7);
    if (ret != 0) { all_ok = 0; printf("update_all(22) returns %d\n", ret); }
    }

  ret = set_fair(pa.fair);
  if (ret != 0) { all_ok = 0; printf("update_all(9) returns %d\n", ret);  }
  ret = set_req_lvl(pa.reqlvl);
  if (ret != 0) { all_ok = 0; printf("update_all(10) returns %d\n", ret); }
  ret = set_rel_mode(pa.relmode);
  if (ret != 0) { all_ok = 0; printf("update_all(11) returns %d\n", ret); }
  ret = set_vme_to(pa.vmeto);
  if (ret != 0) { all_ok = 0; printf("update_all(12) returns %d\n", ret); }
  ret = set_arb_to(pa.arbto);
  if (ret != 0) { all_ok = 0; printf("update_all(13) returns %d\n", ret); }
  ret = set_arb_mode(pa.arbmode); 
  if (ret != 0) { all_ok = 0; printf("update_all(14) returns %d\n", ret); }
  ret = full_initialisation(pa.full_init);
  if (ret != 0) { all_ok = 0; printf("update_all(15) returns %d\n", ret); }
  ret = set_irq(pa.irqs);
  if (ret != 0) { all_ok = 0;printf("update_all(16) returns %d\n", ret); }
  ret = set_useram(pa.useram1, pa.useram2);
  if (ret != 0) { all_ok = 0; printf("update_all(17) returns %d\n", ret); }
  ret = set_swap(pa.ms, pa.ss, pa.fs);
  if (ret != 0) { all_ok = 0; printf("update_all(18) returns %d\n", ret); }
 
  if (!all_ok)
  {
    printf("Your configuration file contains errors. The Universe chip is not properly programmed!\n");
    exit(-1);
  } 
  else
    printf("Universe registers programmed\n");

  status = VME_Update(&pa.irq_mode[0]);
  if (status)
  {
    VME_ErrorPrint(status);
    exit(-1);
  }  
  printf("VME_RCC driver synchronized\n");
  return(0);
}


/*****************/
int get_slave_map()
/*****************/
{
  static int decoder = 0;
  int maxmap;
  slavemap *smap;

  if (utype == 1)
    maxmap = 3;
  else
    maxmap = 7;

  printf("Enter number of map decoder        <0..%d> ", maxmap);
  decoder = getdecd(decoder);
  if (utype == 1 && decoder > 3) decoder = 3;
  if (utype == 2 && decoder > 7) decoder = 7;
  if (decoder < 0) decoder = 0;

  if (decoder == 0) smap = &params.smap0;
  if (decoder == 1) smap = &params.smap1;
  if (decoder == 2) smap = &params.smap2;
  if (decoder == 3) smap = &params.smap3;
  if (decoder == 4) smap = &params.smap4;
  if (decoder == 5) smap = &params.smap5;
  if (decoder == 6) smap = &params.smap6;
  if (decoder == 7) smap = &params.smap7;

  smap->decoder = decoder;
  printf("Enable map decoder            <0=no 1=yes> ");
  smap->enable = getdecd((int)smap->enable);
  printf("smap->enable is %d\n", smap->enable);

  if (smap->enable)
  {
    printf("Select VMEbus base address                   ");
    smap->vbase = gethexd(smap->vbase);

    if (decoder == 0)
      printf("Select PCI base address (0x1 = auto-map BPA) ");
    else 
      printf("Select PCI base address                      ");
    smap->pbase = gethexd(smap->pbase);

    if (smap->pbase == 1)  //auto map
    {
      printf("Auto-map defaults are:\n");
      printf(" PCI base address      = BPA base address\n");
      printf(" Slave-map window size = BPA size\n");
      printf(" Write posting         = Yes\n");
      printf(" Read prefetching      = Yes\n");
      printf(" Address space         = A32\n");
      smap->pcispace  = 0;   //will be determined dynamically
      smap->size      = 0;   //will be determined dynamically
      smap->wpost     = 1;
      smap->rprefetch = 1;
      smap->vmespace  = 2;
    }
    else
    {
      printf("Select PCI space            <0=MEM, 1=I/O>   ");
      smap->pcispace = getdecd((int)smap->pcispace);

      printf("Select the window size (bytes)               ");
      smap->size = gethexd(smap->size);

      printf("Select Write posting          <0=no 1=yes>   ");
      smap->wpost = getdecd((int)smap->wpost);

      printf("Select read prefetching       <0=no 1=yes>   ");
      smap->rprefetch = getdecd((int)smap->rprefetch);

      printf("Select address space <0=A16, 1=A24, 2=A32>   ");
      smap->vmespace = getdecd((int)smap->vmespace);
    }
  }
  return(0);
}
 
 
/******************/
int get_master_map()
/******************/
{
  static int decoder = 0;
  int maxmap, res;
  mastermap *mmap;

  if (utype == 1)
    maxmap = 3;
  else
    maxmap = 7;

  printf("Enter number of map decoder        <0..%d> ", maxmap);
  decoder = getdecd(decoder);
  if (utype == 1 && decoder > 3) decoder = 3;
  if (utype == 2 && decoder > 7) decoder = 7;
  if (decoder < 0) decoder = 0;

  if (decoder == 0) mmap = &params.mmap0;
  if (decoder == 1) mmap = &params.mmap1;
  if (decoder == 2) mmap = &params.mmap2;
  if (decoder == 3) mmap = &params.mmap3;
  if (decoder == 4) mmap = &params.mmap4;
  if (decoder == 5) mmap = &params.mmap5;
  if (decoder == 6) mmap = &params.mmap6;
  if (decoder == 7) mmap = &params.mmap7;

  res = pci_addr_finder_checkautomode(params);

  mmap->decoder = decoder;
  printf("Enable map decoder            <0=no 1=yes> ");
  mmap->enable = getdecd((int)mmap->enable);
 
  if (mmap->enable)
  { 
    printf("Select VMEbus base address                          ");
    mmap->vbase = gethexd(mmap->vbase);

    if(res < 1) {
      if(res == -1) {
	printf("NEW: vmeconfig is now able to allocate PCI addresses for you.\n");
	printf("If you want to control the PCI<->VME mapping yourself you just have\n");
	printf("to enter the PCI address for the master mapping below as you did in\n");
	printf("the past.\n");
	printf("If you want to use the new automatic allocation you have to enter a\n");
	printf("PCI base address of \"0\" for all master map decoders.\n");
	printf("The PCI addresses will be allocated automatically when the <vmetab>\n");
	printf("file gets loaded into the H/W at boot time. You can check the result\n");
	printf("with function 2/2 of cctscope.\n");
	printf("In case of an error during the address allocation\n");
	printf("the file /proc/vme_rcc will tell you that the Universe chip has not\n");
	printf("been initialized. The error messages can be found in the boot log\n");
	printf("file.\n");
      }
      printf("Select PCI base address                             ");
      mmap->pbase = gethexd(mmap->pbase);
    }

    printf("Select the window size (bytes)                      ");
    mmap->size = gethexd(mmap->size);

    printf("Select Write posting                   <0=no 1=yes> ");
    mmap->wpost = getdecd((int)mmap->wpost);

    printf("Select address space\n");
    printf("  <0=A16, 1=A24, 2=A32, 5=CR/CSR, 6=USER1, 7=USER2> ");
    mmap->vmespace = getdecd((int)mmap->vmespace);

    printf("Select cycle type            <0=User, 1=Supervisor> ");
    mmap->supervisor = getdecd((int)mmap->supervisor);

    printf("Select cycle type               <0=Data, 1=Program> ");
    mmap->program = getdecd((int)mmap->program);    
  }
  return(0);
} 


/**************************************************************/
int get_swap(unsigned int *ms,unsigned int *ss,unsigned int *fs)
/**************************************************************/
{
  printf("Enable master byte swapping <0=no 1=yes> "); 
  *ms = getdecd((int)*ms);
  printf("Enable slave byte swapping  <0=no 1=yes> "); 
  *ss = getdecd((int)*ss);
  printf("Enable fast byte swapping   <0=no 1=yes> "); 
  *fs = getdecd((int)*fs);

  return(0);
}


/******************************/ 
int get_fair(unsigned int *fair)
/******************************/
{
  printf("ATTENTION: Due to a bug in rev. 1 of the UNIVERSE fair arbitration\n");
  printf("           must not be used. It should work with rev. 2\n\n");
  printf("Select fair arbitration <0=no 1=yes> "); 
  *fair = getdecd((int)*fair);
  return(0);
}


/**********************************/ 
int get_req_lvl(unsigned int *level)
/**********************************/ 
{
  printf("Select bus request level <0..3> ");
  *level = getdecd((int)*level);
  return(0);
}


/**********************************/ 
int get_rel_mode(unsigned int *mode)
/**********************************/ 
{
  printf("Select release mode <0=RWD 1=ROR> ");
  *mode = getdecd((int)*mode);
  return(0);
}


/******************************/ 
int get_vme_to(unsigned int *to)
/******************************/ 
{
  printf("Select VMEbus time-out\n");
  printf("<0=disabled 1=16us 2=32us 3=64us 4=128us 5=256us 6=512us 7=1024us> ");
  *to = getdecd((int)*to);
  return(0);
}


/******************************/ 
int get_arb_to(unsigned int *to)
/******************************/ 
{
  printf("Select arbitration time-out <0=disabled 1=16us 2=256us> ");
  *to = getdecd((int)*to);
  return(0);
}


/**********************************/ 
int get_arb_mode(unsigned int *mode)
/**********************************/ 
{
  printf("Select release mode <0=Round Robin 1=Priority> ");
  *mode = getdecd((int)*mode);
  return(0);
} 


/**************************************************/ 
int get_useram(unsigned int *am1, unsigned int *am2)
/**************************************************/ 
{
  printf("Enter AM code 1: 0x");
  *am1 = gethexd((int)*am1);
  if ((*am1 < 0x10) && (*am1 > 0x1f))
    printf("Error: AM code has to be in the range 0x10...0x1f\n");

  printf("Enter AM code 2: 0x");
  *am2 = gethexd((int)*am2);  
  if ((*am2 < 0x10) && (*am2 > 0x1f))
    printf("Error: AM code has to be in the range 0x10...0x1f\n");

  return(0);
}


/*****************************************************/ 
int get_irq(unsigned int *mask, unsigned int *irq_mode)
/*****************************************************/ 
{
  unsigned int value;

  printf("Enable interrupts on level 1 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 1) & 0x01);
  if (value) *mask |= 0x02; else *mask &= 0xfffffffd;
  if (!value)
    irq_mode[1] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[1] = getdecd((int)irq_mode[1]);
  }  

  printf("Enable interrupts on level 2 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 2) & 0x1);
  if (value) *mask |= 0x04; else *mask &= 0xfffffffb;
  if (!value)
    irq_mode[2] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[2] = getdecd((int)irq_mode[2]);
  }
    
  printf("Enable interrupts on level 3 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 3) & 0x1);
  if (value) *mask |= 0x08; else *mask &= 0xfffffff7;
  if (!value)
    irq_mode[3] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[3] = getdecd((int)irq_mode[3]);
  }
    
  printf("Enable interrupts on level 4 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 4) & 0x1);
  if (value) *mask |= 0x10; else *mask &= 0xffffffef;
  if (!value)
    irq_mode[4] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[4] = getdecd((int)irq_mode[4]);
  }
    
  printf("Enable interrupts on level 5 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 5) & 0x1);
  if (value) *mask |= 0x20; else *mask &= 0xffffffdf;
  if (!value)
    irq_mode[5] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[5] = getdecd((int)irq_mode[5]);
  }
    
  printf("Enable interrupts on level 6 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 6) & 0x1);
  if (value) *mask |= 0x40; else *mask &= 0xffffffbf;
  if (!value)
    irq_mode[6] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[6] = getdecd((int)irq_mode[6]);
  }
    
  printf("Enable interrupts on level 7 (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 7) & 0x1);
  if (value) *mask |= 0x80; else *mask &= 0xffffff7f;
  if (!value)
    irq_mode[7] = VME_LEVELISDISABLED;
  else
  {
    printf("Select the mode (%d = ROAK,  %d = RORA) : ", VME_INT_ROAK, VME_INT_RORA);
    irq_mode[7] = getdecd((int)irq_mode[7]);
  }

  printf("Enable SYSFAIL interrupt (0=no 1=yes) : ");
  value = getdecd(((int)*mask >> 14) & 0x1);
  if (value) *mask |= 0x4000; else *mask &= 0xffffbfff;
  if (!value)
    irq_mode[8] = VME_LEVELISDISABLED;
  else
    irq_mode[8] = VME_INT_RORA;

  irq_mode[0] = 1; //Valid
  return(0);
}


/********************************/
int set_master_map(mastermap mmap)
/********************************/
{
  u_int ctld, not_ok, give_it_up = 100, gap_offset;
  volatile u_int *vp1, *vp2, *vp3, *vp4;

  /*check the input*/
  if (utype == 1 && mmap.decoder > 3) return(-1);
  if (utype == 2 && mmap.decoder > 7) return(-1);
  if (mmap.wpost > 1) return(-2);
  if (mmap.vmespace == 3 || mmap.vmespace == 4) return(-3);
  if (mmap.decoder == 0 || mmap.decoder == 3) mmap.size &= 0xfffff000; else mmap.size &= 0xffff0000;
  if (mmap.size == 0 && mmap.enable == 1) return(-4);
  if (mmap.enable && (mmap.decoder == 0 || mmap.decoder == 3) && mmap.size == 0)
  {
    mmap.size = 0x1000; 
    printf("Master map decoder %d: Size set to 0x1000\n", mmap.decoder);
  }
  if (mmap.enable && mmap.decoder != 0 && mmap.decoder != 3 && mmap.size == 0)
  {
    mmap.size = 0x10000;
    printf("Master map decoder %d: Size set to 0x10000\n", mmap.decoder);
  }

  ctld = (0x800000 | (mmap.enable << 31) | (mmap.wpost << 30) | (mmap.vmespace << 16) | (mmap.supervisor << 12) | (mmap.program << 14));
 
  if (mmap.decoder < 4)
    gap_offset = 0;
  else
    gap_offset = 80;

  vp1 = (u_int *)((u_long)&uni->lsi0_ctl + 0x14 * mmap.decoder + gap_offset);
  vp2 = (u_int *)((u_long)&uni->lsi0_bs + 0x14 * mmap.decoder + gap_offset);
  vp3 = (u_int *)((u_long)&uni->lsi0_bd + 0x14 * mmap.decoder + gap_offset);
  vp4 = (u_int *)((u_long)&uni->lsi0_to + 0x14 * mmap.decoder + gap_offset);
  
  not_ok = 1;
  while(not_ok && give_it_up)
  {
    not_ok = 0;
    give_it_up--;
    *vp1 = BSWAP(ctld);
    *vp2 = BSWAP(mmap.pbase);
    *vp3 = BSWAP(mmap.pbase + mmap.size);
    *vp4 = BSWAP(mmap.vbase - mmap.pbase);

    if (*vp1 != ctld)
      not_ok = 1;

    if (*vp2 != mmap.pbase)
      not_ok = 1;

    if (*vp3 != (mmap.pbase + mmap.size))
      not_ok = 1;

    if (*vp4 != (mmap.vbase - mmap.pbase))
      not_ok = 1;

    if (not_ok)
    {
      printf("Error in set_master_map.\n");
      printf("(set_master_map)   mmap.decoder  = %d\n", mmap.decoder); 
      printf("(set_master_map)   mmap.wpost    = %d\n", mmap.wpost); 
      printf("(set_master_map)   mmap.vmespace = %d\n", mmap.vmespace); 
      printf("(set_master_map)   mmap.enable   = %d\n", mmap.enable); 
      printf("(set_master_map)   mmap.size     = 0x%08x\n", mmap.size); 
      printf("(set_master_map)   mmap.pbase    = 0x%08x\n", mmap.pbase); 
      printf("(set_master_map)   mmap.vbase    = 0x%08x\n", mmap.vbase); 
      printf("(set_master_map)   vp1 is 0x%08x\n", *vp1);
      printf("(set_master_map)   vp2 is 0x%08x\n", *vp2);
      printf("(set_master_map)   vp3 is 0x%08x\n", *vp3);
      printf("(set_master_map)   vp4 is 0x%08x\n", *vp4);    
    }
  }
  return(0);
}


/******************************/
int set_slave_map(slavemap smap)
/******************************/
{
  unsigned int lpbase, lpsize, ctld;
  char sram_name[] = "/proc/cmem_rcc";
  char bpa_name[] = "/proc/bigphysarea";
  char cdummy[500], c2[500];
  char *spos;
  FILE *sram_fp, *bpa_fp;

  //check the input
  if (utype == 1 && smap.decoder > 3) return(-1);
  if (utype == 2 && smap.decoder > 7) return(-1);
  if (smap.wpost > 1) return(-2);
  if (smap.rprefetch > 1) return(-3);
  if (smap.vmespace > 2) return(-4);
  if (smap.pcispace > 2) return(-5);
  if (smap.decoder == 0 || smap.decoder == 3) smap.size &= 0xfffff000; else smap.size &= 0xffff0000;
  if (smap.enable && (smap.decoder == 0 || smap.decoder == 3) && smap.size == 0 && smap.pbase != 1)
  {
    smap.size = 0x1000;
    printf("Slave map decoder %d: Size set to 0x1000\n", smap.decoder);
  }
  if (smap.enable && smap.decoder != 0 && smap.decoder != 3 && smap.size == 0)
  {
    smap.size = 0x10000;
    printf("Slave map decoder %d: Size set to 0x10000\n", smap.decoder);
  }

  ctld = (0xf00000 | (smap.enable << 31) | (smap.wpost << 30) | (smap.rprefetch << 29) | (smap.vmespace << 16) | (smap.pcispace));

  if (smap.decoder == 0)
  {
    if (smap.pbase == 1)
    {
      sram_fp = fopen(sram_name, "r");
      if (sram_fp == 0)
      {
        printf("Auto-map: Can't open %s\n", sram_name);
        printf("Auto-map: PCI base address of BPA defaults to 0x00000000\n");
        printf("Auto-map: iom_sram and related packages will not work\n");
        lpbase = 0;
        lpsize = 0;
      }
      else
      {
        fscanf(sram_fp,"%x", &lpbase);
        fclose(sram_fp);
            
	bpa_fp = fopen(bpa_name, "r");
	if (bpa_fp == 0)
	{
          printf("Auto-map: Can't open %s\n", bpa_name);
          printf("Auto-map: BPA size defaults to 0x00000000\n");
          printf("Auto-map: iom_sram and related packages will not work\n");
          lpsize = 0;
	}
	else
	{
          fgets(cdummy, 200, bpa_fp);
          spos = strstr(cdummy, "size");
          strcpy(c2, &cdummy[spos - cdummy + 5]);
          sscanf(c2, "%u", &lpsize);
          lpsize *= 1024;

          printf("Auto-map: PCI base address of BPA = 0x%08x\n", lpbase);
          printf("Auto-map: Window size of BPA      = 0x%08x\n", lpsize);
          fclose(bpa_fp);
	}
      }
    }
    
    
    else
    {
      lpbase = smap.pbase;
      lpsize = smap.size;
    }
    uni->vsi0_bs  = BSWAP(smap.vbase);
    uni->vsi0_bd  = BSWAP(smap.vbase + lpsize);
    uni->vsi0_to  = BSWAP(lpbase - smap.vbase);
    uni->vsi0_ctl = BSWAP(ctld);
  }
  if (smap.decoder == 1)
  {
    uni->vsi1_bs  = BSWAP(smap.vbase);
    uni->vsi1_bd  = BSWAP(smap.vbase + smap.size);
    uni->vsi1_to  = BSWAP(smap.pbase - smap.vbase);
    uni->vsi1_ctl = BSWAP(ctld); 
  } 
  if (smap.decoder == 2)
  {
    uni->vsi2_bs  = BSWAP(smap.vbase);
    uni->vsi2_bd  = BSWAP(smap.vbase + smap.size);
    uni->vsi2_to  = BSWAP(smap.pbase - smap.vbase);
    uni->vsi2_ctl = BSWAP(ctld); 
  } 
  if (smap.decoder == 3)
  {
    uni->vsi3_bs  = BSWAP(smap.vbase);
    uni->vsi3_bd  = BSWAP(smap.vbase + smap.size);
    uni->vsi3_to  = BSWAP(smap.pbase - smap.vbase);
    uni->vsi3_ctl = BSWAP(ctld);
  } 
  if (smap.decoder == 4)
  {
    uni->vsi4_bs  = BSWAP(smap.vbase);
    uni->vsi4_bd  = BSWAP(smap.vbase + smap.size);
    uni->vsi4_to  = BSWAP(smap.pbase - smap.vbase);
    uni->vsi4_ctl = BSWAP(ctld);
  }
  if (smap.decoder == 5)
  {
    uni->vsi5_bs  = BSWAP(smap.vbase);
    uni->vsi5_bd  = BSWAP(smap.vbase + smap.size);
    uni->vsi5_to  = BSWAP(smap.pbase - smap.vbase);
    uni->vsi5_ctl = BSWAP(ctld);
  }
  if (smap.decoder == 6)
  {
    uni->vsi6_bs  = BSWAP(smap.vbase);
    uni->vsi6_bd  = BSWAP(smap.vbase + smap.size);
    uni->vsi6_to  = BSWAP(smap.pbase - smap.vbase);
    uni->vsi6_ctl = BSWAP(ctld);
  }
  if (smap.decoder == 7)
  {
    uni->vsi7_bs  = BSWAP(smap.vbase);
    uni->vsi7_bd  = BSWAP(smap.vbase + smap.size);
    uni->vsi7_to  = BSWAP(smap.pbase - smap.vbase);
    uni->vsi7_ctl = BSWAP(ctld);
  }
  return(0);
}


/*****************************/
int set_fair(unsigned int fair)
/*****************************/
{
  //0=not fair 1=fair
  if (fair == 1)
  {
    uni->mast_ctl |= BSWAP(0x00200000);
    return(0);
  }
  else if (fair == 0)
  {
    uni->mast_ctl &= BSWAP(0xffdfffff);
    return(0);
  }
  else
    return(-1);
}


/*********************************/
int set_req_lvl(unsigned int level)
/*********************************/
{
  //check input
  if (level > 3) return(-1);

  uni->mast_ctl &= BSWAP(0xff3fffff);
  uni->mast_ctl |= BSWAP(level << 22);
  return(0);
}


/*********************************/
int set_rel_mode(unsigned int mode)
/*********************************/
{
  //0=RWD 1=ROR
  if (mode == 1)
  {
    uni->mast_ctl |= BSWAP(0x00100000);
    return(0);
  }
  else if (mode == 0)
  {
    uni->mast_ctl &= BSWAP(0xffefffff);
    return(0);
  }
  else
    return(-1);
}


/*****************************/
int set_vme_to(unsigned int to)
/*****************************/
{
  //check input
  if(to>7) return(-1);

  uni->misc_ctl &= BSWAP(0x0fffffff);
  uni->misc_ctl |= BSWAP(to << 28);
  return(0);
}


/*********************************/
int set_arb_mode(unsigned int mode)
/*********************************/
{
  //0=round robin  1=priority
  if (mode == 1)
  {
    uni->misc_ctl |= BSWAP(0x04000000);
    return(0);
  }
  else if (mode == 0)
  {
    uni->misc_ctl &= BSWAP(0xfbffffff);
    return(0);
  }
  else
    return(-1);
}


/***********************************************************/
int set_swap(unsigned int ms,unsigned int ss,unsigned int fs)
/***********************************************************/
{
  unsigned int ret, data;
  data = (ms << 3);
  data |= (ss << 4);
  data |= (fs << 5);
  ret = (unsigned int)	VME_CCTSetSwap(data);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  return(0);
}


/****************************/
int set_irq(unsigned int mask)
/****************************/
{
  unsigned int value;

  value = BSWAP(uni->lint_en);
  value &= 0xffffbf01;    //mask all interrupts
  value |= mask;     //enable selected interrupts
  uni->lint_en = BSWAP(value);
  return(0);
}


/****************************************/
int full_initialisation(unsigned int mode)
/****************************************/
{
  unsigned int data;

  //0=only base initialisation   1=full initialisation
  if (mode == 0)
    return(0);  //nothing to do
  else if(mode == 1)
    {
    //generic stuff for all Universe based cards
    //Set latency timer to maximum
    //printf("uni->pci_misc0=0x%08x\n", BSWAP(uni->pci_misc0));
    uni->pci_misc0 |= BSWAP(0x0000f800);
    //printf("uni->pci_misc0=0x%08x\n", BSWAP(uni->pci_misc0));

    //Enable PCI master
    uni->pci_csr |= BSWAP(0x00000004);

    if (utype == 1)
      {
      //Avoid bugs 6 and 14 in Universe I
      data = BSWAP(uni->mast_ctl);
      data &= 0x0fffefff;
      uni->mast_ctl = BSWAP(data); 
      }
    else
      {
      //set PCI aligned burst size to the max.
      data = BSWAP(uni->mast_ctl);
      data |= 0x00002000;
      uni->mast_ctl = BSWAP(data); 
      }

    return(0);
    }
  else
    return(-1);
}


/*****************************/
int set_arb_to(unsigned int to)
/*****************************/
{
  //check input
  if (to > 2) return(-1);

  uni->misc_ctl &= BSWAP(0xfcffffff);
  uni->misc_ctl |= BSWAP(to << 24);
  return(0);
}


/************************************************/
int set_useram(unsigned int am1, unsigned int am2)
/************************************************/
{
  int data;
  
  am1 &= 0xf;
  am2 &= 0xf;
  data = am1 << 26;
  data |= am2 << 18;
  uni->user_am = data;
  return(0);
}


/*****************/
int open_universe()
/*****************/
{
  unsigned int ret;

  //Get a virtual address for the UNIVERSE registers
  ret = VME_Open();
  
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_UniverseMap(&unibase);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  uni = (universe_regs_t *) unibase;
  return(0);
}


/******************/
int close_universe()
/******************/
{
  unsigned int ret;

  ret = VME_UniverseUnmap(unibase);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
    
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  return(0);
} 


/***************************/
int pciaddr_finder_init(void)
/***************************/
{
  FILE *fp;
  char pci_sig[] = "PCI Bus 0000:\0\0";
  char iomem_cmd[40];
  ulong pci_addr_alt;
  
  fp = popen("lspci -n | grep 10e3:0000 | cut -d \":\" -f1", "r");
  if (!fp) 
  {
    printf("Cannot execute command: '%s'!\n", "lspci -n | grep 10e3:0000 | cut -d \":\" -f1");
    return 1;
  }

  fscanf(fp, "%2s", pci_sig + 13);
  pclose(fp);

  if (strlen(pci_sig + 13) != 2) 
  {
    printf("Universe PCI signature not found! Are you sure there is an Universe VME-Bus Bridge installed?\n");
    return 2;
  }

  printf("Found Universe PCI signature at %s\n", pci_sig);

  sprintf(iomem_cmd, "grep -A1 \"%s\" /proc/iomem", pci_sig);

  fp = popen(iomem_cmd, "r");
  if (!fp) 
  {
    printf("Cannot execute command: '%s'!\n", iomem_cmd);
    return 3;
  }
  
  fscanf(fp, "%*[\t ]%16lx-%16lx%*[^\n]%*[\t \n]%16lx-", &pci_addr_base, &pci_addr_alt, &pci_addr_size);  
  pclose(fp);

  if (!pci_addr_base) 
  {
    printf("Couldn't detect the PCI base address, as the evaluation failed!\n");
    return 4;
  }

  if (pci_addr_base & 0xFFFF) 
  {
    pci_addr_base += 0x10000; // add 64 KB
    pci_addr_base &= ~0xFFFF; // round to full 64 KB
  }

  if (!pci_addr_size && (pci_addr_alt - pci_addr_base >= 0x80000000))  
  {
    printf("Warning: Couldn't detect absolute size of the free PCI address space, assuming 2048 MB instead.\n");
    pci_addr_size = 0x80000000;
  } 
  else if (pci_addr_size) 
    pci_addr_size -= pci_addr_base;  
  else 
  {
    printf("Couldn't detect the size of the PCI address space, as the evaluation failed.\n");
    return 5;
  }

  if(pci_addr_size < 0x80000000) // possibly invalid bios config
    printf("Warning: Possible invalid PCI bios configuration! PCI address range is only 0x%lx bytes.\n", pci_addr_size);

  pci_addr_offset = 0;

  printf("Found free PCI region: base (0x%lx), size (0x%lx bytes)\n", pci_addr_base, pci_addr_size);
  return 0;
}


/****************************************/
u_long pciaddr_finder_getaddr(u_int *size)
/****************************************/
{
  if (!*size) 
  {
    printf("Size parameter out of range. Size cannot be zero!\n");
    return 0;
  }

  if (*size & 0xFFFF) 
  {
    printf("Warning: Automatically adjusted size from 0x%x ", *size);
    printf("to 0x%x, to match the 64 KB resolution!\n", (*size = (*size + 0x10000) & ~0xFFFF));
  }

  if (pci_addr_offset + *size > pci_addr_size) 
  {
    printf("Size parameter out of range. Cannot allocate 0x%x bytes. Free PCI space exhausted!\n", *size);
    return 0;
  }

  u_long ret_addr = pci_addr_base + pci_addr_offset;
  pci_addr_offset += *size;

  printf("Successfully allocated 0x%x bytes to PCI Address 0x%lx!\n", *size, ret_addr);
  return ret_addr;
}


/*******************************************/
int pci_addr_finder_checkautomode(unipara pa)
/*******************************************/
{
  int autoi = 0, enabi = 0;

  if(pa.mmap0.enable) { enabi++; if(!pa.mmap0.pbase) autoi++; }
  if(pa.mmap1.enable) { enabi++; if(!pa.mmap1.pbase) autoi++; }
  if(pa.mmap2.enable) { enabi++; if(!pa.mmap2.pbase) autoi++; }
  if(pa.mmap3.enable) { enabi++; if(!pa.mmap3.pbase) autoi++; }
  if(utype == 2) 
  {
    if(pa.mmap4.enable) { enabi++; if(!pa.mmap4.pbase) autoi++; }
    if(pa.mmap5.enable) { enabi++; if(!pa.mmap5.pbase) autoi++; }
    if(pa.mmap6.enable) { enabi++; if(!pa.mmap6.pbase) autoi++; }
    if(pa.mmap7.enable) { enabi++; if(!pa.mmap7.pbase) autoi++; }
  }

  if(autoi) 
  {
    if(autoi == enabi)
      return autoi;
    else
      return 0; // can't be here
  } 
  else 
  {
    if(autoi == enabi)
      return -1; // no auto or manual mmaps's enabled
    else
      return -(enabi + 1); // manual mode enabled
  }
}
