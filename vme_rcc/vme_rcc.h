// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc.h							*/
/*									*/
/* This is the public header file for RCC VMEbus library		*/
/*									*/
/* 12. Oct. 01  MAJO  created						*/
/*									*/
/* 011101 JOP add error codes						*/
/* 020418 JOP ROAK / RORA						*/
/* 020527 JOP update interrupt API					*/
/*									*/
/************ C 2008 - The software with that certain something *********/

#ifndef _VME_RCC_H 
#define _VME_RCC_H

#include <linux/types.h>
#include "vme_rcc_common.h"

/**********************************/
/*VMEbus transfer type definitions*/
/**********************************/
#define VME_RP             0x01
#define VME_WP             0x02
#define VME_A16            0x0
#define VME_A24            0x1
#define VME_A32            0x2
#define VME_CRCSR          0x5
#define VME_USER1          0x6
#define VME_USER2          0x7
#define VME_AM09           (VME_A32)
#define VME_AM39           (VME_A24)
#define VME_AM29           (VME_A16)
#define VME_AM2f           (VME_CRCSR)
#define VME_AM_PROGRAM     0x00004000
#define VME_AM_DATA        0x00000000
#define VME_AM_USER        0x00000000
#define VME_AM_SUPERVISOR  0x00001000

/*Universe bit definitions*/
#define VME_DMA_READ_UNI       0x00000000
#define VME_DMA_WRITE_UNI      0x80000000
#define VME_DMA_D16_UNI        0x00400000
#define VME_DMA_D32_UNI        0x00800000
#define VME_DMA_D64_UNI        0x00c00000
#define VME_DMA_DEF_UNI        0x00020100                       //A32, user, data, BLT
#define VME_DMA_DEF_A24_UNI    0x00010100                       //A24, user, data, BLT
#define VME_FIFO_DMA_D32W_UNI  0x80820200                       //Write in A32/D32 single cycle mode to a constant address
#define VME_FIFO_DMA_D32R_UNI  0x00820200                       //Read in A32/D32 single cycle mode from a constant address
#define VME_DMA_D16W_UNI       (VME_DMA_DEF_UNI | VME_DMA_D16_UNI | VME_DMA_WRITE_UNI)
#define VME_DMA_D16R_UNI       (VME_DMA_DEF_UNI | VME_DMA_D16_UNI | VME_DMA_READ_UNI)
#define VME_DMA_D32W_UNI       (VME_DMA_DEF_UNI | VME_DMA_D32_UNI | VME_DMA_WRITE_UNI)
#define VME_DMA_D32R_UNI       (VME_DMA_DEF_UNI | VME_DMA_D32_UNI | VME_DMA_READ_UNI)
#define VME_DMA_D64W_UNI       (VME_DMA_DEF_UNI | VME_DMA_D64_UNI | VME_DMA_WRITE_UNI)
#define VME_DMA_D64R_UNI       (VME_DMA_DEF_UNI | VME_DMA_D64_UNI | VME_DMA_READ_UNI)
#define VME_DMA_A24D32W_UNI    (VME_DMA_DEF_A24_UNI | VME_DMA_D32_UNI | VME_DMA_WRITE_UNI)
#define VME_DMA_A24D32R_UNI    (VME_DMA_DEF_A24_UNI | VME_DMA_D32_UNI | VME_DMA_READ_UNI)
#define VME_DMA_UNI_ERR        0x800

/*TSI14 bit definitions*/
#define VME_DMA_D16_TSI         0x100
#define VME_DMA_D32_TSI         0x100
#define VME_DMA_D64_TSI         0x200
#define VME_DMA_D16W_TSI_VME    (0x10000002 | VME_DMA_D16_TSI) //For the destination register
#define VME_DMA_D16R_TSI_VME    (0x10000002 | VME_DMA_D16_TSI) //For the source register  
#define VME_DMA_D32W_TSI_VME    (0x10000042 | VME_DMA_D32_TSI) //For the destination register
#define VME_DMA_D32R_TSI_VME    (0x10000042 | VME_DMA_D32_TSI) //For the source register  
#define VME_DMA_D64W_TSI_VME    (0x10000042 | VME_DMA_D64_TSI) //For the destination register     
#define VME_DMA_D64R_TSI_VME    (0x10000042 | VME_DMA_D64_TSI) //For the source register
#define VME_DMA_A24D32W_TSI_VME (0x10000041 | VME_DMA_D32_TSI) //For the destination register
#define VME_DMA_A24D32R_TSI_VME (0x10000041 | VME_DMA_D32_TSI) //For the source register
#define VME_DMA_TSI_PCI         0x00000000                   
#define VME_DMA_TSI_ERR         0x02000000             


/*logical definitions           */
/* Bit 0:   1=read 0=write      */
/* Bit 1,2: 2=D64, 1=D32  0=D16 */
/* Bit 3:   1=A24  0=A32        */
/* Bit 4:   1=SC   0=DMA        */
#define VME_DMA_MODE_WRITE 0
#define VME_DMA_MODE_READ  1

#define VME_DMA_MODE_D16   0
#define VME_DMA_MODE_D32   2
#define VME_DMA_MODE_D64   4

#define VME_DMA_MODE_A32   0
#define VME_DMA_MODE_A24   8

#define VME_DMA_MODE_DMA   0
#define VME_DMA_MODE_SC    16

#define VME_DMA_D16W       (VME_DMA_MODE_WRITE | VME_DMA_MODE_D16 | VME_DMA_MODE_A32 | VME_DMA_MODE_DMA) //0000   
#define VME_DMA_D16R       (VME_DMA_MODE_READ | VME_DMA_MODE_D16 | VME_DMA_MODE_A32 | VME_DMA_MODE_DMA)  //0001 
#define VME_DMA_D32W       (VME_DMA_MODE_WRITE | VME_DMA_MODE_D32 | VME_DMA_MODE_A32 | VME_DMA_MODE_DMA) //0000   
#define VME_DMA_D32R       (VME_DMA_MODE_READ | VME_DMA_MODE_D32 | VME_DMA_MODE_A32 | VME_DMA_MODE_DMA)  //0001 
#define VME_DMA_D64W       (VME_DMA_MODE_WRITE | VME_DMA_MODE_D64 | VME_DMA_MODE_A32 | VME_DMA_MODE_DMA) //0010 
#define VME_DMA_D64R       (VME_DMA_MODE_READ | VME_DMA_MODE_D64 | VME_DMA_MODE_A32 | VME_DMA_MODE_DMA)  //0011 
#define VME_DMA_A24D32W    (VME_DMA_MODE_WRITE | VME_DMA_MODE_D32 | VME_DMA_MODE_A24 | VME_DMA_MODE_DMA) //0100  
#define VME_DMA_A24D32R    (VME_DMA_MODE_READ | VME_DMA_MODE_D32 | VME_DMA_MODE_A24 | VME_DMA_MODE_DMA)  //0101 
#define VME_FIFO_DMA_D32W  (VME_DMA_MODE_WRITE | VME_DMA_MODE_D32 | VME_DMA_MODE_A32 | VME_DMA_MODE_SC)  //1000 
#define VME_FIFO_DMA_D32R  (VME_DMA_MODE_READ | VME_DMA_MODE_D32 | VME_DMA_MODE_A32 | VME_DMA_MODE_SC)   //1001 


/************************************************/
/*Identifier definitions for CR/CSR space access*/
/*                                              */
/* Bits 31..28  Number of bytes to read         */
/* Bits 27..24  Data width		        */
/*              0 = D08				*/
/*              1 = D16				*/
/*              2 = D32				*/
/* Bits 19..00  Offset of first byte            */
/************************************************/
#define ONE_BYTE           0x10000000
#define TWO_BYTE           0x20000000
#define THREE_BYTE         0x30000000
#define FOUR_BYTE          0x40000000

#define CRCSR_D08          0x00000000
#define CRCSR_D16          0x01000000
#define CRCSR_D32          0x02000000

#define CR_CHECKSUM        (CRCSR_D08 | ONE_BYTE   | 0x00003)
#define CR_LENGTH          (CRCSR_D08 | THREE_BYTE | 0x00007)
#define CR_CRACCESSWIDTH   (CRCSR_D08 | ONE_BYTE   | 0x00013)
#define CR_CSRACCESSWIDTH  (CRCSR_D08 | ONE_BYTE   | 0x00017)
#define CR_CRCSRSPEC       (CRCSR_D08 | ONE_BYTE   | 0x0001b)
#define CR_ASCII1          (CRCSR_D08 | ONE_BYTE   | 0x0001f)
#define CR_ASCII2          (CRCSR_D08 | ONE_BYTE   | 0x00023)
#define CR_MANUFID         (CRCSR_D08 | THREE_BYTE | 0x00027)
#define CR_BOARDID         (CRCSR_D08 | FOUR_BYTE  | 0x00033)
#define CR_REVID           (CRCSR_D08 | FOUR_BYTE  | 0x00043)
       
#define CSR_BAR            (CRCSR_D08 | ONE_BYTE   | 0x7ffff)
#define CSR_BSR            (CRCSR_D08 | ONE_BYTE   | 0x7fffb)
#define CSR_BCR            (CRCSR_D08 | ONE_BYTE   | 0x7fff7)
#define CSR_ADER7          (CRCSR_D08 | FOUR_BYTE  | 0x7ffd3)
#define CSR_ADER6          (CRCSR_D08 | FOUR_BYTE  | 0x7ffc3)
#define CSR_ADER5          (CRCSR_D08 | FOUR_BYTE  | 0x7ffb3)
#define CSR_ADER4          (CRCSR_D08 | FOUR_BYTE  | 0x7ffa3)
#define CSR_ADER3          (CRCSR_D08 | FOUR_BYTE  | 0x7ff93)
#define CSR_ADER2          (CRCSR_D08 | FOUR_BYTE  | 0x7ff83)
#define CSR_ADER1          (CRCSR_D08 | FOUR_BYTE  | 0x7ff73)
#define CSR_ADER0          (CRCSR_D08 | FOUR_BYTE  | 0x7ff63)

/***************************************/
/*various upper limits for arrays, etc.*/
/***************************************/
#define VME_MAXSTRING   256 


/******************/
/*type definitions*/
/******************/
typedef struct
{
  int                     number_of_items;
  VME_BlockTransferItem_t list_of_items[VME_MAXCHAINEL];
} VME_BlockTransferList_t;

typedef struct
{
  int                 number_of_items;
  VME_InterruptItem_t list_of_items[VME_MAXINTERRUPT];
} VME_InterruptList_t;


/************/
/*Prototypes*/
/************/
#ifdef __cplusplus
extern "C" {
#endif


/*******************************/ 
/*Official functions of the API*/
/*******************************/ 
int VME_ErrorPrint(VME_ErrorCode_t error_code);
int VME_ErrorString(VME_ErrorCode_t error_code, char *error_string);
int VME_ErrorNumber(VME_ErrorCode_t error_code, int *error_number);
VME_ErrorCode_t VME_Open(void);
VME_ErrorCode_t VME_Close(void);
VME_ErrorCode_t VME_ReadCRCSR(int slot_number, u_int crcsr_identifier, u_int *value);
VME_ErrorCode_t VME_WriteCRCSR(int slot_number, u_int crcsr_identifier, u_int value);
VME_ErrorCode_t VME_MasterMap(VME_MasterMap_t *master_map, int *master_mapping);
VME_ErrorCode_t VME_MasterMapCRCSR(int *master_mapping);
VME_ErrorCode_t VME_MasterMapVirtualLongAddress(int master_mapping, u_long *virtual_address);
VME_ErrorCode_t VME_MasterMapVirtualAddress(int master_mapping, u_int *virtual_address) __attribute__ ((deprecated));
VME_ErrorCode_t VME_ReadSafeUInt(int master_mapping, u_int address_offset, u_int *value);
VME_ErrorCode_t VME_ReadSafeUShort(int master_mapping, u_int address_offset, u_short *value);
VME_ErrorCode_t VME_ReadSafeUChar(int master_mapping, u_int address_offset, u_char *value);
VME_ErrorCode_t VME_WriteSafeUInt(int master_mapping, u_int address_offset, u_int value);
VME_ErrorCode_t VME_WriteSafeUShort(int master_mapping, u_int address_offset, u_short value);
VME_ErrorCode_t VME_WriteSafeUChar(int master_mapping, u_int address_offset, u_char value);
VME_ErrorCode_t VME_MasterUnmap(int master_mapping);
VME_ErrorCode_t VME_MasterMapDump(void);
VME_ErrorCode_t VME_BusErrorRegisterSignal(int signal_number);
VME_ErrorCode_t VME_BusErrorInfoGet(VME_BusErrorInfo_t *bus_error_info);
VME_ErrorCode_t VME_SlaveMap(VME_SlaveMap_t *slave_map, int *slave_mapping);
VME_ErrorCode_t VME_SlaveMapVmebusAddress(int slave_mapping, u_int *vmebus_address);
VME_ErrorCode_t VME_SlaveUnmap(int slave_mapping);
VME_ErrorCode_t VME_SlaveMapDump(void);
VME_ErrorCode_t VME_BlockTransferInit(VME_BlockTransferList_t *block_transfer_list, int *block_transfer);
VME_ErrorCode_t VME_BlockTransferStart(int block_transfer);
VME_ErrorCode_t VME_BlockTransferWait(int block_transfer, int time_out, VME_BlockTransferList_t *block_transfer_list);
VME_ErrorCode_t VME_BlockTransferEnd(int block_transfer);
VME_ErrorCode_t VME_BlockTransfer(VME_BlockTransferList_t *block_transfer_list, int time_out);
VME_ErrorCode_t VME_BlockTransferDump(void);
VME_ErrorCode_t VME_BlockTransferStatus(VME_BlockTransferList_t *block_transfer_list, int position_of_block, VME_ErrorCode_t *status);
VME_ErrorCode_t VME_BlockTransferRemaining(VME_BlockTransferList_t *block_transfer_list, int position_of_block, int *remaining);
VME_ErrorCode_t VME_InterruptLink(VME_InterruptList_t* vmebus_interrupt_list, int *interrupt);
VME_ErrorCode_t VME_InterruptReenable(int interrupt);
VME_ErrorCode_t VME_InterruptWait(int interrupt, int time_out, VME_InterruptInfo_t* ir_info);
VME_ErrorCode_t VME_InterruptRegisterSignal(int interrupt, int signal_number);
VME_ErrorCode_t VME_InterruptInfoGet(int interrupt, VME_InterruptInfo_t* ir_info);
VME_ErrorCode_t VME_InterruptUnlink(int interrupt);
VME_ErrorCode_t VME_InterruptGenerate(int level, u_char vector);
VME_ErrorCode_t VME_InterruptDump(void);
VME_ErrorCode_t VME_SysfailInterruptLink(void);
VME_ErrorCode_t VME_SysfailInterruptRegisterSignal(int signal_number);
VME_ErrorCode_t VME_SysfailInterruptWait(int time_out);
VME_ErrorCode_t VME_SysfailInterruptUnlink(void);
VME_ErrorCode_t VME_SysfailInterruptReenable(void);
VME_ErrorCode_t VME_SysfailSet(void);
VME_ErrorCode_t VME_SysfailReset(void);
VME_ErrorCode_t VME_SysfailPoll(int *flag);
VME_ErrorCode_t VME_UniverseMap(u_long *virtual_address);
VME_ErrorCode_t VME_UniverseUnmap(u_long virtual_address);
VME_ErrorCode_t VME_Tsi148Map(u_long *virtual_address);
VME_ErrorCode_t VME_Tsi148Unmap(u_long virtual_address);
VME_ErrorCode_t VME_CCTSetSwap(u_char data);
VME_ErrorCode_t VME_Update(u_int *data);
VME_ErrorCode_t VME_SendSysreset(void);
void VME_ReadFastUInt(int master_mapping, u_int address_offset, u_int *value);
void VME_ReadFastUShort(int master_mapping, u_int address_offset, u_short *value);
void VME_ReadFastUChar(int master_mapping, u_int address_offset, u_char *value);
void VME_WriteFastUInt(int master_mapping, u_int address_offset, u_int value);
void VME_WriteFastUShort(int master_mapping, u_int address_offset, u_short value);
void VME_WriteFastUChar(int master_mapping, u_int address_offset, u_char value);

/******************************/
/* Internal service functions */
/******************************/
VME_ErrorCode_t vmercc_err_get(err_pack err, err_str pid, err_str code);

/**********************************/ 
/*Additional (temporary) functions*/
/**********************************/
VME_ErrorCode_t VME_test(void);

#ifdef __cplusplus
}
#endif

#endif
