// $Id$
#ifndef __tsi148_h
#define __tsi148_h

/* Full register space of Tundra Tsi148 ASIC */

typedef struct 
{
  volatile unsigned int noswap_devi_veni;             // 0x000
  volatile unsigned int noswap_stat_cmmd;             // 0x004
  volatile unsigned int noswap_clas_revi;             // 0x008
  volatile unsigned int noswap_head_mlat_clsz;        // 0x00c
  volatile unsigned int noswap_mbarl;                 // 0x010
  volatile unsigned int noswap_mbaru;                 // 0x014
  volatile unsigned int r1[5];                        // 0x018 - 0x028
  volatile unsigned int noswap_subi_subv;             // 0x02c
  volatile unsigned int r2[1];                        // 0x030
  volatile unsigned int noswap_capp;                  // 0x034
  volatile unsigned int r3[1];                        // 0x038
  volatile unsigned int noswap_mxla_mngn_intp_intl;   // 0x03c
  volatile unsigned int noswap_pcixcap;               // 0x040
  volatile unsigned int noswap_pcixstat;              // 0x044
  volatile unsigned int r4[46];                       // 0x048 - 0x0fc
  volatile unsigned int otsau0;                       // 0x100
  volatile unsigned int otsal0;                       // 0x104
  volatile unsigned int oteau0;                       // 0x108
  volatile unsigned int oteal0;                       // 0x10c
  volatile unsigned int otofu0;                       // 0x110
  volatile unsigned int otofl0;                       // 0x114
  volatile unsigned int otbs0;                        // 0x118
  volatile unsigned int otat0;                        // 0x11c
  volatile unsigned int otsau1;                       // 0x120
  volatile unsigned int otsal1;                       // 0x124
  volatile unsigned int oteau1;                       // 0x128
  volatile unsigned int oteal1;                       // 0x12c
  volatile unsigned int otofu1;                       // 0x130
  volatile unsigned int otofl1;                       // 0x134
  volatile unsigned int otbs1;                        // 0x138
  volatile unsigned int otat1;                        // 0x13c
  volatile unsigned int otsau2;                       // 0x140
  volatile unsigned int otsal2;                       // 0x144
  volatile unsigned int oteau2;                       // 0x148
  volatile unsigned int oteal2;                       // 0x14c
  volatile unsigned int otofu2;                       // 0x150
  volatile unsigned int otofl2;                       // 0x154
  volatile unsigned int otbs2;                        // 0x158
  volatile unsigned int otat2;                        // 0x15c
  volatile unsigned int otsau3;                       // 0x160
  volatile unsigned int otsal3;                       // 0x164
  volatile unsigned int oteau3;                       // 0x168
  volatile unsigned int oteal3;                       // 0x16c
  volatile unsigned int otofu3;                       // 0x170
  volatile unsigned int otofl3;                       // 0x174
  volatile unsigned int otbs3;                        // 0x178
  volatile unsigned int otat3;                        // 0x17c
  volatile unsigned int otsau4;                       // 0x180
  volatile unsigned int otsal4;                       // 0x184
  volatile unsigned int oteau4;                       // 0x188
  volatile unsigned int oteal4;                       // 0x18c
  volatile unsigned int otofu4;                       // 0x190
  volatile unsigned int otofl4;                       // 0x194
  volatile unsigned int otbs4;                        // 0x198
  volatile unsigned int otat4;                        // 0x19c
  volatile unsigned int otsau5;                       // 0x1a0
  volatile unsigned int otsal5;                       // 0x1a4
  volatile unsigned int oteau5;                       // 0x1a8
  volatile unsigned int oteal5;                       // 0x1ac
  volatile unsigned int otofu5;                       // 0x1b0
  volatile unsigned int otofl5;                       // 0x1b4
  volatile unsigned int otbs5;                        // 0x1b8
  volatile unsigned int otat5;                        // 0x1bc
  volatile unsigned int otsau6;                       // 0x1c0
  volatile unsigned int otsal6;                       // 0x1c4
  volatile unsigned int oteau6;                       // 0x1c8
  volatile unsigned int oteal6;                       // 0x1cc
  volatile unsigned int otofu6;                       // 0x1d0
  volatile unsigned int otofl6;                       // 0x1d4
  volatile unsigned int otbs6;                        // 0x1d8
  volatile unsigned int otat6;                        // 0x1dc
  volatile unsigned int otsau7;                       // 0x1e0
  volatile unsigned int otsal7;                       // 0x1e4
  volatile unsigned int oteau7;                       // 0x1e8
  volatile unsigned int oteal7;                       // 0x1ec
  volatile unsigned int otofu7;                       // 0x1f0
  volatile unsigned int otofl7;                       // 0x1f4
  volatile unsigned int otbs7;                        // 0x1f8
  volatile unsigned int otat7;                        // 0x1fc
  volatile unsigned int r5[1];                        // 0x200
  volatile unsigned char rc1[3];                      // 0x204
  volatile unsigned char viack1;                      // 0x207 - offset for 8bit vector
  volatile unsigned char rc2[3];                      // 0x208
  volatile unsigned char viack2;                      // 0x20b - offset for 8bit vector
  volatile unsigned char rc3[3];                      // 0x20c
  volatile unsigned char viack3;                      // 0x20f - offset for 8bit vector
  volatile unsigned char rc4[3];                      // 0x210
  volatile unsigned char viack4;                      // 0x213 - offset for 8bit vector
  volatile unsigned char rc5[3];                      // 0x214
  volatile unsigned char viack5;                      // 0x217 - offset for 8bit vector
  volatile unsigned char rc6[3];                      // 0x218
  volatile unsigned char viack6;                      // 0x21b - offset for 8bit vector
  volatile unsigned char rc7[3];                      // 0x21c
  volatile unsigned char viack7;                      // 0x21f - offset for 8bit vector
  volatile unsigned int rmwau;                        // 0x220
  volatile unsigned int rmwal;                        // 0x224
  volatile unsigned int rmwen;                        // 0x228
  volatile unsigned int rmwc;                         // 0x22c
  volatile unsigned int rmws;                         // 0x230
  volatile unsigned int vmctrl;                       // 0x234
  volatile unsigned int vctrl;                        // 0x238
  volatile unsigned int vstat;                        // 0x23c
  volatile unsigned int pcsr;                         // 0x240
  volatile unsigned int r6[3];                        // 0x244 - 0x24c
  volatile unsigned int vmefl;                        // 0x250
  volatile unsigned int r7[3];                        // 0x254 - 0x25c
  volatile unsigned int veau;                         // 0x260
  volatile unsigned int veal;                         // 0x264
  volatile unsigned int veat;                         // 0x268
  volatile unsigned int r8[1];                        // 0x26c
  volatile unsigned int edpau;                        // 0x270
  volatile unsigned int edpal;                        // 0x274
  volatile unsigned int edpxa;                        // 0x278
  volatile unsigned int edpxs;                        // 0x27c
  volatile unsigned int edpat;                        // 0x280
  volatile unsigned int r9[31];                       // 0x284 - 0x2fc
  volatile unsigned int itsau0;                       // 0x300
  volatile unsigned int itsal0;                       // 0x304
  volatile unsigned int iteau0;                       // 0x308
  volatile unsigned int iteal0;                       // 0x30c
  volatile unsigned int itofu0;                       // 0x310
  volatile unsigned int itofl0;                       // 0x314
  volatile unsigned int itat0;                        // 0x318
  volatile unsigned int r10[1];                       // 0x31c
  volatile unsigned int itsau1;                       // 0x320
  volatile unsigned int itsal1;                       // 0x324
  volatile unsigned int iteau1;                       // 0x328
  volatile unsigned int iteal1;                       // 0x32c
  volatile unsigned int itofu1;                       // 0x330
  volatile unsigned int itofl1;                       // 0x334
  volatile unsigned int itat1;                        // 0x338
  volatile unsigned int r11[1];                       // 0x33c
  volatile unsigned int itsau2;                       // 0x340
  volatile unsigned int itsal2;                       // 0x344
  volatile unsigned int iteau2;                       // 0x348
  volatile unsigned int iteal2;                       // 0x34c
  volatile unsigned int itofu2;                       // 0x350
  volatile unsigned int itofl2;                       // 0x354
  volatile unsigned int itat2;                        // 0x358
  volatile unsigned int r12[1];                       // 0x35c
  volatile unsigned int itsau3;                       // 0x360
  volatile unsigned int itsal3;                       // 0x364
  volatile unsigned int iteau3;                       // 0x368
  volatile unsigned int iteal3;                       // 0x36c
  volatile unsigned int itofu3;                       // 0x370
  volatile unsigned int itofl3;                       // 0x374
  volatile unsigned int itat3;                        // 0x378
  volatile unsigned int r13[1];                       // 0x37c
  volatile unsigned int itsau4;                       // 0x380
  volatile unsigned int itsal4;                       // 0x384
  volatile unsigned int iteau4;                       // 0x388
  volatile unsigned int iteal4;                       // 0x38c
  volatile unsigned int itofu4;                       // 0x390
  volatile unsigned int itofl4;                       // 0x394
  volatile unsigned int itat4;                        // 0x398
  volatile unsigned int r14[1];                       // 0x39c
  volatile unsigned int itsau5;                       // 0x3a0
  volatile unsigned int itsal5;                       // 0x3a4
  volatile unsigned int iteau5;                       // 0x3a8
  volatile unsigned int iteal5;                       // 0x3ac
  volatile unsigned int itofu5;                       // 0x3b0
  volatile unsigned int itofl5;                       // 0x3b4
  volatile unsigned int itat5;                        // 0x3b8
  volatile unsigned int r15[1];                       // 0x3bc
  volatile unsigned int itsau6;                       // 0x3c0
  volatile unsigned int itsal6;                       // 0x3c4
  volatile unsigned int iteau6;                       // 0x3c8
  volatile unsigned int iteal6;                       // 0x3cc
  volatile unsigned int itofu6;                       // 0x3d0
  volatile unsigned int itofl6;                       // 0x3d4
  volatile unsigned int itat6;                        // 0x3d8
  volatile unsigned int r16[1];                       // 0x3dc
  volatile unsigned int itsau7;                       // 0x3e0
  volatile unsigned int itsal7;                       // 0x3e4
  volatile unsigned int iteau7;                       // 0x3e8
  volatile unsigned int iteal7;                       // 0x3ec
  volatile unsigned int itofu7;                       // 0x3f0
  volatile unsigned int itofl7;                       // 0x3f4
  volatile unsigned int itat7;                        // 0x3f8
  volatile unsigned int r17[1];                       // 0x3fc
  volatile unsigned int gbau;                         // 0x400
  volatile unsigned int gbal;                         // 0x404
  volatile unsigned int gcsrat;                       // 0x408
  volatile unsigned int cbau;                         // 0x40c
  volatile unsigned int cbal;                         // 0x410
  volatile unsigned int crgat;                        // 0x414
  volatile unsigned int crou;                         // 0x418
  volatile unsigned int crol;                         // 0x41c
  volatile unsigned int crat;                         // 0x420
  volatile unsigned int lmbau;                        // 0x424
  volatile unsigned int lmbal;                        // 0x428
  volatile unsigned int lmat;                         // 0x42c
  volatile unsigned int r64bcu;                       // 0x430
  volatile unsigned int r64bcl;                       // 0x434
  volatile unsigned int bpgtr;                        // 0x438
  volatile unsigned int bpctr;                        // 0x43c
  volatile unsigned int vicr;                         // 0x440
  volatile unsigned int r18[1];                       // 0x444
  volatile unsigned int inten;                        // 0x448
  volatile unsigned int inteo;                        // 0x44c
  volatile unsigned int ints;                         // 0x450
  volatile unsigned int intc;                         // 0x454
  volatile unsigned int intm1;                        // 0x458
  volatile unsigned int intm2;                        // 0x45c
  volatile unsigned int r19[40];                      // 0x460 - 0x4fc
  volatile unsigned int dctl0;                        // 0x500
  volatile unsigned int dsta0;                        // 0x504
  volatile unsigned int dcsau0;                       // 0x508
  volatile unsigned int dcsal0;                       // 0x50c
  volatile unsigned int dcdau0;                       // 0x510
  volatile unsigned int dcdal0;                       // 0x514
  volatile unsigned int dclau0;                       // 0x518
  volatile unsigned int dclal0;                       // 0x51c
  volatile unsigned int dsau0;                        // 0x520
  volatile unsigned int dsal0;                        // 0x524
  volatile unsigned int ddau0;                        // 0x528
  volatile unsigned int ddal0;                        // 0x52c
  volatile unsigned int dsat0;                        // 0x530
  volatile unsigned int ddat0;                        // 0x534
  volatile unsigned int dnlau0;                       // 0x538
  volatile unsigned int dnlal0;                       // 0x53c
  volatile unsigned int dcnt0;                        // 0x540
  volatile unsigned int ddbs0;                        // 0x544
  volatile unsigned int r20[14];                      // 0x548 - 0x57c
  volatile unsigned int dctl1;                        // 0x580
  volatile unsigned int dsta1;                        // 0x584
  volatile unsigned int dcsau1;                       // 0x588
  volatile unsigned int dcsal1;                       // 0x58c
  volatile unsigned int dcdau1;                       // 0x590
  volatile unsigned int dcdal1;                       // 0x594
  volatile unsigned int dclau1;                       // 0x598
  volatile unsigned int dclal1;                       // 0x59c
  volatile unsigned int dsau1;                        // 0x5a0
  volatile unsigned int dsal1;                        // 0x5a4
  volatile unsigned int ddau1;                        // 0x5a8
  volatile unsigned int ddal1;                        // 0x5ac
  volatile unsigned int dsat1;                        // 0x5b0
  volatile unsigned int ddat1;                        // 0x5b4
  volatile unsigned int dnlau1;                       // 0x5b8
  volatile unsigned int dnlal1;                       // 0x5bc
  volatile unsigned int dcnt1;                        // 0x5c0
  volatile unsigned int ddbs1;                        // 0x5c4
  volatile unsigned int r21[14];                      // 0x5c8 - 0x5fc
  volatile unsigned int devi_veni_2;                  // 0x600
  volatile unsigned int gctrl;                        // 0x604
  volatile unsigned int semaphore0_1_2_3;             // 0x608
  volatile unsigned int semaphore4_5_6_7;             // 0x60c
  volatile unsigned int mbox0;                        // 0x610
  volatile unsigned int mbox1;                        // 0x614
  volatile unsigned int mbox2;                        // 0x618
  volatile unsigned int mbox3;                        // 0x61c
  volatile unsigned int r22[629];                     // 0x620 - 0xff0
  volatile unsigned int csrbcr;                       // 0xff4
  volatile unsigned int csrbsr;                       // 0xff8
  volatile unsigned int cbar;                         // 0xffc
} tsi148_regs_t;


// Interrupt masks
#define BM_LINT_DMA                0x01000000
#define BM_LINT_LERR               0x00002000
#define BM_LINT_VERR               0x00001000
#define BM_LINT_SYSFAIL            0x00000200
#define BM_LINT_ACFAIL             0x00000100
#define BM_LINT_VIRQ7              0x00000080
#define BM_LINT_VIRQ6              0x00000040
#define BM_LINT_VIRQ5              0x00000020
#define BM_LINT_VIRQ4              0x00000010
#define BM_LINT_VIRQ3              0x00000008
#define BM_LINT_VIRQ2              0x00000004
#define BM_LINT_VIRQ1              0x00000002

#endif /* __tsi148_h */
