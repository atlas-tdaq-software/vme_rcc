// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_common.h						*/
/*									*/
/* This is the common public header file for RCC VMEbus library		*/
/* and driver								*/
/*									*/
/*  2. Nov. 01  MAJO  created						*/
/*  2. Nov. 01  JOP   interrupts					*/
/*  26 Nov. 01  JOP   bus errors					*/
/*  19 Apr. 02  JOP   ROAK / RORA					*/
/*  27 May. 02  JOP   modify interrupt API				*/
/*									*/
/************ C 2011 - The software with that certain something *********/

#ifndef _VME_RCC_COMMON_H 
#define _VME_RCC_COMMON_H

#ifdef __KERNEL__
  #include <linux/types.h>
  #include <linux/ioctl.h>
  #define P_ID_VMERCC 2   // Needs to be re-defined here since we do not want to include rcc_error.h at this level
#else
  #include <sys/types.h>
  #include <sys/ioctl.h>
#endif

/***************/
/*CCT SBC types*/
/***************/
#define VP_UNKNOWN 0
#define VP_PSE     1
#define VP_PMC     2
#define VP_100     3
#define VP_CP1     4
#define VP_110     5
#define VP_315     6
#define VP_317     7
#define VP_325     8
#define VP_327     9 
#define VP_417    10 
#define VP_717    11 
#define VP_917    12 
#define VP_EXX    13 
#define VP_BXX    14 

/*******************/
/* Other constants */
/*******************/
#define TEXT_SIZE1              3000
#define TEXT_SIZE2              20000
#define VME_MAXINTERRUPT        256	 // Max # vectors per handle
#define VME_MAX_BERR_PROCS      50	 // Max # processes with BERR handling
#define VME_MAX_MASTERMAP       200      // Max # of concurrent master mappings in all processes
#define VME_MAXCHAINEL          32       // Max number of elements in DMA chain 
#define VME_MAXCHAIN            16       // Max number of internal DMA chains
#define DMA_DESC_SIZE           (VME_MAXCHAINEL * VME_MAXCHAIN * sizeof(VME_UniDmaChain_t))  //Using VME_UniDmaChain_t here because this is the bigger one of the two descriptor types 

enum 
{
  VME_LEVELISDISABLED = 0,
  VME_INT_ROAK,
  VME_INT_RORA
};

/*************/
/*error codes*/
/*************/

//error codes
enum
{
  VME_SUCCESS = 0,
  VME_NOTKNOWN = (P_ID_VMERCC << 8) + 1,
  VME_UNKNOWN,
  VME_NOTOPEN,
  VME_RANGE,
  VME_BUSERROR,
  VME_ALIGN,
  VME_NOCHAINMEM,
  VME_NOBUSERROR,
  VME_TOOLONG,
  VME_DMABUSY,          // 10
  VME_TIMEOUT,
  VME_FILE,
  VME_NOMAP,
  VME_NOSTATMAP,
  VME_IRGBUSY,
  VME_EIO,
  VME_EFAULT,
  VME_VIRT,
  VME_REMAP,
  VME_ENOSYS,		// 20
  VME_NOSIZE,
  VME_CMEM_FAIL,
  VME_ERESTARTSYS,
  VME_DMAERR,
  VME_PCI_ERR,
  VME_VME_ERR,
  VME_PROTOCOL_ERR,
  VME_NOT_EXECUTED,     
  VME_MUNMAP,
  VME_ILLREV,		// 30
  VME_IOREMAP,
  VME_REQIRQ,
  VME_TOOMANYINT,
  VME_TOOMANYHDL,
  VME_INTUSED,
  VME_ILLINTLEVEL,
  VME_ILLINTTYPE,
  VME_INTCONF,
  VME_LVLDISABLED,
  VME_LVLISNOTRORA,	// 40
  VME_ILLINTHANDLE,
  VME_INTBYSIGNAL,
  VME_NOINTERRUPT,
  VME_ENOMEM,           
  VME_KMALLOC,
  VME_BERRTBLFULL,
  VME_BERRNOTFOUND,
  VME_ERROR_FAIL,
  VME_ILL_TO,
  VME_NODOMEMEM,	// 50
  VME_NO_CODE,
  VME_UNKNOWN_BOARD,
  VME_IO_FAIL,
  VME_SYSFAILTBLFULL,
  VME_SYSFAILTBLNOTLINKED,
  VME_SYSFAILNOTLINKED,
  VME_NOSTATMAP2,
  VME_IOUNMAP,
  VME_INTDISABLED,
  VME_BADSBC,
  VME_NOCRCSRMAP
};


/*************/
/*ioctl codes*/
/*************/
#define VME_RCC_CONSISTENCY_CHECK 0x12344321  //To be able to check if the driver has included the proper file.
#define VME_RCC_MAGIC 'x'


#define VMEMASTERMAP             _IOW(VME_RCC_MAGIC, 8, VME_MasterMapInt_t)
#define VMEMASTERUNMAP           _IOW(VME_RCC_MAGIC, 9, unsigned long)
#define VMEMASTERMAPDUMP         _IO(VME_RCC_MAGIC, 10)
#define VMEBERRREGISTERSIGNAL    _IOW(VME_RCC_MAGIC, 11, VME_RegSig_t)
#define VMEBERRINFO              _IOR(VME_RCC_MAGIC, 12, VME_BusErrorInfo_t)
#define VMESLAVEMAP              _IOW(VME_RCC_MAGIC, 13, VME_SlaveMapInt_t)
#define VMESLAVEMAPDUMP          _IO(VME_RCC_MAGIC, 14)
#define VMESCSAFE                _IOW(VME_RCC_MAGIC, 15, VME_SingleCycle_t)
#define VMEDMASTART              _IOW(VME_RCC_MAGIC, 16, VME_DMAstart_t)
#define VMEDMAPOLL               _IOR(VME_RCC_MAGIC, 17, VME_DMAhandle_t)
#define VMEDMADUMP               _IO(VME_RCC_MAGIC, 18)
#define VMELINK                  _IOW(VME_RCC_MAGIC, 19, VME_IntHandle_t)
#define VMEINTENABLE             _IOW(VME_RCC_MAGIC, 20, VME_IntEnable_t)
#define VMEINTDISABLE            _IOW(VME_RCC_MAGIC, 21, VME_IntEnable_t)
#define VMEWAIT                  _IOR(VME_RCC_MAGIC, 22, VME_WaitInt_t)
#define VMEREGISTERSIGNAL        _IOW(VME_RCC_MAGIC, 23, VME_RegSig_t)
#define VMEINTERRUPTINFOGET      _IOR(VME_RCC_MAGIC, 24, VME_WaitInt_t)
#define VMEUNLINK                _IOW(VME_RCC_MAGIC, 25, VME_IntHandle_t)
#define VMEUPDATE                _IOW(VME_RCC_MAGIC, 26, VME_Update_t)
#define VMESYSFAILUNLINK         _IO(VME_RCC_MAGIC, 27)
#define VMESYSFAILWAIT           _IOR(VME_RCC_MAGIC, 28, VME_WaitInt_t)
#define VMESYSFAILREGISTERSIGNAL _IOW(VME_RCC_MAGIC, 29, VME_RegSig_t)
#define VMESYSFAILLINK           _IO(VME_RCC_MAGIC, 30)
#define VMESYSFAILREENABLE       _IO(VME_RCC_MAGIC, 31)
#define VMESYSFAILPOLL           _IOR(VME_RCC_MAGIC, 32, int)
#define VMESYSFAILSET            _IO(VME_RCC_MAGIC, 33)
#define VMESYSFAILRESET          _IO(VME_RCC_MAGIC, 34)
#define VMESYSRST                _IO(VME_RCC_MAGIC, 35)
#define VMESBCTYPE               _IOR(VME_RCC_MAGIC, 36, int)
#define VMETEST                  _IOW(VME_RCC_MAGIC, 37, int)
#define VMEMASTERMAPCRCSR        _IOW(VME_RCC_MAGIC, 38, VME_MasterMapInt_t)

#define COMCOMCOM "cccc1111" //this is a hack to check that this header and vme_rcc_lib.cpp match (useful for the consistency of the driver ioctl codes)

/*
#define VMEMASTERMAP                   222340000
#define VMEMASTERUNMAP                 222340001
#define VMEMASTERMAPDUMP               222340002
#define VMEBERRREGISTERSIGNAL          222340003
#define VMEBERRINFO                    222340004
#define VMESLAVEMAP                    222340005
#define VMESLAVEMAPDUMP                222340006
#define VMESCSAFE                      222340007
#define VMEDMASTART                    222340008
#define VMEDMAPOLL                     222340009
#define VMEDMADUMP                     222340010
#define VMELINK                        222340011
#define VMEINTENABLE                   222340012
#define VMEINTDISABLE                  222340013
#define VMEWAIT                        222340014
#define VMEREGISTERSIGNAL              222340015
#define VMEINTERRUPTINFOGET            222340016
#define VMEUNLINK                      222340017
#define VMEUPDATE                      222340018
#define VMESYSFAILUNLINK               222340019
#define VMESYSFAILWAIT                 222340020
#define VMESYSFAILREGISTERSIGNAL       222340021
#define VMESYSFAILLINK                 222340022
#define VMESYSFAILREENABLE             222340023
#define VMESYSFAILPOLL                 222340024
#define VMESYSFAILSET                  222340025
#define VMESYSFAILRESET                222340026
#define VMESYSRST                      222340027
#define VMESBCTYPE                     222340028
#define VMETEST                        222340029
#define VMEMASTERMAPCRCSR              222340030
*/

/*
enum 
{
  VMEMASTERMAP = 12340000,
  VMEMASTERUNMAP,
  VMEMASTERMAPDUMP,
  VMEBERRREGISTERSIGNAL,
  VMEBERRINFO,
  VMESLAVEMAP,
  VMESLAVEMAPDUMP,
  VMESCSAFE,
  VMEDMASTART,
  VMEDMAPOLL,               //10
  VMEDMADUMP,
  VMELINK,
  VMEINTENABLE,
  VMEINTDISABLE,
  VMEWAIT,
  VMEREGISTERSIGNAL,
  VMEINTERRUPTINFOGET,
  VMEUNLINK,                 //20
  VMEUPDATE,
  VMESYSFAILUNLINK,
  VMESYSFAILWAIT,
  VMESYSFAILREGISTERSIGNAL,
  VMESYSFAILLINK,
  VMESYSFAILREENABLE,
  VMESYSFAILPOLL,
  VMESYSFAILSET,
  VMESYSFAILRESET,
  VMESYSRST,                //30
  VMESBCTYPE,
  VMETEST,
  VMEMASTERMAPCRCSR
};
*/


/***********/
/*SBC Types*/
/***********/
enum 
{
  VME_SBC_UNIVERSE = 1,
  VME_SBC_TSI148
};


/*******************/
/*Other definitions*/
/*******************/
#define VME_DMA_BUSY_TSI 0x01000000

/******************/
/*type definitions*/
/******************/
typedef struct 
{
  u_int vmebus_address;
  u_int window_size;
  u_int address_modifier;
  u_int options; 
} VME_MasterMap_t;

typedef struct 
{
  VME_MasterMap_t    in;
  unsigned long long pci_address;
  u_long             virt_address;  //for the user
  u_long             kvirt_address; //for the kernel
  u_int              used;
} VME_MasterMapInt_t;

typedef struct 
{
  unsigned long long system_iobus_address;
  u_int              window_size;
  u_int              address_width;
  u_int              options;
} VME_SlaveMap_t;

typedef struct 
{
  VME_SlaveMap_t  in;
  u_int           vme_address;
  u_int           used;
} VME_SlaveMapInt_t;

typedef struct 
{
  u_long kvirt_address;
  u_int offset;
  u_int data;
  u_int nbytes;
  u_int rw;
} VME_SingleCycle_t;

typedef struct 
{
  u_int dctl;         //Universe chain descriptor
  u_int dtbc;         //Universe chain descriptor
  u_int dla;          //Universe chain descriptor
  u_int reserved1;    //Universe chain descriptor
  u_int dva;          //Universe chain descriptor
  u_int reserved2;    //Universe chain descriptor
  u_int dcpp;         //Universe chain descriptor
  u_int reserved3;    //Universe chain descriptor
  u_int ref;          //used by the library
  u_int reserved4[7]; //the size of the whole structure has to be a multiple of 32 bytes
} VME_UniDmaChain_t;

typedef struct 
{
  u_int dsau;         //Tsi148 chain descriptor
  u_int dsal;         //Tsi148 chain descriptor
  u_int ddau;         //Tsi148 chain descriptor
  u_int ddal;         //Tsi148 chain descriptor
  u_int dsat;         //Tsi148 chain descriptor
  u_int ddat;         //Tsi148 chain descriptor
  u_int dnlau;        //Tsi148 chain descriptor
  u_int dnlal;        //Tsi148 chain descriptor
  u_int dcnt;         //Tsi148 chain descriptor
  u_int ddbs;         //Tsi148 chain descriptor
  u_int ref;          //used by the library
  u_int reserved4[1]; //the size of the whole structure has to be a multiple of 8 bytes
} VME_TsiDmaChain_t;

typedef struct
{
  u_int ctrl;
  u_long paddr;
  u_int counter;
  int timeoutval; //This is the duration of the timeout in ms
  u_int timeout;  //This is a flag. 0=timeout has happened, 1=no timeout  //no longer needed for the universe driver
  u_int dclau;    //Only for Tsi148
  u_int dclal;    //Only for Tsi148
} VME_DMAhandle_t;

typedef struct
{
  u_int nvectors;
  u_int vector[VME_MAXINTERRUPT];    // use int internally
  u_int level;			     // SAME for all vectors
  u_int type;			     // SAME for all vectors
} VME_IntHandle_t;

typedef struct
{
  u_int level;
  u_int type;
} VME_IntEnable_t;

typedef struct
{
  VME_IntHandle_t int_handle;
  int timeout;
  u_int level;
  u_int type;
  u_int vector;	// use int internally
  u_int multiple;
} VME_WaitInt_t;

typedef struct
{
  VME_IntHandle_t int_handle;
  int signum;
} VME_RegSig_t;

typedef struct
{
  u_long paddr;
  u_int handle;
} VME_DMAstart_t;

typedef struct
{
  u_int irq_mode[9];
} VME_Update_t;

typedef u_int VME_ErrorCode_t;

typedef struct 
{
  u_int vmebus_address;
  u_long system_iobus_address;
  u_int size_requested;
  u_int control_word;
  u_int size_remaining;
  u_int status_word;
} VME_BlockTransferItem_t;

typedef struct
{
  u_char vector;
  u_int level;
  u_int type;
} VME_InterruptItem_t;

typedef struct 
{
  u_char vector;
  u_int level;
  u_int type;
  u_int multiple;
} VME_InterruptInfo_t;

typedef struct
{
  u_int vmebus_address;
  u_int address_modifier;
  u_int lword;
  u_int iack;
  u_int ds0;
  u_int ds1;
  u_int wr;
  int  multiple;
} VME_BusErrorInfo_t;

typedef struct
{
  u_int pid;        
  u_long paddr;
} VME_DMAhandle_todo_t;

typedef struct
{
  u_int in_use;
  u_int pid;        
  u_int ctrl;
  u_long paddr;
  u_int counter;
  u_int dclau;    //Only for Tsi148
  u_int dclal;    //Only for Tsi148
} VME_DMAhandle_done_t;

/********/
/*Macros*/
/********/
#ifdef __powerpc__
  #define BSWAP(x) bswap(x)
  #define SYNC __asm__("eieio")
#endif

#ifdef __i386__
  #define BSWAP(x) (x)
  #define SYNC
#endif

#ifdef __x86_64__
  #define BSWAP(x) (x)
  #define SYNC
#endif

#ifndef BSWAP
  #include <byteswap.h>
  #define BSWAP(x) bswap_32(x)
#endif

#ifndef SYNC
  #define SYNC
#endif

#endif
