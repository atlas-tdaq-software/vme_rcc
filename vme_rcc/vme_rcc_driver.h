// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_driver.h						*/
/*									*/
/* This is the private header file for RCC VMEbus driver		*/
/*									*/
/* 26. Oct. 01  MAJO  created						*/
/* 02. Nov. 01  JOP   interrupts					*/
/* 26. Nov. 01  JOP   bus errors					*/
/* 17. Apr. 02  JOP   RORA						*/
/*									*/
/************ C 2008 - The software with that certain something *********/

#ifndef _VME_RCC_DRIVER_H 
#define _VME_RCC_DRIVER_H

#include <linux/types.h>
#include "vme_rcc_common.h"

#define VME_VCTSIZE        256
#define VME_DMADONESIZE    64      //max. number of completed DMA transactions
#define VME_DMATODOSIZE    32      //max. number of pending DMA transactions
#define MAX_PROC_TEXT_SIZE 0x10000 //The output of "more /proc/vme_rcc" must not generate more characters than that
#define BAR0               0

// I/O registers
#define CMOSA              0x70
#define CMOSD              0x71
#define BID1               0x35
#define BID2               0x36


/********/
/*Macros*/
/********/
#ifdef DRIVER_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef DRIVER_ERROR
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif


/******************/
/*Type definitions*/
/******************/
typedef struct 
{
  unsigned long long vbase;
  unsigned long long vtop;
  unsigned long long pbase;
  unsigned long long ptop;
  u_int am;   
  u_int enab; 
  u_int wp;   
  u_int rp;   
  u_int space;
  u_int options;
} mstslvmap_t;

typedef struct 
{
  mstslvmap_t master[8];
  mstslvmap_t slave[8]; 
} all_mstslvmap_t;  

// A link between an interrupt and a program is described by a LINK DESCRIPTOR
typedef struct
{
  int                 pid;            // pid of linked process, 0 if not linked 
  u_int               vct;            // IRQ vector
  u_int               lvl;            // IRQ level
  u_int               type;           // type (RORA or ROAK)
  u_int               pending;        // interrupt pending 
  u_int               total_count;    // total # interrupts with this vector (after LINK)
  VME_IntHandle_t     *group_ptr;     // pointer to the structure of vectors with same handle 
  struct semaphore    sem;            // its semaphore
  int                 sig;            // its signal
} link_dsc_t;

typedef struct
{
  link_dsc_t          link_dsc[VME_VCTSIZE];
  struct semaphore    link_dsc_sem;
} link_dsc_table_t;

//  a link between BERR and a program is described by a BERR LINK DESCRIPTOR
typedef struct
{
  int                 pid;            // pid of linked process, 0 if not linked 
  int                 sig;            // its signal
} berr_link_dsc_t;

typedef struct
{
  berr_link_dsc_t     berr_link_dsc[VME_MAX_BERR_PROCS];
  struct semaphore    proc_sem;
} berr_proc_table_t;

// holds info about the bus errors - filled by the BERR ISR
typedef struct
{
  u_int    vmeadd;            // last latched VMEbus address 
  u_int    am;                // and its AM code
  u_int    iack;              // indicates BERR for IACK cycle
  u_int    lword;             // Only for VP-100 & VP-110
  u_int    ds0;               // Only for VP-100 & VP-110
  u_int    ds1;               // Only for VP-100 & VP-110
  u_int    wr;                // Only for VP-100 & VP-110
  int	   multiple;
  int      flag;
} berr_dsc_t;
  
typedef struct
{
  int                pid;         // pid of linked process, 0 if not linked 
  int                sig;         // the signal (0 if semaphore used)
  u_int              num;         // number of interrupts received
  struct semaphore   sem;         // external semaphore
  struct semaphore   proc_sem;    // internal semaphore
} sysfail_link_dsc_t; 

typedef struct
{
  int                pid;         // pid of process owning the master mapping
  u_long             vaddr;       // address returned by ioremap
} master_map_dsc_t;

typedef struct
{
  master_map_dsc_t map[VME_MAX_MASTERMAP];
  struct semaphore sem;
} master_map_t;

typedef struct
{       
  u_int              berr_dsc_flag[VME_MAX_BERR_PROCS];
  u_int              link_dsc_flag[VME_VCTSIZE];
  u_int              sysfail_dsc_flag;
  u_int              dma[VME_DMADONESIZE];
  u_int              mastermap[VME_MAX_MASTERMAP];
  VME_BusErrorInfo_t VME_BerrInfo;
  VME_IntHandle_t    VME_int_handle2;
  VME_IntEnable_t    VME_IntEnable;
} private_data_t;

typedef struct InterruptCounters_t 
{
  u_int acfail;
  u_int sysfail;
  u_int sw_int;
  u_int sw_iack;
  u_int verr;
  u_int lerr;
  u_int dma;
  u_int virq[7];
  u_int vown;
} InterruptCounters_t;

struct vme_proc_data_t
{
  char name[10];
  char value[100];
};
 
#endif
